﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.CustomerModels
{
    public class CustomerFactory : EntityFactory<TableCurrentKey>
    {
        private static CustomerFactory instance = new CustomerFactory();

        public static CustomerFactory Instance
        {
            get { return instance; }
        }

        public Customer CreateCustomer(DbContext dbContext)
        {
            Customer newcustomer = new Customer();
            newcustomer.CustomerID = base.GetPrimaryKey("Customer", dbContext).CurrentKey;
            newcustomer.RecordStatus = true;
            newcustomer.RecordVersion = 0;

            return newcustomer;
        }

        public CustomerAttribute CreateCustomerAttribute(DbContext dbContext)
        {
            CustomerAttribute attribute = new CustomerAttribute();
            attribute.AttributeID = base.GetPrimaryKey("CustomerAttribute", dbContext).CurrentKey;
            attribute.RecordStatus = true;
            attribute.RecordVersion = 0;

            return attribute;
        }

        public CustomerAddressXRef CreateCustomerAddress(DbContext dbContext)
        {
            CustomerAddressXRef addressXRef = new CustomerAddressXRef();
            addressXRef.AddressID = base.GetPrimaryKey("CustomerAddressXRef", dbContext).CurrentKey;
            addressXRef.RecordStatus = true;
            addressXRef.RecordVersion = 0;

            return addressXRef;
        }

        public CustomerBillingOptionsXRef CreateCustomerBillingOptionsXRef(DbContext dbContext)
        {
            CustomerBillingOptionsXRef customerBillingOptionsXRef = new CustomerBillingOptionsXRef();
            customerBillingOptionsXRef.CustomerBillingOptID = base.GetPrimaryKey("CustomerBillingOptionsXRef", dbContext).CurrentKey;
            customerBillingOptionsXRef.RecordStatus = true;
            customerBillingOptionsXRef.RecordVersion = 0;

            return customerBillingOptionsXRef;
        }
    }
}
