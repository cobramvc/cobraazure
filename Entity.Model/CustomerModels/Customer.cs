//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.Model.CustomerModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
        public Customer()
        {
            this.CustomerAddressXRefs = new HashSet<CustomerAddressXRef>();
            this.CustomerAttributes = new HashSet<CustomerAttribute>();
            this.CustomerGroupXRefs = new HashSet<CustomerGroupXRef>();
            this.CustomerGroupXRefs1 = new HashSet<CustomerGroupXRef>();
            this.CustomerBillingOptionsXRefs = new HashSet<CustomerBillingOptionsXRef>();
        }
    
        public long CustomerID { get; set; }
        public string CustomerNo { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public Nullable<int> CategoryCodeID { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Landline { get; set; }
        public Nullable<int> BillingOptionTypeCodeID { get; set; }
        public long CreateBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public long UpdateBy { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public bool RecordStatus { get; set; }
        public int RecordVersion { get; set; }
        public Nullable<long> ParentCustomerID { get; set; }
        public Nullable<int> BillingToParentCodeID { get; set; }
    
        public virtual CodeListDetail BillingOptionTypeCode { get; set; }
        public virtual CodeListDetail CategoryCode { get; set; }
        public virtual ICollection<CustomerAddressXRef> CustomerAddressXRefs { get; set; }
        public virtual ICollection<CustomerAttribute> CustomerAttributes { get; set; }
        public virtual ICollection<CustomerGroupXRef> CustomerGroupXRefs { get; set; }
        public virtual ICollection<CustomerGroupXRef> CustomerGroupXRefs1 { get; set; }
        public virtual ICollection<CustomerBillingOptionsXRef> CustomerBillingOptionsXRefs { get; set; }
        public virtual CodeListDetail BillingToParentCode { get; set; }
    }
}
