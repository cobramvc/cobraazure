﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.CustomerModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerAddressXRefMetaData))]
    public partial class CustomerAddressXRef : BaseEntity<CustomerAddressXRef>
    {
    }

    public class CustomerAddressXRefMetaData
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        public string AddressLine1 { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        public string AddressLine2 { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        [SelectList(CodeListKey.StateList)]
        public int StateCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        [SelectList(CodeListKey.CityList)]
        [DependencyColumn("StateCodeID")]
        public int CityCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, 100, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        [SelectList(CodeListKey.RegionList)]
        [DependencyColumn("CityCodeID")]
        public int RegionCodeID { get; set; }

        [SelectList(CodeListKey.AddressTypeList)]
        public int AddressTypeCodeID { get; set; }
    }
}
