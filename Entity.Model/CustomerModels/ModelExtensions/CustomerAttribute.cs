﻿using Entity.ModelHelpers;
using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.CustomerModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerAttributeMetaData))]
    public partial class CustomerAttribute : BaseEntity<CustomerAttribute>
    {
        
    }


    public class CustomerAttributeMetaData
    {
        [System.ComponentModel.DataAnnotations.Required()]
        [SelectList(CodeListKey.AttributeCategoryList)]
        public int CategoryCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        [SelectList(CodeListKey.AttributeTypeList)]
        public int TypeCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public string AttributeValue { get; set; }
    }
}
