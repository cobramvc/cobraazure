﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.CustomerModels
{
    public static class CustomerConnectionString
    {
        public static string GetConnectionString()
        {
            string providerName = "System.Data.SqlClient";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["COBRADbContext"].ConnectionString;

            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

            //Set the provider name.
            entityBuilder.Provider = providerName;

            // Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = connectionString;

            // Set the Metadata location.
            entityBuilder.Metadata = @"res://*/CustomerModels.CustomerModels.csdl|
                            res://*/CustomerModels.CustomerModels.ssdl|
                            res://*/CustomerModels.CustomerModels.msl";

            return entityBuilder.ToString();
        }
    }
}
