﻿using Entity.ModelHelpers;
using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Model.CustomerModels
{
    public partial class CustomerDbContext
    {
        public CustomerDbContext(string connectionString)
            : base(connectionString)
        {
            
        }

        #region Public Properties
        public ObjectContext DbObjectContext
        {
            get
            {
                return ((IObjectContextAdapter)this).ObjectContext;
            }
        }

        public User User
        {
            get; set;
        }


        #endregion

        #region Public methods               
        #endregion

        #region DbContext Overriden methods
        public override int SaveChanges()
        {
            // Validate Custom Validatins defined at Entity object level           

            if (!DbContextHelper.Instance.Validate(this))
            {
                return ApplicationKeys.SaveUnsucessful;
            }

            DbContextHelper.Instance.UpdateAuditFields(this);
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync()
        {
            // Validate Custom Validatins defined at Entity object level.
            // Note:Data annotations and Validation Block Attributes are being validated already before reaching this point
            // either client-side(for Data Annotations) or server-side(Validation block attributes, Data Annotations)
            // The below method is to execute custom validation methods defined with SelfValidation attribute tagged 
            // in respective entity class

            // If there are any validation errors return "-1" indicating Save is Unsuccessful due to validation error
            if (!DbContextHelper.Instance.Validate(this))
            {
                return Task.FromResult<int>(ApplicationKeys.SaveUnsucessful);
            }

            DbContextHelper.Instance.UpdateAuditFields(this);
            return base.SaveChangesAsync();

        }
        #endregion        
    }
}
