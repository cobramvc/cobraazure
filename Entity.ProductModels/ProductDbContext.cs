﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    public partial class ProductDbContext
    {
        public ProductDbContext(string connectionString)
            : base(connectionString)
        {

        }

        #region Public Properties
        public ObjectContext DbObjectContext
        {
            get
            {
                return ((IObjectContextAdapter)this).ObjectContext;
            }
        }

        // To get User Information of current DbContext and this has to be
        // referred in all DbContext types 
        public User User
        {
            get; set;
        }
        #endregion

        public override int SaveChanges()
        {
            // Validate Custom Validatins defined at Entity object level           

            if (!DbContextHelper.Instance.Validate(this))
            {
                return ApplicationKeys.SaveUnsucessful;
            }

            DbContextHelper.Instance.UpdateAuditFields(this);
            return base.SaveChanges();
        }                       
    }
}
