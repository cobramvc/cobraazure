//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.ProductModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceGroupPricingDetailXRef
    {
        public long ServiceGroupPricingDetailXRefID { get; set; }
        public long ServiceID { get; set; }
        public Nullable<int> CategoryCodeID { get; set; }
        public Nullable<long> ServiceAreaID { get; set; }
        public Nullable<decimal> SubscriptionCharges { get; set; }
        public Nullable<decimal> OneTimeCharges { get; set; }
        public long CreateBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public long UpdateBy { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public bool RecordStatus { get; set; }
        public int RecordVersion { get; set; }
        public Nullable<long> ServiceGroupPricingID { get; set; }
    
        public virtual CodeListDetail CategoryCode { get; set; }
        public virtual Service Service { get; set; }
        public virtual ServiceArea ServiceArea { get; set; }
        public virtual ServiceGroupPricing ServiceGroupPricing { get; set; }
        public virtual ServiceGroupPricingDetailXRef ServiceGroupPricingDetailXRef1 { get; set; }
        public virtual ServiceGroupPricingDetailXRef ServiceGroupPricingDetailXRef2 { get; set; }
    }
}
