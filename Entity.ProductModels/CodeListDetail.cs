//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.ProductModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class CodeListDetail
    {
        public CodeListDetail()
        {
            this.Products = new HashSet<Product>();
            this.Products1 = new HashSet<Product>();
            this.Services = new HashSet<Service>();
            this.ServiceAreas = new HashSet<ServiceArea>();
            this.ServiceAreas1 = new HashSet<ServiceArea>();
            this.ServiceAreas2 = new HashSet<ServiceArea>();
            this.ServiceGroupPricings = new HashSet<ServiceGroupPricing>();
            this.ServiceGroupPricingDetailXRefs = new HashSet<ServiceGroupPricingDetailXRef>();
            this.ServicePricings = new HashSet<ServicePricing>();
            this.ServicePricingDetailXRefs = new HashSet<ServicePricingDetailXRef>();
            this.Discounts = new HashSet<Discount>();
            this.Discounts1 = new HashSet<Discount>();
            this.DiscountDetailXRefs = new HashSet<DiscountDetailXRef>();
            this.DiscountDetailXRefs1 = new HashSet<DiscountDetailXRef>();
        }
    
        public int CodeID { get; set; }
        public int CodeListID { get; set; }
        public string Description { get; set; }
        public System.DateTime CreateDate { get; set; }
        public long CreateBy { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public long UpdateBy { get; set; }
        public bool RecordStatus { get; set; }
        public int RecordVersion { get; set; }
        public Nullable<int> ParentCodeListID { get; set; }
    
        public virtual CodeList CodeList { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Product> Products1 { get; set; }
        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<ServiceArea> ServiceAreas { get; set; }
        public virtual ICollection<ServiceArea> ServiceAreas1 { get; set; }
        public virtual ICollection<ServiceArea> ServiceAreas2 { get; set; }
        public virtual ICollection<ServiceGroupPricing> ServiceGroupPricings { get; set; }
        public virtual ICollection<ServiceGroupPricingDetailXRef> ServiceGroupPricingDetailXRefs { get; set; }
        public virtual ICollection<ServicePricing> ServicePricings { get; set; }
        public virtual ICollection<ServicePricingDetailXRef> ServicePricingDetailXRefs { get; set; }
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<Discount> Discounts1 { get; set; }
        public virtual ICollection<DiscountDetailXRef> DiscountDetailXRefs { get; set; }
        public virtual ICollection<DiscountDetailXRef> DiscountDetailXRefs1 { get; set; }
    }
}
