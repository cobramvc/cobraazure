﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    public class ProductFactory : EntityFactory<TableCurrentKey>
    {
        private static ProductFactory instance = new ProductFactory();

        public static ProductFactory Instance
        {
            get { return instance; }
        }

        //public ProductBase CreateProductBase(DbContext dbContext)
        //{
        //    ProductBase newProductBase = new ProductBase();
        //    newProductBase.ProductBaseID = base.GetPrimaryKey("ProductBase", dbContext).CurrentKey;
        //    newProductBase.RecordStatus = true;
        //    newProductBase.RecordVersion = 0;

        //    return newProductBase;
        //}

        public Product CreateProduct(DbContext dbContext)
        {
            Product newProduct = new Product();
            newProduct.ProductID = base.GetPrimaryKey("Product", dbContext).CurrentKey;
            newProduct.RecordStatus = true;
            newProduct.RecordVersion = 0;
            newProduct.Version = 0;
            newProduct.BeginDate = DateTime.Now.Date;

            return newProduct;
        }

        //public ProductDetailXRef CreateProductDetailXRef(DbContext dbContext)
        //{
        //    ProductDetailXRef newProductDetailXRef = new ProductDetailXRef();
        //    newProductDetailXRef.ProductDetailXRefID = base.GetPrimaryKey("ProductDetailXRef", dbContext).CurrentKey;
        //    newProductDetailXRef.RecordStatus = true;
        //    newProductDetailXRef.RecordVersion = 0;

        //    return newProductDetailXRef;
        //}
    }
}
