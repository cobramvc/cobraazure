﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    public class ServiceAreaFactory : EntityFactory<TableCurrentKey>
    {
        private static ServiceAreaFactory instance = new ServiceAreaFactory();

        public static ServiceAreaFactory Instance
        {
            get { return instance; }
        }

        public ServiceArea CreateServiceArea(DbContext dbContext)
        {
            ServiceArea newServiceArea = new ServiceArea();
            newServiceArea.ServiceAreaID = base.GetPrimaryKey("ServiceArea", dbContext).CurrentKey;
            newServiceArea.RecordStatus = true;
            newServiceArea.RecordVersion = 0;

            return newServiceArea;
        }

    }
}
