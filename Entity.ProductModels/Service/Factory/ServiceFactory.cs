﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    public class ServiceFactory :  EntityFactory<TableCurrentKey>
    {
        private static ServiceFactory instance = new ServiceFactory();

        public static ServiceFactory Instance
        {
            get { return instance; }
        }

        public Service CreateService(DbContext dbContext)
        {
            Service newService = new Service();
            newService.ServiceID = base.GetPrimaryKey("Service", dbContext).CurrentKey;
            newService.RecordStatus = true;
            newService.RecordVersion = 0;

            return newService;
        }

        public ServicePricing CreateServicePricing(DbContext dbContext)
        {
            ServicePricing newServicePricing = new ServicePricing();
            newServicePricing.ServicePricingID = base.GetPrimaryKey("ServicePricing", dbContext).CurrentKey;
            newServicePricing.RecordStatus = true;
            newServicePricing.RecordVersion = 0;

            return newServicePricing;
        }

        public ServicePricingDetailXRef CreateServicePricingDetailXRef(DbContext dbContext)
        {
            ServicePricingDetailXRef newServicePricingDetailXRef = new ServicePricingDetailXRef();
            newServicePricingDetailXRef.ServicePricingDetailXRefID = base.GetPrimaryKey("ServicePricingDetailXRef", dbContext).CurrentKey;            
            newServicePricingDetailXRef.ServiceAreaID = 0;
            newServicePricingDetailXRef.RecordStatus = true;
            newServicePricingDetailXRef.RecordVersion = 0;

            return newServicePricingDetailXRef;
        }
    }
}
