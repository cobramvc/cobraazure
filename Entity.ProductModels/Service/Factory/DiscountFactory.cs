﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    public class DiscountFactory : EntityFactory<TableCurrentKey>
    {
        private static DiscountFactory instance = new DiscountFactory();

        public static DiscountFactory Instance
        {
            get { return instance; }
        }

        public Discount CreateDiscount(DbContext dbContext)
        {
            Discount newDiscount = new Discount();
            newDiscount.DiscountID = Convert.ToInt32(base.GetPrimaryKey("Discount", dbContext).CurrentKey);
            newDiscount.RecordStatus = true;
            newDiscount.RecordVersion = 0;            
            
            return newDiscount;
        }

        public DiscountDetailXRef CreateDiscountDetailXRef(DbContext dbContext)
        {
            DiscountDetailXRef newDiscountDetailXRef = new DiscountDetailXRef();
            newDiscountDetailXRef.DiscountDetailXRefID = Convert.ToInt32(base.GetPrimaryKey("DiscountDetailXRef", dbContext).CurrentKey);
            newDiscountDetailXRef.RecordStatus = true;
            newDiscountDetailXRef.RecordVersion = 0;
            newDiscountDetailXRef.BeginDate = DateTime.Today.Date;
            newDiscountDetailXRef.EndDate = DateTime.Today.Date;

            return newDiscountDetailXRef;
        }

    }
}
