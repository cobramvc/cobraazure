﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    public class ServiceGroupFactory : EntityFactory<TableCurrentKey>
    {
        private static ServiceGroupFactory instance = new ServiceGroupFactory();

        public static ServiceGroupFactory Instance
        {
            get { return instance; }
        }

        public ServiceGroup CreateServiceGroup(DbContext dbContext)
        {
            ServiceGroup newServiceGroup = new ServiceGroup();
            newServiceGroup.ServiceGroupID = base.GetPrimaryKey("ServiceGroup", dbContext).CurrentKey;
            newServiceGroup.RecordStatus = true;
            newServiceGroup.RecordVersion = 0;

            return newServiceGroup;
        }

        public ServiceGroupDetailXRef CreateServiceGroupDetailXRef(DbContext dbContext)
        {
            ServiceGroupDetailXRef newServiceGroupDetailXRef = new ServiceGroupDetailXRef();
            newServiceGroupDetailXRef.ServiceGroupDetailXRefID = base.GetPrimaryKey("ServiceGroupDetailXRef", dbContext).CurrentKey;
            newServiceGroupDetailXRef.RecordStatus = true;
            newServiceGroupDetailXRef.RecordVersion = 0;

            return newServiceGroupDetailXRef;
        }

        public ServiceGroupPricing CreateServiceGroupPricing(DbContext dbContext)
        {
            ServiceGroupPricing newServiceGroupPricing = new ServiceGroupPricing();
            newServiceGroupPricing.ServiceGroupPricingID = base.GetPrimaryKey("ServiceGroupPricing", dbContext).CurrentKey;
            newServiceGroupPricing.RecordStatus = true;
            newServiceGroupPricing.RecordVersion = 0;

            return newServiceGroupPricing;
        }

        public ServiceGroupPricingDetailXRef LoadChildrenServiceGroupPricingDetailXRef(DbContext dbContext, ServiceGroupPricingDetailXRef source, bool isNew = false)
        {
            DbSet<CodeListDetail> codeListDetailDbSet = dbContext.Set<CodeListDetail>();
            DbSet<Service> serviceDbSet = dbContext.Set<Service>();
            ServiceGroupPricingDetailXRef newServiceGroupPricingDetailXRef = null;

            if (isNew)
            {
                newServiceGroupPricingDetailXRef = this.CreateServiceGroupPricingDetailXRef(dbContext);                
                newServiceGroupPricingDetailXRef.ServiceID = source.ServiceID;
                newServiceGroupPricingDetailXRef.SubscriptionCharges = source.SubscriptionCharges;
                newServiceGroupPricingDetailXRef.OneTimeCharges = source.OneTimeCharges;
            
            }
            else
            {
                newServiceGroupPricingDetailXRef = source;
            }
                        
            newServiceGroupPricingDetailXRef.Service = serviceDbSet.Where(service => service.ServiceID == source.ServiceID).FirstOrDefault();

            if (source.ServiceAreaID.HasValue)
            {
                DbSet<ServiceArea> serviceAreaDbSet = dbContext.Set<ServiceArea>();
                newServiceGroupPricingDetailXRef.ServiceAreaID = source.ServiceAreaID.Value;                
                newServiceGroupPricingDetailXRef.ServiceArea = serviceAreaDbSet.Where(serviceArea => serviceArea.RecordStatus
                    && serviceArea.ServiceAreaID == source.ServiceAreaID).FirstOrDefault();                
            }

            if (source.CategoryCodeID.HasValue)
            {
                newServiceGroupPricingDetailXRef.CategoryCodeID = source.CategoryCodeID;
                newServiceGroupPricingDetailXRef.CategoryCode = codeListDetailDbSet.Where(code => code.CodeID == source.CategoryCodeID).FirstOrDefault();                
            }

            newServiceGroupPricingDetailXRef.ServiceArea.StateCode = codeListDetailDbSet.Where(code => code.CodeID == newServiceGroupPricingDetailXRef.ServiceArea.StateCodeID.Value).FirstOrDefault();
            newServiceGroupPricingDetailXRef.ServiceArea.CityCode = codeListDetailDbSet.Where(code => code.CodeID == newServiceGroupPricingDetailXRef.ServiceArea.CityCodeID.Value).FirstOrDefault();
            newServiceGroupPricingDetailXRef.ServiceArea.RegionCode = codeListDetailDbSet.Where(code => code.CodeID == newServiceGroupPricingDetailXRef.ServiceArea.RegionCodeID.Value).FirstOrDefault();
            
            return newServiceGroupPricingDetailXRef;
        }

        public ServiceGroupPricingDetailXRef CreateServiceGroupPricingDetailXRef(DbContext dbContext)
        {
            ServiceGroupPricingDetailXRef newServiceGroupPricingDetailXRef = new ServiceGroupPricingDetailXRef();
            newServiceGroupPricingDetailXRef.ServiceGroupPricingDetailXRefID = base.GetPrimaryKey("ServiceGroupPricingDetailXRef", dbContext).CurrentKey;
            newServiceGroupPricingDetailXRef.RecordStatus = true;
            newServiceGroupPricingDetailXRef.RecordVersion = 0;

            return newServiceGroupPricingDetailXRef;
        }
    }
}
