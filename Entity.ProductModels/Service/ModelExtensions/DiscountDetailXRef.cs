﻿using Entity.ModelHelpers;
using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(DiscountDetailXRefMetaData))]
    public partial class DiscountDetailXRef : BaseEntity<DiscountDetailXRef>
    {
        [SelfValidation]
        internal void IsBeginDateLessThanEndDate(DbContext context)
        {
            DateTime beginDate = DateTime.Today;

            //Set BeginDate to minimum of already saved BeginDate Vs Current Date
            if (this.DbObjectBeforeChanged != null
                && beginDate > this.DbObjectBeforeChanged.BeginDate)
            {
                beginDate = this.DbObjectBeforeChanged.BeginDate;                
            }
             

            if (this.BeginDate < beginDate)
            {
                this.ValidationErrors.Add(new ValidationResult("BeginDate should be greater than or equal to " + beginDate.ToShortDateString(), this, "BeginDate", "IsBeginDateLessThanEndDate", null));
                return;
            }

         
            if (this.BeginDate > this.EndDate)
            {
                this.ValidationErrors.Add(new ValidationResult("BeginDate should be less than or equal to EndDate", this, "BeginDate", "IsBeginDateLessThanEndDate", null));
                return;
            }               
        }

        [SelfValidation]
        internal void IsValidProduct(DbContext context)
        {
            if (this.ProductID <= 0)
            {
                this.ValidationErrors.Add(new ValidationResult("Product is required", this, "ProductID", "IsValidProduct", null));
                return;
            }         
        }


        [SelfValidation]
        internal void CanEdit(DbContext context)
        {
            if (this.CanWrite)
                return;

            if (this.DbObjectBeforeChanged == null)
                return;

            if (this.DbObjectBeforeChanged.Discount == this.Discount)
                return;

            this.ValidationErrors.Add(new ValidationResult("Discount cannot be modified", this, "Discount", "CanEdit", null));
        }
    }

    public class DiscountDetailXRefMetaData
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        public long ProductID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        public System.DateTime BeginDate { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        public System.DateTime EndDate { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue,  ErrorMessage = "Invalid discount")]
        public int Discount { get; set; }

        [SelectList(CodeListKey.CustomerCategoryList)]
        [System.ComponentModel.DataAnnotations.Required()]
        public Nullable<int> CustomerCategoryCodeID { get; set; }

        [SelectList(CodeListKey.CityList)]
        [System.ComponentModel.DataAnnotations.Required()]
        public Nullable<int> CityCodeID { get; set; }
    }
}
