﻿using Entity.ModelHelpers;
using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ServiceMetaData))]
    public partial class Service : BaseEntity<Service>
    {
        public bool HasActiveProduct { get; set; }

        public ServicePricing CurrentServicePricing(int contractTermTypeCodeID)
        {
            return this.ServicePricings.Where(pricing => pricing.RecordStatus
                && pricing.ContractTermTypeCodeID == contractTermTypeCodeID)
                .OrderByDescending(pricing => pricing.Version)    
                .FirstOrDefault();
        }

        public ServicePricing GetServicePricing(int contractTermTypeCodeID, int version)
        {
            return this.ServicePricings.Where(pricing => pricing.RecordStatus
                && pricing.ContractTermTypeCodeID == contractTermTypeCodeID
                && pricing.Version == version)
                .OrderByDescending(pricing => pricing.Version)
                .FirstOrDefault();
        }

        #region Validation Rules
        [SelfValidation]
        internal void CheckIfSameNameExists(DbContext context)
        {
            if(!(context is ProductDbContext))
            {
                return;
            }

            if (this.RecordVersion > 0)
                return;

            ProductDbContext productContext = (ProductDbContext)context;

            if (productContext.Services.FirstOrDefault(service => service.Name == this.Name
                                                            && service.ServiceID != this.ServiceID) != null)
            {
                this.ValidationErrors.Add(new ValidationResult("Service with same Name already exists", this, "Name", "CheckIfSameNameExists", null));
                return;
            }
        }
        #endregion
    }

    public class ServiceMetaData
    {
        [System.ComponentModel.DataAnnotations.Required()]
        public string Name { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public string Description { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required()]
        [SelectList(CodeListKey.ServiceTypeList)]
        public int TypeCodeID { get; set; }

    }
}
