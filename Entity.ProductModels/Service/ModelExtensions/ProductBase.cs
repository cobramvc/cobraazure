﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ProductBaseMetaData))]
    public partial class ProductBase : AbstractEntity
    {
    }

    public class ProductBaseMetaData
    {
        [System.ComponentModel.DataAnnotations.Required()]
        public string Name { get; set; }      
    }
}
