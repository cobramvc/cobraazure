﻿using Entity.ModelHelpers;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ServiceGroupMetaData))]
    public partial class ServiceGroup : BaseEntity<ServiceGroup>
    {
        public bool HasServiceGroupPricing { get; set; }

        public ServiceGroupPricing CurrentServiceGroupPricing(int contractTermTypeCodeID)
        {
            return this.ServiceGroupPricings.Where(pricing => pricing.RecordStatus
                && pricing.ContractTermTypeCodeID == contractTermTypeCodeID)
                .OrderByDescending(pricing => pricing.Version)
                .FirstOrDefault();            
        }

        public ServiceGroupPricing GetServiceGroupPricing(int contractTermTypeCodeID, int version)
        {
            return this.ServiceGroupPricings.Where(pricing => pricing.RecordStatus
                && pricing.ContractTermTypeCodeID == contractTermTypeCodeID
                && pricing.Version == version)
                .OrderByDescending(pricing => pricing.Version)
                .FirstOrDefault();
        }

        #region Custom Validations
        [SelfValidation]
        internal void MoreThanOneServiceAdded(DbContext context)
        {
            if (this.ServiceGroupDetailXRefs.Where(detail => detail.RecordStatus).Count() <= 1)
            {
                this.ValidationErrors.Add(new ValidationResult("More than one service to be added to create as Service Group", this, "ServiceGroupID", "AtleastOneServiceAdded", null));
                return;
            }
        }

        [SelfValidation]
        internal void DuplicateServicesAdded(DbContext context)
        {
            List<ServiceGroupDetailXRef> svcDetails = this.ServiceGroupDetailXRefs.ToList();
            if (svcDetails.Exists(detail => detail.RecordStatus
                && svcDetails.Exists(svcDetail => svcDetail.ServiceID == detail.ServiceID
                                        && svcDetail.RecordStatus
                                        && svcDetail.ServiceGroupDetailXRefID != detail.ServiceGroupDetailXRefID)))
            {
                this.ValidationErrors.Add(new ValidationResult("Duplicate services added", this, "ServiceGroupID", "DuplicateServicesAdded", null));
                return;
            }
        }

        [SelfValidation]
        internal void CheckIfSameNameExists(DbContext context)
        {
            if (!(context is ProductDbContext))
            {
                return;
            }

            if (this.RecordVersion > 0)
                return;

            ProductDbContext productContext = (ProductDbContext)context;

            if (productContext.ServiceGroups.FirstOrDefault(serviceGroup => serviceGroup.Name == this.Name
                                                            && serviceGroup.ServiceGroupID != this.ServiceGroupID) != null)
            {
                this.ValidationErrors.Add(new ValidationResult("ServiceGroup with same Name already exists", this, "Name", "CheckIfSameNameExists", null));
                return;
            }
        }
        #endregion

    }

    public class ServiceGroupMetaData
    {
        [System.ComponentModel.DataAnnotations.Required()]
        public string Name { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public string Description { get; set; }
    }
}
