﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ServiceGroupPricingDetailXRefMetaData))]
    public partial class ServiceGroupPricingDetailXRef : BaseEntity<ServiceGroupPricingDetailXRef>
    {
        public int StateCodeID { get; set; }
        public int CityCodeID { get; set; }
        public int RegionCodeID { get; set; }
        public string PricingGroupKey
        {
            get
            {                
                return this.CategoryCodeID.Value.ToString() + "-" + this.ServiceAreaID.Value.ToString();
            }
        }

        public string OrderedPricingGroupKey
        {
            get
            {
                int stateCodeID = this.ServiceArea != null && this.ServiceArea.StateCodeID.HasValue ? this.ServiceArea.StateCodeID.Value : 0;
                int cityCodeID = this.ServiceArea != null && this.ServiceArea.CityCodeID.HasValue ? this.ServiceArea.CityCodeID.Value : 0;
                int regionCodeID = this.ServiceArea != null && this.ServiceArea.RegionCodeID.HasValue ? this.ServiceArea.RegionCodeID.Value : 0;
                return this.CategoryCodeID.Value.ToString() + "-" + stateCodeID + "-" + cityCodeID + "-" + regionCodeID + "-" + this.ServiceAreaID.Value.ToString();
            }
        }
    }

    public class ServiceGroupPricingDetailXRefMetaData
    {        
        [System.ComponentModel.DataAnnotations.Required()]
        public long ServiceID { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required()]
        public long CategoryCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public long ServiceAreaID { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public decimal SubscriptionCharges { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public decimal OneTimeCharges { get; set; }

    }
}
