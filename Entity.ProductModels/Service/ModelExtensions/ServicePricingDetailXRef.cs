﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ServicePricingDetailXRefMetaData))]
    public partial class ServicePricingDetailXRef : BaseEntity<ServicePricingDetailXRef>
    {
        private int stateCodeID = Int32.MinValue;
        private int cityCodeID = Int32.MinValue;
        private int regionCodeID = Int32.MinValue;

        [System.ComponentModel.DataAnnotations.Required()]
        public int StateCodeID 
        { 
            get
            {
                if (stateCodeID == Int32.MinValue)
                    stateCodeID = this.ServiceArea != null ? this.ServiceArea.StateCodeID .Value: Int32.MinValue;

                return stateCodeID;
            }
            set
            {
                stateCodeID = value;
            }
        }

        public int CityCodeID
        {
            get
            {
                if (cityCodeID == Int32.MinValue)
                    cityCodeID = this.ServiceArea != null ? this.ServiceArea.CityCodeID.Value : Int32.MinValue;

                return cityCodeID;
            }
            set
            {
                cityCodeID = value;
            }
        }

        public int RegionCodeID
        {
            get
            {
                if (regionCodeID == Int32.MinValue)
                    regionCodeID = this.ServiceArea != null ? this.ServiceArea.RegionCodeID.Value : Int32.MinValue;

                return regionCodeID;
            }
            set
            {
                regionCodeID = value;
            }
        }

        public string PricingGroupKey
        {
            get
            {
                return this.CategoryCodeID.Value.ToString() + "-" + this.ServiceAreaID.Value.ToString();
            }
        }

        public Service Service
        {
            get
            {
                return this.ServicePricing != null ? this.ServicePricing.Service : null;
            }
        }
    }

    public class ServicePricingDetailXRefMetaData
    {        
        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required()]
        [SelectList(CodeListKey.CustomerCategoryList)]
        public long CategoryCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public long ServiceAreaID { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public decimal SubscriptionCharges { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public decimal OneTimeCharges { get; set; }

        [SelectList(CodeListKey.StateList)]
        public int StateCodeID { get; set; }

        [SelectList(CodeListKey.CityList)]
        [DependencyColumn("StateCodeID")]
        public int CityCodeID { get; set; }

        [SelectList(CodeListKey.RegionList)]
        [DependencyColumn("CityCodeID")]
        public int RegionCodeID { get; set; }


    }
}
