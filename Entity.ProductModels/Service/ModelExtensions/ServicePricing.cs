﻿using Entity.ModelHelpers;
using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ServicePricingMetaData))]
    public partial class ServicePricing : BaseEntity<ServicePricing>
    {
        //public DateTime? DbEffectiveDate {get; set;}
        
        public List<ServicePricingDetailXRef> CurrentServicePricingDetailXRefs
        {
            get
            {
                return this.ServicePricingDetailXRefs
                    .Where(pricing => pricing.RecordStatus)
                    .ToList();
            }
        }

        [SelfValidation]
        internal void IsDuplicatePricingSpecified(DbContext context)
        {           
            if(this.CurrentServicePricingDetailXRefs
                .Any(pricing =>
                    this.CurrentServicePricingDetailXRefs.Exists(p => p.CategoryCodeID == pricing.CategoryCodeID
                                                                && p.StateCodeID == pricing.StateCodeID
                                                                && p.CityCodeID == pricing.CityCodeID
                                                                && p.RegionCodeID == pricing.RegionCodeID
                                                                && p.ServicePricingDetailXRefID != pricing.ServicePricingDetailXRefID)
                   ))                    
            {
                this.ValidationErrors.Add(new ValidationResult("Duplicate Pricing specified for the same category, State, City and Region", this, "ServicePricingID", "IsDuplicatePricingSpecified", null));
            }
        }

        [SelfValidation]
        internal void AtleastOnePricingSpecified(DbContext context)
        {
            if (this.CurrentServicePricingDetailXRefs.Count <= 0)
            {
                this.ValidationErrors.Add(new ValidationResult("Pricing not defined for this service", this, "ServicePricingID", "AtleastOnePricingSpecified", null));
            }
        }

        [SelfValidation]
        internal void HasEffectiveDateChanged(DbContext context)
        {
            if (!this.EffectiveDate.HasValue)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date is required", this, "EffectiveDate", "HasEffectiveDateChanged", null));
                return;
            }

            if (this.EffectiveDate.Value < DateTime.Today.Date)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date can not be less than current date", this, "EffectiveDate", "HasEffectiveDateChanged", null));
                return;
            }

            if (this.DbObjectBeforeChanged != null
                && this.DbObjectBeforeChanged.EffectiveDate.HasValue
                && this.DbObjectBeforeChanged.EffectiveDate.Value == this.EffectiveDate.Value)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date has not been changed", this, "EffectiveDate", "HasEffectiveDateChanged", null));
                return;
            }
        }
    }

    public class ServicePricingMetaData
    {
        [SelectList(CodeListKey.ContractTermTypeCodeList)]
        public int ContractTermTypeCodeID { get; set; }
    }
}
