﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ProductDetailXRefMetaData))]
    public partial class ProductDetailXRef : AbstractEntity
    {
    }

    public class ProductDetailXRefMetaData
    {
        [System.ComponentModel.DataAnnotations.Required()]
        public long ProductID { get; set; }
        
        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required()]
        public long CategoryCodeID { get; set; }
        
        [System.ComponentModel.DataAnnotations.Required()]
        public decimal SubscriptionCharges { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public decimal OneTimeCharges { get; set; }

    }
}
