﻿using Entity.ModelHelpers;
using Infrastructure;
using Infrastructure.Resources;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ProductMetaData))]
    public partial class Product : BaseEntity<Product>
    {
        public bool HasActiveContract { get; set; }

        public DateTime? PricingEffectiveDate { get; set; }

        [SelfValidation]
        internal void IsSeviceOrServiceGroupSpecified(DbContext context)
        {
            if (this.ServiceID.HasValue && this.ServiceID.Value > 0)
                return;

            if (this.ServiceGroupID.HasValue && this.ServiceGroupID.Value > 0)
                return;

            this.ValidationErrors.Add(new ValidationResult("Service/Service Group not defined for this Product", this, "ProductID", "IsSeviceOrServiceGroupSpecified", null));            
        }


        [SelfValidation]
        internal void IsServicePricingSpecified(DbContext context)
        {
            if (this.Version <= 0)
            {
                if (this.ServiceID.HasValue
                    && this.ServiceID.Value > 0)
                {
                    this.ValidationErrors.Add(new ValidationResult("ServicePricing not defined for this Service and ContractTerm", this, "ServiceID", "IsServicePricingSpecified", null));
                    return;
                }                                    

                if (this.ServiceGroupID.HasValue
                    && this.ServiceGroupID.Value > 0)
                {
                    this.ValidationErrors.Add(new ValidationResult("ServicePricing not defined for this ServiceGroup and ContractTerm", this, "ServiceGroupID", "IsServicePricingSpecified", null));
                    return;
                }
            }                      
        }

        [SelfValidation]
        internal void CheckIfBeginDateGreaterThanEndDate(DbContext context)
        {
            if (this.EndDate.HasValue
                    && this.BeginDate.Date >= this.EndDate.Value.Date)
            {
                this.ValidationErrors.Add(new ValidationResult("Begindate gretaer than or equal to EndDate", this, "BeginDate", "CheckIfBeginDateGreaterThanEndDate", null));
                return;
            }
        }

        [SelfValidation]
        internal void CheckIfBeginDateIsValid(DbContext context)
        {
            if (!this.PricingEffectiveDate.HasValue)
                return;

            if (this.PricingEffectiveDate.Value != DateTime.MinValue
                && this.BeginDate >= this.PricingEffectiveDate.Value) 
                return;

            this.ValidationErrors.Add(new ValidationResult("No Pricing defined for Service/Service Group on Begin Date", this, "BeginDate", "CheckIfBeginDateIsValid", null));                        
        }

        [SelfValidation]
        internal void CheckIfSameNameExists(DbContext context)
        {
            if (!(context is ProductDbContext))
            {
                return;
            }

            if (this.RecordVersion > 0)
                return;

            ProductDbContext productContext = (ProductDbContext)context;

            if (productContext.Products.FirstOrDefault(p => p.Name == this.Name
                                                            && p.ProductID != this.ProductID) != null)
            {
                this.ValidationErrors.Add(new ValidationResult("Product with same Name already exists", this, "Name", "CheckIfSameNameExists", null));
                return;
            }
        }

        [SelfValidation]
        internal void CanProductCancelled(DbContext context)
        {
            // If new product
            if (this.RecordVersion == 0)
                return;

            if (!this.StatusCodeID.HasValue)
                return;

            if (this.StatusCodeID == ProductStatusKey.Active)
                return;
            
            if(!this.HasActiveContract)
                return;

            this.ValidationErrors.Add(new ValidationResult("Product can not be cancelled as it has active Contract", this, "StatusCodeID", "CanProductCancelled", null));
        }
    }

    public class ProductMetaData
    {
        [System.ComponentModel.DataAnnotations.Required()]
        public string Name { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        public string Description { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required()]
        [SelectList(CodeListKey.ProductTypeCodeList)]
        public int TypeCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required()]
        [SelectList(CodeListKey.ContractTermTypeCodeList)]
        public int ContractTypeCodeID { get; set; }

        [SelectList(CodeListKey.ProductStatusCodeList)]
        [System.ComponentModel.DataAnnotations.Required()]
        public Nullable<int> StatusCodeID { get; set; }

    }
}
