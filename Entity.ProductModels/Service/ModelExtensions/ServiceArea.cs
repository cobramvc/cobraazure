﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ServiceAreaMetaData))]
    public partial class ServiceArea : AbstractEntity
    {

    }

    public class ServiceAreaMetaData
    {
        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Required()]
        public int StateCodeID { get; set; }
    }
}
