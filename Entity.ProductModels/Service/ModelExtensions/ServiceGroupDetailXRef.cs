﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(ServiceGroupDetailXRefMetaData))]
    public partial class ServiceGroupDetailXRef : BaseEntity<ServiceGroupDetailXRef>
    {
    }

    public class ServiceGroupDetailXRefMetaData
    {
        [System.ComponentModel.DataAnnotations.Required()]
        public long ServiceGroupID { get; set; }

        [System.ComponentModel.DataAnnotations.Required()]
        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        public long ServiceID { get; set; }
    }
}
