﻿using Entity.ModelHelpers;
using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ProductModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(DiscountMetaData))]
    public partial class Discount : BaseEntity<Discount>
    {
        public List<DiscountDetailXRef> LockedDiscountDetailXRefs { get; set; }

        public List<DiscountDetailXRef> CurrentDiscountDetailXRefs
        {
            get { return this.DiscountDetailXRefs.Where(detail => detail.RecordStatus).ToList() ;  }
        }

        [SelfValidation]
        internal void CheckOverlapingTimeLines(DbContext context)
        {
            this.CurrentDiscountDetailXRefs.ForEach(detail =>
            {
                DiscountDetailXRef duplicateDetail = this.CurrentDiscountDetailXRefs.FirstOrDefault(dupDetail =>
                dupDetail.DiscountDetailXRefID != detail.DiscountDetailXRefID
                && dupDetail.ProductID == detail.ProductID
                && dupDetail.CustomerCategoryCodeID == detail.CustomerCategoryCodeID
                && dupDetail.CityCodeID == detail.CityCodeID                
                && dupDetail.BeginDate <= detail.EndDate
                && dupDetail.EndDate >= detail.BeginDate);

                if(duplicateDetail != null)
                    this.ValidationErrors.Add(new ValidationResult("Duplicate Discount Detail specified with overlapping timelines", this, "DiscountID", "IsBeginDateLessThanEndDate", null));
            });
        }

        [SelfValidation]
        internal void AtLeastOneActiveDetailXRef(DbContext context)
        {
            if (this.CurrentDiscountDetailXRefs.Count == 0)
            {                
                this.ValidationErrors.Add(new ValidationResult("Atleast one Discount has to be specified", this, "DiscountID", "AtLeastOneActiveDetailXRef", null));
            }                    
        }

        [SelfValidation]
        internal void CheckIfSameNameExists(DbContext context)
        {
            if (!(context is ProductDbContext))
            {
                return;
            }
         
            ProductDbContext productContext = (ProductDbContext)context;

            if (productContext.Discounts.FirstOrDefault(d => d.Name == this.Name
                                                            && d.DiscountID != this.DiscountID) != null)
            {
                this.ValidationErrors.Add(new ValidationResult("Discount with same Name already exists", this, "Name", "CheckIfSameNameExists", null));
                return;
            }
        }
    }

    public class DiscountMetaData
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage ="This field is required")]
        [System.ComponentModel.DataAnnotations.MaxLength(25, ErrorMessage = "Name should be less than 25 characters")]
        public string Name { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.MaxLength(25, ErrorMessage = "Description should be less than 25 characters")]
        public string Description { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [SelectList(CodeListKey.DiscountCategoryTypeCodeList)]
        public int CategoryCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "This field is required")]
        [SelectList(CodeListKey.DiscountTypeCodeList)]
        public int TypeCodeID { get; set; }
    }
}
