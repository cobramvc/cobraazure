//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.ProductModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServicePricingDetailXRef
    {
        public long ServicePricingDetailXRefID { get; set; }
        public Nullable<int> CategoryCodeID { get; set; }
        public Nullable<long> ServiceAreaID { get; set; }
        public Nullable<decimal> SubscriptionCharges { get; set; }
        public Nullable<decimal> OneTimeCharges { get; set; }
        public long CreateBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public long UpdateBy { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public bool RecordStatus { get; set; }
        public int RecordVersion { get; set; }
        public Nullable<long> ServicePricingID { get; set; }
    
        public virtual CodeListDetail CategoryCode { get; set; }
        public virtual ServiceArea ServiceArea { get; set; }
        public virtual ServicePricing ServicePricing { get; set; }
    }
}
