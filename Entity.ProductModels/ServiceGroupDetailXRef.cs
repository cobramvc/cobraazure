//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.ProductModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceGroupDetailXRef
    {
        public long ServiceGroupDetailXRefID { get; set; }
        public long ServiceGroupID { get; set; }
        public long ServiceID { get; set; }
        public long CreateBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public long UpdateBy { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public bool RecordStatus { get; set; }
        public int RecordVersion { get; set; }
    
        public virtual Service Service { get; set; }
        public virtual ServiceGroup ServiceGroup { get; set; }
    }
}
