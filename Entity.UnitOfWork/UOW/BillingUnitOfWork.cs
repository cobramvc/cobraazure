﻿using Entity.BillingModels;
using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public class BillingUnitOfWork : IUnitOfWork, IDisposable
    {
        private BillingDbContext billingDbContext = null;

        public BillingUnitOfWork()
        {
            billingDbContext = new BillingDbContext();
        }
            
        internal BillingDbContext DbContext
        {
            get { return billingDbContext; }
        }

        public void SetCurrentUser(User currentUser)
        {
            this.billingDbContext.User = currentUser;
        }


        public bool SaveChanges()
        {
            billingDbContext.SaveChanges();
            return true;
        }

        public void Dispose()
        {
            this.billingDbContext.Dispose();
        }
    }
}
