﻿using Entity.ContractModels;
using Entity.ModelHelpers;
using Infrastructure.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public class CustomerContractUnitOfWork : IUnitOfWork, IDisposable
    {
        private ContractDbContext contractDbContext = null;

        public CustomerContractUnitOfWork()
        {            
            contractDbContext = new ContractDbContext();
        }
            
        internal ContractDbContext DbContext
        {
            get { return contractDbContext; }
        }

        public void SetCurrentUser(User currentUser)
        {
            this.contractDbContext.User = currentUser;
        }


        public bool SaveChanges()
        {
            contractDbContext.SaveChanges();
            return true;
        }

        public void Dispose()
        {
            this.contractDbContext.Dispose();
        }
    }
}
