﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        bool SaveChanges();
        void SetCurrentUser(User currentUser);
    }
}
