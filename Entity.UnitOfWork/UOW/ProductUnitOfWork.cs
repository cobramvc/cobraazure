﻿using Entity.ModelHelpers;
using Entity.ProductModels;
//using Entity.Model.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public class ProductUnitOfWork : IUnitOfWork, IDisposable
    {
        private ProductDbContext productDbContext = new ProductDbContext();

        internal ProductDbContext DbContext
        {
            get { return productDbContext; }
        }

        public bool SaveChanges()
        {
            productDbContext.SaveChanges();
            return true;
        }

        public void SetCurrentUser(User currentUser)
        {
            this.productDbContext.User = currentUser;
        }

        public void Dispose()
        {
            this.productDbContext.Dispose();
        }
    }
}
