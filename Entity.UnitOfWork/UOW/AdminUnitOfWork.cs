﻿using Entity.AdminModels;
using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public class AdminUnitOfWork : IUnitOfWork, IDisposable
    {
        private AdminDbContext adminDbContext = null;

        public AdminUnitOfWork()
        {
            adminDbContext = new AdminDbContext();
        }

        internal AdminDbContext DbContext
        {
            get { return adminDbContext; }
        }

        public void SetCurrentUser(User currentUser)
        {
            this.adminDbContext.User = currentUser;
        }


        public bool SaveChanges()
        {
            adminDbContext.SaveChanges();
            return true;
        }

        public void Dispose()
        {
            this.adminDbContext.Dispose();
        }

    }
}
