﻿using Entity.Model.CustomerModels;
using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public class CustomerUnitOfWork : IUnitOfWork, IDisposable
    {
        private CustomerDbContext customerDbContext = new CustomerDbContext();

        internal CustomerDbContext DbContext
        {
            get { return customerDbContext; }
        }

        public bool SaveChanges()
        {
            customerDbContext.SaveChanges();
            return true;
        }

        public void SetCurrentUser(User currentUser)
        {
            this.customerDbContext.User = currentUser;
        }


        public void Dispose()
        {
            this.customerDbContext.Dispose();
        }
    }
}
