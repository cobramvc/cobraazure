﻿using Entity.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public interface IServiceAreaRepository
    {
        ServiceArea CreateServiceArea();
        ServiceArea GetServiceAreaByID(long serviceAreaID);
        List<ServiceArea> GetServiceAreaByState(int stateCodeID);
        List<ServiceArea> GetServiceAreaByCity(int cityCodeID);
        List<ServiceArea> GetServiceAreaByRegion(int regionCodeID);
        List<ServiceArea> GetServiceAreaByArea(int stateCodeID, int cityCodeID, int regionCodeID);
    }
}
