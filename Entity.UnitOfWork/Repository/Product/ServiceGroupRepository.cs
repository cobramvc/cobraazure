﻿using COBRA.Models;
using Entity.ModelHelpers;
using Entity.ProductModels;
using Entity.UnitOfWork.BusinessRules;
using Infrastructure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public class ServiceGroupRepository : BaseRepository, IServiceGroupRepository
    {
        private ProductUnitOfWork _uow = null;
        private ProductDbContext _dbContext = null;
        private CodeListDetailRepository codeLisDetailRepository = null;

        public ServiceGroupRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            this._uow = (ProductUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;
            this.codeLisDetailRepository = new CodeListDetailRepository(this._uow);
            this._dbContext.DbObjectContext.ObjectMaterialized += DbObjectContext_ObjectMaterialized;
        }

        public ServiceGroup CreateServiceGroup()
        {
            ServiceGroup serviceGroup = ServiceGroupFactory.Instance.CreateServiceGroup(this._dbContext);
            this.ExecuteBusinessRules(serviceGroup, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return serviceGroup;
        }

        public ServiceGroupDetailXRef CreateServiceGroupDetailXRef()
        {
            ServiceGroupDetailXRef serviceGroupDetailXRef = ServiceGroupFactory.Instance.CreateServiceGroupDetailXRef(this._dbContext);
            this.ExecuteBusinessRules(serviceGroupDetailXRef, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return serviceGroupDetailXRef;
        }

        public ServiceGroupPricing CreateServiceGroupPricing()
        {
            ServiceGroupPricing pricing = ServiceGroupFactory.Instance.CreateServiceGroupPricing(this._dbContext);
            this.ExecuteBusinessRules(pricing, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return pricing;
        }

        public ServiceGroupPricingDetailXRef CreateServiceGroupPricingDetailXRef()
        {
            ServiceGroupPricingDetailXRef pricingDetailXRef =  ServiceGroupFactory.Instance.CreateServiceGroupPricingDetailXRef(this._dbContext);
            this.ExecuteBusinessRules(pricingDetailXRef, BusinessRulesActionTypeEnum.ObjectMaterialize, "ReadOnly");
            return pricingDetailXRef;
        }

        public void AddServiceGroup(ServiceGroup serviceGroup)
        {
            List<ServiceGroupDetailXRef> deletedServiceDetails = serviceGroup.ServiceGroupDetailXRefs.Where(detail => !detail.RecordStatus).ToList();
            deletedServiceDetails.ForEach(detail =>
                {
                    serviceGroup.ServiceGroupDetailXRefs.Remove(detail);
                });

            this._dbContext.ServiceGroups.Add(serviceGroup);
        }

        public void UpdateServiceGroup(ServiceGroup serviceGroup)
        {
            ServiceGroup dbServiceGroup = this.GetServiceGroupByID(serviceGroup.ServiceGroupID);
            dbServiceGroup.IsDirty = DbContextHelper.Instance.UpdateProperties(dbServiceGroup, serviceGroup);

            if (dbServiceGroup.IsDirty)
            {
                this._dbContext.Entry(dbServiceGroup).State = EntityState.Modified;
            }

            string[] keysToCompare = { "ServiceGroupDetailXRefID" };
            DbContextHelper.Instance.UpdateChangeStatusForEntityCollection(serviceGroup.ServiceGroupDetailXRefs.ToList(), dbServiceGroup.ServiceGroupDetailXRefs.ToList(), keysToCompare, this._dbContext);            
        }

        public void UpdateServiceGroupPricingDetails(ServiceGroupPricing serviceGroupPricing)
        {
            serviceGroupPricing.Version = serviceGroupPricing.Version + 1;

            ServiceGroupPricing newServiceGroupPricing = this.CreateServiceGroupPricing();
            string[] propertyExcludeList = { "ServiceGroupPricingID" };
            DbContextHelper.Instance.UpdateProperties(newServiceGroupPricing, serviceGroupPricing, propertyExcludeList);
            

            List<ServiceGroupPricingDetailXRef> serviceGroupPricingDetailXRefs = serviceGroupPricing.ServiceGroupPricingDetailXRefs.ToList();

            serviceGroupPricingDetailXRefs.ForEach(serviceGroupPricingDetailXRef =>
            {                                
                ServiceGroupPricingDetailXRef newServiceGroupPricingDetailXRef = this.CreateServiceGroupPricingDetailXRef();
                string[] excludeList = { "ServiceGroupPricingDetailXRefID" };
                DbContextHelper.Instance.UpdateProperties(newServiceGroupPricingDetailXRef, serviceGroupPricingDetailXRef, excludeList);
                newServiceGroupPricingDetailXRef.ServiceGroupPricingID = newServiceGroupPricing.ServiceGroupPricingID;
                newServiceGroupPricingDetailXRef.RecordStatus = serviceGroupPricingDetailXRef.RecordStatus;
                newServiceGroupPricing.ServiceGroupPricingDetailXRefs.Add(newServiceGroupPricingDetailXRef);
            });

            this._dbContext.ServiceGroupPricings.Add(newServiceGroupPricing);
        }

        public ServiceGroup GetServiceGroupByID(long serviceGroupID)
        {
            ServiceGroup tempServiceGroup = this._dbContext.ServiceGroups.Where(serviceGroup => serviceGroup.ServiceGroupID == serviceGroupID
                && serviceGroup.RecordStatus).FirstOrDefault();

            return tempServiceGroup;
        }

        public void AddCalculatedServiceGroupPricingDetails(ServiceGroupPricing serviceGroupPricing)
        {
            List<ServiceGroupPricingDetailXRef> pricingDetails = this.GetServiceGroupPricingDetailXRefs(serviceGroupPricing);
            List<ServiceGroupPricingDetailXRef> dbPricingDetails = serviceGroupPricing.ServiceGroupPricingDetailXRefs.ToList();
            pricingDetails.ForEach(pricingDetail =>
            {
                if (!dbPricingDetails.Exists(dbPricingDetail => dbPricingDetail.ServiceID == pricingDetail.ServiceID
                    && dbPricingDetail.PricingGroupKey == pricingDetail.PricingGroupKey))
                {
                    serviceGroupPricing.ServiceGroupPricingDetailXRefs.Add(ServiceGroupFactory.Instance.LoadChildrenServiceGroupPricingDetailXRef(this._dbContext, pricingDetail));
                }
            });            
        }
        
        public List<ServiceGroup> GetServiceGroups()
        {
            this.DisbaleBusinessRules = true;
            List<ServiceGroup> serviceGroups =  this._dbContext.ServiceGroups.Where(svcGroup => svcGroup.RecordStatus).ToList();
            this.DisbaleBusinessRules = false;
            return serviceGroups;
        }

        public List<ServiceGroup> GetServiceGroupsBySearchCriteria(ServiceGroupSearchCriteriaModel searchCriteria)
        {
            this.DisbaleBusinessRules = true;
            var query = this._dbContext.ServiceGroups.Where(svcGroup => svcGroup.RecordStatus);

            if (!string.IsNullOrEmpty(searchCriteria.ServiceGroupName))
            {
                query = query.Where(svcGroup => svcGroup.Name.StartsWith(searchCriteria.ServiceGroupName));
            }

            List<ServiceGroup> serviceGroups = query.ToList();
            this.DisbaleBusinessRules = false;
            return serviceGroups;
        }
                        
        public Service GetServiceByID(long serviceID)
        {
            return this._dbContext.Services.Where(service => service.ServiceID == serviceID).FirstOrDefault();
        }

        public ServiceGroupPricing GetServiceGroupPricingByID(long serviceGroupPricingID)
        {
            return this._dbContext.ServiceGroupPricings.FirstOrDefault(pricing => pricing.ServiceGroupPricingID == serviceGroupPricingID);
        }

        public int SaveChanges()
        {
            return this._dbContext.SaveChanges();
        }

        public bool Validate(AbstractEntity entity) 
        {
            bool isValid = true;

            if (entity is ServiceGroup)
            {
                ServiceGroup serviceGroup = (ServiceGroup)entity;
                isValid = DbContextHelper.Instance.ValidateEnity<ServiceGroup>(this._dbContext, serviceGroup);
                serviceGroup.ServiceGroupDetailXRefs.Where(sd => sd.RecordStatus).ToList().ForEach(serviceDetail =>
                {
                    if (!DbContextHelper.Instance.ValidateEnity<ServiceGroupDetailXRef>(this._dbContext, serviceDetail))
                        isValid = false;                
                });
            }
            else if (entity is ServiceGroupPricing)
            {
                ServiceGroupPricing serviceGroupPricing = (ServiceGroupPricing)entity;
                isValid = DbContextHelper.Instance.ValidateEnity<ServiceGroupPricing>(this._dbContext, serviceGroupPricing);
            }

            return isValid;            
        }

        public override void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties)
        {
            base.ExecuteBusinessRules(entity, actionType, properties);
            if (entity is ServiceGroup)
            {
                ServiceGroup serviceGroup = (ServiceGroup)entity;
                serviceGroup.ServiceGroupDetailXRefs.ToList().ForEach(detail =>
                {
                    base.ExecuteBusinessRules(detail, actionType, properties);
                });
                return;
            }

            if (entity is ServiceGroupPricing)
            {
                ServiceGroupPricing serviceGroupPricing = (ServiceGroupPricing)entity;
                serviceGroupPricing.ServiceGroupPricingDetailXRefs.ToList().ForEach(detail =>
                {
                    this.ExecuteBusinessRules(detail, actionType, properties);
                });
                return;
            }
        }
        
        public void Dispose()
        {
            this._uow.Dispose();
        }

        private List<ServiceGroupPricingDetailXRef> GetServiceGroupPricingDetailXRefs(ServiceGroupPricing serviceGroupPricing)
        {
            List<ServiceGroupPricingDetailXRef> serviceGroupPricingDetailXRefsAggregate = new List<ServiceGroupPricingDetailXRef>();
            List<ServiceGroupPricingDetailXRef> tempServiceGroupPricingDetailXRefsAggregate = null;
            List<ServicePricingDetailXRef> tempServicePricingDetailXRefs = new List<ServicePricingDetailXRef>();
            ServiceGroupPricingDetailXRef objSericeGroupPricingDetailXRef = null;
            ArrayList addedServices = null;
            ArrayList processedPricingGroupKeys = new ArrayList();
            int servicesCount = 0;

            ServiceGroup serviceGroup = serviceGroupPricing.ServiceGroup;

            // Get List of services mapped to Service Group
            List<ServiceGroupDetailXRef> serviceGroupDetailXRefs = serviceGroup.ServiceGroupDetailXRefs
                                        .Where(serviceGroupDetailXRef => serviceGroupDetailXRef.RecordStatus).ToList();
            servicesCount = serviceGroupDetailXRefs.Count;

            // Get Service Pricing details of each service mapped to service group
            serviceGroupDetailXRefs.ForEach(serviceGroupDetailXRef =>
            {
                Service service = serviceGroupDetailXRef.Service;
                ServicePricing servicePricing = service.CurrentServicePricing(serviceGroupPricing.ContractTermTypeCodeID);
                if (servicePricing != null)
                {
                    tempServicePricingDetailXRefs.AddRange(service.CurrentServicePricing(serviceGroupPricing.ContractTermTypeCodeID).CurrentServicePricingDetailXRefs);
                }                
            });

            tempServicePricingDetailXRefs.ForEach(svcPricingDetailXRef =>
            {
                addedServices = new ArrayList();
                tempServiceGroupPricingDetailXRefsAggregate = new List<ServiceGroupPricingDetailXRef>();

                // If Service Area does not exist in the Aggregate Collection
                if (!processedPricingGroupKeys.Contains(svcPricingDetailXRef.PricingGroupKey))
                {
                    // Add the current Service to Aggregate Collection 
                    objSericeGroupPricingDetailXRef = CreateServiceGroupPricingDetailXRef(svcPricingDetailXRef, serviceGroupPricing, svcPricingDetailXRef.ServiceAreaID.Value);
                    tempServiceGroupPricingDetailXRefsAggregate.Add(objSericeGroupPricingDetailXRef);
                    addedServices.Add(objSericeGroupPricingDetailXRef.ServiceID);

                    // Find other services with same Service Area and add to Aggregate collection
                    tempServicePricingDetailXRefs.Where(tmp => !addedServices.Contains(tmp.ServicePricing.ServiceID)
                        && tmp.PricingGroupKey == svcPricingDetailXRef.PricingGroupKey).ToList()
                        .ForEach(tmp =>
                        {
                            objSericeGroupPricingDetailXRef = CreateServiceGroupPricingDetailXRef(tmp, serviceGroupPricing, svcPricingDetailXRef.ServiceAreaID.Value);
                            tempServiceGroupPricingDetailXRefsAggregate.Add(objSericeGroupPricingDetailXRef);
                            addedServices.Add(objSericeGroupPricingDetailXRef.ServiceID);
                        });

                    // Find other Services within same Parent Service Area(City) and add to Aggregate Collection
                    if (svcPricingDetailXRef.ServiceArea.RegionCodeID > 0)
                    {
                        tempServicePricingDetailXRefs.Where(tmp => !addedServices.Contains(tmp.ServicePricing.ServiceID)
                        && tmp.CategoryCodeID == svcPricingDetailXRef.CategoryCodeID
                        && tmp.ServiceArea.CityCodeID == svcPricingDetailXRef.ServiceArea.CityCodeID
                        && tmp.ServiceArea.RegionCodeID <= 0).ToList()
                        .ForEach(tmp =>
                        {
                            objSericeGroupPricingDetailXRef = CreateServiceGroupPricingDetailXRef(tmp, serviceGroupPricing, svcPricingDetailXRef.ServiceAreaID.Value);
                            tempServiceGroupPricingDetailXRefsAggregate.Add(objSericeGroupPricingDetailXRef);
                            addedServices.Add(objSericeGroupPricingDetailXRef.ServiceID);
                        });
                    }

                    // Find other Services within same Parent Service Area(State) and add to Aggregate Collection
                    if (svcPricingDetailXRef.ServiceArea.CityCodeID > 0)
                    {
                        tempServicePricingDetailXRefs.Where(tmp => !addedServices.Contains(tmp.ServicePricing.ServiceID)
                        && tmp.CategoryCodeID == svcPricingDetailXRef.CategoryCodeID
                        && tmp.ServiceArea.StateCodeID == svcPricingDetailXRef.ServiceArea.StateCodeID
                        && tmp.ServiceArea.CityCodeID <= 0).ToList()
                        .ForEach(tmp =>
                        {
                            objSericeGroupPricingDetailXRef = CreateServiceGroupPricingDetailXRef(tmp, serviceGroupPricing, svcPricingDetailXRef.ServiceAreaID.Value);
                            tempServiceGroupPricingDetailXRefsAggregate.Add(objSericeGroupPricingDetailXRef);
                            addedServices.Add(objSericeGroupPricingDetailXRef.ServiceID);
                        });
                    }
                }

                if (tempServiceGroupPricingDetailXRefsAggregate.Count == servicesCount)
                    serviceGroupPricingDetailXRefsAggregate.AddRange(tempServiceGroupPricingDetailXRefsAggregate);

                if (!processedPricingGroupKeys.Contains(svcPricingDetailXRef.PricingGroupKey))
                    processedPricingGroupKeys.Add(svcPricingDetailXRef.PricingGroupKey);
            });

            return serviceGroupPricingDetailXRefsAggregate;
        }

        private ServiceGroupPricingDetailXRef CreateServiceGroupPricingDetailXRef(ServicePricingDetailXRef svcPricingDetailXRef, ServiceGroupPricing serviceGroupPricing, long serviceAreaID)
        {
            ServiceGroupPricingDetailXRef objSericeGroupPricingDetailXRef = new ServiceGroupPricingDetailXRef();
            objSericeGroupPricingDetailXRef.ServiceGroupPricingID = serviceGroupPricing.ServiceGroupPricingID;
            objSericeGroupPricingDetailXRef.ServiceAreaID = serviceAreaID;            
            objSericeGroupPricingDetailXRef.ServiceID = svcPricingDetailXRef.ServicePricing.ServiceID;
            objSericeGroupPricingDetailXRef.SubscriptionCharges = svcPricingDetailXRef.SubscriptionCharges;
            objSericeGroupPricingDetailXRef.OneTimeCharges = svcPricingDetailXRef.OneTimeCharges;
            objSericeGroupPricingDetailXRef.CategoryCodeID = svcPricingDetailXRef.CategoryCodeID;
            objSericeGroupPricingDetailXRef.RecordStatus = true;
            return objSericeGroupPricingDetailXRef;
        }        
    }
}
