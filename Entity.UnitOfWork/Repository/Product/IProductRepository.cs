﻿using COBRA.Models;
using Entity.ProductModels;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public interface IProductRepository : IRepository, IDisposable
    {        
        Product CreateProduct();
        Discount CreateDiscount();
        DiscountDetailXRef CreateDiscountDetailXRef();

        void AddProduct(Product product);
        Product UpdateProduct(Product product);
        void AddDiscount(Discount discount);
        Discount UpdateDiscount(Discount discount);

        Product GetProductByID(long productID);
        List<Product> GetProducts();       
        List<ProductPricing> GetProductPricingByCustomerIDProductID(long productID, long customerID);
        //List<ProductPricing> GetProductPricingByServicePricingIDAreaID(long productID, long servicePricingID, long areaID);
        //List<ProductPricing> GetProductPricingByServiceGroupPricingIDAreaID(long productID, long serviceGroupPricingID, long areaID);
        List<Product> GetProductsBySearchCriteria(ProductSearchCriteriaModel searchCriteria);        
        bool HasActiveContract(long productID);
        int GetVersionByProduct(Product product);
        DateTime GetPricingEffectiveDate(Product product);
        Discount GetDiscountByID(int discountID);
        DiscountDetailXRef GetDiscountDetailXRefByID(int discountDetailXRefID);
        List<Discount> GetDiscountsBySearchCriteria(DiscountSearchCriteriaModel searchCriteria);
        List<DiscountDetailXRef> GetDiscountDetailXRefsLockedByContract(long discountID);
    }
}
