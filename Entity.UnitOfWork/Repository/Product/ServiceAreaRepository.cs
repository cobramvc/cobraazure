﻿using Entity.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public class ServiceAreaRepository : IServiceAreaRepository
    {
        private ProductUnitOfWork _uow = null;
        private ProductDbContext _dbContext = null;        

        public ServiceAreaRepository(IUnitOfWork unitOfWork)
        {
            this._uow = (ProductUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;            
        }

        public ServiceArea CreateServiceArea()
        {
            return ServiceAreaFactory.Instance.CreateServiceArea(this._dbContext);
        }

        public ServiceArea GetServiceAreaByID(long serviceAreaID)
        {
            return this._dbContext.ServiceAreas.Where(serviceArea => serviceArea.ServiceAreaID == serviceAreaID
                && serviceArea.RecordStatus).FirstOrDefault();
        }

        public List<ServiceArea> GetServiceAreaByState(int stateCodeID)
        {
            return this._dbContext.ServiceAreas.Where(serviceArea => serviceArea.StateCodeID.Value == stateCodeID
                && serviceArea.RecordStatus).ToList();
        }

        public List<ServiceArea> GetServiceAreaByCity(int cityCodeID)
        {
            return this._dbContext.ServiceAreas.Where(serviceArea => serviceArea.CityCodeID.Value == cityCodeID
                && serviceArea.RecordStatus).ToList();
        }

        public List<ServiceArea> GetServiceAreaByRegion(int regionCodeID)
        {
            return this._dbContext.ServiceAreas.Where(serviceArea => serviceArea.RegionCodeID.Value == regionCodeID
                && serviceArea.RecordStatus).ToList();
        }

        public List<ServiceArea> GetServiceAreaByArea(int stateCodeID, int cityCodeID, int regionCodeID)
        {
            return this._dbContext.ServiceAreas.Where(serviceArea => serviceArea.RegionCodeID.Value == regionCodeID
                && serviceArea.CityCodeID.Value == cityCodeID
                && serviceArea.StateCodeID.Value == stateCodeID
                && serviceArea.RecordStatus).ToList();
        }
    }
}
