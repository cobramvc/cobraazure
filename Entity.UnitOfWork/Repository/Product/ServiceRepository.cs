﻿using COBRA.Models;
using Entity.ModelHelpers;
using Entity.ProductModels;
using Entity.UnitOfWork.BusinessRules;
using Infrastructure;
//using Entity.Model.ProductModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public class ServiceRepository : BaseRepository, IServiceRepository
    {
        private ProductUnitOfWork _uow = null;
        private ProductDbContext _dbContext = null;
        private IServiceAreaRepository serviceAreaRepository = null;

        public ServiceRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            this._uow = (ProductUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;
            this.serviceAreaRepository = new ServiceAreaRepository(this._uow);
            this._dbContext.DbObjectContext.ObjectMaterialized += DbObjectContext_ObjectMaterialized;
        }

        public Service CreateService()
        {
            Service newService = ServiceFactory.Instance.CreateService(this._dbContext);

            //Only ReadOnly businessrule is executed to apply security rules on the new object created
            // If there are any other business rules to be executed then ExecuteBusinessRules has to be 
            // invoked again(after object construction is completed) with
            // BusinessRulesActionTypeEnum.ObjectMaterialize and list of rules that are to be executed
            this.ExecuteBusinessRules(newService, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newService;
        }

        public ServicePricing CreateServicePricing()
        {
            ServicePricing newServicePricing =  ServiceFactory.Instance.CreateServicePricing(this._dbContext);
            this.ExecuteBusinessRules(newServicePricing, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newServicePricing;
        }

        public ServicePricingDetailXRef CreateServicePricingDetailXRef()
        {
            ServicePricingDetailXRef newServicePricingDetailXRef = ServiceFactory.Instance.CreateServicePricingDetailXRef(this._dbContext);
            this.ExecuteBusinessRules(newServicePricingDetailXRef, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newServicePricingDetailXRef;
        }

        public void AddService(Service service)
        {
            this._dbContext.Services.Add(service);
        }

        public void UpdateService(Service service)
        {
            Service dbService = this.GetServiceByID(service.ServiceID);
            dbService.IsDirty = DbContextHelper.Instance.UpdateProperties(dbService, service);

            //Check if this has changed
            if (dbService.IsDirty)
            {
                this._dbContext.Entry(dbService).State = EntityState.Modified;
            }           
        }

        public ServicePricing UpdateServicePricingDetails(ServicePricing servicePricing)
        {            
            servicePricing.Version = servicePricing.Version + 1;

            ServicePricing newServicePricing = this.CreateServicePricing();
            string[] propertyExcludeList = { "ServicePricingID" };
            DbContextHelper.Instance.UpdateProperties(newServicePricing, servicePricing, propertyExcludeList);
            
            List<ServicePricingDetailXRef> servicePricingDetailXRefs = servicePricing.CurrentServicePricingDetailXRefs.ToList();

            // Identify the existing ServiceArea to map
            servicePricingDetailXRefs.ForEach(svcPricingDetailXRef =>
                {                    
                    ServiceArea serviceArea = this.serviceAreaRepository.GetServiceAreaByArea(svcPricingDetailXRef.StateCodeID, svcPricingDetailXRef.CityCodeID, svcPricingDetailXRef.RegionCodeID).FirstOrDefault();
                    if (serviceArea == null)
                    {
                        serviceArea = this.serviceAreaRepository.CreateServiceArea();
                        serviceArea.StateCodeID = svcPricingDetailXRef.StateCodeID;
                        serviceArea.CityCodeID = svcPricingDetailXRef.CityCodeID;
                        serviceArea.RegionCodeID = svcPricingDetailXRef.RegionCodeID;

                        this._dbContext.ServiceAreas.Add(serviceArea);
                    }

                    svcPricingDetailXRef.ServiceAreaID = serviceArea.ServiceAreaID;

                    ServicePricingDetailXRef newServicePricingDetailXRef = this.CreateServicePricingDetailXRef();
                    string[] excludeList = { "ServicePricingDetailXRefID" };
                    DbContextHelper.Instance.UpdateProperties(newServicePricingDetailXRef, svcPricingDetailXRef, excludeList);
                    newServicePricingDetailXRef.ServicePricingID = newServicePricing.ServicePricingID;
                    newServicePricing.ServicePricingDetailXRefs.Add(newServicePricingDetailXRef);                    
                });

                this._dbContext.ServicePricings.Add(newServicePricing);
                this.ExecuteBusinessRules(newServicePricing, BusinessRulesActionTypeEnum.ObjectMaterialize, string.Empty);
                
                return newServicePricing;
        }

        public Service GetServiceByID(long serviceID)
        {
            Service service = this._dbContext.Services.Where(s => s.ServiceID == serviceID).FirstOrDefault();            
            return service;
        }
        
        public ServicePricing GetServicePricingByID(long servicePricingID)
        {
            return this._dbContext.ServicePricings.Where(pricing => pricing.RecordStatus
                && pricing.ServicePricingID == servicePricingID).FirstOrDefault();
        }

        public List<Service> GetServices()
        {
            this.DisbaleBusinessRules = true;
            List<Service> services = this._dbContext.Services.Where(service => service.RecordStatus).ToList();
            this.DisbaleBusinessRules = false;
            return services;
        }

        public List<Service> GetServicesBySearchCriteria(ServiceSearchCriteriaModel searchCriteria)
        {
            this.DisbaleBusinessRules = true;
            var query = this._dbContext.Services.Where(service => service.RecordStatus);

            if (!string.IsNullOrEmpty(searchCriteria.ServiceName))
            {
                query = query.Where(service => service.Name.Contains(searchCriteria.ServiceName));
            }

            if(searchCriteria.TypeCodeID > 0)
            {
                query = query.Where(service => service.TypeCodeID == searchCriteria.TypeCodeID);
            }

            List<Service> services = query.ToList();
            this.DisbaleBusinessRules = false;
            return services;
        }

        public bool HasActiveProduct(long serviceID)
        {
            this.DisbaleBusinessRules = true;
            Service service = this._dbContext.Database
                                   .SqlQuery<Service>("IsServiceHasActiveContract @ServiceID"
                                    , new SqlParameter("@ServiceID", serviceID)).FirstOrDefault();

            this.DisbaleBusinessRules = false;
            return service != null;
        }

        public override void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties)
        {            
            base.ExecuteBusinessRules(entity, actionType, properties);
            if (entity is ServicePricing)
            {
                ServicePricing servicePricing = (ServicePricing)entity;
                servicePricing.ServicePricingDetailXRefs.ToList().ForEach(detail =>
                {
                    base.ExecuteBusinessRules(detail, actionType, properties);
                });
            }            
        }     

        public bool Validate(AbstractEntity entity) 
        {
            bool isValid = true;

            if (entity is Service)
            {
                Service service = (Service)entity;
                isValid = DbContextHelper.Instance.ValidateEnity<Service>(this._dbContext, service); 
            }
            else if (entity is ServicePricing)
            {
                ServicePricing servicePricing = (ServicePricing)entity;
                isValid = DbContextHelper.Instance.ValidateEnity<ServicePricing>(this._dbContext, servicePricing); 
                servicePricing.CurrentServicePricingDetailXRefs.ToList().ForEach(pricingDetail =>
                {
                    if (!DbContextHelper.Instance.ValidateEnity<ServicePricingDetailXRef>(this._dbContext, pricingDetail))
                        isValid = false;                  
                });
            }

            return isValid;
        }

        public int SaveChanges()
        {
            return this._dbContext.SaveChanges();
        }
   
        public void Dispose()
        {
            this._uow.Dispose();
        }
    }
}
