﻿using COBRA.Models;
using Entity.ModelHelpers;
using Entity.ProductModels;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public interface IServiceGroupRepository : IRepository, IDisposable
    {
        ServiceGroup CreateServiceGroup();
        ServiceGroupDetailXRef CreateServiceGroupDetailXRef();
        ServiceGroupPricing CreateServiceGroupPricing();
        ServiceGroupPricingDetailXRef CreateServiceGroupPricingDetailXRef();
        void AddServiceGroup(ServiceGroup serviceGroup);
        void UpdateServiceGroup(ServiceGroup serviceGroup);
        void UpdateServiceGroupPricingDetails(ServiceGroupPricing serviceGroupPricing);
        ServiceGroup GetServiceGroupByID(long serviceGroupID);
        ServiceGroupPricing GetServiceGroupPricingByID(long serviceGroupPricingID);
        List<ServiceGroup> GetServiceGroups();
        List<ServiceGroup> GetServiceGroupsBySearchCriteria(ServiceGroupSearchCriteriaModel searchCriteria);        
        Service GetServiceByID(long serviceID);
        void AddCalculatedServiceGroupPricingDetails(ServiceGroupPricing serviceGroupPricing);        
        //void ExecuteBusinessRules(ServiceGroup serviceGroup, BusinessRulesActionTypeEnum actionType, params string[] properties);
        //void ExecuteBusinessRules(ServiceGroupPricing serviceGroupPricing, BusinessRulesActionTypeEnum actionType, params string[] properties);
    }
}
