﻿using COBRA.Models;
using Entity.ModelHelpers;
using Entity.ProductModels;
using Entity.UnitOfWork.BusinessRules;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public class ProductRepository : BaseRepository, IProductRepository
    {
        private ProductUnitOfWork _uow = null;
        private ProductDbContext _dbContext = null;

        public ProductRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            this._uow = (ProductUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;
            this._dbContext.DbObjectContext.ObjectMaterialized += DbObjectContext_ObjectMaterialized;
        }

        public Product CreateProduct()
        {
            Product newProduct = ProductFactory.Instance.CreateProduct(this._dbContext);
            this.ExecuteBusinessRules(newProduct, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newProduct;
        }

        public Discount CreateDiscount()
        {
            Discount newDiscount = DiscountFactory.Instance.CreateDiscount(this._dbContext);
            this.ExecuteBusinessRules(newDiscount, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newDiscount;
        }

        public DiscountDetailXRef CreateDiscountDetailXRef()
        {
            DiscountDetailXRef newDiscountDetailXRef = DiscountFactory.Instance.CreateDiscountDetailXRef(this._dbContext);
            this.ExecuteBusinessRules(newDiscountDetailXRef, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newDiscountDetailXRef;
        }

        public void AddProduct(Product product)
        {
            product.Version = this.GetVersionByProduct(product);
            this._dbContext.Products.Add(product);
            //this._dbContext.Products.Add(product);
        }

        public Product UpdateProduct(Product product)
        {            
            Product dbProduct = this.GetProductByID(product.ProductID);

            string[] productExcludeList = { "ProductID" };
            dbProduct.IsDirty = DbContextHelper.Instance.UpdateProperties(dbProduct, product, productExcludeList);

            if (dbProduct.IsDirty)
                this._dbContext.Entry(dbProduct).State = EntityState.Modified;

            return dbProduct;
        }

        public void AddDiscount(Discount discount)
        {

            List<DiscountDetailXRef> deletedDiscounts = discount.DiscountDetailXRefs.Where(d => !d.RecordStatus).ToList();
            deletedDiscounts.ForEach(d =>
            {
                discount.DiscountDetailXRefs.Remove(d);
            });

            discount.DiscountDetailXRefs.ToList()
                .ForEach(detail =>
                {
                    detail.DiscountID = discount.DiscountID;
                });

            this._dbContext.Discounts.Add(discount);            
        }

        public Discount UpdateDiscount(Discount discount)
        {
            Discount dbDiscount = this.GetDiscountByID(discount.DiscountID);

            string[] discountExcludeList = { "DiscountID" };
            dbDiscount.IsDirty = DbContextHelper.Instance.UpdateProperties(dbDiscount, discount, discountExcludeList);

            if (dbDiscount.IsDirty)
                this._dbContext.Entry(dbDiscount).State = EntityState.Modified;

            string[] keysToCompareDetail = { "DiscountDetailXRefID" };
            DbContextHelper.Instance.UpdateChangeStatusForEntityCollection(discount.DiscountDetailXRefs.ToList(), dbDiscount.DiscountDetailXRefs.ToList(), keysToCompareDetail, this._dbContext);

            return dbDiscount;
        }

        public Product GetProductByID(long productID)
        {
            return this._dbContext.Products.Where(product => product.RecordStatus
                && product.ProductID == productID).FirstOrDefault();
        }

        public int GetVersionByProduct(Product product)
        {
            int version = 0;

            if (product.Version > 0)
                return product.Version;

            if (product.ContractTypeCodeID <= 0)
                return version;

            if (product.ServiceID.HasValue
                && product.ServiceID.Value > 0)
            {
                ServicePricing servicePricing = this._dbContext.ServicePricings.Where(pricing =>
                    pricing.ServiceID == product.ServiceID.Value
                    && pricing.ContractTermTypeCodeID == product.ContractTypeCodeID
                    && pricing.RecordStatus)
                    .OrderByDescending(pricing => pricing.Version)
                    .FirstOrDefault();
                version = (servicePricing == null) ? 0 : servicePricing.Version;
            }

            else if (product.ServiceGroupID.HasValue
                && product.ServiceGroupID.Value > 0)
            {
                ServiceGroupPricing serviceGroupPricing = this._dbContext.ServiceGroupPricings.Where(pricing =>
                    pricing.ServiceGroupID == product.ServiceGroupID.Value
                    && pricing.ContractTermTypeCodeID == product.ContractTypeCodeID
                    && pricing.RecordStatus)
                    .OrderByDescending(pricing => pricing.Version)
                    .FirstOrDefault();

                version = (serviceGroupPricing == null) ? 0 : serviceGroupPricing.Version;
            }

            return version;
        }

        public DateTime GetPricingEffectiveDate(Product product)
        {
            DateTime effectiveDate = DateTime.MinValue;

            if (product.PricingEffectiveDate.HasValue)
                return product.PricingEffectiveDate.Value;

            if (product.ContractTypeCodeID <= 0)
                return effectiveDate;

            if (product.ServiceID.HasValue
                && product.ServiceID.Value > 0)
            {
                ServicePricing servicePricing = this._dbContext.ServicePricings.Where(pricing =>
                    pricing.ServiceID == product.ServiceID.Value
                    && pricing.ContractTermTypeCodeID == product.ContractTypeCodeID
                    && pricing.RecordStatus
                    && pricing.EffectiveDate.HasValue)
                    .OrderBy(pricing => pricing.Version)
                    .FirstOrDefault();
                effectiveDate = (servicePricing == null) ? DateTime.MinValue : (servicePricing.EffectiveDate.HasValue ? servicePricing.EffectiveDate.Value : DateTime.MinValue);
            }

            else if (product.ServiceGroupID.HasValue
                && product.ServiceGroupID.Value > 0)
            {
                ServiceGroupPricing serviceGroupPricing = this._dbContext.ServiceGroupPricings.Where(pricing =>
                    pricing.ServiceGroupID == product.ServiceGroupID.Value
                    && pricing.ContractTermTypeCodeID == product.ContractTypeCodeID
                    && pricing.RecordStatus
                    && pricing.EffectiveDate.HasValue)
                    .OrderBy(pricing => pricing.Version)
                    .FirstOrDefault();

                effectiveDate = (serviceGroupPricing == null) ? DateTime.MinValue : (serviceGroupPricing.EffectiveDate.HasValue ? serviceGroupPricing.EffectiveDate.Value : DateTime.MinValue);
            }

            return effectiveDate;
        }

        public List<Product> GetProducts()
        {
            this.DisbaleBusinessRules = true;
            List<Product> products = this._dbContext.Products.Where(product => product.RecordStatus).ToList();
            this.DisbaleBusinessRules = false;
            return products;
        }

        public List<ProductPricing> GetProductPricingByCustomerIDProductID(long productID, long customerID)
        {
            List<ProductPricing> productPricings = new List<ProductPricing>();

            Product product = this.GetProductByID(productID);
            if (product == null)
            {
                return productPricings;
            }

            if (product.ServiceGroupID.HasValue)
            {
                //Call stored procedure to pull ServiceGroupPricings
                List<ServiceGroupPricingDetailXRef> serviceGroupPricings = this._dbContext.Database
                                   .SqlQuery<ServiceGroupPricingDetailXRef>("GetCustomerProductPricingByServiceGroup @CustomerID, @ServiceGroupID, @ProductID"
                                    , new SqlParameter("@CustomerID", customerID), new SqlParameter("@ServiceGroupID", product.ServiceGroupID), new SqlParameter("@ProductID", product.ProductID)).ToList();

                serviceGroupPricings.ForEach(serviceGroupPricing =>
                {
                    ProductPricing productPricing = this.CreateProductPricingForServiceGroup(product, serviceGroupPricing);
                    productPricings.Add(productPricing);
                });

            }
            else if (product.ServiceID.HasValue)
            {
                //Call stored procedure to pull Service pricings
                List<ServicePricingDetailXRef> servicePricings = this._dbContext.Database
                                   .SqlQuery<ServicePricingDetailXRef>("GetCustomerProductPricingByService @CustomerID, @ServiceID, @ProductID"
                                    , new SqlParameter("@CustomerID", customerID), new SqlParameter("@ServiceID", product.ServiceID), new SqlParameter("@ProductID", product.ProductID)).ToList();

                servicePricings.ForEach(servicePricing =>
                {
                    ProductPricing productPricing = this.CreateProductPricingForService(product, servicePricing);
                    productPricings.Add(productPricing);
                });
            }

            return productPricings;
        }

        public List<Product> GetProductsBySearchCriteria(ProductSearchCriteriaModel searchCriteria)
        {
            this.DisbaleBusinessRules = true;

            if (searchCriteria.IsContractProductSearch)
            {
                List<Product> contractProducts = this.GetCustomerContractActiveProducts(searchCriteria);
                this.DisbaleBusinessRules = false;
                return contractProducts;
            }
                
            var query = this._dbContext.Products.Where(product => product.RecordStatus);

            if (!string.IsNullOrEmpty(searchCriteria.ProductDescription))
            {
                query = query.Where(product => product.Description.StartsWith(searchCriteria.ProductDescription));
            }

            List<Product> products = query.ToList();
            this.DisbaleBusinessRules = false;
            return products;
        }

        public List<DiscountDetailXRef> GetDiscountDetailXRefsLockedByContract(long discountID)
        {
            List<DiscountDetailXRef> detailXRefs = this._dbContext.Database
                                   .SqlQuery<DiscountDetailXRef>("GetDiscountsLockedByContracts @DiscountID"
                                    , new SqlParameter("@DiscountID", discountID)).ToList();

            return detailXRefs;
        }

        public DiscountDetailXRef GetDiscountDetailXRefByID(int discountDetailXRefID)
        {
            return this._dbContext.DiscountDetailXRefs.FirstOrDefault(d => d.DiscountDetailXRefID == discountDetailXRefID);
        }

        public bool HasActiveContract(long productID)
        {
            this.DisbaleBusinessRules = true;
            bool hasContract = false;
            using (CustomerContractDbContextHelper helper = new CustomerContractDbContextHelper())
            {
                hasContract = helper.CustomerContractRepositoryInstance.HasActiveContractForProduct(productID);
            }
            this.DisbaleBusinessRules = false;
            return hasContract;
        }

        public Discount GetDiscountByID(int discountID)
        {
            return this._dbContext.Discounts.Where(discount => discount.RecordStatus
                && discount.DiscountID == discountID).FirstOrDefault();
        }

        public List<Discount> GetDiscountsBySearchCriteria(DiscountSearchCriteriaModel searchCriteria)
        {
            this.DisbaleBusinessRules = true;
            var query = this._dbContext.Discounts.Where(discount => discount.RecordStatus);

            if (!string.IsNullOrEmpty(searchCriteria.DiscountName))
            {
                query = query.Where(discount => discount.Name.StartsWith(searchCriteria.DiscountName));
            }

            List<Discount> discounts = query.ToList();
            this.DisbaleBusinessRules = false;
            return discounts;
        }

        public int SaveChanges()
        {
            return this._dbContext.SaveChanges();
        }

        public bool Validate(AbstractEntity entity) 
        {
            bool isValid = true;
            if (entity is Product)
            {
                Product product = (Product)entity;
                return DbContextHelper.Instance.ValidateEnity<Product>(this._dbContext, product);
            }

            if (entity is Discount)
            {
                Discount discount = (Discount)entity;
                isValid = DbContextHelper.Instance.ValidateEnity<Discount>(this._dbContext, discount);
                discount.DiscountDetailXRefs.Where(sd => sd.RecordStatus).ToList().ForEach(d =>
                {
                    if (!DbContextHelper.Instance.ValidateEnity<DiscountDetailXRef>(this._dbContext, d))
                        isValid = false;
                });

                return isValid;
            }

            return true;
        }

        public override void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties)
        {
            base.ExecuteBusinessRules(entity, actionType, properties);

            if (entity is Discount)
            {
                Discount discount = (Discount)entity;

                discount.DiscountDetailXRefs.ToList().ForEach(attr =>
                {
                    base.ExecuteBusinessRules(attr, actionType, properties);
                });
            }
        }


        public void Dispose()
        {
            this._uow.Dispose();
        }

        #region Private methods
        private ProductPricing CreateProductPricingForService(Product product, ServicePricingDetailXRef servicePricing)
        {
                    ProductPricing productPricing = new ProductPricing();
                    Service service = servicePricing.Service;
                    if (service == null)
                    {
                        service = this._dbContext.Services.FirstOrDefault(s => s.ServiceID == product.ServiceID
                            && s.RecordStatus);
                    }

                    if (servicePricing.ServicePricing == null)
                    {
                        servicePricing.ServicePricing = this._dbContext.ServicePricings.FirstOrDefault(pricing => pricing.ServicePricingID == servicePricing.ServicePricingID.Value);
                    }
                    
                    productPricing.ProductID = product.ProductID;
                    productPricing.IsServiceGroup = false;
                    productPricing.ServicePricingID = servicePricing.ServicePricingID.Value;
                    productPricing.ProductDescription = product.Name;
                    productPricing.ServiceID = service != null ? service.ServiceID : 0;
                    productPricing.ServiceDescription = service != null ? service.Description : string.Empty;
                    productPricing.ServiceAreaID = servicePricing.ServiceAreaID.HasValue ? servicePricing.ServiceAreaID.Value : 0;
                    productPricing.CategoryCodeID = servicePricing.CategoryCodeID.HasValue ? servicePricing.CategoryCodeID.Value : 0;
                    productPricing.SubscriptionCharges = servicePricing.SubscriptionCharges.HasValue ? servicePricing.SubscriptionCharges.Value : 0;
                    productPricing.OneTimeCharges = servicePricing.OneTimeCharges.HasValue ? servicePricing.OneTimeCharges.Value : 0;
                    productPricing.Installation = 0;
                    productPricing.EffectiveDate = (servicePricing.ServicePricing == null || !servicePricing.ServicePricing.EffectiveDate.HasValue) ? DateTime.MinValue : servicePricing.ServicePricing.EffectiveDate.Value;
                    return productPricing;

        }

        private ProductPricing CreateProductPricingForServiceGroup(Product product, ServiceGroupPricingDetailXRef serviceGroupPricing)
        {            
            ProductPricing productPricing = new ProductPricing();
            Service service = serviceGroupPricing.Service;
            if (service == null)
            {
                service = this._dbContext.Services.FirstOrDefault(s => s.ServiceID == serviceGroupPricing.ServiceID
                    && s.RecordStatus);
            }

            if (serviceGroupPricing.ServiceGroupPricing == null)
            {
                serviceGroupPricing.ServiceGroupPricing = this._dbContext.ServiceGroupPricings.FirstOrDefault(pricing => pricing.ServiceGroupPricingID == serviceGroupPricing.ServiceGroupPricingID.Value);
            }

            productPricing.ProductID = product.ProductID;
            productPricing.IsServiceGroup = true;
            productPricing.ServiceGroupPricingID = serviceGroupPricing.ServiceGroupPricingID.Value;
            productPricing.ProductDescription = product.Name;
            productPricing.ServiceID = service != null ? service.ServiceID : 0;
            productPricing.ServiceDescription = service != null ? service.Description : string.Empty;
            productPricing.ServiceAreaID = serviceGroupPricing.ServiceAreaID.HasValue ? serviceGroupPricing.ServiceAreaID.Value : 0;
            productPricing.CategoryCodeID = serviceGroupPricing.CategoryCodeID.HasValue ? serviceGroupPricing.CategoryCodeID.Value : 0;
            productPricing.SubscriptionCharges = serviceGroupPricing.SubscriptionCharges.HasValue ? serviceGroupPricing.SubscriptionCharges.Value : 0;
            productPricing.OneTimeCharges = serviceGroupPricing.OneTimeCharges.HasValue ? serviceGroupPricing.OneTimeCharges.Value : 0;
            productPricing.Installation = 0;
            productPricing.EffectiveDate = (serviceGroupPricing.ServiceGroupPricing == null || !serviceGroupPricing.ServiceGroupPricing.EffectiveDate.HasValue) ? DateTime.MinValue : serviceGroupPricing.ServiceGroupPricing.EffectiveDate.Value;

            return productPricing;

        }

        private List<Product> GetCustomerContractActiveProducts(ProductSearchCriteriaModel searchCriteria)
        {
            List<Product> activeProducts = this._dbContext.Database
                                   .SqlQuery<Product>("GetCustomerContractActiveProducts @CustomerID"
                                    , new SqlParameter("@CustomerID", searchCriteria.productCustomerID)).ToList();

            List<Product> allProducts = this._dbContext.Database
                                   .SqlQuery<Product>("GetCustomerProducts @CustomerID, @ContractTermTypeCodeID, @IsOnlyNewProducts"
                                    , new SqlParameter("@CustomerID", searchCriteria.productCustomerID)
                                    , new SqlParameter("@ContractTermTypeCodeID", searchCriteria.productContractTermTypeCodeID)
                                    , new SqlParameter("@IsOnlyNewProducts", false)).ToList();

            if (!string.IsNullOrEmpty(searchCriteria.ProductDescription))
            {
                allProducts = allProducts.Where(p => p.Description.Contains(searchCriteria.ProductDescription)).ToList();
            }

            allProducts.ForEach(p =>
            {
                if (activeProducts.Exists(newProduct => newProduct.ProductID == p.ProductID))
                    p.HasActiveContract = true;
                else
                    p.HasActiveContract = false;
            });

            return allProducts;
        }
        #endregion

    }
}
