﻿using COBRA.Models;
using Entity.ModelHelpers;
using Entity.ProductModels;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ProductRepo
{
    public interface IServiceRepository : IRepository, IDisposable
    {
        Service CreateService();
        ServicePricing CreateServicePricing();
        ServicePricingDetailXRef CreateServicePricingDetailXRef();
        Service GetServiceByID(long serviceID);
        bool HasActiveProduct(long serviceID);
        ServicePricing GetServicePricingByID(long servicePricingID);
        void AddService(Service service);
        void UpdateService(Service service);
        ServicePricing UpdateServicePricingDetails(ServicePricing servicePricing);
        List<Service> GetServices();
        List<Service> GetServicesBySearchCriteria(ServiceSearchCriteriaModel searchCriteria);
        //void ExecuteBusinessRules(ServicePricing servicePricing, BusinessRulesActionTypeEnum actionType, params string[] properties);
        //void ExecuteBusinessRules(Service service, BusinessRulesActionTypeEnum actionType, params string[] properties);        
    }
}
