﻿using Entity.Model.CustomerModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.Model;
using COBRA.Models;
using System.Data.Entity.Infrastructure;
using System.Reflection;
using Entity.ModelHelpers;
using System.Data.Entity.Core.Objects;
using Infrastructure.Resources;
using Entity.UnitOfWork.BusinessRules;
using Infrastructure;
using System.Data.SqlClient;


namespace Entity.UnitOfWork.CustomerRepo
{
    public class CustomerRepository : BaseRepository, ICustomerRepository, IDisposable
    {
        private CustomerUnitOfWork _uow = null;
        private CustomerDbContext _dbContext = null;

        //public bool DisbaleBusinessRules { get; set; }

        public CustomerRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this._uow = (CustomerUnitOfWork)unitOfWork;            
            this._dbContext = this._uow.DbContext;
            this._dbContext.DbObjectContext.ObjectMaterialized += DbObjectContext_ObjectMaterialized;
        }

        public Customer CreateCustomer()
        {
            Customer newCustomer = CustomerFactory.Instance.CreateCustomer(this._dbContext);

            CustomerAddressXRef homeAddress = this.CreateCustomerAddressXRef();
            homeAddress.AddressTypeCodeID = CustomerAddressTypeKey.Home;            

            CustomerAddressXRef billingAddress = this.CreateCustomerAddressXRef();
            billingAddress.AddressTypeCodeID = CustomerAddressTypeKey.Billing;

            newCustomer.CustomerAddressXRefs.Add(billingAddress);
            newCustomer.CustomerAddressXRefs.Add(homeAddress);            

            //Apply security rules
            this.ExecuteBusinessRules(newCustomer, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly, "IsParent");
            return newCustomer;
        }

        public CustomerAttribute CreateCustomerAttribute()
        {
            CustomerAttribute newAttribute = CustomerFactory.Instance.CreateCustomerAttribute(this._dbContext);
            this.ExecuteBusinessRules(newAttribute, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newAttribute;
        }


        public CustomerAddressXRef CreateCustomerAddressXRef()
        {
            CustomerAddressXRef newAddressXRef = CustomerFactory.Instance.CreateCustomerAddress(this._dbContext);
            return newAddressXRef;
        }

        public CustomerBillingOptionsXRef CreateCustomerBillingOptionsXRef()
        {
            CustomerBillingOptionsXRef newBillingOptionXRef = CustomerFactory.Instance.CreateCustomerBillingOptionsXRef(this._dbContext);
            return newBillingOptionXRef;
        }

        public void AddCustomer(Customer customer)
        {
            customer.CustomerNo = this.GetCustomerNo(customer);
            List<CustomerAttribute> deletedAttributes = customer.CustomerAttributes.Where(attr => !attr.RecordStatus).ToList();
            deletedAttributes.ForEach(attr =>
                {
                    customer.CustomerAttributes.Remove(attr);
                });

            this._dbContext.Customers.Add(customer);
        }        

        public Customer UpdateCustomer(Customer customer)
        {
            string[] keysToCompareAddress = { "AddressID" };
            string[] keysToCompareAttribute = { "AttributeID" };

            Customer dbCustomer = customer.DbObjectBeforeChanged != null ? customer.DbObjectBeforeChanged : this._dbContext.Customers.FirstOrDefault(c => c.CustomerID == customer.CustomerID);
            dbCustomer.IsDirty =  DbContextHelper.Instance.UpdateProperties(dbCustomer, customer);

            if (dbCustomer.IsDirty)
            {
                this._dbContext.Entry(dbCustomer).State = EntityState.Modified;
            }
                      
            DbContextHelper.Instance.UpdateChangeStatusForEntityCollection(customer.CustomerAddressXRefs.ToList(), dbCustomer.CustomerAddressXRefs.ToList(), keysToCompareAddress, this._dbContext);
            DbContextHelper.Instance.UpdateChangeStatusForEntityCollection(customer.CustomerAttributes.ToList(), dbCustomer.CustomerAttributes.ToList(), keysToCompareAttribute, this._dbContext);

            List<CustomerBillingOptionsXRef> billingOptionXRef = dbCustomer.CustomerBillingOptionsXRefs.ToList().Where(billing => billing.RecordStatus).ToList();

            billingOptionXRef.ForEach(billing =>
            {                
                CustomerBillingOptionsXRef tempbilling = customer.CustomerBillingOptionsXRefs.Where(tmpbilling => tmpbilling.TypeCodeID == billing.TypeCodeID).FirstOrDefault();

                if (tempbilling == null)
                {
                    billing.RecordStatus = false;
                    this._dbContext.Entry(billing).State = System.Data.Entity.EntityState.Modified;
                }                
            });

            customer.CustomerBillingOptionsXRefs.ToList().ForEach(cb =>
            {
                if (!billingOptionXRef.Exists(billing => cb.TypeCodeID == billing.TypeCodeID))
                {
                    CustomerBillingOptionsXRef newBillingOptionXRef = null;
                    newBillingOptionXRef = (cb.CustomerBillingOptID > 0) ? cb : this.CreateCustomerBillingOptionsXRef();
                    newBillingOptionXRef.TypeCodeID = cb.TypeCodeID;
                    newBillingOptionXRef.CustomerID = customer.CustomerID;
                    this._dbContext.CustomerBillingOptionsXRefs.Add(newBillingOptionXRef);
                }
            });

            return dbCustomer;
        }

        public void AddCustomerAttribute(CustomerAttribute customerAttribute)
        {
            this._dbContext.CustomerAttributes.Add(customerAttribute);
        }

        public void AddCustomerAddress(CustomerAddressXRef customerAddress)
        {
            this._dbContext.CustomerAddressXRefs.Add(customerAddress);
        }

        public void AddCustomerBillingOptionsXRef(CustomerBillingOptionsXRef customerBillingOptionsXRef)
        {
            this._dbContext.CustomerBillingOptionsXRefs.Add(customerBillingOptionsXRef);
        }
       
        public int SaveChanges()
        {
            return this._dbContext.SaveChanges();            
        }

        public bool Validate(AbstractEntity entity)
        {
            Customer customer = (Customer)entity;
            bool isValid = true;

            isValid = DbContextHelper.Instance.ValidateEnity<Customer>(this._dbContext, customer); 
            customer.CustomerAttributes.Where(attribute => attribute.RecordStatus).ToList().ForEach(attribute =>
            {
                if (!DbContextHelper.Instance.ValidateEnity<CustomerAttribute>(this._dbContext, attribute))
                    isValid = false;              
            });

            customer.CustomerAddressXRefs.Where(address => address.RecordStatus).ToList().ForEach(address =>
            {
                if (!DbContextHelper.Instance.ValidateEnity<CustomerAddressXRef>(this._dbContext, address))
                    isValid = false;                  
            });
         
            return isValid;
        }

        public List<Customer> GetCustomers()
        {
            this.DisbaleBusinessRules = true;
            List<Customer> customers =  this._dbContext.Customers.ToList();
            this.DisbaleBusinessRules = false;
            return customers;
        }

        public Customer GetCustomerByID(long customerID)
        {
            return this._dbContext.Customers.Where(customer => customer.CustomerID == customerID).FirstOrDefault();
        }

        public List<Customer> GetCustomersBySearchCriteria(CustomerSearchCriteriaModel searchCriteria)
        {
            this.DisbaleBusinessRules = true;

            var query = this._dbContext.Customers.AsNoTracking().Where(customer => customer.RecordStatus);

            if (!string.IsNullOrEmpty(searchCriteria.SFirstName))
            {
                query = query.Where(customer => customer.FirstName.Contains(searchCriteria.SFirstName));
            }

            if (!string.IsNullOrEmpty(searchCriteria.SLastName))
            {
                query = query.Where(customer => customer.LastName.Contains(searchCriteria.SLastName));
            }


            if (!string.IsNullOrEmpty(searchCriteria.SMobile))
            {
                query = query.Where(customer => customer.Mobile == searchCriteria.SMobile);
            }

            if (!string.IsNullOrEmpty(searchCriteria.SEmail))
            {
                query = query.Where(customer => customer.Email.Contains(searchCriteria.SEmail));
            }

            if (!string.IsNullOrEmpty(searchCriteria.SCustomerNo))
            {
                query = query.Where(customer => customer.CustomerNo == searchCriteria.SCustomerNo);
            }

            if (!string.IsNullOrEmpty(searchCriteria.SAddress))
            {
                query = query.Join(this._dbContext.CustomerAddressXRefs.Where(a => a.AddressTypeCodeID == CustomerAddressTypeKey.Home),
                    customer => customer.CustomerID,
                    address => address.CustomerID,
                    (customer, address) => new { customer, address.AddressLine1 })
                    .Where(address => address.AddressLine1.Contains(searchCriteria.SAddress)
                    )
                    .Select(address => address.customer);
            }

            List<Customer> customers = query.ToList();
            this.DisbaleBusinessRules = false;
            return customers;
        }

        public List<CustomerBillingOptionsXRef> GetCustomerBillingOptionsXRefByCustomerID(long customerID)
        {
            return this._dbContext.CustomerBillingOptionsXRefs.Where(billingOption =>
                billingOption.CustomerID == customerID
                && billingOption.RecordStatus).ToList();

        }

        public List<Customer> GetChildCustomers(long customerID)
        {
            this.DisbaleBusinessRules = true;
            List<Customer> childCustomers = this._dbContext.Customers.Where(customer => customer.RecordStatus
                && customer.ParentCustomerID == customerID).ToList();
            this.DisbaleBusinessRules = false;
            return childCustomers;
        }

        public bool HasActiveContract(long customerID)
        {
            this.DisbaleBusinessRules = true;
            bool hasContract = false;
            using (CustomerContractDbContextHelper helper = new CustomerContractDbContextHelper())
            {
                hasContract = helper.CustomerContractRepositoryInstance.HasActiveContractForCustomer(customerID);
            }
            this.DisbaleBusinessRules = false;
            return hasContract;
        }
        
        public override void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties)
        {
            base.ExecuteBusinessRules(entity, actionType, properties);

            if(entity is Customer)
            {
                Customer customer = (Customer)entity;
                
                customer.CustomerAttributes.ToList().ForEach(attr =>
                    {
                        base.ExecuteBusinessRules(attr, actionType, properties);
                    });

                customer.CustomerAddressXRefs.ToList().ForEach(address =>
                {
                    base.ExecuteBusinessRules(address, actionType, properties);
                });                
            }
        }     

        public void Dispose()
        {
            this._uow.Dispose();
        }

        private string GetCustomerNo(Customer customer)
        {
            return "A-" + customer.CustomerID;
        }
    }
}
