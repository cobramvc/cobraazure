﻿using COBRA.Models;
using Entity.Model.CustomerModels;
using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.CustomerRepo
{
    public interface ICustomerRepository : IRepository, IDisposable
    {
        Customer CreateCustomer();
        CustomerAttribute CreateCustomerAttribute();
        CustomerAddressXRef CreateCustomerAddressXRef();
        CustomerBillingOptionsXRef CreateCustomerBillingOptionsXRef();
        void AddCustomer(Customer customer);
        Customer UpdateCustomer(Customer customer);
        void AddCustomerAttribute(CustomerAttribute customerAttribute);
        void AddCustomerAddress(CustomerAddressXRef customerAddress);
        void AddCustomerBillingOptionsXRef(CustomerBillingOptionsXRef customerBillingOptionsXRef);
        List<Customer> GetCustomers();
        List<Customer> GetChildCustomers(long customerID);
        Customer GetCustomerByID(long customerID);
        List<CustomerBillingOptionsXRef> GetCustomerBillingOptionsXRefByCustomerID(long customerID);        
        List<Customer> GetCustomersBySearchCriteria(CustomerSearchCriteriaModel searchCriteria);
        //void ExecuteBusinessRules(Customer customer, BusinessRulesActionTypeEnum actionType, params string[] properties);
        bool HasActiveContract(long customerID);        
    }
}
