﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public interface IRepository
    {
        int SaveChanges();
        bool Validate(AbstractEntity entity);
        void DisableBusinessRules(bool disable);
        void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties);
    }
}
