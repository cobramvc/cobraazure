﻿using COBRA.Models;
using Entity.ContractModels;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ContractRepo
{
    public interface ICustomerContractRepository : IRepository, IDisposable
    {
        CustomerContract CreateCustomerContract(long customerID);
        CustomerContractProductXRef CreateCustomerContractProductXRef();
        CustomerContractProductXRefStage CreateCustomerContractProductXRefStage();
        CustomerContractAdjustmentXRef CreateCustomerContractAdjustmentXRef();
        CustomerContractAdhocDiscountXRef CreateCustomerContractAdhocDiscountXRef();


        void AddCustomerContract(CustomerContract customerContract);
        void UpdateCustomerContract(CustomerContract customerContract);
        void ConfirmContract(CustomerContract customerContract);
        void AddCustomerContractAdjustmentXRef(CustomerContractAdjustmentXRef customerContractAdjustmentXRef);
        void UpdateCustomerContractAdjustmentXRef(CustomerContractAdjustmentXRef customerContractAdjustmentXRef);
        void AddCustomerContractAdhocDiscountXRef(CustomerContractAdhocDiscountXRef customerContractAdhocDiscountXRef);
        void UpdateCustomerContractAdhocDiscountXRef(CustomerContractAdhocDiscountXRef customerContractAdhocDiscountXRef);

        CustomerContract GetCustomerContractByID(long contractID);
        CustomerContractProductXRefStage GetCustomerContractProductXRefStageByID(long contractProductXRefStageID);
        CustomerContractAdhocDiscountXRef GetCustomerContractAdhocDiscountXRefByID(long customerContractAdhocDiscountXRefID);
        CustomerContractAdjustmentXRef GetCustomerContractAdjustmentXRefByID(long customerContractAdjustmentXRefID);
        //List<Product> GetProductsForContract(long customerID, int contractTermTypeCodeID, bool isNewProducts = false);
        //List<CustomerContractCart> GetCustomerContractsBySearchCriteria(ContractSearchCriteriaModel searchCriteria);
        //void LoadCostInformationCustomerContractCart(CustomerContractProductXRefCart cstomerContractProductXRefCart, long customerID);
        List<CustomerContract> GetCustomerContractsBySearchCriteria(ContractSearchCriteriaModel searchCriteria);
        void LoadContractPricingDetails(CustomerContractProductXRefStage cstomerContractProductXRefStage, long customerID);
        Customer GetCustomerByID(long customerID);
        Product GetProductByID(long productID);
        void SetDraftCustomerContractProductXRefStage(CustomerContract customerContract);             
        bool HasActiveContractForCustomer(long customerID);
        bool HasActiveContractForProduct(long productID);
        string ConfirmContractStatusChange(long customerContractID);
        string CancelContractStatusChange(long customerContractID);
        void InitiateContractStatusChange(CustomerContract customerContract);
        List<Customer> GetActiveContractChildCustomersByID(long customerID);
        List<CustomerContract> GetActiveContractByContractID(string contractID);
    }
}
