﻿using Entity.ContractModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ContractRepo
{
    public interface ICodeListDetailRepository
    {
        List<CodeListDetail> GetByListName(string codeListName);
        List<CodeListDetail> GetByCodeListID(int codeListID);
        CodeListDetail GetByCodeID(int codeID);
        List<CodeListDetail> GetByListNameParentCodeID(string codeListName, int codeID);
    }
}
