﻿using COBRA.Models;
using Entity.ContractModels;
using Entity.ModelHelpers;
using Entity.UnitOfWork.BusinessRules;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ContractRepo
{
    public class CustomerContractRepository : BaseRepository, ICustomerContractRepository
    {
        private CustomerContractUnitOfWork _uow = null;
        private ContractDbContext _dbContext = null;

        public CustomerContractRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            this._uow = (CustomerContractUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;
            this._dbContext.DbObjectContext.ObjectMaterialized += DbObjectContext_ObjectMaterialized;
        }

        #region Create Entity methods
        public CustomerContract CreateCustomerContract(long customerID)
        {
            CustomerContract customerContract = Contractfactory.Instance.CreateCustomerContract(this._dbContext);
            customerContract.CustomerID = customerID;
            this.ExecuteBusinessRules(customerContract, BusinessRulesActionTypeEnum.ObjectMaterialize, "Customer", BusinessRulesKeys.ReadOnly);
            return customerContract;
        }

        public CustomerContractProductXRef CreateCustomerContractProductXRef()
        {
            CustomerContractProductXRef customerContractProductXRef = Contractfactory.Instance.CreateCustomerContractProductXRef(this._dbContext);
            this.ExecuteBusinessRules(customerContractProductXRef, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return customerContractProductXRef;
        }

        public CustomerContractProductXRefStage CreateCustomerContractProductXRefStage()
        {
            CustomerContractProductXRefStage customerContractProductXRefStage = Contractfactory.Instance.CreateCustomerContractProductXRefStage(this._dbContext);
            this.ExecuteBusinessRules(customerContractProductXRefStage, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return customerContractProductXRefStage;
        }

        public CustomerContractAdjustmentXRef CreateCustomerContractAdjustmentXRef()
        {
            CustomerContractAdjustmentXRef customerContractAdjustmentXRef = Contractfactory.Instance.CreateCustomerContractAdjustmentXRef(this._dbContext);
            this.ExecuteBusinessRules(customerContractAdjustmentXRef, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return customerContractAdjustmentXRef;
        }

        public CustomerContractAdhocDiscountXRef CreateCustomerContractAdhocDiscountXRef()
        {
            CustomerContractAdhocDiscountXRef customerContractAdhocDiscountXRef = Contractfactory.Instance.CreateCustomerContractAdhocDiscountXRef(this._dbContext);
            this.ExecuteBusinessRules(customerContractAdhocDiscountXRef, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return customerContractAdhocDiscountXRef;
        }

        #endregion

        #region Add/Update Entities methods

        /// <summary>
        /// Add a new Contract
        /// </summary>
        /// <param name="customerContract"></param>
        public void AddCustomerContract(CustomerContract customerContract)
        {
            this.DeleteNewStageProductXRefs(customerContract);

            if (string.IsNullOrEmpty(customerContract.ContractID))
                customerContract.ContractID = "Contract-" + customerContract.CustomerContractID;

            this._dbContext.CustomerContracts.Add(customerContract);
        }

        /// <summary>
        /// Update the changes done to Contract
        /// </summary>
        /// <param name="customerContract"></param>
        public void UpdateCustomerContract(CustomerContract customerContract)
        {
            CustomerContract dbCustomerContract = (customerContract.DbObjectBeforeChanged != null) ? customerContract.DbObjectBeforeChanged : this.GetCustomerContractByID(customerContract.CustomerContractID);
            dbCustomerContract.IsDirty = DbContextHelper.Instance.UpdateProperties(dbCustomerContract, customerContract);

            // Update Contract level information only if it is not activated atleast onec
            // (i.e. new contract which has to be approved yet)
            // Otherwise Contract level information  is updated only in Stage record and
            // updates to Contract record only upon confirmation
            if (dbCustomerContract.IsDirty
                && dbCustomerContract.IsNewContract)
            {
                this._dbContext.Entry(dbCustomerContract).State = EntityState.Modified;
            }

            this.DeleteNewStageProductXRefs(customerContract);

            string[] keysToCompare = { "CustomerContractProductXRefStageID" };
            DbContextHelper.Instance.UpdateChangeStatusForEntityCollection(customerContract.DraftCustomerContractProductXRefStages.ToList(), dbCustomerContract.DraftCustomerContractProductXRefStages, keysToCompare, this._dbContext);
        }

        /// <summary>
        /// Confirm the changes in the contract (after the user click on Confirm changes upon reviewing the changes)
        /// </summary>
        /// <param name="customerContract"></param>
        public void ConfirmContract(CustomerContract customerContract)
        {
            CustomerContract dbCustomerContract = (customerContract.DbObjectBeforeChanged != null) ? customerContract.DbObjectBeforeChanged : this.GetCustomerContractByID(customerContract.CustomerContractID);

            CustomerContractProductXRef existingProductXRef = dbCustomerContract.ActiveCustomerContractProductXRef;
            CustomerContractProductXRefStage stageProduct = dbCustomerContract.ActiveDraftCustomerContractProductXRefStages.First();
            DateTime effectiveDate = stageProduct.BeginDate.Value;

            //Update ProductID and StatusCode to contract
            dbCustomerContract.StatusCodeID = stageProduct.StatusCodeID;
            dbCustomerContract.ProductID = stageProduct.ProductID;

            //Step 1 - Existing Contract - End date the existing record
            if (existingProductXRef != null)
            {
                string[] productPropertyExcludeList = { "CustomerContractProductXRefID" };
                bool isDirty = DbContextHelper.Instance.IsAnyPropertyDirty(stageProduct, existingProductXRef, productPropertyExcludeList);
                if (!isDirty)
                    return;

                existingProductXRef.SetEndDate(effectiveDate);
                this._dbContext.Entry(existingProductXRef).State = EntityState.Modified;
            }

            //Step 2 - Copy Stage record to ProductXRef table
            this.CreateContractProductFromStage(stageProduct, customerContract.CustomerContractID);

            // Step 3 - Raise Status Change request to trigger Contract is Initiated and needs to be approved
            // This is ONLY if Status is either Initiated Or Upgrade Requet
            if (dbCustomerContract.StatusCodeID == ContractProductStatusKey.Pending_Activation
                || dbCustomerContract.StatusCodeID == ContractProductStatusKey.Pending_Upgrade)
            {
                CustomerContractStatusChangeRequestXRef initiatedStatusChangeRequest = dbCustomerContract.InitiatedStatusChangeRequest;

                if (initiatedStatusChangeRequest == null)
                {
                    CustomerContractStatusChangeRequestXRef statusChangeRequestXRef = Contractfactory.Instance.CreateCustomerContractStatusChangeRequestXRef(this._dbContext);
                    statusChangeRequestXRef.CustomerContractID = customerContract.CustomerContractID;
                    statusChangeRequestXRef.StatusCodeID = stageProduct.StatusCodeID;
                    statusChangeRequestXRef.BeginDate = effectiveDate.Date;
                    this._dbContext.CustomerContractStatusChangeRequestXRefs.Add(statusChangeRequestXRef);
                }
                else
                {
                    initiatedStatusChangeRequest.BeginDate = effectiveDate;
                    this._dbContext.Entry(initiatedStatusChangeRequest).State = EntityState.Modified;
                }
            }

            //Step 4- Set IsDraft flag to false for all DRAFT Stage records
            dbCustomerContract.DraftCustomerContractProductXRefStages.ForEach(stage =>
            {
                stage.IsDraft = false;
                this._dbContext.Entry(stage).State = EntityState.Modified;
            });
        }

        /// <summary>
        /// To confirm the status which is in Pending status
        /// </summary>
        /// <param name="customerContractID"></param>
        /// <returns></returns>
        public string ConfirmContractStatusChange(long customerContractID)
        {
            CustomerContract dbCustomerContract = this.GetCustomerContractByID(customerContractID);
            CustomerContractProductXRef existingCurrentProductXRef = dbCustomerContract.CurrentActiveCustomerContractProductXRef;

            CustomerContractStatusChangeRequestXRef initiatedStatusChangeRequest = dbCustomerContract.InitiatedStatusChangeRequest;

            int statusCodeID = ContractProductStatusKey.Pending_Activation;

            if (initiatedStatusChangeRequest != null)
            {
                statusCodeID = initiatedStatusChangeRequest.StatusCodeID;
                initiatedStatusChangeRequest.RequestStatusCodeID = ContractStatusChangeRequestStatusCodeKey.Confirmed;
                this._dbContext.Entry(initiatedStatusChangeRequest).State = EntityState.Modified;
            }

            if (statusCodeID == ContractProductStatusKey.Pending_Activation)
                statusCodeID = ContractProductStatusKey.Active;
            else if (statusCodeID == ContractProductStatusKey.Pending_Disconnect)
                statusCodeID = ContractProductStatusKey.Disconnect;
            //else if (statusCodeID == ContractProductStatusKey.Pending_Reconnection)
            //    statusCodeID = ContractProductStatusKey.Active;
            else if (statusCodeID == ContractProductStatusKey.Pending_Upgrade)
            {
                statusCodeID = ContractProductStatusKey.Active;

                //This is Upgarde Request. So, the existing Contract has to be Cancelled/Closed
                CustomerContract existingContract = this.GetActiveContractByContractID(dbCustomerContract.ContractID)
                    .Where(c => c.CustomerContractID != dbCustomerContract.CustomerContractID).FirstOrDefault();

               this.CancelContract(existingContract);
            }
            else if (statusCodeID == ContractProductStatusKey.Pending_Suspend)
                statusCodeID = ContractProductStatusKey.Suspend;
            else if (statusCodeID == ContractProductStatusKey.Pending_Terminate)
                statusCodeID = ContractProductStatusKey.Terminate;
            else
                return "Errored";

            dbCustomerContract.StatusCodeID = statusCodeID;
            this._dbContext.Entry(dbCustomerContract).State = EntityState.Modified;

            // Void all future -dated products
            dbCustomerContract.FutureActiveCustomerContractProductXRefs.ForEach(product =>
            {
                product.Delete();
                this._dbContext.Entry(product).State = EntityState.Modified;
            });

            // Set Enddate of current active product and create a new product with Status that is confirmed
            if (existingCurrentProductXRef != null)
            {
                // If the contract is New Initiated
                if (existingCurrentProductXRef.StatusCodeID == ContractProductStatusKey.Pending_Activation)
                {
                    existingCurrentProductXRef.StatusCodeID = statusCodeID;
                }
                else
                {
                    string[] productPropertyExcludeList = { "CustomerContractProductXRefID" };
                    CustomerContractProductXRef newProduct = this.CreateCustomerContractProductXRef();
                    DbContextHelper.Instance.UpdateProperties(newProduct, existingCurrentProductXRef, productPropertyExcludeList);

                    //Set BeginDate and Status of new Contract Product record
                    newProduct.CustomerContractID = customerContractID;
                    newProduct.BeginDate = DateTime.Now.Date;
                    newProduct.StatusCodeID = statusCodeID;
                    this._dbContext.CustomerContractProductXRefs.Add(newProduct);

                    // End date existing Product record
                    existingCurrentProductXRef.SetEndDate(DateTime.Now.Date);
                }

                this._dbContext.Entry(existingCurrentProductXRef).State = EntityState.Modified;
            }

            // Set IsDraft flag to false for all DRAFT Stage records
            dbCustomerContract.DraftCustomerContractProductXRefStages.ForEach(stage =>
            {
                stage.IsDraft = false;
                this._dbContext.Entry(stage).State = EntityState.Modified;
            });

            int saveStatus = this._dbContext.SaveChanges();

            if (saveStatus == ApplicationKeys.SaveUnsucessful)
                return "Errored";

            return this._dbContext.CodeListDetails.FirstOrDefault(c => c.CodeID == statusCodeID).Description;
        }

        /// <summary>
        /// To confirm the status which is in Pending status
        /// </summary>
        /// <param name="customerContractID"></param>
        /// <returns></returns>
        public string CancelContractStatusChange(long customerContractID)
        {
            CustomerContractStatusChangeRequestXRef initiatedStatusChangeRequest = this._dbContext.CustomerContractStatusChangeRequestXRefs
                .FirstOrDefault(status => status.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated
                && status.CustomerContractID == customerContractID);

            int statusCodeID = ContractStatusChangeRequestStatusCodeKey.Cancelled;

            if (initiatedStatusChangeRequest != null)
            {
                initiatedStatusChangeRequest.RequestStatusCodeID = statusCodeID;
                this._dbContext.Entry(initiatedStatusChangeRequest).State = EntityState.Modified;
            }

            CustomerContract dbCustomerContract = this.GetCustomerContractByID(customerContractID);
            CustomerContractProductXRef existingProductXRef = dbCustomerContract.ActiveCustomerContractProductXRef;

            // Cancel New contract that was Initiated
            if (dbCustomerContract.StatusCodeID == ContractProductStatusKey.Pending_Activation)
            {
                statusCodeID = ContractProductStatusKey.Cancelled;
                dbCustomerContract.StatusCodeID = statusCodeID;
                this._dbContext.Entry(dbCustomerContract).State = EntityState.Modified;

                if (existingProductXRef != null)
                {
                    existingProductXRef.StatusCodeID = statusCodeID;
                    this._dbContext.Entry(existingProductXRef).State = EntityState.Modified;
                }
            }

            int saveStatus = this._dbContext.SaveChanges();

            if (saveStatus == ApplicationKeys.SaveUnsucessful)
                return "Errored";

            return this._dbContext.CodeListDetails.FirstOrDefault(c => c.CodeID == dbCustomerContract.StatusCodeID).Description;
        }

        public void InitiateContractStatusChange(CustomerContract customerContract)
        {
            int statusCodeID = customerContract.StatusCodeID.Value;

            //if (customerContract.StatusCodeID == ContractProductStatusKey.Active)
            //    statusCodeID = ContractProductStatusKey.Pending_Activation;
            //else if (customerContract.StatusCodeID == ContractProductStatusKey.Disconnect)
            //    statusCodeID = ContractProductStatusKey.Pending_Disconnect;
            //else if (customerContract.StatusCodeID == ContractProductStatusKey.Suspend)
            //    statusCodeID = ContractProductStatusKey.Pending_Suspend;
            //else if (customerContract.StatusCodeID == ContractProductStatusKey.Terminate)
            //    statusCodeID = ContractProductStatusKey.Pending_Terminate;

            // Initiate a new Status Change Request which has to be approve to reflect in Contract Product Information
            CustomerContractStatusChangeRequestXRef statusChangeRequestXRef = Contractfactory.Instance.CreateCustomerContractStatusChangeRequestXRef(this._dbContext);
            statusChangeRequestXRef.CustomerContractID = customerContract.CustomerContractID;
            statusChangeRequestXRef.StatusCodeID = statusCodeID;
            statusChangeRequestXRef.BeginDate = customerContract.viewEffectiveDate.Value.Date;

            if (statusChangeRequestXRef.StatusCodeID != ContractProductStatusKey.Active)
                statusChangeRequestXRef.StatusReasonCodeID = customerContract.viewStatusReasonCodeID;
            else
                statusChangeRequestXRef.StatusReasonComments = customerContract.viewStatusReason;

            this._dbContext.CustomerContractStatusChangeRequestXRefs.Add(statusChangeRequestXRef);
        }

        public void AddCustomerContractAdjustmentXRef(CustomerContractAdjustmentXRef customerContractAdjustmentXRef)
        {
            this._dbContext.CustomerContractAdjustmentXRefs.Add(customerContractAdjustmentXRef);
        }

        public void UpdateCustomerContractAdjustmentXRef(CustomerContractAdjustmentXRef customerContractAdjustmentXRef)
        {
            CustomerContractAdjustmentXRef dbCustomerContractAdjustmentXRef = this.GetCustomerContractAdjustmentXRefByID(customerContractAdjustmentXRef.AdjustmentsXRefID);
            dbCustomerContractAdjustmentXRef.IsDirty = DbContextHelper.Instance.UpdateProperties(dbCustomerContractAdjustmentXRef, customerContractAdjustmentXRef);

            if (dbCustomerContractAdjustmentXRef.IsDirty)
            {
                this._dbContext.Entry(dbCustomerContractAdjustmentXRef).State = EntityState.Modified;
            }
        }

        public void AddCustomerContractAdhocDiscountXRef(CustomerContractAdhocDiscountXRef customerContractAdhocDiscountXRef)
        {
            this._dbContext.CustomerContractAdhocDiscountXRefs.Add(customerContractAdhocDiscountXRef);
        }

        public void UpdateCustomerContractAdhocDiscountXRef(CustomerContractAdhocDiscountXRef customerContractAdhocDiscountXRef)
        {
            CustomerContractAdhocDiscountXRef dbCustomerContractAdhocDiscountXRef = this.GetCustomerContractAdhocDiscountXRefByID(customerContractAdhocDiscountXRef.AdhocDiscountXRefID);
            dbCustomerContractAdhocDiscountXRef.IsDirty = DbContextHelper.Instance.UpdateProperties(dbCustomerContractAdhocDiscountXRef, customerContractAdhocDiscountXRef);

            if (dbCustomerContractAdhocDiscountXRef.IsDirty)
            {
                this._dbContext.Entry(dbCustomerContractAdhocDiscountXRef).State = EntityState.Modified;
            }
        }


        public void LoadContractPricingDetails(CustomerContractProductXRefStage cstomerContractProductXRefStage, long customerID)
        {
            ProductDbContextHelper productHelper = new ProductDbContextHelper();
            List<ProductPricing> productPricingList = productHelper.ProductRepositoryInstance.GetProductPricingByCustomerIDProductID(cstomerContractProductXRefStage.ProductID, customerID);
            BusinessRulesHelper.Instance.LoadContractProductPricing(cstomerContractProductXRefStage, productPricingList);
        }
        #endregion

        #region Get methods

        public CustomerContract GetCustomerContractByID(long contractID)
        {
            return this._dbContext.CustomerContracts.FirstOrDefault(contract => contract.CustomerContractID == contractID
                && contract.RecordStatus);
        }

        public CustomerContractProductXRefStage GetCustomerContractProductXRefStageByID(long contractProductXRefStageID)
        {
            return this._dbContext.CustomerContractProductXRefStages.FirstOrDefault(stage => stage.CustomerContractProductXRefStageID == contractProductXRefStageID);
        }

        public CustomerContractAdhocDiscountXRef GetCustomerContractAdhocDiscountXRefByID(long customerContractAdhocDiscountXRefID)
        {
            return this._dbContext.CustomerContractAdhocDiscountXRefs.FirstOrDefault(adhocDiscount => adhocDiscount.AdhocDiscountXRefID == customerContractAdhocDiscountXRefID
                && adhocDiscount.RecordStatus);
        }

        public CustomerContractAdjustmentXRef GetCustomerContractAdjustmentXRefByID(long customerContractAdjustmentXRefID)
        {
            return this._dbContext.CustomerContractAdjustmentXRefs.FirstOrDefault(adjustment => adjustment.AdjustmentsXRefID == customerContractAdjustmentXRefID
                && adjustment.RecordStatus);
        }


        /// <summary>
        /// Get Child customers having non-terminated contracts
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public List<Customer> GetActiveContractChildCustomersByID(long customerID)
        {
            this.DisableBusinessRules(true);

            List<Customer> customerList = this._dbContext.Database
                                   .SqlQuery<Customer>("GetActiveContractChildCustomersByID @CustomerID"
                                    , new SqlParameter("@CustomerID", customerID)
                                    ).ToList();

            this.DisableBusinessRules(false);
            return customerList;
        }

        /// <summary>
        /// Get the list of CustomerContracts baseed on SearchCriteria
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public List<CustomerContract> GetCustomerContractsBySearchCriteria(ContractSearchCriteriaModel searchCriteria)
        {
            List<CustomerContract> searchResults = new List<CustomerContract>();
            var query = this._dbContext.CustomerContracts.Where(c => c.RecordStatus);

            if (searchCriteria == null)
            {
                return query.ToList();
            }

            if (searchCriteria.CustomerID > 0)
            {
                query = query.Where(c => c.CustomerID == searchCriteria.CustomerID);
            }

            if (!string.IsNullOrEmpty(searchCriteria.ContractID))
            {
                query = query.Where(c => c.ContractID == searchCriteria.ContractID);
            }

            if (!string.IsNullOrEmpty(searchCriteria.ActionType))
            {
                if (searchCriteria.ActionType == "Disconnect")
                {
                    query = query.Where(c =>
                    c.CustomerContractStatusChangeRequestXRefs.FirstOrDefault(status => status.RecordStatus
                    && status.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated) == null
                    && c.StatusCodeID.HasValue
                    && (c.StatusCodeID.Value == ContractProductStatusKey.Active
                    || c.StatusCodeID.Value == ContractProductStatusKey.Suspend));
                }
                else if (searchCriteria.ActionType == "Suspend")
                {
                    query = query.Where(c =>
                    c.CustomerContractStatusChangeRequestXRefs.FirstOrDefault(status => status.RecordStatus
                    && status.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated) == null
                    && c.StatusCodeID.HasValue
                    && c.StatusCodeID.Value == ContractProductStatusKey.Active);
                }
                else if (searchCriteria.ActionType == "Reconnect")
                {
                    query = query.Where(c =>
                    c.CustomerContractStatusChangeRequestXRefs.FirstOrDefault(status => status.RecordStatus
                    && status.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated) == null
                    && c.StatusCodeID.HasValue
                    && (c.StatusCodeID.Value == ContractProductStatusKey.Disconnect
                    || c.StatusCodeID.Value == ContractProductStatusKey.Suspend));
                }
                else if (searchCriteria.ActionType == "Terminate")
                {
                    query = query.Where(c =>
                    c.CustomerContractStatusChangeRequestXRefs.FirstOrDefault(status => status.RecordStatus
                    && status.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated) == null
                    && c.StatusCodeID.HasValue
                    && c.StatusCodeID.Value == ContractProductStatusKey.Disconnect);
                }
                else if (searchCriteria.ActionType == "Upgrade")
                {
                    query = query.Where(c =>
                    c.CustomerContractStatusChangeRequestXRefs.FirstOrDefault(status => status.RecordStatus
                    && status.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated) == null
                    && c.StatusCodeID.HasValue
                    && c.StatusCodeID.Value == ContractProductStatusKey.Active
                    && c.ContractTermTypeCodeID != ContractTermTypeKey.Yearly);
                }
                else if (searchCriteria.ActionType == "StatusConfirm")
                {
                    query = query.Join(this._dbContext.CustomerContractStatusChangeRequestXRefs
                        .Where(statusChange => statusChange.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated
                        && statusChange.BeginDate <= DateTime.Now
                        && (statusChange.StatusCodeID == ContractProductStatusKey.Pending_Activation
                        || statusChange.StatusCodeID == ContractProductStatusKey.Pending_Disconnect
                        //|| statusChange.StatusCodeID == ContractProductStatusKey.Pending_Reconnection
                        || statusChange.StatusCodeID == ContractProductStatusKey.Pending_Upgrade
                        || statusChange.StatusCodeID == ContractProductStatusKey.Pending_Suspend
                        || statusChange.StatusCodeID == ContractProductStatusKey.Pending_Terminate)),
                    contract => contract.CustomerContractID,
                    statusChange => statusChange.CustomerContractID,
                    (contract, statusChange) => new { contract, statusChange.StatusCodeID })
                    .Select(statusChange => statusChange.contract);

                    this.DisbaleBusinessRules = true;
                    searchResults = query.ToList();
                    this.DisbaleBusinessRules = false;


                    return searchResults;
                }
            }

            this.DisbaleBusinessRules = true;
            searchResults = query.ToList();
            this.DisbaleBusinessRules = false;
            return searchResults;
        }

        /// <summary>
        /// Get Customer by ID
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public Customer GetCustomerByID(long customerID)
        {
            return this._dbContext.Customers.FirstOrDefault(c => c.CustomerID == customerID);
        }

        ///Checks if customer has atleast one actie contract or not
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool HasActiveContractForCustomer(long customerID)
        {
            this.DisbaleBusinessRules = true;
            CustomerContract contract = this._dbContext.Database
                                   .SqlQuery<CustomerContract>("HasActiveCustomerContract @CustomerID"
                                    , new SqlParameter("@CustomerID", customerID)).FirstOrDefault();


            this.DisbaleBusinessRules = false;
            return contract != null;
        }

        ///Checks if product has atleast one actie contract or not
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool HasActiveContractForProduct(long productID)
        {
            this.DisbaleBusinessRules = true;
            CustomerContractProductXRef contractProduct = this._dbContext.CustomerContractProductXRefs
                                        .Where(cpXRef => cpXRef.RecordStatus
                                        && (!cpXRef.EndDate.HasValue || cpXRef.EndDate >= DateTime.Today.Date)
                                        && cpXRef.ProductID == productID
                                        && cpXRef.StatusCodeID != ContractProductStatusKey.Terminate).FirstOrDefault();

            this.DisbaleBusinessRules = false;
            return contractProduct != null;
        }

        public Product GetProductByID(long productID)
        {
            return this._dbContext.Products.FirstOrDefault(p => p.ProductID == productID);
        }

        public void SetDraftCustomerContractProductXRefStage(CustomerContract customerContract)
        {
            if (customerContract.ActionType == CustomerContractActionType.InstanceChange)
            {
                // If there is already draft Stage record exsts
                if (customerContract.ActiveDraftCustomerContractProductXRefStages.Count > 0)
                    return;

                CustomerContractProductXRef productXRef = customerContract.ActiveCustomerContractProductXRef;
                if (productXRef != null)
                {
                    CustomerContractProductXRefStage newProductStage = this.CreateContractProductXRefStageFromProductXRef(productXRef, customerContract.CustomerContractID);
                    this.LoadContractPricingDetails(newProductStage, customerContract.CustomerID);
                    this._dbContext.CustomerContractProductXRefStages.Add(newProductStage);
                }
            }
            else
            {
                this.DeleteDraftStageProductXRefs(customerContract);
            }

            this._dbContext.SaveChanges();
        }

        public List<CustomerContract> GetActiveContractByContractID(string contractID)
        {
            this.DisableBusinessRules(true);

            List<CustomerContract> contracts = this._dbContext.CustomerContracts.Where(contract =>
                    contract.RecordStatus
                    && contract.StatusCodeID.HasValue
                    && contract.StatusCodeID != ContractProductStatusKey.Terminate
                    && contract.ContractID == contractID).ToList();

            this.DisableBusinessRules(false);

            return contracts;
        }

        #endregion

        public override void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties)
        {
            base.ExecuteBusinessRules(entity, actionType, properties);

            if (entity is CustomerContract)
            {
                CustomerContract customerContract = (CustomerContract)entity;
                customerContract.DraftCustomerContractProductXRefStages.ToList().ForEach(productXRef =>
                {
                    this.ExecuteBusinessRules(productXRef, actionType, properties);
                });
                return;
            }
        }

        public int SaveChanges()
        {
            return this._dbContext.SaveChanges();
        }

        public bool Validate(AbstractEntity entity)
        {
            CustomerContract customerContract = (CustomerContract)entity;
            bool isValid = true;

            isValid = DbContextHelper.Instance.ValidateEnity<CustomerContract>(this._dbContext, customerContract);
            customerContract.ActiveDraftCustomerContractProductXRefStages.ToList().ForEach(productXRef =>
            {
                if (!DbContextHelper.Instance.ValidateEnity<CustomerContractProductXRefStage>(this._dbContext, productXRef))
                    isValid = false;
            });
            return isValid;
        }

        public void Dispose()
        {
            this._uow.Dispose();
        }

        private void CreateContractProductFromStage(CustomerContractProductXRefStage stageProduct, long customerContractID)
        {
            string[] productPropertyExcludeList = { "CustomerContractProductXRefID" };

            CustomerContractProductXRef newProduct = this.CreateCustomerContractProductXRef();
            newProduct.IsDirty = DbContextHelper.Instance.CopyProperties(newProduct, stageProduct, productPropertyExcludeList);
            newProduct.CustomerContractID = customerContractID;
            this._dbContext.CustomerContractProductXRefs.Add(newProduct);
        }

        private CustomerContractProductXRefStage CreateContractProductXRefStageFromProductXRef(CustomerContractProductXRef productXRef, long customerContractID)
        {
            string[] productPropertyExcludeList = { "CustomerContractProductXRefStageID" };

            CustomerContractProductXRefStage newProductStage = this.CreateCustomerContractProductXRefStage();
            newProductStage.IsDirty = DbContextHelper.Instance.CopyProperties(newProductStage, productXRef, productPropertyExcludeList);
            newProductStage.CustomerContractID = customerContractID;
            return newProductStage;
        }

        private void DeleteNewStageProductXRefs(CustomerContract customerContract)
        {
            List<CustomerContractProductXRefStage> deletedStages = customerContract.DraftCustomerContractProductXRefStages.Where(attr => !attr.RecordStatus && attr.RecordVersion == 0).ToList();
            deletedStages.ForEach(stage =>
            {
                customerContract.CustomerContractProductXRefStages.Remove(stage);
            });
        }

        private void DeleteDraftStageProductXRefs(CustomerContract customerContract)
        {
            List<CustomerContractProductXRefStage> draftStages = customerContract.DraftCustomerContractProductXRefStages.Where(attr => attr.RecordStatus).ToList();
            draftStages.ForEach(stage =>
            {
                stage.RecordStatus = false;
                this._dbContext.Entry(stage).State = EntityState.Modified;
            });
        }

        private void CancelContract(CustomerContract existingContract)
        {
            if (existingContract == null)
                return;

            // Set Cancel status to Contract
            existingContract.StatusCodeID = ContractProductStatusKey.Cancelled;
            this._dbContext.Entry(existingContract).State = EntityState.Modified;

            // Set Cancel status to active product
            CustomerContractProductXRef existingProductXRef = existingContract.CurrentActiveCustomerContractProductXRef;
            if (existingProductXRef != null)
            {
                existingProductXRef.StatusCodeID = ContractProductStatusKey.Cancelled;
                existingProductXRef.SetEndDate(DateTime.Now.Date);
                this._dbContext.Entry(existingProductXRef).State = EntityState.Modified;
            }

            // Set IsDraft to false for any  Stage records in Draft mode
            existingContract.DraftCustomerContractProductXRefStages.ForEach(stage =>
            {
                stage.IsDraft = false;
                this._dbContext.Entry(stage).State = EntityState.Modified;
            });

        }
    }
}
