﻿using Entity.ContractModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ContractRepo
{
    public class CodeListDetailRepository : ICodeListDetailRepository
    {
        private CustomerContractUnitOfWork _uow = null;
        private ContractDbContext _dbContext = null;

        public CodeListDetailRepository(IUnitOfWork unitOfWork)
        {
            this._uow = (CustomerContractUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;
        }

        public DbQuery<CodeListDetail> CodeListDetailAsNoTracking
        {
            get
            {
                return this._dbContext.CodeListDetails.AsNoTracking();
            }
        }

        public List<CodeListDetail> GetByListName(string codeListName)
        {
            return CodeListDetailAsNoTracking.Where(code => code.CodeList.ListName == codeListName).ToList();
        }

        public List<CodeListDetail> GetByCodeListID(int codeListID)
        {
            return CodeListDetailAsNoTracking.Where(code => code.CodeListID == codeListID).ToList();
        }

        public CodeListDetail GetByCodeID(int codeID)
        {
            return CodeListDetailAsNoTracking.Where(code => code.CodeID == codeID).FirstOrDefault();
        }

        public List<CodeListDetail> GetByListNameParentCodeID(string codeListName, int codeID)
        {
            if (string.IsNullOrEmpty(codeListName))
                return CodeListDetailAsNoTracking.Where(code => code.ParentCodeListID == codeID).ToList();

            return CodeListDetailAsNoTracking.Where(code => code.CodeList.ListName == codeListName
                && code.ParentCodeListID == codeID).ToList();
        }
    }
}
