﻿using COBRA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ReportsRepo
{
    public interface IReportRepository
    {
        List<ReportItem<string, decimal>> CategoryWiseCustomerReport();
        List<ReportItem<string, decimal>> ProductWiseContractReport();
        List<ReportItem<string, decimal>> ContractStatusSplitupReport();
        List<ReportItem<string, decimal>> RegionWiseBillingReport();
    }
}
