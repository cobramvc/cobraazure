﻿using COBRA.Models;
using Entity.Model.CustomerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.ReportsRepo
{
    public class ReportRepository :IReportRepository, IDisposable
    {
        private CustomerUnitOfWork _uow = null;
        private CustomerDbContext _dbContext = null;

        //public bool DisbaleBusinessRules { get; set; }

        public ReportRepository(IUnitOfWork unitOfWork) 
        {
            this._uow = (CustomerUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;            
        }


        public List<ReportItem<string, decimal>> CategoryWiseCustomerReport()
        {
            StringBuilder sqlQuery = new StringBuilder("SELECT TOP 3 cd.Description As XValue, CAST(COUNT(C.CustomerID) AS decimal) AS YValue FROM dbo.Customer C  WITH(NOLOCK) ");
            sqlQuery.Append(" INNER JOIN dbo.CodeListDetail cd WITH(NOLOCK) ON C.CategoryCodeID = cd.CodeID ");
            sqlQuery.Append(" WHERE C.RecordStatus = 1 GROUP BY cd.Description ");
            sqlQuery.Append(" ORDER BY COUNT(C.CustomerID) DESC ");

            List<ReportItem<string, decimal>> report = this._dbContext.Database
                                   .SqlQuery<ReportItem<string, decimal>>(sqlQuery.ToString()).ToList();
            return report;
        }

        public List<ReportItem<string, decimal>> ProductWiseContractReport()
        {
            StringBuilder sqlQuery = new StringBuilder("SELECT TOP 3 p.Name As XValue, CAST(COUNT(C.CustomerContractID) AS decimal) AS YValue FROM dbo.CustomerContract C  WITH(NOLOCK) ");
            sqlQuery.Append(" INNER JOIN dbo.Product p WITH(NOLOCK) ON p.ProductID = C.ProductID ");
            sqlQuery.Append(" WHERE C.RecordStatus = 1 AND p.RecordStatus = 1 AND C.StatusCodeID = 48 GROUP BY p.Name ");
            sqlQuery.Append(" ORDER BY COUNT(C.CustomerContractID) DESC ");

            List<ReportItem<string, decimal>> report = this._dbContext.Database
                                   .SqlQuery<ReportItem<string, decimal>>(sqlQuery.ToString()).ToList();
            return report;
        }

        public List<ReportItem<string, decimal>> ContractStatusSplitupReport()
        {
            StringBuilder sqlQuery = new StringBuilder("SELECT TOP 3 cd.Description As XValue, CAST(COUNT(C.CustomerContractID) AS decimal) AS YValue FROM dbo.CustomerContract C  WITH(NOLOCK) ");
            sqlQuery.Append(" INNER JOIN dbo.CodeListDetail cd WITH(NOLOCK) ON C.StatusCodeID = cd.CodeID ");
            sqlQuery.Append(" WHERE C.RecordStatus = 1 AND C.StatusCodeID IS NOT NULL GROUP BY cd.Description ");
            sqlQuery.Append(" ORDER BY COUNT(C.CustomerContractID) DESC ");

            List<ReportItem<string, decimal>> report = this._dbContext.Database
                                   .SqlQuery<ReportItem<string, decimal>>(sqlQuery.ToString()).ToList();
            return report;
        }

        public List<ReportItem<string, decimal>> RegionWiseBillingReport()
        {
            List<ReportItem<string, decimal>> report = this._dbContext.Database
                                   .SqlQuery<ReportItem<string, decimal>>("GetRegionWiseBillingReport").ToList();
            return report;
        }
        public void Dispose()
        {
            this._uow.Dispose();
        }
    }
}
