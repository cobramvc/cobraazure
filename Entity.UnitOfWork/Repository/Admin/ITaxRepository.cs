﻿using Entity.AdminModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.AdminRepo
{
    public interface ITaxRepository : IRepository, IDisposable
    {
        Tax CreateTax();
        void AddTax(Tax tax);
        Tax UpdateTax(Tax tax);
        Tax GetTaxByID(int taxID);
        List<Tax> GetTaxesByTypeCodeIDStateCodeID(int typeCodeID, int stateCodeID);
        List<Tax> GetTaxes();
    }
}
