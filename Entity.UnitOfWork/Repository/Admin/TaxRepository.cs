﻿using Entity.AdminModels;
using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.AdminRepo
{
    public class TaxRepository : BaseRepository, ITaxRepository, IDisposable
    {
        private AdminUnitOfWork _uow = null;
        private AdminDbContext _dbContext = null;

        //public bool DisbaleBusinessRules { get; set; }

        public TaxRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this._uow = (AdminUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;
            this._dbContext.DbObjectContext.ObjectMaterialized += DbObjectContext_ObjectMaterialized;
        }

        public Tax CreateTax()
        {
            Tax newTax = TaxFactory.Instance.CreateTax(this._dbContext);
            newTax.EffectiveDate = DateTime.Today;            

            //Apply security rules
            this.ExecuteBusinessRules(newTax, BusinessRulesActionTypeEnum.ObjectMaterialize, BusinessRulesKeys.ReadOnly);
            return newTax;
        }

        public void AddTax(Tax tax)
        {
            Tax dbTax = tax.DbObjectBeforeChanged != null ? tax.DbObjectBeforeChanged
                                    : this.GetTaxesByTypeCodeIDStateCodeID(tax.TypeCodeID, tax.StateCodeID.Value).OrderByDescending(t => t.Version).FirstOrDefault();

            if (dbTax != null)
                tax.Version = dbTax.Version + 1;

            this._dbContext.Taxes.Add(tax);
        }

        public Tax UpdateTax(Tax tax)
        {
            string[] keysToCompareAddress = { "TaxID" };            

            Tax dbTax = tax.DbObjectBeforeChanged != null ? tax.DbObjectBeforeChanged : this._dbContext.Taxes.FirstOrDefault(t => t.TaxID == tax.TaxID);
            dbTax.IsDirty = DbContextHelper.Instance.UpdateProperties(dbTax, tax);

            if (dbTax.IsDirty)
            {
                this._dbContext.Entry(dbTax).State = EntityState.Modified;
            }
        
            return dbTax;
        }

        public Tax GetTaxByID(int taxID)
        {
            return this._dbContext.Taxes.FirstOrDefault(t => t.TaxID == taxID);
        }

        public List<Tax> GetTaxesByTypeCodeIDStateCodeID(int typeCodeID, int stateCodeID)
        {
            return this._dbContext.Taxes
                .Where(t => t.TypeCodeID == typeCodeID
                && t.StateCodeID == stateCodeID).ToList();
        }

        public List<Tax> GetTaxes()
        {
            this.DisbaleBusinessRules = true;
            List<Tax> taxes = this._dbContext.Taxes.Where(t => t.RecordStatus).ToList();
            this.DisbaleBusinessRules = false;
            return taxes;
        }
        public int SaveChanges()
        {
            return this._dbContext.SaveChanges();
        }

        public bool Validate(AbstractEntity entity)
        {
            Tax tax = (Tax)entity;
            bool isValid = true;

            isValid = DbContextHelper.Instance.ValidateEnity<Tax>(this._dbContext, tax);
            
            return isValid;
        }

        public void Dispose()
        {
            this._uow.Dispose();
        }


    }
}
