﻿using Entity.ModelHelpers;
using Entity.UnitOfWork.BusinessRules;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public class BaseRepository
    {
        //protected IUnitOfWork _unitOfWork = null;

        protected bool DisbaleBusinessRules { get; set; }

        public BaseRepository(IUnitOfWork uow)
        {
            
        }

        public void DisableBusinessRules(bool disable)
        {
            this.DisbaleBusinessRules = disable;
        }

        public virtual void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties)
        {
            if (this.DisbaleBusinessRules)
                return;

            // Get BusinessRules Type
            string typeName = string.Empty;
            string baseTypeName = entity.GetType().BaseType.Name;

            if (baseTypeName.StartsWith("BaseEntity") || baseTypeName.StartsWith("AbstractEntity"))
                typeName = entity.GetType().Name;
            else
                typeName = baseTypeName;

            string businessRuleName = "Entity.UnitOfWork.BusinessRules." + typeName + "BusinessRules";
            Type businessRuleType = Type.GetType(businessRuleName);

            if (businessRuleType == null)
                return;

            if (!IsValidBusinessRules(businessRuleType))
                return;

            //Invoke Execute method of BusinessRules class to trigger business rules asssociated with the entity
            using (BaseBusinessRules rulesObj = (BaseBusinessRules)Activator.CreateInstance(businessRuleType, new object[] { entity, this }))
            {
                rulesObj.Execute(actionType, properties);
            }                        
        }


        private  bool IsValidBusinessRules(Type businessRuleType)
        {
            bool isValid = false;
            ConstructorInfo[] info = businessRuleType.GetConstructors();
            info.ToList().ForEach(c =>
            {
                ParameterInfo[] parameters = c.GetParameters();
                if (parameters.Length != 2)
                    isValid = false;
                else if (parameters[1].ParameterType.IsAssignableFrom(this.GetType()))
                    isValid = true;
            });
            return isValid;
        }

        protected void DbObjectContext_ObjectMaterialized(object sender, ObjectMaterializedEventArgs e)
        {
            if (this.DisbaleBusinessRules)
                return;

            if (e.Entity is AbstractEntity)
                this.ExecuteBusinessRules((AbstractEntity)e.Entity, BusinessRulesActionTypeEnum.ObjectMaterialize, string.Empty);
        }
    }
}
