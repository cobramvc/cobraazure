﻿using COBRA.Models;
using Entity.BillingModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BillingRepo
{
    public interface IBillingRepository : IRepository, IDisposable
    {
        CustomerBilling CreateCustomerBilling();
        CustomerBillingDetailXRef CreateCustomerBillingDetailXRef();
        CustomerBillingDiscountXRef CreateCustomerBillingDiscountXRef();
        void AddCustomerBilling(CustomerBilling customerBilling);

        List<ContractBillingDetailXRef> GetBillingDetailsByContract(long contractID, DateTime beginDate, DateTime endDate, int contractBillingTypeCodeID);
        CustomerBilling GetMostRecentCustomerBilling(long customerContractID, int typeCodeID = 0);
        CustomerBilling GetCustomerBillingByID(long customerBillingID);
        List<CustomerBilling> GetBillingBySearchCriteria(BillingSearchCriteriaModel searchCriteria);

        Service GetServiceByID(long serviceID);
        Tax GetTaxByID(int taxID);
        CodeListDetail GetCodeByID(int codeID);
        CustomerContractProductXRef GetCustomerContractProductXRefByID(long id);
        CustomerContractProductXRefStage GeCustomerContractProductXRefStageByID(long id);
        DiscountDetailXRef GetDiscountDetailXRefByID(long id);
        List<DiscountDetailXRef> GetDiscountsByContractID(long customerContractID, DateTime asOfDate);
        CustomerContract GetCustomerContractByID(long customerContractID);
    }
}
