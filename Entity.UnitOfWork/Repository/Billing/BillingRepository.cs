﻿using COBRA.Models;
using Entity.BillingModels;
using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infrastructure;

namespace Entity.UnitOfWork.BillingRepo
{
    public class BillingRepository : BaseRepository, IBillingRepository
    {
        private BillingUnitOfWork _uow = null;
        private BillingDbContext _dbContext = null;

        public BillingRepository(IUnitOfWork unitOfWork):base(unitOfWork)
        {
            this._uow = (BillingUnitOfWork)unitOfWork;
            this._dbContext = this._uow.DbContext;
            //this._dbContext.DbObjectContext.ObjectMaterialized += DbObjectContext_ObjectMaterialized;
        }

        #region Create Entity methods
        public CustomerBilling CreateCustomerBilling()
        {
            return BillingFactory.Instance.CreateCustomerBilling(this._dbContext);
        }

        public CustomerBillingDetailXRef CreateCustomerBillingDetailXRef()
        {
            return BillingFactory.Instance.CreateCustomerBillingDetailXRef(this._dbContext);
        }

        public CustomerBillingDiscountXRef CreateCustomerBillingDiscountXRef()
        {
            return BillingFactory.Instance.CreateCustomerBillingDiscountXRef(this._dbContext);
        }

        public List<ContractBillingDetailXRef> GetBillingDetailsByContract(long contractID, DateTime beginDate, DateTime endDate, int contractBillingTypeCodeID)
        {
            List<ContractBillingDetailXRef> contractBillingDetailXRefList = new List<ContractBillingDetailXRef>();
            
            contractBillingDetailXRefList = this._dbContext.Database
                                   .SqlQuery(typeof(ContractBillingDetailXRef), "GetBillingDetailsByContractID @CustomerContractID, @BeginDate, @EndDate, @ContractBillingTypeCodeID"
                                    , new SqlParameter("@CustomerContractID", contractID)
                                    , new SqlParameter("@BeginDate", beginDate)
                                    , new SqlParameter("@EndDate", endDate)
                                    , new SqlParameter("@ContractBillingTypeCodeID", contractBillingTypeCodeID)).Cast<ContractBillingDetailXRef>().ToList();
                        
            return contractBillingDetailXRefList;
        }

        public CustomerBilling GetMostRecentCustomerBilling(long customerContractID, int typeCodeID = 0)
        {            
            var query = this._dbContext.CustomerBillings.Where(cb =>
                                                 cb.RecordStatus
                                                 && cb.CustomerContractID == customerContractID);
            if (typeCodeID > 0)
            {
                query = query.Where(cb => cb.TypeCodeID == typeCodeID);
            }

            query = query.OrderByDescending(cb => cb.CustomerBillingID);
            return query.FirstOrDefault();            
        }

        public List<CustomerBilling> GetBillingBySearchCriteria(BillingSearchCriteriaModel searchCriteria)
        {
            var query = this._dbContext.CustomerBillings.Where(billing => billing.RecordStatus);

            if (searchCriteria.CustomerID > 0)
            {
                query = query.Where(billing => billing.CustomerID == searchCriteria.CustomerID);
            }

            if (searchCriteria.BillingTypeCodeID > 0)
            {
                query = query.Where(billing => billing.TypeCodeID == searchCriteria.BillingTypeCodeID);
            }

            return query.OrderByDescending(billing => billing.CustomerBillingID).Take(10).ToList();
        }
        #endregion

        #region Add/Update Entities methods

        public void AddCustomerBilling(CustomerBilling customerBilling)
        {
            this._dbContext.CustomerBillings.Add(customerBilling);
        }

        public CustomerBilling GetCustomerBillingByID(long customerBillingID)
        {
           return  this._dbContext.CustomerBillings.FirstOrDefault(c => c.CustomerBillingID == customerBillingID);
        }

        public Service GetServiceByID(long serviceID)
        {
            return this._dbContext.Services.FirstOrDefault(s => s.ServiceID == serviceID);
        }

        public Tax GetTaxByID(int taxID)
        {
            return this._dbContext.Taxes.FirstOrDefault(t => t.TaxID == taxID);
        }

        public CodeListDetail GetCodeByID(int codeID)
        {
            return this._dbContext.CodeListDetails.FirstOrDefault(c => c.CodeID == codeID);
        }

        public CustomerContractProductXRef GetCustomerContractProductXRefByID(long id)
        {
            return this._dbContext.CustomerContractProductXRefs.FirstOrDefault(cp => cp.CustomerContractProductXRefID == id);
        }

        public CustomerContractProductXRefStage GeCustomerContractProductXRefStageByID(long id)
        {
            return this._dbContext.CustomerContractProductXRefStages.FirstOrDefault(cp => cp.CustomerContractProductXRefStageID == id);
        }

        public CustomerContract GetCustomerContractByID(long customerContractID)
        {
            return this._dbContext.CustomerContracts.FirstOrDefault(c => c.CustomerContractID == customerContractID);
        }

        public DiscountDetailXRef GetDiscountDetailXRefByID(long id)
        {
            return this._dbContext.DiscountDetailXRefs.FirstOrDefault(d => d.DiscountDetailXRefID == id);
        }

        public List<DiscountDetailXRef> GetDiscountsByContractID(long customerContractID, DateTime asOfDate)
        {
            List<DiscountDetailXRef> discountDetails = this._dbContext.Database
                                   .SqlQuery<DiscountDetailXRef>("GetDiscountsByContract @CustomerContractID, @AsOfDate"
                                    , new SqlParameter("@CustomerContractID", customerContractID)
                                    , new SqlParameter("@AsOfDate", asOfDate)).ToList();

            return discountDetails;            
        }
        #endregion

        public int SaveChanges()
        {
            return this._dbContext.SaveChanges();
        }

        public bool Validate(AbstractEntity entity)
        {
            // No implementation as of now
            return true;
        }

        public override void ExecuteBusinessRules(AbstractEntity entity, BusinessRulesActionTypeEnum actionType, params string[] properties)
        {
            return;
        }

        public void Dispose()
        {
            this._uow.Dispose();
        }
    }
}
