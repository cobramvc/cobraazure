﻿using Entity.UnitOfWork.ContractRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork
{
    public class CustomerContractDbContextHelper : IDisposable
    {
        private ICustomerContractRepository _customerContractRepository = null;
        private IUnitOfWork _uow = null;

        public CustomerContractDbContextHelper()
        {
            _uow = new CustomerContractUnitOfWork();            
        }

        public ICustomerContractRepository CustomerContractRepositoryInstance
        {
            get
            {
                if (this._customerContractRepository == null)
                    this._customerContractRepository = new CustomerContractRepository(_uow);

                return this._customerContractRepository;
            }
        }

        public void Dispose()
        {
            if (this._customerContractRepository != null)
                this._customerContractRepository.Dispose();           
        }

    }
}
