﻿using Entity.UnitOfWork;
using Entity.UnitOfWork.ProductRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Entity.UnitOfWork
{
    public class ProductDbContextHelper : IDisposable
    {
        private IProductRepository _productRepositoty = null;
        private IServiceGroupRepository _serviceGroupRepositoty = null;
        private IServiceRepository _serviceRepositoty = null;    
        private IUnitOfWork _uow = null;

        public ProductDbContextHelper()
        {
            _uow = new ProductUnitOfWork();            
        }

        public IProductRepository ProductRepositoryInstance
        {
            get
            {
                if (this._productRepositoty == null)
                    this._productRepositoty = new ProductRepository(_uow);

                return this._productRepositoty;
            }
        }

        public IServiceGroupRepository ServiceGroupRepositoryInstance
        {
            get
            {
                if (this._serviceGroupRepositoty == null)
                    this._serviceGroupRepositoty = new ServiceGroupRepository(_uow);

                return this._serviceGroupRepositoty;
            }
        }

        public IServiceRepository ServiceRepositoryInstance
        {
            get
            {
                if (this._serviceRepositoty == null)
                    this._serviceRepositoty = new ServiceRepository(_uow);

                return this._serviceRepositoty;
            }
        }

        public void Dispose()
        {
            if (this._productRepositoty != null)
                this._productRepositoty.Dispose();
            if (this._serviceGroupRepositoty != null)
                this._serviceGroupRepositoty.Dispose();
            if (this._serviceRepositoty != null)
                this._serviceRepositoty.Dispose();
        }
    }
}