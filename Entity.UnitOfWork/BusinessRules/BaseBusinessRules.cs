﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public abstract class BaseBusinessRules : IDisposable
    {
        public void Execute(BusinessRulesActionTypeEnum actionTypeEnum, params string[] propertyName)
        {            
            Type rulesType = this.GetType();
            List<MethodInfo> methods = rulesType.GetMethods().ToList();
            bool isAllowedActionType = false;

            methods.ForEach(method =>
            {
                List<BusinessRulesPropertyAttribute> attributes = method.GetCustomAttributes<BusinessRulesPropertyAttribute>(true).ToList();
                isAllowedActionType = false;

                if(actionTypeEnum == BusinessRulesActionTypeEnum.None)
                    isAllowedActionType = true;
                else
                {
                    List<BusinessRuleActionTypeAttribute> actionTypeattributes = method.GetCustomAttributes<BusinessRuleActionTypeAttribute>(true).ToList();

                    if (actionTypeattributes.Count == 0)
                        isAllowedActionType = true;                            

                    actionTypeattributes.ForEach(actionTypeAttribute =>
                        {
                            if(actionTypeAttribute.ActionType == BusinessRulesActionTypeEnum.None
                                || actionTypeAttribute.ActionType == actionTypeEnum)
                                isAllowedActionType = true;                            
                        });
                }

                if (attributes != null && attributes.Count > 0
                    && isAllowedActionType)
                {
                    if(propertyName.Length == 0)
                        method.Invoke(this, new object[] { });
                    else if(string.IsNullOrEmpty(propertyName[0]))
                        method.Invoke(this, new object[] { });
                    else if(attributes.FirstOrDefault(a => propertyName.Contains(a.PropertyName)) != null )
                        method.Invoke(this, new object[] { });
                }
            });
        }

        [BusinessRulesProperty(BusinessRulesKeys.ReadOnly)]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.None)]
        public abstract void ApplySecurityRules();
       
        public void Dispose()
        {
            
        }
    }
}
