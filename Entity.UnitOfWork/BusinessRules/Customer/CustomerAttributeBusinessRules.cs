﻿using Entity.Model.CustomerModels;
using Entity.UnitOfWork.CustomerRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerAttributeBusinessRules : BaseBusinessRules
    {
        private CustomerAttribute _customerAttribute = null;
        private ICustomerRepository _repository = null;

        public CustomerAttributeBusinessRules(CustomerAttribute customerAttribute, ICustomerRepository repository)
        {
            this._repository = repository;
            this._customerAttribute = customerAttribute;
        }

        public override void ApplySecurityRules()
        {
            this._customerAttribute.ReadOnlyProperties.Add("AttributeID");
            this._customerAttribute.ReadOnlyProperties.Add("CustomerID");
            this._customerAttribute.ReadOnlyProperties.Add("RecordStatus");
            this._customerAttribute.ReadOnlyProperties.Add("RecordVersion");
            this._customerAttribute.CanWrite = true;
        }
    }
}
