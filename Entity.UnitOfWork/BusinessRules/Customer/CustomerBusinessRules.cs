﻿using Entity.Model.CustomerModels;
using Entity.UnitOfWork.CustomerRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerBusinessRules : BaseBusinessRules
    {
        private Customer _customer = null;
        private ICustomerRepository _repository = null;

        public CustomerBusinessRules(Customer customer, ICustomerRepository repository)
        {
            this._repository = repository;
            this._customer = customer;
        }

        [BusinessRulesProperty("IsParent")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.ObjectMaterialize)]
        public void UpdateIsParent()
        {
            this._customer.IsParent = this._customer.ParentCustomerID.HasValue ? false : true;
        }

        [BusinessRulesProperty("ParentCustomer")]        
        public void UpdateParentCustomer()
        {
            if (this._customer.ParentCustomerID.HasValue
                && this._customer.ParentCustomer == null)
                this._customer.ParentCustomer = this._repository.GetCustomerByID(this._customer.ParentCustomerID.Value);
        }

        [BusinessRulesProperty("ChildCustomers")]        
        public void UpdateChildCustomers()
        {
            if (this._customer.ChildCustomers == null)
                this._customer.ChildCustomers = this._repository.GetChildCustomers(this._customer.CustomerID);
        }
                
        [BusinessRulesProperty("CustomerID")]        
        public void UpdateCustomerID()
        {
            this._customer.CustomerAttributes.ToList().ForEach(attribute =>
            {
                attribute.CustomerID = this._customer.CustomerID;
            });

            this._customer.CustomerAddressXRefs.ToList().ForEach(address =>
            {
                address.CustomerID = this._customer.CustomerID;
            });
        }

        [BusinessRulesProperty("BillingToParentCodeID")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateBillingToParentCodeID()
        {
            if (this._customer.ParentCustomerID.HasValue)
                return;

            this._customer.BillingToParentCodeID = BillingToParentKey.No;
        }

        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._customer.DbObjectBeforeChanged != null)
                return;

            this._customer.DbObjectBeforeChanged = this._repository.GetCustomerByID(this._customer.CustomerID);
        }

        public override void ApplySecurityRules()
        {
            this._customer.ReadOnlyProperties.Add("CustomerID");
            this._customer.ReadOnlyProperties.Add("CustomerNo");
            this._customer.ReadOnlyProperties.Add("RecordVersion");
            this._customer.ReadOnlyProperties.Add("RecordStatus");
            this._customer.ReadOnlyProperties.Add("IsParent");

            //If the customer is not new or already existing customer
            if (this._customer.RecordVersion > 0)
            {
                this._customer.ReadOnlyProperties.Add("CategoryCodeID");
            }

            if (this._repository.HasActiveContract(this._customer.CustomerID))
            {
                this._customer.HasActiveContract = true;
                this._customer.ReadOnlyProperties.Add("BillingToParentCodeID");
            }         
        }
    }
}
