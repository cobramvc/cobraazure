﻿using Entity.Model.CustomerModels;
using Entity.UnitOfWork.CustomerRepo;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerAddressXRefBusinessRules : BaseBusinessRules
    {
        private CustomerAddressXRef  _customerAddressXRef = null;
        private ICustomerRepository _repository = null;

        public CustomerAddressXRefBusinessRules(CustomerAddressXRef customerAddressXRef, ICustomerRepository repository)
        {
            this._repository = repository;
            this._customerAddressXRef = customerAddressXRef;
        }

        public override void ApplySecurityRules()
        {
            this._customerAddressXRef.ReadOnlyProperties.Add("AddressID");
            this._customerAddressXRef.ReadOnlyProperties.Add("AddressTypeCodeID");
            this._customerAddressXRef.ReadOnlyProperties.Add("CustomerID");
            this._customerAddressXRef.ReadOnlyProperties.Add("RecordStatus");

            this._customerAddressXRef.CanWrite = true;

            // If the customer has active contract, Home Address can not be changed
            if (this._customerAddressXRef.RecordVersion > 0
                && this._customerAddressXRef.AddressTypeCodeID == CustomerAddressTypeKey.Home
                && this._customerAddressXRef.CustomerID > 0
                && this._repository.HasActiveContract(this._customerAddressXRef.CustomerID))
            {                
                this._customerAddressXRef.ReadOnlyProperties.Add("AddressLine1");
                this._customerAddressXRef.ReadOnlyProperties.Add("AddressLine2");
                this._customerAddressXRef.ReadOnlyProperties.Add("StateCodeID");
                this._customerAddressXRef.ReadOnlyProperties.Add("StateCodeID");
                this._customerAddressXRef.ReadOnlyProperties.Add("CityCodeID");
                this._customerAddressXRef.ReadOnlyProperties.Add("RegionCodeID");
            }
        }
    }
}
