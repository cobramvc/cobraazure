﻿using COBRA.Models;
using Entity.ContractModels;
using Entity.ModelHelpers;
using Entity.UnitOfWork.ContractRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerContractBusinessRules : BaseBusinessRules
    {
        private CustomerContract _customerContract = null;
        private ICustomerContractRepository _repository = null;

        public CustomerContractBusinessRules(CustomerContract customerContract, ICustomerContractRepository repository)
        {
            this._customerContract = customerContract;
            this._repository = repository;
        }

        [BusinessRulesProperty("Customer")]
        public void LoadCustomer()
        {
            if (this._customerContract.CustomerID > 0
                && this._customerContract.Customer == null)
                this._customerContract.Customer = this._repository.GetCustomerByID(this._customerContract.CustomerID);
            
        }

        [BusinessRulesProperty("Product")]
        public void LoadProduct()
        {
            if (!this._customerContract.ProductID.HasValue)
                return;

            if (this._customerContract.ProductID.Value <= 0)
                return;

            if (this._customerContract.Product != null)
                return;
            
            this._customerContract.Product = this._repository.GetProductByID(this._customerContract.ProductID.Value);
        }

        [BusinessRulesProperty("CustomerContractID")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateCustomerContractID()
        {
            this._customerContract.DraftCustomerContractProductXRefStages.ForEach(stage =>
            {
                stage.CustomerContractID = this._customerContract.CustomerContractID;
            });
        }

        [BusinessRulesProperty("ProductPricing")]
        public void LoadContractProductPricings()
        {            
            this._customerContract.ActiveDraftCustomerContractProductXRefStages.ToList()
                .ForEach(productStage =>
                {         
                    // If Product Pricing not yet loaded
                    if(productStage.ProductPricings == null
                        || productStage.ProductPricings.Count == 0)           
                        this._repository.LoadContractPricingDetails(productStage, this._customerContract.CustomerID); 
                });
        }

        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._customerContract.DbObjectBeforeChanged != null)
                return;

            this._customerContract.DbObjectBeforeChanged = this._repository.GetCustomerContractByID(this._customerContract.CustomerContractID);
        }

        //[BusinessRulesProperty("IsDirty")]
        //[BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        //public void UpdateIsDirty()
        //{
        //    if (this._customerContract.RecordVersion == 0)
        //        return;

        //    if (this._customerContract.DbObjectBeforeChanged == null)
        //        this.UpdateDbObjectBeforeChanged();

        //    if (this._customerContract.DbObjectBeforeChanged == null)
        //        return;

        //    // Compare with database values to check if there is any change to the object
        //    string[] propertyToExclude = { };
        //    this._customerContract.IsDirty =  DbContextHelper.Instance.IsAnyPropertyDirty(this._customerContract, this._customerContract.DbObjectBeforeChanged, propertyToExclude);           
        //}

        [BusinessRulesProperty("ActionType")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateActionType()
        {
            if (this._customerContract.RecordVersion == 0)
                return;

            if (this._customerContract.DbObjectBeforeChanged == null)
                this.UpdateDbObjectBeforeChanged();

            if (this._customerContract.DbObjectBeforeChanged == null)
                return;

            int instances = 0;
            int dbInstances = 0;

            if (this._customerContract.ActiveDraftCustomerContractProductXRefStages.Count == 0)
                return;

            if (this._customerContract.DbObjectBeforeChanged.ActiveCustomerContractProductXRef == null)
                return;

            CustomerContractProductXRefStage stage = this._customerContract.ActiveDraftCustomerContractProductXRefStages.First();
            CustomerContractProductXRef productXRef = this._customerContract.DbObjectBeforeChanged.ActiveCustomerContractProductXRef;

            instances = stage.Instances.HasValue ? stage.Instances.Value : 0;
            dbInstances = productXRef.Instances.Value;

            this._customerContract.ActionType = CustomerContractActionType.NoChange;

            if (instances < dbInstances)
                this._customerContract.ActionType = CustomerContractActionType.InstanceDownGrade;

            if(instances > dbInstances)
                this._customerContract.ActionType = CustomerContractActionType.InstanceChange;

            if(stage.ProductID != productXRef.ProductID)
                this._customerContract.ActionType = CustomerContractActionType.ProductChange;

            if (stage.StatusCodeID != productXRef.StatusCodeID)
                this._customerContract.ActionType = CustomerContractActionType.StatusChange;

        }


        [BusinessRulesProperty("PricingID")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdatePricingID()
        {
            this._customerContract.ActiveDraftCustomerContractProductXRefStages
                .Where(stage => stage.BeginDate.HasValue
                && stage.ProductID > 0)
                .ToList()
                .ForEach(productStage =>
                {                    
                    // If Product Pricing not yet loaded
                    if (productStage.ProductPricings == null
                        || productStage.ProductPricings.Count == 0)
                        this.LoadContractProductPricings();

                    // Get current Pricing Information
                    ProductPricing latestPricing = productStage.ProductPricings.Where(pricing => pricing.EffectiveDate <= productStage.BeginDate.Value)
                                                .OrderByDescending(pricing => pricing.EffectiveDate)
                                                .FirstOrDefault();

                    if(latestPricing != null)
                    {
                        if (latestPricing.IsServiceGroup)
                            productStage.ServiceGroupPricingID = latestPricing.ServiceGroupPricingID;
                        else
                            productStage.ServicePricingID = latestPricing.ServicePricingID;
                        productStage.ServiceAreaID = latestPricing.ServiceAreaID;
                    }
                });
        }

        [BusinessRulesProperty("viewEffectiveDate")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateEffectiveDate()
        {
            if (this._customerContract.StatusCodeID != ContractProductStatusKey.Pending_Upgrade)
                return;            

            CustomerContract existingContract = this._repository.GetActiveContractByContractID(this._customerContract.ContractID)
                .Where(c => c.CustomerContractID != this._customerContract.CustomerContractID).FirstOrDefault();

            if(existingContract != null
                && existingContract.ActiveCustomerContractProductXRef != null)
            {
                this._customerContract.viewEffectiveDate = existingContract.ActiveCustomerContractProductXRef.BeginDate.Value;
            }
        }

        public override void ApplySecurityRules()
        {            
            this._customerContract.ReadOnlyProperties.Add("CustomerContractID");
            this._customerContract.ReadOnlyProperties.Add("ContractID");
            this._customerContract.ReadOnlyProperties.Add("CustomerID");            
            this._customerContract.ReadOnlyProperties.Add("RecordVersion");
            this._customerContract.ReadOnlyProperties.Add("RecordStatus");
            this._customerContract.ReadOnlyProperties.Add("ProductID");
            this._customerContract.ReadOnlyProperties.Add("StatusCodeID");

            this._customerContract.CanWrite = true;

            if (this._customerContract.CustomerContractProductXRefs.Count > 0)
            {
                this._customerContract.ReadOnlyProperties.Add("ContractTypeCodeID");
                this._customerContract.ReadOnlyProperties.Add("ContractTermTypeCodeID");
            }

            CustomerContractProductXRef activeProductXRef = this._customerContract.CustomerContractProductXRefs.FirstOrDefault(p => !p.EndDate.HasValue && p.RecordStatus);
            if (activeProductXRef != null
                && (activeProductXRef.StatusCodeID == ContractProductStatusKey.Terminate
                || activeProductXRef.StatusCodeID == ContractProductStatusKey.Cancelled))
            {
                this._customerContract.CanWrite = false;
            }
        }

    }
}
