﻿using Entity.ContractModels;
using Entity.UnitOfWork.ContractRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerContractAdjustmentXRefBusinessRules : BaseBusinessRules
    {
        private CustomerContractAdjustmentXRef _customerContractAdjustmentXRef = null;
        private ICustomerContractRepository _repository = null;

        public CustomerContractAdjustmentXRefBusinessRules(CustomerContractAdjustmentXRef customerContractAdjustmentXRef, ICustomerContractRepository repository)
        {
            this._customerContractAdjustmentXRef = customerContractAdjustmentXRef;
            this._repository = repository;
        }

        public override void ApplySecurityRules()
        {
            this._customerContractAdjustmentXRef.ReadOnlyProperties.Add("AdjustmentsXRefID");
            this._customerContractAdjustmentXRef.ReadOnlyProperties.Add("RecordStatus");        
        }

    }
}
