﻿using Entity.ContractModels;
using Entity.UnitOfWork.ContractRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerContractProductXRefCartBusinessRules : BaseBusinessRules
    {
        private CustomerContractProductXRefCart _customerContractProductXRefCart = null;
        private ICustomerContractRepository _repository = null;

        public CustomerContractProductXRefCartBusinessRules(CustomerContractProductXRefCart customerContractProductXRefCart, ICustomerContractRepository repository)
        {
            this._customerContractProductXRefCart = customerContractProductXRefCart;
            this._repository = repository;
        }

        [BusinessRulesProperty("Product")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateProduct()
        {
            if(!this._customerContractProductXRefCart.RecordStatus)
                return;

            if (this._customerContractProductXRefCart.Product != null)
                return;

            this._customerContractProductXRefCart.Product = this._repository.GetProductByID(this._customerContractProductXRefCart.ProductID);

        }

        public override void ApplySecurityRules()
        {
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("CustomerContractProductXRefCartID");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("RecordStatus");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("ServiceGroupPricingID");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("ServicePricingID");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("ServiceAreaID");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("EndDate");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("ProductID");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("CustomerContractCartID");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("ProductDescription");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("ServiceNames");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("SubscriptionCharges");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("OneTimeCharges");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("Installation");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("Tax");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("TotalWithOutDiscount");
            this._customerContractProductXRefCart.ReadOnlyProperties.Add("CustomerContractProductXRefID");
                        
            this._customerContractProductXRefCart.CanDelete = false;
            this._customerContractProductXRefCart.CanWrite = true;

            // If Cart product is new then disable the status as it it is Initiated
            if (!this._customerContractProductXRefCart.CustomerContractProductXRefID.HasValue
                || this._customerContractProductXRefCart.CustomerContractProductXRefID.Value == 0)
            {
                this._customerContractProductXRefCart.CanDelete = true;
                this._customerContractProductXRefCart.ReadOnlyProperties.Add("StatusCodeID");
            }

            // Should not allow to edit Instances, Discount
            if (this._customerContractProductXRefCart.StatusCodeID == ContractProductStatusKey.Disconnect
                || this._customerContractProductXRefCart.StatusCodeID == ContractProductStatusKey.Suspend
                || this._customerContractProductXRefCart.StatusCodeID == ContractProductStatusKey.Terminate
                || this._customerContractProductXRefCart.StatusCodeID == ContractProductStatusKey.Cancelled)
            {                                                
                this._customerContractProductXRefCart.ReadOnlyProperties.Add("Instances");
                this._customerContractProductXRefCart.ReadOnlyProperties.Add("Discount");
            }

            // Contract is closed and should not allow to edit
            if (this._customerContractProductXRefCart.StatusCodeID == ContractProductStatusKey.Terminate
                || this._customerContractProductXRefCart.StatusCodeID == ContractProductStatusKey.Cancelled)
            {
                this._customerContractProductXRefCart.ReadOnlyProperties.Add("BeginDate");
                this._customerContractProductXRefCart.ReadOnlyProperties.Add("StatusCodeID");
                this._customerContractProductXRefCart.CanWrite = false;
            }
        }
    }
}
