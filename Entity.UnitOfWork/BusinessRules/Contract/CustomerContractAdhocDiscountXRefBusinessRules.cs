﻿using Entity.ContractModels;
using Entity.UnitOfWork.ContractRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules.Contract
{
    public class CustomerContractAdhocDiscountXRefBusinessRules : BaseBusinessRules
    {
        private CustomerContractAdhocDiscountXRef _customerContractAdhocDiscountXRef = null;
        private ICustomerContractRepository _repository = null;

        public CustomerContractAdhocDiscountXRefBusinessRules(CustomerContractAdhocDiscountXRef customerContractAdhocDiscountXRef, ICustomerContractRepository repository)
        {
            this._customerContractAdhocDiscountXRef = customerContractAdhocDiscountXRef;
            this._repository = repository;
        }

        public override void ApplySecurityRules()
        {
            this._customerContractAdhocDiscountXRef.ReadOnlyProperties.Add("AdhocDiscountXRefID");
            this._customerContractAdhocDiscountXRef.ReadOnlyProperties.Add("RecordStatus");
        }
    }
}
