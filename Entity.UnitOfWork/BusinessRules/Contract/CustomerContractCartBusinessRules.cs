﻿using COBRA.Models;
using Entity.ContractModels;
using Entity.UnitOfWork.ContractRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerContractCartBusinessRules : BaseBusinessRules
    {
        private CustomerContractCart _customerContractCart = null;
        private ICustomerContractRepository _repository = null;

        public CustomerContractCartBusinessRules(CustomerContractCart customerContractCart, ICustomerContractRepository repository)
        {
            this._customerContractCart = customerContractCart;
            this._repository = repository;
        }

        [BusinessRulesProperty("CustomerContractCartID")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateCustomerContractCartID()
        {
            this._customerContractCart.CustomerContractProductXRefCarts.ToList()
                        .ForEach(product =>
                        {
                            product.CustomerContractCartID = this._customerContractCart.CustomerContractCartID;
                        });                 
        }

        [BusinessRulesProperty("Customer")]
        public void LoadCustomer()
        {
            if (this._customerContractCart.CustomerID > 0
                && this._customerContractCart.Customer == null)
                this._customerContractCart.Customer = this._repository.GetCustomerByID(this._customerContractCart.CustomerID);
        }

        [BusinessRulesProperty("ProductPricing")]        
        public void LoadCostInformationCustomerContractCarts()
        {
            ProductDbContextHelper productDbContextHelper = new ProductDbContextHelper();
            this._customerContractCart.ActiveCustomerContractProductXRefCarts.ToList()
                .ForEach(productCart =>
                {
                    List<ProductPricing> productPricings = new List<ProductPricing>();                  
                    productCart.ProductPricings = productDbContextHelper.ProductRepositoryInstance.GetProductPricingByCustomerIDProductID(productCart.ProductID, this._customerContractCart.CustomerID);                    
                    BusinessRulesHelper.Instance.LoadCostInformationByProductPricing(productCart, productCart.ProductPricings);
                });
        }

        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._customerContractCart.DbObjectBeforeChanged != null)
                return;

            this._customerContractCart.DbObjectBeforeChanged = this._repository.GetCustomerContractCartByID(this._customerContractCart.CustomerContractCartID);
        }

        [BusinessRulesProperty("EffectiveTotalCharges")]        
        public void UpdateInstanceOrStatusChange()
        {
            List<CustomerContractProductXRefCart> cartProducts =  this._customerContractCart.CustomerContractProductXRefCarts.ToList();

            CustomerContract contract = null;
            if (this._customerContractCart.CustomerContractID.HasValue)            
                contract = this._repository.GetCustomerContractByID(this._customerContractCart.CustomerContractID.Value);

            // Inactive products
            cartProducts.Where(p => !p.RecordStatus).ToList().ForEach(p =>
                {
                    p.EffectiveTotalCharges = 0;
                });

            // Active products
            cartProducts.Where(p=> p.RecordStatus).ToList().ForEach(product =>
                {
                    //New product
                    int currentmonthDays = DateTimeHelper.Instance.GetNumberOfDaysLeftInThisMonth(product.BeginDate.Value.Date);
                    decimal currentMonthCharges = 0;
                    product.EffectiveTotalCharges = 0;

                    CustomerContractProductXRef enrolledProduct = null;

                    // Get the existing Contract Product record
                    if (product.CustomerContractProductXRefID.HasValue
                        && contract != null)
                        enrolledProduct = contract.CustomerContractProductXRefs.FirstOrDefault(p => p.CustomerContractProductXRefID == product.CustomerContractProductXRefID.Value);

                    // If there is no existing product for this contract(new product record)
                    if (enrolledProduct == null)
                    {                        
                        if (product.BeginDate.HasValue
                            && product.Instances.HasValue)
                        {
                            product.HasInstanceChanged = true;
                            currentmonthDays = DateTimeHelper.Instance.GetNumberOfDaysLeftInThisMonth(product.BeginDate.Value.Date);
                            currentMonthCharges = Math.Round((product.SubscriptionCharges / 30) * currentmonthDays, 2) * product.Instances.Value;
                            product.EffectiveTotalCharges = product.Total + currentMonthCharges;                       
                        }                        
                    }
                    else
                    {
                        product.ContractEffectiveDate = enrolledProduct.BeginDate;
                        // If there is a change in Status
                        if (enrolledProduct.StatusCodeID != product.StatusCodeID)
                        {
                            product.HasStatusChanged = true;

                            // Rollback any changes in Instances
                            if (enrolledProduct.Instances.HasValue
                            && product.Instances.HasValue
                            && enrolledProduct.Instances.Value != product.Instances.Value)
                                product.Instances = enrolledProduct.Instances;

                            // Rollback any changes in Discount
                            if (enrolledProduct.Discount.HasValue
                            && product.Discount.HasValue
                            && enrolledProduct.Discount.Value != product.Discount.Value)
                                product.Discount = enrolledProduct.Discount;
                        }

                        // If there is a change in Instances
                        if (enrolledProduct.Instances.HasValue
                            && product.Instances.HasValue
                            && enrolledProduct.Instances.Value != product.Instances.Value)
                        {
                            // set this to indicate Instance count has changed from Original contract
                            product.HasInstanceChanged = true;
                            if (enrolledProduct.Instances.Value > product.Instances.Value)
                            {
                                // decrease in Instance count
                                int instancesReduced = enrolledProduct.Instances.Value - product.Instances.Value;
                                currentMonthCharges = Math.Round((product.SubscriptionCharges / 30) * currentmonthDays, 2) * instancesReduced;
                                product.EffectiveTotalCharges = 0 - product.SubscriptionCharges - currentMonthCharges;                                  
                            }

                            if (enrolledProduct.Instances.Value < product.Instances.Value)
                            {
                                // increase in Instance count
                                int instancesAdded = product.Instances.Value - enrolledProduct.Instances.Value;
                                currentMonthCharges = Math.Round((product.SubscriptionCharges / 30) * currentmonthDays, 2) * instancesAdded;
                                product.EffectiveTotalCharges = ((product.Total) / product.Instances.Value) * instancesAdded + currentMonthCharges;                                 
                            }                             
                        }
                    }
                });           
        }

        
        public override void ApplySecurityRules()
        {
            this._customerContractCart.ReadOnlyProperties.Add("CustomerContractCartID");
            this._customerContractCart.ReadOnlyProperties.Add("CustomerContractID");
            this._customerContractCart.ReadOnlyProperties.Add("CustomerID");
            this._customerContractCart.ReadOnlyProperties.Add("IsDraft");
            this._customerContractCart.ReadOnlyProperties.Add("RecordVersion");
            this._customerContractCart.ReadOnlyProperties.Add("RecordStatus");

            this._customerContractCart.CanWrite = true; 

            if (this._customerContractCart.CustomerContractID.HasValue)
            {
                this._customerContractCart.ReadOnlyProperties.Add("ContractTypeCodeID");
                this._customerContractCart.ReadOnlyProperties.Add("ContractTermTypeCodeID");                
            }   
            
            CustomerContractProductXRefCart activeProductXRef = this._customerContractCart.ActiveCustomerContractProductXRefCarts.FirstOrDefault();
            if (activeProductXRef != null
                && (activeProductXRef.StatusCodeID == ContractProductStatusKey.Terminate
                || activeProductXRef.StatusCodeID == ContractProductStatusKey.Cancelled))
            {
                this._customerContractCart.CanWrite = false;
            }
        }
    }
}
