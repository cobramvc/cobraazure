﻿using COBRA.Models;
using Entity.ContractModels;
using Entity.UnitOfWork.ContractRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerContractProductXRefStageBusinessRules : BaseBusinessRules
    {
        private CustomerContractProductXRefStage _customerContractProductXRefStage = null;
        private ICustomerContractRepository _repository = null;

        public CustomerContractProductXRefStageBusinessRules(CustomerContractProductXRefStage customerContractProductXRefStage, ICustomerContractRepository repository)
        {
            this._customerContractProductXRefStage = customerContractProductXRefStage;
            this._repository = repository;
        }

        [BusinessRulesProperty("Product")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateProduct()
        {
            if (!this._customerContractProductXRefStage.RecordStatus)
                return;

            if (this._customerContractProductXRefStage.Product != null)
                return;

            this._customerContractProductXRefStage.Product = this._repository.GetProductByID(this._customerContractProductXRefStage.ProductID);

        }
       
        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._customerContractProductXRefStage.DbObjectBeforeChanged != null)
                return;

            this._customerContractProductXRefStage.DbObjectBeforeChanged = this._repository.GetCustomerContractProductXRefStageByID(this._customerContractProductXRefStage.CustomerContractProductXRefStageID);
        }
       

        public override void ApplySecurityRules()
        {
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("CustomerContractProductXRefStageID");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("RecordStatus");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("ServiceGroupPricingID");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("ServicePricingID");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("ServiceAreaID");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("EndDate");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("ProductID");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("CustomerContractID");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("ProductDescription");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("ServiceNames");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("StatusCodeID");
            this._customerContractProductXRefStage.ReadOnlyProperties.Add("IsDraft");

            this._customerContractProductXRefStage.CanDelete = false;
            this._customerContractProductXRefStage.CanWrite = true;

            // If Contract is not created Completely
            if (this._customerContractProductXRefStage.CustomerContract == null
               || this._customerContractProductXRefStage.CustomerContract.CustomerContractProductXRefs.Count == 0)
            {
                this._customerContractProductXRefStage.CanDelete = true;
                
            }

            // Should not allow to edit Instances, Discount
            if (this._customerContractProductXRefStage.StatusCodeID == ContractProductStatusKey.Disconnect
                || this._customerContractProductXRefStage.StatusCodeID == ContractProductStatusKey.Suspend
                || this._customerContractProductXRefStage.StatusCodeID == ContractProductStatusKey.Terminate
                || this._customerContractProductXRefStage.StatusCodeID == ContractProductStatusKey.Cancelled)
            {
                this._customerContractProductXRefStage.ReadOnlyProperties.Add("Instances");                
            }

            // Contract is closed and should not allow to edit
            if (this._customerContractProductXRefStage.StatusCodeID == ContractProductStatusKey.Terminate
                || this._customerContractProductXRefStage.StatusCodeID == ContractProductStatusKey.Cancelled)
            {
                this._customerContractProductXRefStage.ReadOnlyProperties.Add("BeginDate");                
                this._customerContractProductXRefStage.CanWrite = false;
            }
        }
    }
}
