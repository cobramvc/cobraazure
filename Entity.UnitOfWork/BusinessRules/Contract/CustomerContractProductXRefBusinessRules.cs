﻿using Entity.ContractModels;
using Entity.UnitOfWork.ContractRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class CustomerContractProductXRefBusinessRules : BaseBusinessRules
    {
        private CustomerContractProductXRef _customerContractProductXRef = null;
        private ICustomerContractRepository _repository = null;

        public CustomerContractProductXRefBusinessRules(CustomerContractProductXRef customerContractProductXRef, ICustomerContractRepository repository)
        {
            this._customerContractProductXRef = customerContractProductXRef;
            this._repository = repository;
        }

        public override void ApplySecurityRules()
        {
            this._customerContractProductXRef.CanWrite = true;
        }
    }
}
