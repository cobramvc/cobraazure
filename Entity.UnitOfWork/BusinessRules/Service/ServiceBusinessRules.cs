﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ServiceBusinessRules: BaseBusinessRules
    {
        private Service _service = null;
        private IServiceRepository _repository = null;

        public ServiceBusinessRules(Service service, IServiceRepository repository)
        {
            this._repository = repository;
            this._service = service;
        }

        public override void ApplySecurityRules()
        {
            this._service.ReadOnlyProperties.Add("ServiceID");
            this._service.ReadOnlyProperties.Add("RecordVersion");
            this._service.ReadOnlyProperties.Add("RecordStatus");
            this._service.CanWrite = true;

            this.UpdateHasActiveProduct();

            if (this._service.HasActiveProduct)
            {
                this._service.ReadOnlyProperties.Add("Name");
                this._service.ReadOnlyProperties.Add("TypeCodeID");
            }
        }
        private void UpdateHasActiveProduct()
        {
            this._service.HasActiveProduct = this._repository.HasActiveProduct(this._service.ServiceID);
        }
    }
}
