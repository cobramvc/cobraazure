﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ServicePricingDetailXRefBusinessRules : BaseBusinessRules
    {
        private ServicePricingDetailXRef _servicePricingDetailXRef = null;
        private IServiceRepository _repository = null;

        public ServicePricingDetailXRefBusinessRules(ServicePricingDetailXRef servicePricingDetailXRef, IServiceRepository repository)
        {
            this._repository = repository;
            this._servicePricingDetailXRef = servicePricingDetailXRef;
        }

        public override void ApplySecurityRules()
        {
            this._servicePricingDetailXRef.CanWrite = true;
            this._servicePricingDetailXRef.CanDelete = true;

            this._servicePricingDetailXRef.ReadOnlyProperties.Add("ServicePricingDetailXRefID");
            this._servicePricingDetailXRef.ReadOnlyProperties.Add("RecordStatus");
            this._servicePricingDetailXRef.ReadOnlyProperties.Add("RecordVersion");
            this._servicePricingDetailXRef.ReadOnlyProperties.Add("ServiceAreaID");

            // If ServicePricing can not be deleted then set CanDelete to False
            if (this._servicePricingDetailXRef.RecordVersion > 0
                && this._servicePricingDetailXRef.ServicePricing != null
                && !this._servicePricingDetailXRef.ServicePricing.CanDelete)
            {                
                this._servicePricingDetailXRef.CanDelete = false;
            }
        }    
    }
}
