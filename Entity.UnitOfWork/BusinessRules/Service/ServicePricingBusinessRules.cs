﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ServicePricingBusinessRules : BaseBusinessRules
    {
        private ServicePricing _servicePricing = null;
        private IServiceRepository _repository = null;

        public ServicePricingBusinessRules(ServicePricing servicePricing, IServiceRepository repository)
        {
            this._repository = repository;
            this._servicePricing = servicePricing;
        }

        [BusinessRulesProperty("Service")]
        public void LoadService()
        {
            if (this._servicePricing.Service == null
                && this._servicePricing.ServiceID > 0)
                this._servicePricing.Service = this._repository.GetServiceByID(this._servicePricing.ServiceID);
        }

        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._servicePricing.DbObjectBeforeChanged != null)
                return;

            this._servicePricing.DbObjectBeforeChanged = this._repository.GetServicePricingByID(this._servicePricing.ServicePricingID);
        }

        [BusinessRulesProperty("Service")]
        public void UpdateService()
        {
            if (this._servicePricing.ServiceID <= 0)
                return;

            if (this._servicePricing.Service != null)
                return;

            this._servicePricing.Service = this._repository.GetServiceByID(this._servicePricing.ServiceID);
        }

        public override void ApplySecurityRules()
        {
            this._servicePricing.ReadOnlyProperties.Add("ServicePricingID");            
            this._servicePricing.ReadOnlyProperties.Add("ServiceID");
            this._servicePricing.ReadOnlyProperties.Add("Version");            
            this._servicePricing.ReadOnlyProperties.Add("RecordVersion");
            this._servicePricing.ReadOnlyProperties.Add("RecordStatus");

            this._servicePricing.CanWrite = true;
            this._servicePricing.CanDelete = true;

            if (this._servicePricing.Service != null
                && this._servicePricing.Service.HasActiveProduct
                && this._servicePricing.RecordVersion > 0)
                this._servicePricing.CanDelete = false;
        }
    }
}
