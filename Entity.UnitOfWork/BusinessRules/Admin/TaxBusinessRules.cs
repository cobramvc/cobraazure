﻿using Entity.AdminModels;
using Entity.UnitOfWork.AdminRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class TaxBusinessRules : BaseBusinessRules
    {
        private Tax _tax = null;
        private ITaxRepository _repository = null;

        public TaxBusinessRules(Tax tax, ITaxRepository repository)
        {
            this._repository = repository;
            this._tax = tax;
        }

        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._tax.DbObjectBeforeChanged != null)
                return;

            if (!this._tax.StateCodeID.HasValue)
                return;

            this._tax.DbObjectBeforeChanged = this._repository.GetTaxesByTypeCodeIDStateCodeID(this._tax.TypeCodeID, this._tax.StateCodeID.Value).OrderByDescending(t => t.Version).FirstOrDefault();
        }

        public override void ApplySecurityRules()
        {
            this._tax.ReadOnlyProperties.Add("TaxID");
            this._tax.ReadOnlyProperties.Add("RecordStatus");
            this._tax.CanWrite = true;
        }

    }
}
