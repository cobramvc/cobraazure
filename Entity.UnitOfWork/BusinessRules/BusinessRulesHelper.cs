﻿using COBRA.Models;
using Entity.ContractModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class BusinessRulesHelper
    {
        private static BusinessRulesHelper helper = new BusinessRulesHelper();

        private BusinessRulesHelper()
        {

        }

        public static BusinessRulesHelper Instance
        {
            get
            {
                return helper;
            }
        }
      
        public void LoadContractProductPricing(CustomerContractProductXRefStage productStage, List<ProductPricing> productPricings)
        {
            if (productPricings.Count == 0)
                return;

            string serviceNames = string.Empty;
            ProductPricing firstPricing = productPricings[0];
            productStage.ProductPricings = productPricings;
            bool isServiceGroup = firstPricing.IsServiceGroup;

            List<ProductPricing> aggregateProductPricing = new List<ProductPricing>();

            if (isServiceGroup)
            {
                productPricings.ForEach(pricing =>
                {
                    ProductPricing agProductPricing = aggregateProductPricing.FirstOrDefault(agPricing => agPricing.ServiceGroupPricingID == pricing.ServiceGroupPricingID);

                    if (agProductPricing == null)
                    {
                        List<ProductPricing> pricingList = productPricings.Where(p => p.ServiceGroupPricingID == pricing.ServiceGroupPricingID).ToList();
                        agProductPricing = new ProductPricing();
                        agProductPricing.ServiceGroupPricingID = pricing.ServiceGroupPricingID;
                        agProductPricing.ServiceAreaID = pricing.ServiceAreaID;
                        agProductPricing.ProductID = pricing.ProductID;
                        agProductPricing.ProductDescription = pricing.ProductDescription;                                                
                        agProductPricing.IsServiceGroup = true;
                        agProductPricing.EffectiveDate = pricing.EffectiveDate;

                        agProductPricing.SubscriptionCharges = pricingList.Sum(p => p.SubscriptionCharges);
                        agProductPricing.OneTimeCharges = pricingList.Sum(p => p.OneTimeCharges);

                        serviceNames = string.Empty;
                        agProductPricing.ServiceDescription = pricingList.Aggregate(serviceNames, (s, p) =>
                        {
                            s += p.ServiceDescription + ";";
                            return s;
                        });

                        aggregateProductPricing.Add(agProductPricing);
                    }
                });

                firstPricing = aggregateProductPricing[0];
                productStage.ProductPricings = aggregateProductPricing;
            }
            
            productStage.ProductDescription = firstPricing.ProductDescription;
            productStage.ServiceNames = firstPricing.ServiceDescription;            
        }
    }
}
