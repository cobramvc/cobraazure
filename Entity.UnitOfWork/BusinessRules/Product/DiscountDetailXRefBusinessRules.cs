﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class DiscountDetailXRefBusinessRules : BaseBusinessRules
    {
        private DiscountDetailXRef _discountDetailXRef = null;
        private IProductRepository _repository = null;

        public DiscountDetailXRefBusinessRules(DiscountDetailXRef discountDetailXRef, IProductRepository repository)
        {
            this._repository = repository;
            this._discountDetailXRef = discountDetailXRef;
        }

        [BusinessRulesProperty("Product")]
        public void LoadProduct()
        {
            if (this._discountDetailXRef.Product == null
                && this._discountDetailXRef.ProductID > 0)
                this._discountDetailXRef.Product = this._repository.GetProductByID(this._discountDetailXRef.ProductID);
        }

        [BusinessRulesProperty("Discount1")]
        public void LoadDiscount1()
        {
            this._repository.DisableBusinessRules(true);
            if (this._discountDetailXRef.Discount1 == null
                && this._discountDetailXRef.DiscountID > 0)
                this._discountDetailXRef.Discount1 = this._repository.GetDiscountByID(this._discountDetailXRef.DiscountID);
            this._repository.DisableBusinessRules(false);
        }

        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._discountDetailXRef.DbObjectBeforeChanged != null)
                return;

            this._repository.DisableBusinessRules(true);
            this._discountDetailXRef.DbObjectBeforeChanged = this._repository.GetDiscountDetailXRefByID(this._discountDetailXRef.DiscountDetailXRefID);
            this._repository.DisableBusinessRules(false);
        }

        public override void ApplySecurityRules()
        {
            this._discountDetailXRef.ReadOnlyProperties.Add("DiscountDetailXRefID");
            this._discountDetailXRef.ReadOnlyProperties.Add("DiscountID");
            this._discountDetailXRef.ReadOnlyProperties.Add("ProductID");
            this._discountDetailXRef.ReadOnlyProperties.Add("RecordStatus");
            this._discountDetailXRef.ReadOnlyProperties.Add("RecordVersion");

            this._discountDetailXRef.CanWrite = true;
            this._discountDetailXRef.CanDelete = true;

            
            // If DiscountDetailXRef is locked becuase it is being used in Contracts
            if (this._discountDetailXRef.Discount1 == null)
            {
                this.LoadDiscount1();
            }

            if (this._discountDetailXRef.Discount1 != null)
            {
                if (this._discountDetailXRef.Discount1.LockedDiscountDetailXRefs == null)
                {
                    this._repository.DisableBusinessRules(true);
                    this._discountDetailXRef.Discount1.LockedDiscountDetailXRefs = this._repository.GetDiscountDetailXRefsLockedByContract(this._discountDetailXRef.DiscountID);
                    this._repository.DisableBusinessRules(false);
                }

                if (this._discountDetailXRef.Discount1.LockedDiscountDetailXRefs
                    .Exists(detailXRef => detailXRef.DiscountDetailXRefID == this._discountDetailXRef.DiscountDetailXRefID))
                {
                    this._discountDetailXRef.CanWrite = false;
                    this._discountDetailXRef.CanDelete = false;
                    this._discountDetailXRef.ReadOnlyProperties.Add("CustomerCategoryCodeID");
                    this._discountDetailXRef.ReadOnlyProperties.Add("ProductID");
                    this._discountDetailXRef.ReadOnlyProperties.Add("CityCodeID");
                    this._discountDetailXRef.ReadOnlyProperties.Add("BeginDate");
                    this._discountDetailXRef.ReadOnlyProperties.Add("EndDate");
                }
                else if (this._discountDetailXRef.BeginDate < DateTime.Today.Date) // If BeginDate is past-dated
                {
                    this._discountDetailXRef.CanWrite = false;                    
                    this._discountDetailXRef.ReadOnlyProperties.Add("CustomerCategoryCodeID");
                    this._discountDetailXRef.ReadOnlyProperties.Add("ProductID");
                    this._discountDetailXRef.ReadOnlyProperties.Add("CityCodeID");
                    this._discountDetailXRef.ReadOnlyProperties.Add("BeginDate");
                    this._discountDetailXRef.ReadOnlyProperties.Add("EndDate");
                }
            }                        
        }

    }
}
