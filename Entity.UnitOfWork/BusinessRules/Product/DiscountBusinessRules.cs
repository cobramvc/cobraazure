﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class DiscountBusinessRules : BaseBusinessRules
    {
        private Discount _discount = null;
        private IProductRepository _repository = null;

        public DiscountBusinessRules(Discount discount, IProductRepository repository)
        {
            this._repository = repository;
            this._discount = discount;
        }

        [BusinessRulesProperty("DiscountID")]
        public void UpdateDiscountID()
        {
            this._discount.DiscountDetailXRefs.ToList().ForEach(detail =>
            {
                detail.DiscountID = this._discount.DiscountID;
            });
        }

        [BusinessRulesProperty("LockedDiscountDetailXRefs")]
        public void SetLockedDiscountDetailXRefs()
        {
            this._repository.DisableBusinessRules(true);
            this._discount.LockedDiscountDetailXRefs = this._repository.GetDiscountDetailXRefsLockedByContract(this._discount.DiscountID);
            this._repository.DisableBusinessRules(false);
        }

        public override void ApplySecurityRules()
        {
            this._discount.ReadOnlyProperties.Add("DiscountID");
            this._discount.ReadOnlyProperties.Add("RecordStatus");
            this._discount.ReadOnlyProperties.Add("RecordVersion");

            this._discount.CanWrite = true;
        }
    }
}
