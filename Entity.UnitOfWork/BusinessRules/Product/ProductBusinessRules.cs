﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ProductBusinessRules : BaseBusinessRules
    {
        private Product _product = null;
        private IProductRepository _repository = null;

        public ProductBusinessRules(Product product, IProductRepository repository)
        {
            this._repository = repository;
            this._product = product;
        }

        [BusinessRulesProperty("HasActiveContract")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateCustomerID()
        {
            if (this._product.RecordVersion == 0)
                return;

            this._product.HasActiveContract = this._repository.HasActiveContract(this._product.ProductID);
        }

        [BusinessRulesProperty("Version")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateVersion()
        {
            if (this._product.Version > 0)
                return;

            this._product.Version = this._repository.GetVersionByProduct(this._product);
        }

        [BusinessRulesProperty("PricingMEffectiveDate")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdatePricingEffectiveDate()
        {
            if (this._product.PricingEffectiveDate.HasValue)
                return;

            this._product.PricingEffectiveDate = this._repository.GetPricingEffectiveDate(this._product);
        }
        

        public override void ApplySecurityRules()
        {
            this._product.ReadOnlyProperties.Add("ProductID");
            this._product.ReadOnlyProperties.Add("Version");
            this._product.ReadOnlyProperties.Add("RecordVersion");
            this._product.ReadOnlyProperties.Add("RecordStatus");
            this._product.CanWrite = true;

            if (this._product.RecordVersion > 0)
            {
                this._product.ReadOnlyProperties.Add("TypeCodeID");
                this._product.ReadOnlyProperties.Add("ContractTypeCodeID");
                this._product.ReadOnlyProperties.Add("BeginDate");
                this._product.ReadOnlyProperties.Add("ServiceGroupID");
                this._product.ReadOnlyProperties.Add("ServiceID");
                this._product.ReadOnlyProperties.Add("Name");
                this._product.ReadOnlyProperties.Add("Name");                
            }

            if (this._product.StatusCodeID.HasValue
                && this._product.StatusCodeID.Value == ProductStatusKey.Cancel)
            {
                this._product.ReadOnlyProperties.Add("StatusCodeID");    
                this._product.CanWrite = false;
            }
        }
    }
}
