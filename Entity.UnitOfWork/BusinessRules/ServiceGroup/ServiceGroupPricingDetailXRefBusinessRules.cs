﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ServiceGroupPricingDetailXRefBusinessRules : BaseBusinessRules
    {
        private ServiceGroupPricingDetailXRef _serviceGroupPricingDetailXRef = null;
        private IServiceGroupRepository _repository = null;
        private ICodeListDetailRepository _codeListRepository = null;

        public ServiceGroupPricingDetailXRefBusinessRules(ServiceGroupPricingDetailXRef serviceGroupPricingDetailXRef, IServiceGroupRepository repository)
        {
            this._repository = repository;            
            this._serviceGroupPricingDetailXRef = serviceGroupPricingDetailXRef;
        }
        [BusinessRulesProperty("Service")]
        public void UpdateService()
        {
            if (this._serviceGroupPricingDetailXRef.Service != null)
                return;

            this._serviceGroupPricingDetailXRef.Service = this._repository.GetServiceByID(this._serviceGroupPricingDetailXRef.ServiceID);
        }
        public override void ApplySecurityRules()
        {
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("ServiceGroupPricingDetailXRefID");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("RecordStatus");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("RecordVersion");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("ServiceID");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("ServiceAreaID");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("CategoryCodeID");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("StateCodeID");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("CityCodeID");
            this._serviceGroupPricingDetailXRef.ReadOnlyProperties.Add("RegionCodeID");
            this._serviceGroupPricingDetailXRef.CanWrite = true;
        }
    }       
}
