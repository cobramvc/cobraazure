﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ServiceGroupBusinessRules : BaseBusinessRules
    {
        private ServiceGroup _serviceGroup = null;
        private IServiceGroupRepository _repository = null;

        public ServiceGroupBusinessRules(ServiceGroup serviceGroup, IServiceGroupRepository repository)
        {
            this._repository = repository;
            this._serviceGroup = serviceGroup;
        }

        [BusinessRulesProperty("Service")]
        public void LoadServices()
        {
            this._serviceGroup.ServiceGroupDetailXRefs.ToList().ForEach(detail =>
                {
                    if(detail.Service == null)
                        detail.Service = this._repository.GetServiceByID(detail.ServiceID);
                });
        }

        [BusinessRulesProperty("HasServiceGroupPricing")]
        public void UpdateHasServiceGroupPricing()
        {
            if (this._serviceGroup.CurrentServiceGroupPricing(ContractTermTypeKey.Monthly) != null)
            {
                this._serviceGroup.HasServiceGroupPricing = true;
                return;
            }

            if (this._serviceGroup.CurrentServiceGroupPricing(ContractTermTypeKey.Quarterly) != null)
            {
                this._serviceGroup.HasServiceGroupPricing = true;
                return;
            }

            if (this._serviceGroup.CurrentServiceGroupPricing(ContractTermTypeKey.Yearly) != null)
            {
                this._serviceGroup.HasServiceGroupPricing = true;
                return;
            }
        }

        [BusinessRulesProperty("ServiceGroupID")]
        public void LoadServiceGroupID()
        {
            this._serviceGroup.ServiceGroupDetailXRefs.ToList().ForEach(detail =>
            {
                detail.ServiceGroupID = this._serviceGroup.ServiceGroupID;
            });
        }

        public override void ApplySecurityRules()
        {
            this._serviceGroup.ReadOnlyProperties.Add("ServiceGroupID");
            this._serviceGroup.ReadOnlyProperties.Add("RecordVersion");
            this._serviceGroup.ReadOnlyProperties.Add("RecordStatus");
            this._serviceGroup.CanWrite = true;

            if (this._serviceGroup.RecordVersion > 0)
            {
                this._serviceGroup.ReadOnlyProperties.Add("Name");
            }
            
        }
    }
}
