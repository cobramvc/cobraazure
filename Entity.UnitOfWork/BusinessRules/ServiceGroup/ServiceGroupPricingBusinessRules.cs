﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ServiceGroupPricingBusinessRules : BaseBusinessRules
    {
        private ServiceGroupPricing _serviceGroupPricing = null;
        private IServiceGroupRepository _repository = null;

        public ServiceGroupPricingBusinessRules(ServiceGroupPricing serviceGroupPricing, IServiceGroupRepository repository)
        {
            this._repository = repository;
            this._serviceGroupPricing = serviceGroupPricing;
        }

        [BusinessRulesProperty("DbObjectBeforeChanged")]
        [BusinessRuleActionType(BusinessRulesActionTypeEnum.HttpPost)]
        public void UpdateDbObjectBeforeChanged()
        {
            if (this._serviceGroupPricing.DbObjectBeforeChanged != null)
                return;

            this._serviceGroupPricing.DbObjectBeforeChanged = this._repository.GetServiceGroupPricingByID(this._serviceGroupPricing.ServiceGroupPricingID);
        }

        [BusinessRulesProperty("ServiceGroup")]        
        public void UpdateServiceGroup()
        {
            if (this._serviceGroupPricing.ServiceGroupID <= 0)
                return;

            if (this._serviceGroupPricing.ServiceGroup != null)
                return;

            this._serviceGroupPricing.ServiceGroup = this._repository.GetServiceGroupByID(this._serviceGroupPricing.ServiceGroupID);
        }

        public override void ApplySecurityRules()
        {
            this._serviceGroupPricing.ReadOnlyProperties.Add("ServiceGroupPricingID");
            this._serviceGroupPricing.ReadOnlyProperties.Add("ServiceGroupID");
            this._serviceGroupPricing.ReadOnlyProperties.Add("Version");            
            this._serviceGroupPricing.ReadOnlyProperties.Add("RecordVersion");            
            this._serviceGroupPricing.ReadOnlyProperties.Add("RecordStatus");
            this._serviceGroupPricing.CanWrite = true;       
        }
    }    
}
