﻿using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.UnitOfWork.BusinessRules
{
    public class ServiceGroupDetailXRefBusinessRules : BaseBusinessRules
    {
        private ServiceGroupDetailXRef _serviceGroupDetailXRef = null;
        private IServiceGroupRepository _repository = null;

        public ServiceGroupDetailXRefBusinessRules(ServiceGroupDetailXRef serviceGroupDetailXRef, IServiceGroupRepository repository)
        {
            this._repository = repository;
            this._serviceGroupDetailXRef = serviceGroupDetailXRef;
        }

        public override void ApplySecurityRules()
        {
            this._serviceGroupDetailXRef.CanWrite = true;

            this._serviceGroupDetailXRef.ReadOnlyProperties.Add("ServiceGroupDetailXRefID");
            this._serviceGroupDetailXRef.ReadOnlyProperties.Add("RecordStatus");
            this._serviceGroupDetailXRef.ReadOnlyProperties.Add("ServiceID");
            this._serviceGroupDetailXRef.ReadOnlyProperties.Add("ServiceGroupID");
            this._serviceGroupDetailXRef.ReadOnlyProperties.Add("RecordVersion");

            if (this._serviceGroupDetailXRef.ServiceGroup != null
                && this._serviceGroupDetailXRef.ServiceGroup.HasServiceGroupPricing)
            {
                this._serviceGroupDetailXRef.CanWrite = false;
            }                                
        }
    }
}
