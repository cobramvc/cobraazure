﻿using Entity.ModelHelpers;
using Entity.ProductModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class ProductControllerBase : ControllerBase<Product, CodeListDetail>
    {
        protected IProductRepository  _repositoty = null;
        protected ICodeListDetailRepository _codeListrepositoty = null;        

        protected override void InitializeRepositoryMethods()
        {
            base.codeListRepositoryMethod = this._codeListrepositoty.GetByListName;
            base.codeListByParentCodeRepositoryMethod = null; // Need to define repository method if we have cascade dropdpwn functionality similar to Customer
            base.codeListByCodeListIDRepositoryMethod = this._codeListrepositoty.GetByCodeListID;
        }

        protected override void BuildViewBagCollection(AbstractEntity entity)
        {
            this.InitializeRepositoryMethods();
            base.BuildViewBagCollection(entity);

            ViewBag.SerViceTypeCodeList = new SelectList(this._codeListrepositoty.GetByListName(CodeListKey.ServiceTypeList), "CodeID", "Description");

            if (entity == null)
                return;
            if (entity is Product)
            {
                Product product = (Product)entity;
                base.BuildSelectListViewBagCollection<Product>(product);
            }
            else if (entity is Discount)
            {                
                Discount discount = (Discount)entity;
                base.BuildSelectListViewBagCollection<Discount>(discount);
                base.BuildSelectListViewBagCollectionForChildObjects<DiscountDetailXRef>(discount.CurrentDiscountDetailXRefs);
            }
            else if (entity is DiscountDetailXRef)
            {                
                DiscountDetailXRef discountDetailXRef = (DiscountDetailXRef)entity;
                base.BuildSelectListViewBagCollection<DiscountDetailXRef>(discountDetailXRef);                
            }            
        }

        protected override void BindValidationErrors(AbstractEntity entity) 
        {
            //int errors = 0;
            //ModelState.Values.ToList().ForEach(x =>
            //{
            //    if (x.Errors.Count > 0)
            //    {
            //        errors++;
            //    }
            //});

            base.BindValidationErrors(entity);

            if (entity is Discount)
                base.BindValidationErrorsForChildObjects<DiscountDetailXRef>(((Discount)entity).DiscountDetailXRefs);
        }

        protected override bool PerformValidation(AbstractEntity entity)
        {
            bool isValid = this._repositoty.Validate(entity);

            if (!isValid)
                this.BindValidationErrors(entity);

            return isValid;
        }

        public override List<TCodeListDetail> CustomizeCodeListDetailList<TCodeListDetail>(List<TCodeListDetail> codeListDetailList, string codeListKey, int codeListDetailID)
        {            
            if (codeListKey != CodeListKey.CityList)
                return codeListDetailList;

            List<CodeListDetail> tempCodeListDetailList = codeListDetailList.Cast<CodeListDetail>().ToList();
            tempCodeListDetailList = tempCodeListDetailList
                .Union(this._codeListrepositoty.GetByCodeListID(-1))
                .OrderBy(code => code.CodeID)
                .ToList();

            return tempCodeListDetailList.Cast<TCodeListDetail>().ToList();
        }
    }
}