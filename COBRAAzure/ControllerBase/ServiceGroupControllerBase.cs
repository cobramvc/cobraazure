﻿using Entity.ModelHelpers;
using Entity.ProductModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class ServiceGroupControllerBase : ControllerBase<ServiceGroup, CodeListDetail>
    {
        protected IServiceGroupRepository _repositoty = null;
        protected ICodeListDetailRepository _codeListrepositoty = null;
        protected IServiceAreaRepository _serviceAreaRepository = null;
        
        protected override void InitializeRepositoryMethods()
        {
            base.codeListRepositoryMethod = this._codeListrepositoty.GetByListName;
            base.codeListByParentCodeRepositoryMethod = null; // Need to define repository method if we have cascade dropdpwn functionality similar to Customer
            base.codeListByCodeListIDRepositoryMethod = this._codeListrepositoty.GetByCodeListID;
        }

        protected override void BuildViewBagCollection(AbstractEntity entity)
        {
            this.InitializeRepositoryMethods();
            base.BuildViewBagCollection(entity);
            ViewBag.ServiceGroupName = string.Empty;
            ViewBag.SerViceTypeCodeList = new SelectList(this._codeListrepositoty.GetByListName(CodeListKey.ServiceTypeList), "CodeID", "Description");

            if (entity == null)
            {
                base.BuildSelectListViewBagCollection<ServiceGroupPricing>(null);
                ViewBag.ServiceCount = 0;
                return;
            }

            if (entity is ServiceGroup)
            {
                ServiceGroup serviceGroup = (ServiceGroup)entity;
                base.BuildSelectListViewBagCollection<ServiceGroup>(serviceGroup);
            }
            else if (entity is ServiceGroupPricing)
            {
                ServiceGroupPricing serviceGroupPricing = (ServiceGroupPricing)entity;
                base.BuildSelectListViewBagCollection<ServiceGroupPricing>(serviceGroupPricing);

                if (serviceGroupPricing.ServiceGroup != null)
                    ViewBag.ServiceGroupName = serviceGroupPricing.ServiceGroup.Name;
                
                string firstPricingKey = serviceGroupPricing.CurrentServiceGroupPricingDetailXRefs.Count == 0 ? string.Empty : serviceGroupPricing.CurrentServiceGroupPricingDetailXRefs[0].PricingGroupKey;
                ViewBag.ServiceCount = (firstPricingKey == string.Empty) ? 0 : serviceGroupPricing.CurrentServiceGroupPricingDetailXRefs.Count(pricing =>
                    pricing.PricingGroupKey == firstPricingKey);


                // Load CodeListDetail objects for CategoryCodeID etc
                serviceGroupPricing.ServiceGroupPricingDetailXRefs
                    .ToList()
                    .ForEach(pricingDetail =>
                    {
                        if(pricingDetail.CategoryCode == null
                            && pricingDetail.CategoryCodeID.HasValue)
                                pricingDetail.CategoryCode = this._codeListrepositoty.GetByCodeID(pricingDetail.CategoryCodeID.Value);

                        if(pricingDetail.ServiceArea == null
                            && pricingDetail.ServiceAreaID.HasValue)
                        {
                            if (this._serviceAreaRepository == null)
                                this._serviceAreaRepository = new ServiceAreaRepository(this._uow);

                            pricingDetail.ServiceArea = this._serviceAreaRepository.GetServiceAreaByID(pricingDetail.ServiceAreaID.Value);

                        }
                            
                    });

            }
        }

        protected override void BindValidationErrors(AbstractEntity entity) 
        {
            if (entity is ServiceGroup)
            {
                ServiceGroup serviceGroup = (ServiceGroup)entity;
                base.BindValidationErrors(serviceGroup);
                base.BindValidationErrorsForChildObjects(serviceGroup.ServiceGroupDetailXRefs);
            }
            else if (entity is ServiceGroupPricing)
            {
                ServiceGroupPricing serviceGroupPricing = (ServiceGroupPricing)entity;
                base.BindValidationErrors(serviceGroupPricing);
                base.BindValidationErrorsForChildObjects(serviceGroupPricing.ServiceGroupPricingDetailXRefs);
            }
        }

        protected override bool PerformValidation(AbstractEntity entity)
        {
            bool isValid = this._repositoty.Validate(entity);

            if (!isValid)
                this.BindValidationErrors(entity);

            return isValid;
        }
    }
}