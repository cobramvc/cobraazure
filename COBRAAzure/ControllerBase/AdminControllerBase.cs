﻿using Entity.AdminModels;
using Entity.ModelHelpers;
using Entity.UnitOfWork;
using Entity.UnitOfWork.AdminRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class AdminControllerBase : ControllerBase<CodeListDetail, CodeListDetail>
    {
        protected ITaxRepository _taxRepositoty = null;
        protected ICodeListDetailRepository _codeListrepositoty = null;       

        protected override void InitializeRepositoryMethods()
        {
            base.codeListRepositoryMethod = this._codeListrepositoty.GetByListName;
            base.codeListByParentCodeRepositoryMethod = this._codeListrepositoty.GetByListNameParentCodeID;
            base.codeListByCodeListIDRepositoryMethod = this._codeListrepositoty.GetByCodeListID;
        }

        protected override void BuildViewBagCollection(AbstractEntity entity)
        {
            this.InitializeRepositoryMethods();
            base.BuildViewBagCollection(entity);
           
            if (entity == null)
                return;

            if (entity is Tax)
            {
                Tax tax = (Tax)entity;
                base.BuildSelectListViewBagCollection<Tax>(tax);                                
            }            
        }

        protected override void BindValidationErrors(AbstractEntity entity)
        {
            Tax tax = (Tax)entity;
            base.BindValidationErrors(tax);         
        }

        protected override bool PerformValidation(AbstractEntity entity)
        {
            bool isValid = false;

            if (entity is Tax)
                isValid = this._taxRepositoty.Validate(entity);

            if (!isValid)
                this.BindValidationErrors(entity);

            return isValid;
        }
    }
}