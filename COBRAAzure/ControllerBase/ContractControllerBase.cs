﻿using COBRA.Models;
using Entity.ContractModels;
using Entity.ModelHelpers;
using Entity.UnitOfWork;
using Entity.UnitOfWork.ContractRepo;
using Infrastructure;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class ContractControllerBase : ControllerBase<CustomerContract, CodeListDetail>
    {
        protected ICustomerContractRepository _repositoty = null;
        protected ICodeListDetailRepository _codeListrepositoty = null;        
        
        protected override void InitializeRepositoryMethods()
        {
            base.codeListRepositoryMethod = this._codeListrepositoty.GetByListName;
            base.codeListByParentCodeRepositoryMethod = this._codeListrepositoty.GetByListNameParentCodeID;         
            base.codeListByCodeListIDRepositoryMethod = this._codeListrepositoty.GetByCodeListID;
        }

        protected override void BuildViewBagCollection(AbstractEntity entity)
        {
            this.InitializeRepositoryMethods();
            base.BuildViewBagCollection(entity);            

            ViewBag.ContractCustomerNo = string.Empty;

            if (entity == null)
                return;
        
            if (entity is CustomerContract)
            {
                CustomerContract customerContract = (CustomerContract)entity;
                base.BuildSelectListViewBagCollection<CustomerContract>(customerContract);
                base.BuildSelectListViewBagCollectionForChildObjects<CustomerContractProductXRefStage>(customerContract.ActiveDraftCustomerContractProductXRefStages);

                if (customerContract.Customer != null)
                {
                    ViewBag.ContractCustomerNo = customerContract.Customer.CustomerID.ToString();
                }
            }
            else if (entity is CustomerContractProductXRefStage)
            {
                CustomerContractProductXRefStage customerContractProductXRefStage = (CustomerContractProductXRefStage)entity;
                base.BuildSelectListViewBagCollection<CustomerContractProductXRefStage>(customerContractProductXRefStage);
            }
        }

        protected override void BindValidationErrors(AbstractEntity entity)
        {          
            CustomerContract customerContract = (CustomerContract)entity;
            base.BindValidationErrors(customerContract);
            base.BindValidationErrorsForChildObjects(customerContract.DraftCustomerContractProductXRefStages);
        }

        protected override bool PerformValidation(AbstractEntity entity)
        {
            bool isValid = this._repositoty.Validate(entity);

            if (!isValid)
                this.BindValidationErrors(entity);

            return isValid;
        }

        public override List<TCodeListDetail> CustomizeCodeListDetailList<TCodeListDetail>(List<TCodeListDetail> codeListDetailList, string codeListKey, int codeListDetailID)
        {
            if(codeListKey != CodeListKey.ContractTermTypeCodeList)
                return codeListDetailList;

            if (Session["Action"] == null
                || Session["Action"].ToString() != "Upgrade")
            {
                return codeListDetailList;
            }

            int contractTerm = Convert.ToInt32(Session["ContractTerm"]);

            List<CodeListDetail> tempCodeListDetailList = new List<CodeListDetail>();
            if (contractTerm == ContractTermTypeKey.Monthly)
            {
                tempCodeListDetailList = codeListDetailList.Cast<CodeListDetail>().ToList().Where(c =>
                c.CodeID == ContractTermTypeKey.Quarterly
                || c.CodeID == ContractTermTypeKey.Yearly).ToList();
            }
            else if (contractTerm == ContractTermTypeKey.Quarterly)
            {
                tempCodeListDetailList = codeListDetailList.Cast<CodeListDetail>().ToList().Where(c =>
                c.CodeID == ContractTermTypeKey.Yearly).ToList();
            }

            return tempCodeListDetailList.Cast<TCodeListDetail>().ToList();
        }
    }
}