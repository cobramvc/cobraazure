﻿using COBRA.MVC.Models;
using COBRAAzure;
using Entity.Model.CustomerModels;
using Entity.ModelHelpers;
using Entity.UnitOfWork;
using Grid.Mvc.Ajax.GridExtensions;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public abstract class ControllerBase<TEntity, TCodeListDetail> : PdfViewController
    {
        protected Func<string, List<TCodeListDetail>> codeListRepositoryMethod = null;
        protected Func<string, int, List<TCodeListDetail>> codeListByParentCodeRepositoryMethod = null;
        protected Func<int, List<TCodeListDetail>> codeListByCodeListIDRepositoryMethod = null;
        protected IUnitOfWork _uow = null;

        protected abstract void InitializeRepositoryMethods();
        protected abstract bool PerformValidation(AbstractEntity entity);        

        public bool IsModelValid
        {
            get;
            set;
        }

        public virtual List<TCodeListDetail> CustomizeCodeListDetailList<TCodeListDetail>(List<TCodeListDetail> codeListDetailList, string codeListKey, int codeListDetailID)
        {
            return codeListDetailList;
        }

        public User GetContextUser()
        {
            User user = new User();

            IPrincipal currentPrincipal = this.ControllerContext.HttpContext.User;
            if (currentPrincipal is CustomPrincipal)
                 user.UserID = ((CustomPrincipal)currentPrincipal).GetUserProfile().UserID;

            return user;
        }
        public ActionResult GetCascadingList(int codeID)
        {
            this.InitializeRepositoryMethods();
            JsonResult result = new JsonResult();
           
            List<TCodeListDetail> childList = codeListByParentCodeRepositoryMethod(string.Empty, codeID);

            result.Data = GetSelectListByCodeListAndDefualtEmpty(childList);
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return result;
        }

        public ActionResult GetCascadingListIncludeAll(int codeID)
        {
            this.InitializeRepositoryMethods();
            JsonResult result = new JsonResult();
            List<SelectListItem> selectList = new List<SelectListItem>();

            selectList.Add(new SelectListItem { Value = "", Text = "" });
            selectList.Add(new SelectListItem { Value = "0", Text = "---ALL---" });

            List<TCodeListDetail> childList = codeListByParentCodeRepositoryMethod(string.Empty, codeID);

            Type codeListType = typeof(TCodeListDetail);

            childList.ForEach(codeList =>
            {
                SelectListItem item = new SelectListItem();
                item.Value = codeListType.GetProperty("CodeID").GetValue(codeList).ToString();
                item.Text = codeListType.GetProperty("Description").GetValue(codeList).ToString();
                selectList.Add(item);
            });

            result.Data = new SelectList(selectList, "Value", "Text");
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return result;
        }

        protected virtual bool HasDeletedNewChildObjects<T>(ICollection<T> entityCol) where T : AbstractEntity
        {
            if (entityCol == null)
                return false;
            
            bool hasDeletedObjects = false;
            Type objType = typeof(T);
            PropertyInfo recordStatusProperty = objType.GetProperty("RecordStatus");
            PropertyInfo recordVersionProperty = objType.GetProperty("RecordVersion");

            List<T> newDeletedObjects = new List<T>();
            entityCol.ToList().ForEach(obj =>
            {
                if (!(bool)recordStatusProperty.GetValue(obj)
                    && (int)recordVersionProperty.GetValue(obj) == 0)
                {
                    //newDeletedObjects.Add(obj);
                    hasDeletedObjects = true;                    
                }
            });

            return hasDeletedObjects;
        }

        protected virtual void DeletedNewChildObjects<T>(ICollection<T> entityCol) where T : AbstractEntity
        {
            if (entityCol == null)
                return;

            Type objType = typeof(T);
            PropertyInfo recordStatusProperty = objType.GetProperty("RecordStatus");
            PropertyInfo recordVersionProperty = objType.GetProperty("RecordVersion");

            List<T> newDeletedObjects = new List<T>();
            entityCol.ToList().ForEach(obj =>
            {
                if (!(bool)recordStatusProperty.GetValue(obj)
                    && (int)recordVersionProperty.GetValue(obj) == 0)
                {
                    newDeletedObjects.Add(obj);                    
                }
            });

            newDeletedObjects.ForEach(obj =>
            {
                entityCol.Remove(obj);
            });            
        }

        protected virtual void BuildViewBagCollection(AbstractEntity entity)
        {
            ViewBag.EmptyList = new SelectList(codeListByCodeListIDRepositoryMethod(0), "CodeID", "Description");
        }

        protected virtual void BindValidationErrors(AbstractEntity entity) 
        {
            entity.ValidationErrors.ForEach(validation =>
            {
                this.ModelState.AddModelError(validation.Key, validation.Message);
            });
        }        

        protected virtual void BindValidationErrorsForChildObjects<T>(ICollection<T> childCol) where T : AbstractEntity
        {
            int index = 0;
            
            childCol.ToList().ForEach(obj =>
            {
                obj.ValidationErrors.ForEach(validation =>
                {
                    this.ModelState.AddModelError(ExpressionHelpers.Instance.GetModelPrefix(obj, index) + validation.Key, validation.Message);
                });
                
                index++;
            });
        }

        protected virtual void BuildSelectListViewBagCollectionForChildObjects<T>(ICollection<T> childCol) where T : AbstractEntity
        {           
            childCol.ToList().ForEach(obj =>
            {
                this.BuildSelectListViewBagCollection<T>(obj);
            });
        }

        protected virtual void BuildSelectListViewBagCollection<TModel>(TModel entity) where TModel : AbstractEntity
        {
            List<SelectListPropertyInfo> selectListPropertyInfoList = GenericHelpers.Instance.GetSelectListPropertyInfoList(entity);

            if (selectListPropertyInfoList.Count == 0)
                return;

            foreach (SelectListPropertyInfo property in selectListPropertyInfoList)
            {
                if (property.DependencyCodeID >= 0)
                    continue;

                string viewBagListName = GenericHelpers.Instance.GetSelectListNameByProperty(typeof(TModel).Name, property.Property.Name);

                if (!ViewData.ContainsKey(viewBagListName))
                {                    
                    int codeListValue = property.CodeID;
                    
                    List<TCodeListDetail> childList = codeListRepositoryMethod(property.CodeListKey);
                    childList = CustomizeCodeListDetailList(childList, property.CodeListKey, codeListValue);
                    SelectList newselectList = GetSelectListByCodeListAndDefualtEmpty(childList);
                    ViewData.Add(viewBagListName, newselectList);                
                }                
           }

            this.BuildSelectListViewBagByParentCode(entity);
        }

        protected virtual void ExecutePreSaveConditions<T>(AbstractEntity entity, IRepository repositoty, List<T> childCollection) where T : AbstractEntity
        {
            //Set Context User Information
            this._uow.SetCurrentUser(this.GetContextUser());

            // Execute Business rules to update the object property info irrespective if Model is valid or not 
            repositoty.ExecuteBusinessRules(entity, BusinessRulesActionTypeEnum.HttpPost, string.Empty);

            // Check if ModelState is Valid and if the model has new deleted child items
            // If either ModelState is Valid OR the model has new deleted child items
            // we can proceed with Validating the entity for futher processing
            this.IsModelValid = ModelState.IsValid || this.HasDeletedNewChildObjects(childCollection);

            if (!this.IsModelValid)
                this.BindValidationErrors(entity);

            //  if ModelState is Valid and if the model has new deleted child items
            // Execute validation rules on the entity object and bind validation errors to the entity object
            this.IsModelValid = this.IsModelValid && this.PerformValidation(entity);

            // If the model that was posted is valid as below conditions are satisfied
            // 1) ModelState is Valid OR the model has new deleted child objects
            // 2) Validation rules on the entity object are sucessful            
            // the model can  proceed to Save. So, remove any of the  deleted child objects which
            // are not yet added to the database(or new) as we dont want to save new deleted child objects
            if (IsModelValid)
            {                
                this.DeletedNewChildObjects(childCollection);
                return;
            }

            // If Model is Invalid build ViewBag collection to post back the Model
            // and display the validation errors to the user
            this.BuildViewBagCollection(entity);

        }

        protected virtual AjaxGrid<T> GetDefaultAjaxGrid<T>() where T : AbstractEntity
        {
            var entityCol = new List<T>().AsQueryable();
            var ajaxGridFactory = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
            AjaxGrid<T> grid = (AjaxGrid<T>)ajaxGridFactory.CreateAjaxGrid(entityCol, 1, false);
            return grid;
        }

        protected virtual AjaxGrid<T> GetAjaxGridByList<T>(List<T> entityCol, int page = 1, int pagePartitionSize = 0) where T : AbstractEntity
        {            
            var ajaxGridFactory = new Grid.Mvc.Ajax.GridExtensions.AjaxGridFactory();
            AjaxGrid<T> grid = null;

            if (pagePartitionSize == 0)
                grid = (AjaxGrid<T>)ajaxGridFactory.CreateAjaxGrid(entityCol.AsQueryable(), page, false);
            else
                grid = (AjaxGrid<T>)ajaxGridFactory.CreateAjaxGrid(entityCol.AsQueryable(), page, true, pagePartitionSize);

            return grid;
        }

        private void BuildSelectListViewBagByParentCode<TModel>(TModel entity)           
            where TModel : AbstractEntity
        {
            List<SelectListPropertyInfo> selectListPropertyInfoList = GenericHelpers.Instance.GetSelectListPropertyInfoList(entity);

            if (selectListPropertyInfoList.Count == 0)
                return;

            Type objType = typeof(TModel);

            foreach (SelectListPropertyInfo property in selectListPropertyInfoList)
            {
                if (property.DependencyCodeID < 0)
                    continue;
               
                string viewBagListName = GenericHelpers.Instance.GetSelectListNameByProperty(objType.Name, property.Property.Name, property.DependencyCodeID.ToString());

                if (ViewData.ContainsKey(viewBagListName))
                    continue;

                List<TCodeListDetail> childList = null;

                if (property.DependencyCodeID == 0)
                    childList = codeListByCodeListIDRepositoryMethod(-1);
                else
                    childList = codeListByParentCodeRepositoryMethod(property.CodeListKey, property.DependencyCodeID);

                SelectList newselectList = GetSelectListByCodeListAndDefualtEmpty(childList);                
                ViewData.Add(viewBagListName, newselectList);
            }
        }

        private SelectList GetSelectListByCodeListAndDefualtEmpty(List<TCodeListDetail> codeListDetails)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem { Value = "", Text = "" });

            List<TCodeListDetail> childList = null;
           
            Type codeListType = typeof(TCodeListDetail);

            codeListDetails.ForEach(codeList =>
            {
                SelectListItem item = new SelectListItem();
                item.Value = codeListType.GetProperty("CodeID").GetValue(codeList).ToString();
                item.Text = codeListType.GetProperty("Description").GetValue(codeList).ToString();
                selectList.Add(item);
            });
                        
            return new SelectList(selectList, "Value", "Text");
        }                

    }
}