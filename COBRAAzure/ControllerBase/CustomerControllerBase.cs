﻿using Entity.Model.CustomerModels;
using Entity.ModelHelpers;
using Entity.UnitOfWork;
using Entity.UnitOfWork.CustomerRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class CustomerControllerBase : ControllerBase<Customer, CodeListDetail>
    {
        protected ICustomerRepository _repositoty = null;
        protected ICodeListDetailRepository _codeListrepositoty = null;
        
        protected override void InitializeRepositoryMethods()
        {            
            base.codeListRepositoryMethod = this._codeListrepositoty.GetByListName;
            base.codeListByParentCodeRepositoryMethod = this._codeListrepositoty.GetByListNameParentCodeID;
            base.codeListByCodeListIDRepositoryMethod = this._codeListrepositoty.GetByCodeListID;
        }
      
        protected override void BuildViewBagCollection(AbstractEntity entity)
        {
            this.InitializeRepositoryMethods();
            base.BuildViewBagCollection(entity);
            ViewBag.BillingOptionTypeCodeList = new SelectList(this._codeListrepositoty.GetByListName(CodeListKey.BillingOptionsList), "CodeID", "Description");

            if (entity == null)
                return;

            if (entity is Customer)
            {
                Customer customer = (Customer)entity;
                base.BuildSelectListViewBagCollection<Customer>(customer);
                base.BuildSelectListViewBagCollectionForChildObjects(customer.CustomerAddressXRefs);
                base.BuildSelectListViewBagCollectionForChildObjects(customer.CustomerAttributes);     
            }
            else if (entity is CustomerAttribute)
            {
                CustomerAttribute attribute = (CustomerAttribute)entity;
                base.BuildSelectListViewBagCollection<CustomerAttribute>(attribute);
            }
        }
    
        protected void CreateCustomerBillingOptionsXRef(Customer customer, string[] chkBillingOptions)
        {
            if (chkBillingOptions == null)
                return;

            foreach (string typeCode in chkBillingOptions)
            {
                CustomerBillingOptionsXRef billingOptionXRef = customer.RecordVersion == 0 ? this._repositoty.CreateCustomerBillingOptionsXRef() : new CustomerBillingOptionsXRef(); 
                billingOptionXRef.TypeCodeID = Convert.ToInt32(typeCode);
                billingOptionXRef.CustomerID = customer.CustomerID;
                billingOptionXRef.RecordStatus = true;
                customer.CustomerBillingOptionsXRefs.Add(billingOptionXRef);
            }
        }

        protected override void BindValidationErrors(AbstractEntity entity) 
        {
            Customer customer = (Customer)entity;
            base.BindValidationErrors(customer);
            base.BindValidationErrorsForChildObjects(customer.CustomerAttributes);
            base.BindValidationErrorsForChildObjects(customer.CustomerAddressXRefs);           
        }

        protected override bool PerformValidation(AbstractEntity entity)
        {            
            bool isValid = this._repositoty.Validate(entity);

            if (!isValid)
                this.BindValidationErrors(entity);

            return isValid;
        }     
    }
}