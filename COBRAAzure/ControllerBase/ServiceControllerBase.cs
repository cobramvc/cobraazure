﻿using Entity.ModelHelpers;
using Entity.ProductModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.ProductRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class ServiceControllerBase : ControllerBase<Service, CodeListDetail>
    {
        protected IServiceRepository _repositoty = null;
        protected ICodeListDetailRepository _codeListrepositoty = null;
        
        protected override void InitializeRepositoryMethods()
        {
            base.codeListRepositoryMethod = this._codeListrepositoty.GetByListName;
            base.codeListByParentCodeRepositoryMethod = this._codeListrepositoty.GetByListNameParentCodeID; // Need to define repository method if we have cascade dropdpwn functionality similar to Customer
            base.codeListByCodeListIDRepositoryMethod = this._codeListrepositoty.GetByCodeListID;
        }

        protected override void BuildViewBagCollection(AbstractEntity entity)
        {
            this.InitializeRepositoryMethods();
            base.BuildViewBagCollection(entity);
            ViewBag.ServiceName = string.Empty;
            ViewBag.SerViceTypeCodeList = new SelectList(this._codeListrepositoty.GetByListName(CodeListKey.ServiceTypeList), "CodeID", "Description");
            this._codeListrepositoty.IncludeDefaultAll = true;

            if (entity == null)
            {
                base.BuildSelectListViewBagCollection<ServicePricing>(null);
                return;
            }
                
            if (entity is Service)
            {
                Service service = (Service)entity;
                base.BuildSelectListViewBagCollection<Service>(service);
            }
            else if (entity is ServicePricing)
            {
                ServicePricing servicePricing = (ServicePricing)entity;
                base.BuildSelectListViewBagCollection<ServicePricing>(servicePricing);
                base.BuildSelectListViewBagCollectionForChildObjects(servicePricing.CurrentServicePricingDetailXRefs);

                if (servicePricing.Service != null)
                    ViewBag.ServiceName = servicePricing.Service.Description;
            }
            else if (entity is ServicePricingDetailXRef)
            {
                ServicePricingDetailXRef servicePricingDetailXRef = (ServicePricingDetailXRef)entity;
                base.BuildSelectListViewBagCollection<ServicePricingDetailXRef>(servicePricingDetailXRef);                
            }

            this._codeListrepositoty.IncludeDefaultAll = false;
        }
        
        protected override void BindValidationErrors(AbstractEntity entity)
        {
            if (entity is Service)
            {
                Service service = (Service)entity;
                base.BindValidationErrors(service);
            }
            else if (entity is ServicePricing)
            {
                ServicePricing servicePricing = (ServicePricing)entity;
                base.BindValidationErrors(servicePricing);
                base.BindValidationErrorsForChildObjects(servicePricing.CurrentServicePricingDetailXRefs);
            }            
        }

        protected override bool PerformValidation(AbstractEntity entity)
        {           
            bool isValid = this._repositoty.Validate(entity);

            if (!isValid)
                this.BindValidationErrors(entity);

            return isValid;
        }      
    }
}