﻿using COBRA.MVC.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace COBRAAzure
{
    public class CustomPrincipal : IPrincipal
    {
        private IIdentity _identity = null;
        public CustomPrincipal(IIdentity identity)
        {
            this._identity = identity;
        }

        public IIdentity Identity
        {
            get
            {
                return this._identity;
            }
        }

        public bool IsInRole(string role)
        {
            return true;//to be implemented
        }

        public ApplicationUser GetUserProfile()
        {
            // this cannever happen
            if (this.Identity == null)
                return null;

            // this cannever happen
            if (!this.Identity.IsAuthenticated)
                return null;

            using (UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
            {
                ApplicationUser user = userManager.FindByName(this.Identity.Name);
                return user;
            }
        }
    }
}