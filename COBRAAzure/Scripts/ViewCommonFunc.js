﻿
//$(function() {
  	
//    $("select").combobox();
		
//});

    function AjaxCallGet(tempUrl, divToLoad, postData) {
            $.ajax({
                async: false,
                cache: false,
                url: tempUrl,
                data: postData,
                success: function (data) {
                    $(divToLoad).html(data);
                },
                error: function (errorThrown) {
                    $(divToLoad).html(errorThrown.responseText);
                    alert(errorThrown.responseText);
                }
            });
    }

    function AjaxCallGetByForm(tempUrl, divToLoad, frmToSubmit) {
        $.ajax({
            async: false,
            cache: false,
            url: tempUrl,
            data: $("#" + frmToSubmit).serialize(),
            success: function (data) {
                $(divToLoad).html(data);
            },
            error: function (errorThrown) {
                $(divToLoad).html(errorThrown.responseText);
                alert(errorThrown.responseText);
            }
        });
    }

    
    function AjaxCallPostByData(tempUrl, divToLoad, postData) {
        $.ajax({
            async: false,
            cache: false,
            method: "POST",
            url: tempUrl,
            data: postData,
            success: function (data) {
                $("#" + divToLoad).html(data);
            },
            error: function (errorThrown) {
                $("#" + divToLoad).html(errorThrown.responseText);
                alert(errorThrown.responseText);
            }
        });
    }

    function AjaxCallPost(tempUrl, divToLoad, frmToSubmit) {
        $.ajax({
            async: false,
            cache: false,
            method: "POST",
            url: tempUrl,
            data: $("#" + frmToSubmit).serialize(),
            success: function (data) {                
                $("#" + divToLoad).html(data);
            },
            error: function (errorThrown) {
                $(divToLoad).html(errorThrown.responseText);
                alert(errorThrown.responseText);
            }
        });
    }



        function CreateNewTableRowByIndex(tableID, actionUrl, indexControlID)
        {
            var currentIndex = 0;
            currentIndex = $("#" + indexControlID).val();            
            var postData = { index: currentIndex };
            $.ajax({
                async: false,
                cache: false,
                url: actionUrl,
                data: postData,
                success: function (data) {                    
                    $("#" + tableID + " tbody").append("'" + data + "'");
                    currentIndex = parseInt(currentIndex) + 1;
                    $("#" + indexControlID).val(currentIndex);
                },
                error: function (errorThrown) {
                    $("#" + tableID + " tbody").append("'" + errorThrown.responseText + "'");                  
                        alert(errorThrown.responseText);
                }
            });
        }

        function AppendActionResultToDiv(divID, actionUrl, postData) {
            $.ajax({
                async: false,
                cache: false,
                url: actionUrl,
                data: postData,
                success: function (data) {                    
                    $("#" + divID).append("'" + data + "'");                                           
                },
                error: function (errorThrown) {
                    $("#" + divID).append("'" + errorThrown.responseText + "'");
                    alert(errorThrown.responseText);
                }
            });
        }

        function CreateNewTableRow(tableID, actionUrl, postData) {          
            $.ajax({
                async: false,
                cache: false,
                url: actionUrl,
                data: postData,
                success: function (data) {                    
                    var currentIndex = 0;
                    $("#" + tableID + " tbody").each(function (index, value) {
                        if (currentIndex == 0) {
                            $(this).append("'" + data + "'");                            
                        }
                        currentIndex++;
                    });                    
                },
                error: function (errorThrown) {
                    $("#" + tableID + " tbody").append("'" + errorThrown.responseText + "'");
                    alert(errorThrown.responseText);
                }
            });
        }

        function GetCascadingDropDown(parentDropDownId, cascadingDropDownId, actionUrl) {
            var curId = "#" + parentDropDownId;
            var selectedValue = $(curId).val();
            var cascadingDropDown = $("#" + cascadingDropDownId);

            $.ajax({
                cache: false,
                type: "GET",
                url: actionUrl,
                data: { "codeID": selectedValue },
                success: function (data) {
                    cascadingDropDown.html('');
                    $.each(data, function (id, option) {
                        cascadingDropDown.append($('<option></option>').val(option.Value).html(option.Text));
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {                    
                }
            });
        }

        function GetCascadingDropDownListByValue(parentSelectedValue, cascadingDropDownId, defaultValue, actionUrl) {

            var cascadingDropDown = $("#" + cascadingDropDownId);

            $.ajax({
                cache: false,
                type: "GET",
                url: actionUrl,
                data: { "codeID": parentSelectedValue },
                success: function (data) {
                    cascadingDropDown.html('');
                    $.each(data, function (id, option) {
                        cascadingDropDown.append($('<option></option>').val(option.Value).html(option.Text));
                    });
                    cascadingDropDown.val(defaultValue);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert('Failed to retrieve states.');
                }
            });
        }

        function DeleteRow(selectedIndex, tableID, modelObjID) {           
            $("#" + tableID + " tbody").find("tr").each(function (index, value) {                
                if (selectedIndex == index) {
                    $(this).hide();
                    $("#" + modelObjID + "_" + selectedIndex + "__RecordStatus").val(false);                    
                }
            });
        }

        function HideDeletedRows(tableID, modelObjID) {
            $("#" + tableID + " tbody").find("tr").each(function (index, value) {
                if ($("#" + modelObjID + "_" + index + "__RecordStatus").val().toLowerCase() == "false") {
                    $(this).hide();
                }
            });
        }
        
        $(function () {
            DisplayQTipMessages();
         
        });

        function DisplayQTipMessages() {

            // Run this function for all validation error messages
            $('.field-validation-error').each(function () {

                // Get the error text to be displayed
                var errorText = $(this).text();

                // Remove the text from the error message span
                // element and add the classes to display the icon
                $(this).empty();
                $(this).addClass("ui-state-error-icon").addClass("ui-icon-alert");

                // Wire up the tooltip to display the error message
                $(this).qtip({
                    overwrite: true,
                    content: errorText
                });
            });
        }

