﻿/// <reference path="ui/jquery.ui.core.js" />
/// <reference path="ui/jquery.ui.widget.js" />
/// <reference path="ui/jquery.ui.mouse.js" />
/// <reference path="ui/jquery.ui.button.js" />
/// <reference path="ui/jquery.ui.draggable.js" />
/// <reference path="ui/jquery.ui.position.js" />
/// <reference path="ui/jquery.ui.resizable.js" />
/// <reference path="ui/jquery.ui.dialog.js" />
/// <reference path="ui/jquery.ui.effect.js" />



function InitializeDialog(divID) {
        
    var divToInitialize = $("#" + divID);

    if (divToInitialize == null || divToInitialize == "undefined")
        return;

    divToInitialize.dialog({
        autoOpen: false,
        height: 550,
        width: 1100,
        modal: true,
        buttons: {
            Cancel: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            $(this).dialog("close");
        }
    });
}

function OpenDialog(divID) {

    var divToOpen = $("#" + divID);

    if (divToOpen == null || divToOpen == "undefined")
        return;

    if (divToOpen.is(':ui-dialog') == false) {       
        InitializeDialog(divID);        
    }
    
    divToOpen.dialog("open");
}

function CloseDialog(divID) {
    var divToClose = $("#" + divID);

    if (divToClose == null || divToClose == "undefined")
        return;

    divToClose.dialog("close");
}





