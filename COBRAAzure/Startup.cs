﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(COBRAAzure.Startup))]
namespace COBRAAzure
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
