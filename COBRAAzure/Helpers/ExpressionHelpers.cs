﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace COBRA.MVC
{
    public class ExpressionHelpers
    {
        private static ExpressionHelpers helper = new ExpressionHelpers();

        private ExpressionHelpers()
        {
        }

        public static ExpressionHelpers Instance
        {
            get
            {
                return helper;
            }
        }

        public string GetPropertyName<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            MemberExpression memberExpr = expression.Body as MemberExpression ?? ((UnaryExpression)expression.Body).Operand as MemberExpression;
            return memberExpr.Member.Name;
        }

        public string GetModelName<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            Type tModel = typeof(TModel);
            return tModel.Name;
        }

        public string GetModelPrefix<TModel>(TModel model, int index)
        {
            Type modelType = typeof(TModel);
            return modelType.Name + "s[" + index + "].";
        }  
    }
}