﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace COBRA.MVC
{
    public class GenericHelpers
    {
        private static GenericHelpers helper = new GenericHelpers();

        private GenericHelpers()
        {
        }

        public static GenericHelpers Instance
        {
            get { return helper; }
        }

        public string GetSelectListNameByProperty(string modelName, string propertyName, string parentCodeID = "")
        {
            return modelName + "_" + propertyName + "_" + parentCodeID + "_SelectList";
        }

        public List<SelectListPropertyInfo> GetSelectListPropertyInfoList<TModel>(TModel entity) where TModel : AbstractEntity
        {
            Type objType = typeof(TModel);
            List<PropertyInfo> entityProperties = objType.GetProperties().ToList();

            List<MetadataTypeAttribute> customAttributes = objType.GetCustomAttributes<MetadataTypeAttribute>().ToList();
            List<SelectListPropertyInfo> selectListPropertyInfoList = new List<SelectListPropertyInfo>();

            if (customAttributes.Count == 0)
                return selectListPropertyInfoList;

            Type metaDataType = customAttributes.First().MetadataClassType;

            List<PropertyInfo> properties = metaDataType.GetProperties().ToList();

            foreach (PropertyInfo property in properties)
            {
                List<SelectListAttribute> selectListCustomAttributes = property.GetCustomAttributes<SelectListAttribute>().ToList();
                
                if (selectListCustomAttributes.Count() == 0)
                    continue;

                selectListPropertyInfoList.Add(CreateSelectListPropertyInfo(entity, entityProperties, property, selectListCustomAttributes));             
            }

            return selectListPropertyInfoList;
        }

        public SelectListPropertyInfo GetSelectListPropertyInfo<TModel>(TModel entity, string propertyName) where TModel : AbstractEntity
        {
            Type objType = typeof(TModel);
            List<PropertyInfo> entityProperties = objType.GetProperties().ToList();

            List<MetadataTypeAttribute> customAttributes = objType.GetCustomAttributes<MetadataTypeAttribute>().ToList();
            

            if (customAttributes.Count == 0)
                return null;

            Type metaDataType = customAttributes.First().MetadataClassType;

            List<PropertyInfo> properties = metaDataType.GetProperties().ToList();

            PropertyInfo selectListProperty = properties.Where(p => p.Name == propertyName).FirstOrDefault();

            if (selectListProperty == null)
                return null;

            List<SelectListAttribute> selectListCustomAttributes = selectListProperty.GetCustomAttributes<SelectListAttribute>().ToList();

            if (selectListCustomAttributes.Count() == 0)
                return null;

            return CreateSelectListPropertyInfo(entity, entityProperties, selectListProperty, selectListCustomAttributes);           
        }

        private SelectListPropertyInfo CreateSelectListPropertyInfo<TModel>(TModel entity, List<PropertyInfo> entityProperties, PropertyInfo selectListProperty, List<SelectListAttribute> selectListCustomAttributes) where TModel : AbstractEntity
        {
            SelectListPropertyInfo selectListPropertyInfo = new SelectListPropertyInfo();
            selectListPropertyInfo.Property = selectListProperty;
            selectListPropertyInfo.CodeListKey = selectListCustomAttributes.First().CodeListKey;
            selectListPropertyInfo.DependencyCodeID = Int32.MinValue;
            selectListPropertyInfo.CodeID = Int32.MinValue;

            if (entity == null)
                return selectListPropertyInfo;


            PropertyInfo codeIDProperty = entityProperties.Where(p => p.Name == selectListProperty.Name).FirstOrDefault();
            var codeIDobj = codeIDProperty == null ? null : codeIDProperty.GetValue(entity);
            if (codeIDobj != null)
                selectListPropertyInfo.CodeID = Convert.ToInt32(codeIDobj);

            List<DependencyColumnAttribute> dependencyColumnAttributes = selectListProperty.GetCustomAttributes<DependencyColumnAttribute>().ToList();

            if (dependencyColumnAttributes.Count() == 0)
                return selectListPropertyInfo;

            PropertyInfo dependencyProperty = entityProperties.Where(p => p.Name == dependencyColumnAttributes.First().DependencyColumn).FirstOrDefault();
            int dependencyCodeID = 0;
            var dependencyCodeIDobj = dependencyProperty.GetValue(entity);
            if (dependencyCodeIDobj != null)
                dependencyCodeID = Convert.ToInt32(dependencyCodeIDobj);

            selectListPropertyInfo.DependencyCodeID = dependencyCodeID;

            return selectListPropertyInfo;
        }        
    }

    public class SelectListPropertyInfo
    {
        public PropertyInfo Property
        {
            get;
            set;
        }

        public string CodeListKey
        {
            get;
            set;
        }

        public int CodeID
        {
            get;
            set;
        }

        public int DependencyCodeID
        {
            get;
            set;
        }
    }
}