﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COBRA.MVC
{
    public class AttributeHelpers
    {
        private static AttributeHelpers helper = new AttributeHelpers();
        private Hashtable attributeHash = new Hashtable();
        private readonly object m_object= new object();

        private AttributeHelpers()
        {
        }

        public static AttributeHelpers Instance
        {
            get { return helper; }
        }

        public IDictionary<string, object> GetAttributes(string key)
        {
            if (attributeHash.ContainsKey(key))
            {
                return (IDictionary<string, object>)attributeHash[key];
            }

            return null;
        }

        public void AddAttributes(string key, IDictionary<string, object> attributes)
        {
            lock (m_object)
            {
                if (!attributeHash.ContainsKey(key))
                {
                    attributeHash.Add(key, attributes);
                }
            }                        
        }

        public void RemoveAttributes(string key, string attributeKey)
        {
            lock (m_object)
            {
                if (attributeHash.ContainsKey(key))
                {
                    IDictionary<string, object> sourceAttributes = (IDictionary<string, object>)attributeHash[key];
                    if (sourceAttributes.ContainsKey(attributeKey))
                        sourceAttributes.Remove(attributeKey);
                }
            }
        }

        public void MergeAttributes(string targetKey, string sourceKey)
        {
            lock (m_object)
            {
                if (attributeHash.ContainsKey(targetKey)
                    && attributeHash.ContainsKey(sourceKey))
                {
                    IDictionary<string, object> sourceAttributes = (IDictionary<string, object>)attributeHash[sourceKey];
                    IDictionary<string, object> targetAttributes = (IDictionary<string, object>)attributeHash[targetKey];
                    sourceAttributes.ToList().ForEach(keyValuepair =>
                        {
                            if (!targetAttributes.Contains(keyValuepair))
                                targetAttributes.Add(keyValuepair);
                        });

                    //attributeHash[targetKey] = (IDictionary<string, object>)(targetAttributes);                        
                }
            }
        }
    }
}