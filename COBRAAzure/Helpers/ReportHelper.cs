﻿using COBRA.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace COBRAAzure
{
    public class ReportHelper
    {
        private static ReportHelper helper = new ReportHelper();

        private ReportHelper()
        {
        }

        public static ReportHelper Instance
        {
            get { return helper; }
        }

        public void SetPieChartAttributes(Chart chart, List<ReportItem<string, decimal>> reportItems)
        {
            //chart.ChartAreas.Add(CreateChartArea());
            this.SetChartProperties(chart);
            chart.Series.Add(new Series("Data"));
            chart.Legends.Add(new Legend("Stores"));
            chart.Series["Data"].ChartType = SeriesChartType.Pie;
            chart.Series["Data"]["PieLabelStyle"] = "Outside";
            chart.Series["Data"]["PieLineColor"] = "Black";
            for (int x = 0; x < reportItems.Count(); x++)
            {
                int ptIdx = chart.Series["Data"].Points.AddXY(
                     reportItems[x].XValue,
                     reportItems[x].YValue);
                DataPoint pt = chart.Series["Data"].Points[ptIdx];
                pt.LegendText = "#VALX: #VALY";
                pt.LegendUrl = "/Contact/Details/Hey";
            }

            chart.Series["Data"].Label = "#PERCENT{P0}";
            chart.Series["Data"].Font = new Font("Segoe UI", 8.0f, FontStyle.Bold);
            chart.Series["Data"].ChartType = SeriesChartType.Pie;
            chart.Series["Data"]["PieLabelStyle"] = "Outside";
            chart.Series["Data"].Legend = "Stores";
            chart.Legends["Stores"].Docking = Docking.Bottom;

        }

        public void SetChartProperties(Chart chart)
        {
            chart.Width = Unit.Pixel(500);
            chart.BackColor = Color.FromArgb(211, 223, 240);
            chart.BorderlineDashStyle = ChartDashStyle.Solid;
            chart.BackSecondaryColor = Color.White;
            chart.BackGradientStyle = GradientStyle.TopBottom;
            chart.BorderlineWidth = 1;
            chart.Palette = ChartColorPalette.BrightPastel;
            chart.BorderlineColor = Color.FromArgb(26, 59, 105);
            chart.RenderType = RenderType.BinaryStreaming;
            chart.BorderSkin.SkinStyle = BorderSkinStyle.Emboss;
            chart.AntiAliasing = AntiAliasingStyles.All;
            chart.TextAntiAliasingQuality = TextAntiAliasingQuality.Normal;
            //chart.Titles.Add(CreateTitle());
            //chart.Legends.Add(CreateLegend());
            //chart.Series.Add(CreateSeries(reportItems, SeriesChartType.Pie));
            //chart.ChartAreas.Add(CreateChartArea());

        }
        public void SetTitle(Chart chart, string titleName)
        {
            Title title = new Title();
            title.Text = titleName;
            title.ShadowColor = Color.FromArgb(32, 0, 0, 0);
            title.Font = new Font("Trebuchet MS", 14F, FontStyle.Bold);
            title.ShadowOffset = 3;
            title.ForeColor = Color.FromArgb(26, 59, 105);
            chart.Titles.Add(title);
        }

        public void SetLegend(Chart chart)
        {
            Legend legend = new Legend();
            legend.Enabled = true;
            chart.Legends.Add(legend);
        }

        public Series CreateSeries(List<ReportItem<string, decimal>> results, SeriesChartType chartType, string seriesName, string chartAreaName)
        {
            Series seriesDetail = new Series();
            seriesDetail.Name = seriesName;
            seriesDetail.IsValueShownAsLabel = false;
            seriesDetail.Color = Color.FromArgb(198, 99, 99);
            seriesDetail.ChartType = chartType;
            seriesDetail.BorderWidth = 2;
            DataPoint point;

            foreach (ReportItem<string, decimal> result in results)
            {
                point = new DataPoint();
                point.AxisLabel = result.XValue;
                point.YValues = new double[] { Double.Parse(result.YValue.ToString()) };
                seriesDetail.Points.Add(point);
            }

            seriesDetail.ChartArea = chartAreaName;
            return seriesDetail;
        }

        public void SetChartArea(Chart chart, string chartAreaName)
        {
            ChartArea chartArea = new ChartArea();
            chartArea.Name = chartAreaName;
            chartArea.BackColor = Color.Transparent;
            chartArea.AxisX.IsLabelAutoFit = false;
            chartArea.AxisY.IsLabelAutoFit = false;
            chartArea.AxisX.LabelStyle.Font =
               new Font("Verdana,Arial,Helvetica,sans-serif",
                        8F, FontStyle.Regular);
            chartArea.AxisY.LabelStyle.Font =
               new Font("Verdana,Arial,Helvetica,sans-serif",
                        8F, FontStyle.Regular);
            chartArea.AxisY.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.Interval = 1;

            chart.ChartAreas.Add(chartArea);
        }

    }
}