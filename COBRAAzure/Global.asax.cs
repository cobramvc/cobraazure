﻿
using Infrastructure;
using Infrastructure.Caching;
using Microsoft.Practices.EnterpriseLibrary.SemanticLogging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace COBRAAzure
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //ViewEngines.Engines.Add()
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            this.RegisterLoggingEventListeners();
            CachingManager.Instance.RegisterCachingConfiguration();
        }

        /// <summary>
        /// This method to register or enable Semantic Logging Application Block 
        /// event listeners and event sinks
        /// </summary>
        private void RegisterLoggingEventListeners()
        {
            //var flatFileListener = FlatFileLog.CreateListener("CustomerLogSemmantic.log");
            //flatFileListener.EnableEvents(LoggingManager.Instance, System.Diagnostics.Tracing.EventLevel.Warning, LoggingManager.Keywords.Model);

            string connectionString = ConfigurationManager.ConnectionStrings["COBRADbContext"].ConnectionString;
            var databaseListener = SqlDatabaseLog.CreateListener("COBRAAzure", connectionString, "Traces");
            databaseListener.EnableEvents(LoggingManager.Instance, System.Diagnostics.Tracing.EventLevel.Error);
        }        
    }
}
