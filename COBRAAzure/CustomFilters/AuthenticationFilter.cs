﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace COBRAAzure
{
    public class AuthenticationFilterAttribute : FilterAttribute, IAuthenticationFilter
    {
        //
        // Summary:
        //     Authenticates the request.
        //
        // Parameters:
        //   filterContext:
        //     The context to use for authentication.
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            IPrincipal principle = filterContext.HttpContext.User;
            if (principle == null || !principle.Identity.IsAuthenticated)
            {
                return;
            }

            CustomPrincipal customPrincipal = new CustomPrincipal(principle.Identity);
            filterContext.HttpContext.User = customPrincipal;
        }

        //
        // Summary:
        //     Adds an authentication challenge to the current System.Web.Mvc.ActionResult.
        //
        // Parameters:
        //   filterContext:
        //     The context to use for the authentication challenge.
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            object[] attributes = filterContext.ActionDescriptor.GetCustomAttributes(true);

            // Exclude to validate Authentication if the Action is decorated with AllowAnonymousAttribute
            foreach (object attribute in attributes)
            {
                if (attribute is AllowAnonymousAttribute)
                    return;
            }

            IPrincipal principle = filterContext.HttpContext.User;
            if (principle == null || !principle.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
    }
}