﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRAAzure
{
    public class CustomUIExceptionFilter : FilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// Custom Exception Handling protocol to handle any unhandled exceptions or exceptions thrown by the Controller actions
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnException(ExceptionContext filterContext)
        {
            // Handle the Exception before it is being re-directed to Error page
            // Note: ExceptionPolicyName : UICustomFilter is used to log the exception details to the database and consume the Exception with
            // PostHandlingAction as None(not to throw   new exception after handling)
            ExceptionHandlingManager.Instance.HandleException(filterContext.Exception, ExceptionPolicyName.UICustomFilter);

            filterContext.ExceptionHandled = true;

            ViewResult result = new ViewResult();
            result.ViewName = "Error";
            filterContext.Result = result;
        }
    }
}