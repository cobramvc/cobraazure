﻿using Billing;
using Billing.BillingStrategy;
using COBRA.Models;
using COBRA.MVC;
using Entity.BillingModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.BillingRepo;
using Entity.UnitOfWork.ContractRepo;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRAAzure.Controllers
{
    public class BillingController : PdfViewController
    {
        //private  CustomerContractRepository _repository = null;
        private IBillingRepository _repository = null;

        public BillingController()
        {
            this._repository = new BillingRepository(new BillingUnitOfWork());
        }

        public ActionResult SearchBilling()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchBilling(BillingSearchCriteriaModel searchCriteria)
        {
            //_repository = new CustomerContractRepository(new CustomerContractUnitOfWork());
            CustomerContract contract = this._repository.GetCustomerContractByID(searchCriteria.CustomerContractID);
            searchCriteria.BillingTypeCodeID = CustomerBillingTypeCodeKey.Periodic;
            searchCriteria.ContractTermTypeCodeID = contract.ContractTermTypeCodeID;
                 
            BaseBllingStrategy billingStrategy = BillingStrategyFactory.Instance.GetBillingStrategy(searchCriteria);
            CustomerBilling billing = billingStrategy.LoadContractBillingDetails();

            return View(billing);
        }

        public ActionResult ExportToPDF(long pBillingID)
        {
            CustomerBilling billing = this._repository.GetCustomerBillingByID(pBillingID);

            string pdfFileName = "BillingReport_" + DateTime.Now.ToShortDateString() + ".pdf";
            return this.ViewPdf("", "Billing/_BillingSummaryPDF", billing, pdfFileName);

            //ViewAsPdf pdf = new ViewAsPdf("Billing/_BillingSummaryPDF", billing);
            //pdf.FileName = "BillingReport_" + DateTime.Now.ToShortDateString() + ".pdf";
            //pdf = GenericHelpers.Instance.SetPDFProperties(pdf);
            //return pdf;
        }

        public ActionResult GenerateCustomerBilling(long pCustomerID)
        {
            BillingSearchCriteriaModel searchCriteria = new BillingSearchCriteriaModel();
            searchCriteria.BillingTypeCodeID = CustomerBillingTypeCodeKey.Periodic;
            searchCriteria.CustomerID = pCustomerID;
            List<CustomerBilling> billings = this._repository.GetBillingBySearchCriteria(searchCriteria);
            return View("Billing/_CustomerBillingDisplay", billings);           
        }

    }
}