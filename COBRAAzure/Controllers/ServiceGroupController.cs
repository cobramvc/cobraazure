﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Entity.UnitOfWork;
using Infrastructure;
using COBRA.Models;
using Grid.Mvc.Ajax.GridExtensions;

namespace COBRA.MVC.Controllers
{
    public class ServiceGroupController : ServiceGroupControllerBase
    {
        public ServiceGroupController(IServiceGroupRepository repositoryInstance)
        {
            this._repositoty = repositoryInstance;
        }

        public ServiceGroupController()
        {
            _uow = new ProductUnitOfWork();
            this._repositoty = new ServiceGroupRepository(_uow);
            this._codeListrepositoty = new CodeListDetailRepository(_uow);
        }

        // GET: /ServiceGroup/Create
        public ActionResult CreateOrEdit(long? pServiceGroupID)
        {
            try
            {
                ViewBag.Caption = string.Empty;
                ViewBag.SerViceTypeCodeList = new SelectList(this._codeListrepositoty.GetByListName(CodeListKey.ServiceTypeList), "CodeID", "Description");

                if (!pServiceGroupID.HasValue)
                    return View();
                else if (pServiceGroupID.Value == 0)
                {
                    ServiceGroup newServiceGroup = this._repositoty.CreateServiceGroup();
                    ViewBag.Caption = "ADD SERVICE GROUP - " + newServiceGroup.ServiceGroupID.ToString();
                    return View(newServiceGroup);
                }
                else
                {
                    ServiceGroup serviceGroup = this._repositoty.GetServiceGroupByID(pServiceGroupID.Value);
                    ViewBag.Caption = "MODIFY SERVICE GROUP - " + serviceGroup.ServiceGroupID.ToString();
                    return View(serviceGroup);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View();
        }

        // POST: /ServiceGroup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOrEdit(ServiceGroup servicegroup)
        {
            try
            {
                this.ExecutePreSaveConditions(servicegroup, this._repositoty, servicegroup.ServiceGroupDetailXRefs.ToList());
                ViewBag.SerViceTypeCodeList = new SelectList(this._codeListrepositoty.GetByListName(CodeListKey.ServiceTypeList), "CodeID", "Description");

                if (!this.IsModelValid)
                    return View(servicegroup);

                if (servicegroup.RecordVersion == 0)
                {
                    this._repositoty.AddServiceGroup(servicegroup);
                }
                else
                {
                    this._repositoty.UpdateServiceGroup(servicegroup);
                }

                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus != ApplicationKeys.SaveUnsucessful)
                    return View("_ServiceGroupSuccess");              
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(servicegroup);       
        }

        public ActionResult Details(long pServiceGroupID)
        {
            ServiceGroup serviceGroup = this._repositoty.GetServiceGroupByID(pServiceGroupID);
            return View(serviceGroup);
        }

        #region Search Service Groups

        public ActionResult SearchServiceGroup()
        {
            return View("Search/_SearchServiceGroup", new ServiceGroupSearchCriteriaModel());
        }

        public ActionResult GetDefaultSearchResults(ServiceGroupSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;
            AjaxGrid<ServiceGroup> grid = GetDefaultAjaxGrid<ServiceGroup>();
            return View("Search/_ServiceGroupSearchResults", grid);
        }

        public JsonResult GetSearchResults(ServiceGroupSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<ServiceGroup> grid = GetAjaxGridByList<ServiceGroup>(this._repositoty.GetServiceGroupsBySearchCriteria(searchCriteria));
            return Json(new { Html = grid.ToJson("Search/_ServiceGroupSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSearchResultsPaged(ServiceGroupSearchCriteriaModel searchCriteria, int? page)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<ServiceGroup> grid = GetAjaxGridByList<ServiceGroup>(this._repositoty.GetServiceGroupsBySearchCriteria(searchCriteria), page.HasValue ? page.Value : 1, 5);
            return Json(new { Html = grid.ToJson("Search/_ServiceGroupSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    
        public ActionResult CreateServiceGroupDetailXRef(int index)
        {
            try
            {
                ServiceGroupDetailXRef svcGruoupDetailXRef = this._repositoty.CreateServiceGroupDetailXRef();
                ViewData["ServiceGroupDetailXRefIndex"] = index;
                return PartialView("_ServiceGroupDetail", svcGruoupDetailXRef);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return PartialView("_ServiceGroupDetail", null);
        }

        public ActionResult PricingDetails(long? pServiceGroupID, int? pContractTermTypeCodeID)
        {
            try
            {
                ServiceGroupPricing serviceGroupPricing = null;
                ServiceGroup serviceGroup = null;
                
                if (!pServiceGroupID.HasValue
                    || !pContractTermTypeCodeID.HasValue)
                {
                    serviceGroupPricing = new ServiceGroupPricing();
                    serviceGroupPricing.ServiceGroupID = pServiceGroupID.HasValue ? pServiceGroupID.Value : 0;
                    this._repositoty.ExecuteBusinessRules(serviceGroupPricing, BusinessRulesActionTypeEnum.ObjectMaterialize, string.Empty);                  
                    base.BuildViewBagCollection(serviceGroupPricing);
                    return View(serviceGroupPricing);
                }

                
                serviceGroup = this._repositoty.GetServiceGroupByID(pServiceGroupID.Value);
                serviceGroupPricing = serviceGroup.CurrentServiceGroupPricing(pContractTermTypeCodeID.Value);
                if (serviceGroupPricing == null)
                {
                    serviceGroupPricing = this._repositoty.CreateServiceGroupPricing();
                    serviceGroupPricing.ServiceGroupID = serviceGroup.ServiceGroupID;                    
                    serviceGroupPricing.ContractTermTypeCodeID = pContractTermTypeCodeID.Value;
                }

                serviceGroupPricing.ServiceGroup = serviceGroup;
                this._repositoty.AddCalculatedServiceGroupPricingDetails(serviceGroupPricing);

                // The business rules are to be executed explicitly as few of ServicePricingDetailXRef
                // objects in the collection are manually added(neither created through factory class nor loaded from database)
                // and hence the business rules are not be executed automatically.
                // Note: Business rules are configured to execute automatically if the object is either created through repository create method 
                // OR loaded from the database triggering ObjectMaterialize event
                this._repositoty.ExecuteBusinessRules(serviceGroupPricing, BusinessRulesActionTypeEnum.ObjectMaterialize, string.Empty);
                
                base.BuildViewBagCollection(serviceGroupPricing);
                return View(serviceGroupPricing);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PricingDetails(ServiceGroupPricing serviceGroupPricing)
        {            
            try
            {
                this.ExecutePreSaveConditions<ServiceGroupPricing>(serviceGroupPricing, this._repositoty, null);

                if (!this.IsModelValid)
                    return View(serviceGroupPricing);

                this._repositoty.UpdateServiceGroupPricingDetails(serviceGroupPricing);
                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus != ApplicationKeys.SaveUnsucessful)
                    return View("_ServiceGroupSuccess");                

            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(serviceGroupPricing);           
        }

        public ActionResult PricingDetailXRefList(long pserviceGroupID, int pContractTermTypeCodeID, int? pVersion)
        {
            ServiceGroup serviceGroup = this._repositoty.GetServiceGroupByID(pserviceGroupID);
            ServiceGroupPricing currentServiceGroupPricing = null;

            if (pVersion.HasValue)
            {
                currentServiceGroupPricing = serviceGroup.GetServiceGroupPricing(pContractTermTypeCodeID, pVersion.Value);
            }
            else
            {
                currentServiceGroupPricing = serviceGroup.CurrentServiceGroupPricing(pContractTermTypeCodeID);
            }

            List<ServiceGroupPricingDetailXRef> serviceGroupPricings = (currentServiceGroupPricing == null) ? new List<ServiceGroupPricingDetailXRef>() : currentServiceGroupPricing.CurrentServiceGroupPricingDetailXRefs;
            return View("_PricingDetailXRefList", serviceGroupPricings);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._repositoty.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
