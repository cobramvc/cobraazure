﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Entity.UnitOfWork;
using Infrastructure;
using COBRA.Models;
using Grid.Mvc.Ajax.GridExtensions;

namespace COBRA.MVC.Controllers
{
    public class ProductController : ProductControllerBase
    {
        public ProductController(IProductRepository repositoryInstance)
        {
            this._repositoty = repositoryInstance;
        }

        public ProductController()
        {
            _uow = new ProductUnitOfWork();
            this._repositoty = new ProductRepository(_uow);
            this._codeListrepositoty = new CodeListDetailRepository(_uow);
        }

        // GET: /Product/Create
        public ActionResult CreateOrEdit(long? pProductID)
        {
            Product product = null;
            ViewBag.Caption = string.Empty;

            if (!pProductID.HasValue)
            {
                this.BuildViewBagCollection(null);
                return View();
            }
            else if (pProductID.Value == 0)
            {
                product = this._repositoty.CreateProduct();
                ViewBag.Caption = "ADD PRODUCT - " + product.ProductID.ToString();
            }
            else
            {
                product = this._repositoty.GetProductByID(pProductID.Value);
                ViewBag.Caption = "MODIFY PRODUCT - " + product.ProductID.ToString();
            }

            this.BuildViewBagCollection(product);            
            return View(product);                        
        }

        // POST: /Product/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOrEdit(Product product)
        {
            try
            {
                this.ExecutePreSaveConditions<Product>(product, this._repositoty, null);                

                if (!this.IsModelValid)
                    return View(product);

                if (product.RecordVersion == 0)
                {
                    this._repositoty.AddProduct(product);
                }
                else
                {
                    product = this._repositoty.UpdateProduct(product);
                }

                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus != ApplicationKeys.SaveUnsucessful)
                    return View("_ProductSuccess");                

            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(product);         
        }

        public ActionResult Details(long pProductID)
        {
            Product product = this._repositoty.GetProductByID(pProductID);
            return View(product);
        }

        #region Search Products

        public ActionResult SearchProduct()
        {
            return View("Search/_SearchProduct", new ProductSearchCriteriaModel());
        }

        public ActionResult GetDefaultProductSearchResults(ProductSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;
            AjaxGrid<Product> grid = GetDefaultAjaxGrid<Product>();
            return View(GetProductSearchResultsViewName(searchCriteria), grid);
        }

        public JsonResult GetProductSearchResults(ProductSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<Product> grid = GetAjaxGridByList<Product>(this._repositoty.GetProductsBySearchCriteria(searchCriteria));
            return Json(new { Html = grid.ToJson(GetProductSearchResultsViewName(searchCriteria), this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductSearchResultsPaged(ProductSearchCriteriaModel searchCriteria, int? page)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<Product> grid = GetAjaxGridByList<Product>(this._repositoty.GetProductsBySearchCriteria(searchCriteria), page.HasValue ? page.Value : 1, 5);
            return Json(new { Html = grid.ToJson(GetProductSearchResultsViewName(searchCriteria), this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Search Discount

        public ActionResult SearchDiscount()
        {
            return View("Search/_SearchDiscount", new DiscountSearchCriteriaModel());
        }
     
        public ActionResult GetDefaultDiscountSearchResults(DiscountSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;
            AjaxGrid<Discount> grid = GetDefaultAjaxGrid<Discount>();
            return View("Search/_DiscountSearchResults", grid);
        }

        public JsonResult GetDiscountSearchResults(DiscountSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<Discount> grid = GetAjaxGridByList<Discount>(this._repositoty.GetDiscountsBySearchCriteria(searchCriteria));
            return Json(new { Html = grid.ToJson("Search/_DiscountSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDiscountSearchResultsPaged(DiscountSearchCriteriaModel searchCriteria, int? page)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<Discount> grid = GetAjaxGridByList<Discount>(this._repositoty.GetDiscountsBySearchCriteria(searchCriteria), page.HasValue ? page.Value : 1, 5);
            return Json(new { Html = grid.ToJson("Search/_DiscountSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        #endregion


        public ActionResult Discount(int? pDiscountID)
        {
            try
            {
                Discount discount = null;

                if (!pDiscountID.HasValue)
                {
                    return View();
                }
                else if (pDiscountID.Value == 0)
                {
                    discount = this._repositoty.CreateDiscount();                    
                }
                else
                {
                    discount = this._repositoty.GetDiscountByID(pDiscountID.Value);
                }

                base.BuildViewBagCollection(discount);                
                return View(discount);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Discount(Discount discount)
        {
            try
            {
                this.ExecutePreSaveConditions(discount, this._repositoty, discount.DiscountDetailXRefs.ToList());
                
                if (!this.IsModelValid)
                    return View(discount);

                if (discount.RecordVersion == 0)
                    this._repositoty.AddDiscount(discount);
                else
                    this._repositoty.UpdateDiscount(discount);

                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus == ApplicationKeys.SaveUnsucessful)
                    this.BindValidationErrors(discount);
                else
                    return View("_DiscountSuccess");

            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(discount);
        }

        public ActionResult CreateDiscountDetailXRef(int index)
        {
            try
            {
                DiscountDetailXRef discountDetailXRef = this._repositoty.CreateDiscountDetailXRef();
                ViewData["DiscountDetailXRefIndex"] = index;
                base.BuildViewBagCollection(discountDetailXRef);
                return PartialView("_DiscountDetailXRef", discountDetailXRef);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return PartialView("_DiscountDetailXRef", null);
        }

        public ActionResult DiscountDetails(int pDiscountID)
        {
            Discount discount = this._repositoty.GetDiscountByID(pDiscountID);
            return View(discount);
        }

     
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._repositoty.Dispose();
            }
            base.Dispose(disposing);
        }

        private string GetProductSearchResultsViewName(ProductSearchCriteriaModel searchCriteria)
        {
            string viewName = "Search/_ProductSearchResults";

            if (searchCriteria.IsContractProductSearch)
            {
                viewName = "Search/_ProductContractSearchResults";
            }
          
            return viewName;
        }
    }
}
