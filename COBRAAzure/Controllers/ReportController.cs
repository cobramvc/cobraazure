﻿using COBRA.Models;
using Entity.UnitOfWork;
using Entity.UnitOfWork.ReportsRepo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using System.Web.UI.DataVisualization.Charting;

namespace COBRAAzure.Controllers
{
    public class ReportController : Controller
    {
        protected IReportRepository _repositoty = null;
        protected IUnitOfWork _uow = null;

        public ReportController()
        {
            _uow = new CustomerUnitOfWork();
            this._repositoty = new ReportRepository(_uow);
        }

        public ActionResult CategoryWiseCustomerReport()
        {
            List<ReportItem<string, decimal>> reportItems = this._repositoty.CategoryWiseCustomerReport(); 
                                   
            Chart chart = new Chart();            
            ReportHelper.Instance.SetPieChartAttributes(chart, reportItems);
            ReportHelper.Instance.SetTitle(chart, "TOP Customers");
            ReportHelper.Instance.SetLegend(chart);
            ReportHelper.Instance.SetChartArea(chart, "TOP Customers");
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }

        public ActionResult ProductWiseContractReport()
        {
            List<ReportItem<string, decimal>> reportItems = this._repositoty.ProductWiseContractReport();

            Chart chart = new Chart();
            ReportHelper.Instance.SetPieChartAttributes(chart, reportItems);
            ReportHelper.Instance.SetTitle(chart, "TOP Products");
            ReportHelper.Instance.SetLegend(chart);
            ReportHelper.Instance.SetChartArea(chart, "TOP Products");
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }

        public ActionResult RegionWiseBillingReport()
        {
            List<ReportItem<string, decimal>> reportItems = this._repositoty.RegionWiseBillingReport();

            Chart chart = new Chart();
            ReportHelper.Instance.SetPieChartAttributes(chart, reportItems);
            ReportHelper.Instance.SetTitle(chart, "Revenue");
            ReportHelper.Instance.SetLegend(chart);
            ReportHelper.Instance.SetChartArea(chart, "Revenue");
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }

        public ActionResult ContractStatusSplitupReport()
        {
            List<ReportItem<string, decimal>> reportItems = this._repositoty.ContractStatusSplitupReport();

            Chart chart = new Chart();
            ReportHelper.Instance.SetPieChartAttributes(chart, reportItems);
            ReportHelper.Instance.SetTitle(chart, "Contracts");
            ReportHelper.Instance.SetLegend(chart);
            ReportHelper.Instance.SetChartArea(chart, "Contracts");
            var returnStream = new MemoryStream();
            chart.ImageType = ChartImageType.Png;
            chart.SaveImage(returnStream);
            returnStream.Position = 0;
            return new FileStreamResult(returnStream, "image/png");
        }
    }
}