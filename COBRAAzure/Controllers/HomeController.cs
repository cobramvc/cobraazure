﻿
using Entity.Model.CustomerModels;
using Entity.ProductModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.CustomerRepo;
using Entity.UnitOfWork.ProductRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRAAzure.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {

            IUnitOfWork uow = new CustomerUnitOfWork();
            ICustomerRepository repo = new CustomerRepository(uow);
            List<Customer> customer = repo.GetCustomers();

            IUnitOfWork puow = new ProductUnitOfWork();
            IServiceRepository serviceRepo = new ServiceRepository(puow);
            List<Service> services = serviceRepo.GetServices();
            ViewBag.Message = "Your application accessed Customers" + customer.Count + " Services : " + services.Count;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}