﻿using COBRA.Models;
using COBRAAzure;
using Entity.Model.CustomerModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.CustomerRepo;
using Grid.Mvc.Ajax.GridExtensions;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class CustomerController : CustomerControllerBase
    {
        public CustomerController(ICustomerRepository repositoryInstance)
        {
            this._repositoty = repositoryInstance;
        }

        public CustomerController()
        {            
            _uow = new CustomerUnitOfWork();            
            this._repositoty = new CustomerRepository(_uow);
            this._codeListrepositoty = new CodeListDetailRepository(_uow);
        }

        public ActionResult Index()
        {
            return View();
        }
        // GET: /Customer/Create
        public ActionResult Registration(long? pCustomerID)
        {
            try
            {
                Customer customer = null;

                if (!pCustomerID.HasValue)
                {
                    this.BuildViewBagCollection(customer);
                    return View();
                }
                
                if (pCustomerID.HasValue
                    && pCustomerID.Value == 0)
                {
                    customer = this._repositoty.CreateCustomer();                                 
                }
                else
                {
                    customer = this._repositoty.GetCustomerByID(pCustomerID.Value);                    
                }

                this.BuildViewBagCollection(customer);
                return View(customer);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View();
        }

        public ActionResult CreateCustomerAttribute(int index)
        {
            try
            {
                CustomerAttribute attribute = this._repositoty.CreateCustomerAttribute();
                this.BuildViewBagCollection(attribute);
                ViewData["AttributeIndex"] = index;
                return PartialView("_CustomerAttribute", attribute);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return PartialView("_CustomerAttribute", null);
        }
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(Customer customer, string[] chkBillingOptions)
        {
            try
            {                
                this.CreateCustomerBillingOptionsXRef(customer, chkBillingOptions);
                this.ExecutePreSaveConditions(customer, this._repositoty, customer.CustomerAttributes.ToList());

                if(!this.IsModelValid)
                    return View(customer);

                if (customer.RecordVersion == 0) // New Customer
                {
                    this._repositoty.AddCustomer(customer);
                }
                else
                {
                    this._repositoty.UpdateCustomer(customer);
                }

                int status = this._repositoty.SaveChanges();
                if (status == ApplicationKeys.SaveUnsucessful)
                {
                    // Save should not unsucessful as the validations are done prior to invoking SaveChanges method 
                    this.BindValidationErrors(customer);
                    return View(customer);
                }
                else
                {
                    return View("_CustomerSuccess", customer);
                }             
            }
            catch(Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }    
        
            return View(customer);
        }

        public ActionResult Details(long pCustomerID)
        {
            Customer customer = this._repositoty.GetCustomerByID(pCustomerID);
            return View(customer);
        }
               
        [AllowAnonymous]
        public ActionResult GetDataForPDF()
        {
            ViewBag.IsModalDialog = false;
            List<Customer> customers = this._repositoty.GetCustomers();
            
            return View("Customer/_CustomersPDF", customers);
        }

        #region Search Customers

        public ActionResult SearchResultsToPDF()
        {
            List<Customer> customers = new List<Customer>();
            
            if (Session["searchCriteria"] != null)
            {
                CustomerSearchCriteriaModel serachCriteria = (CustomerSearchCriteriaModel)Session["searchCriteria"];
                customers = this._repositoty.GetCustomersBySearchCriteria(serachCriteria);                                           
            }

            string pdfFileName = "CustomerSearchResults_" + DateTime.Now.ToShortDateString() + ".pdf";            
            return this.ViewPdf("", "Customer/_CustomersPDF", customers, pdfFileName);
            //ViewAsPdf pdf = new ViewAsPdf("Customer/_CustomersPDF", customers);
            //pdf.FileName = "CustomerSearchResults_" + DateTime.Now.ToShortDateString() + ".pdf";
            //pdf = GenericHelpers.Instance.SetPDFProperties(pdf);
            //return pdf;

        }

        public ActionResult SearchCustomer()
        {
            return View("Search/_SearchCustomer", new CustomerSearchCriteriaModel());
        }

        public ActionResult GetDefaultSearchResults(CustomerSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;
            AjaxGrid<Customer> grid = GetDefaultAjaxGrid<Customer>();
            return View("Search/_CustomerSearchResults", grid);
        }

        public JsonResult GetSearchResults(CustomerSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;
            Session["searchCriteria"] = searchCriteria;
            AjaxGrid<Customer> grid = GetAjaxGridByList<Customer>(this._repositoty.GetCustomersBySearchCriteria(searchCriteria));
            return Json(new { Html = grid.ToJson("Search/_CustomerSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSearchResultsPaged(CustomerSearchCriteriaModel searchCriteria, int? page)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<Customer> grid = GetAjaxGridByList<Customer>(this._repositoty.GetCustomersBySearchCriteria(searchCriteria), page.HasValue ? page.Value : 1, 5);
            return Json(new { Html = grid.ToJson("Search/_CustomerSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        #endregion
     
        public ActionResult DisplayCustomerInfo(long pCustomerID)
        {
            return View("Customer/_CustomerInfoDisplay", this._repositoty.GetCustomerByID(pCustomerID));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._repositoty.Dispose();                
            }
            base.Dispose(disposing);
        }
	}
}