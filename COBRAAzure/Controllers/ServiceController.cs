﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entity.ProductModels;
using Entity.UnitOfWork.ProductRepo;
using Entity.UnitOfWork;
using Infrastructure;
using COBRA.Models;
using Grid.Mvc.Ajax.GridExtensions;

namespace COBRA.MVC.Controllers
{
    public class ServiceController : ServiceControllerBase
    {
        public ServiceController(IServiceRepository repositoryInstance)
        {
            this._repositoty = repositoryInstance;
        }

        public ServiceController()
        {
            _uow = new ProductUnitOfWork();
            this._repositoty = new ServiceRepository(_uow);
            this._codeListrepositoty = new CodeListDetailRepository(_uow);
        }
       
        // GET: /Service/Create
        public ActionResult CreateOrEdit(long? pServiceID)
        {
            try
            {
                ViewBag.Caption = string.Empty;

                if (!pServiceID.HasValue)
                {
                    base.BuildViewBagCollection(null);
                    return View();
                }

                Service service = null;
                if (pServiceID.Value == 0)
                {                    
                    service = this._repositoty.CreateService();
                    ViewBag.Caption = "ADD SERVICE - " + service.ServiceID.ToString();
                }
                else
                {
                    service = this._repositoty.GetServiceByID(pServiceID.Value);
                    ViewBag.Caption = "MODIFY SERVICE - " + service.ServiceID.ToString();
                }

                base.BuildViewBagCollection(service);
                return View(service);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View();
        }

        // POST: /Service/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOrEdit([Bind(Include = "ServiceID,Name,Description,TypeCodeID,Attribute,CreateBy,CreateDate,UpdateBy,UpdateDate,RecordStatus,RecordVersion")] Service service)
        {
            try
            {
                this.ExecutePreSaveConditions<Service>(service, this._repositoty, null);

                if (!this.IsModelValid)
                    return View(service);

                if (service.RecordVersion == 0)
                {
                    this._repositoty.AddService(service);
                }
                else
                {
                    this._repositoty.UpdateService(service);
                }

                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus == ApplicationKeys.SaveUnsucessful)
                    this.BindValidationErrors(service);
                else
                    return View("_ServiceSuccess");

            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(service);
        }

        public ActionResult Details(long pServiceID)
        {
            Service service = this._repositoty.GetServiceByID(pServiceID);
            return View(service);
        }

        #region Search Services

        public ActionResult SearchService()
        {
            ViewBag.SerViceTypeCodeList = new SelectList(this._codeListrepositoty.GetByListName(CodeListKey.ServiceTypeList), "CodeID", "Description");
            return View("Search/_SearchService", new ServiceSearchCriteriaModel());            
        }

        public ActionResult GetDefaultSearchResults(ServiceSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;
            AjaxGrid<Service> grid = GetDefaultAjaxGrid<Service>();
            return View("Search/_ServiceSearchResults", grid);
        }

        public JsonResult GetSearchResults(ServiceSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<Service> grid = GetAjaxGridByList<Service>(this._repositoty.GetServicesBySearchCriteria(searchCriteria));
            return Json(new { Html = grid.ToJson("Search/_ServiceSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSearchResultsPaged(ServiceSearchCriteriaModel searchCriteria, int? page)
        {
            ViewBag.IsModalDialog = searchCriteria.IsModalDialog;

            AjaxGrid<Service> grid = GetAjaxGridByList<Service>(this._repositoty.GetServicesBySearchCriteria(searchCriteria), page.HasValue ? page.Value : 1, 5);
            return Json(new { Html = grid.ToJson("Search/_ServiceSearchResults", this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        #endregion
     
        public ActionResult PricingDetails(long? pServiceID, int? pContractTermTypeCodeID)
        {
            try
            {
                ServicePricing servicePricing = null;
                Service service = null;
                                   
                if (!pServiceID.HasValue
                    || !pContractTermTypeCodeID.HasValue)
                {
                    servicePricing = new ServicePricing();
                    servicePricing.ServiceID = pServiceID.HasValue ? pServiceID.Value : 0;
                    this._repositoty.ExecuteBusinessRules(servicePricing, BusinessRulesActionTypeEnum.ObjectMaterialize, string.Empty);                  
                    base.BuildViewBagCollection(servicePricing);
                    return View(servicePricing);
                }

                service = this._repositoty.GetServiceByID(pServiceID.Value);                
                servicePricing = service.CurrentServicePricing(pContractTermTypeCodeID.Value);
                if (servicePricing == null)
                {
                    servicePricing = this._repositoty.CreateServicePricing();
                    servicePricing.ServiceID = service.ServiceID;
                    servicePricing.Service = service;
                    servicePricing.ContractTermTypeCodeID = pContractTermTypeCodeID.Value;
                }
                
                base.BuildViewBagCollection(servicePricing);
                return View(servicePricing);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PricingDetails(ServicePricing servicePricing)
        {
            try
            {
                this.ExecutePreSaveConditions(servicePricing, this._repositoty, servicePricing.ServicePricingDetailXRefs.ToList());

                if (!this.IsModelValid)
                    return View(servicePricing);

                this._repositoty.UpdateServicePricingDetails(servicePricing);
                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus == ApplicationKeys.SaveUnsucessful)
                    this.BindValidationErrors(servicePricing);
                else
                    return View("_ServiceSuccess");

            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(servicePricing);            
        }

        public ActionResult CreateServicePricingDetailXRef(int index)
        {
            try
            {
                ServicePricingDetailXRef servicePricingDetailXRef = this._repositoty.CreateServicePricingDetailXRef();
                ViewData["ServicePricingDetailXRefIndex"] = index;
                this.BuildViewBagCollection(servicePricingDetailXRef);
                return PartialView("_PricingDetailXRef", servicePricingDetailXRef);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return PartialView("_PricingDetailXRef", null);
        }

        public ActionResult PricingDetailXRefList(long pserviceID, int pContractTermTypeCodeID, int? pVersion)
        {
            Service service = this._repositoty.GetServiceByID(pserviceID);
            ServicePricing servicePricing = null;

            if (pVersion.HasValue)
            {
                servicePricing = service.GetServicePricing(pContractTermTypeCodeID, pVersion.Value);
            }
            else
            {
                servicePricing = service.CurrentServicePricing(pContractTermTypeCodeID);
            }

            List<ServicePricingDetailXRef> currentPricingDetailXRefs = (servicePricing == null) ? new List<ServicePricingDetailXRef>() : servicePricing.CurrentServicePricingDetailXRefs;
            return View("_PricingDetailXRefList", currentPricingDetailXRefs);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._repositoty.Dispose();                
            }

            base.Dispose(disposing);
        }
    }
}
