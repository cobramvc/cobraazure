﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Entity.ContractModels;
using Entity.UnitOfWork.ContractRepo;
using Entity.UnitOfWork;
using Infrastructure;
using COBRA.Models;
using Billing;
using Infrastructure.Resources;
using Billing.BillingStrategy;
using Grid.Mvc.Ajax.GridExtensions;

namespace COBRA.MVC.Controllers
{
    public class ContractController : ContractControllerBase
    {        
        
        public ContractController(ICustomerContractRepository repositoryInstance)
        {
            this._repositoty = repositoryInstance;
        }

        public ContractController()
        {
            _uow = new CustomerContractUnitOfWork();
            this._repositoty = new CustomerContractRepository(_uow);
            this._codeListrepositoty = new CodeListDetailRepository(_uow);
        }        

        public ActionResult CreateOrEdit(long? pContractID, long? pCustomerID)
        {
            CustomerContract customerContract = null;
            ViewBag.Caption = "ADD CONTRACT";

            if (!pContractID.HasValue)
            {
                customerContract = new CustomerContract();
                this._repositoty.ExecuteBusinessRules(customerContract, BusinessRulesActionTypeEnum.ObjectMaterialize);
                this.BuildViewBagCollection(null);
                return View(customerContract);
            }

            if (pContractID.Value == 0)
            {
                customerContract = this._repositoty.CreateCustomerContract(pCustomerID.Value);
                CustomerContractProductXRefStage contractProductXRefStage = this._repositoty.CreateCustomerContractProductXRefStage();
                customerContract.CustomerContractProductXRefStages.Add(contractProductXRefStage);
                ViewBag.Caption = "ADD CONTRACT - " + customerContract.CustomerContractID.ToString();
            }
            else
            {
                customerContract = this._repositoty.GetCustomerContractByID(pContractID.Value);
                customerContract.ActionType = CustomerContractActionType.InstanceChange;
                this._repositoty.SetDraftCustomerContractProductXRefStage(customerContract);
                ViewBag.Caption = "MODIFY CONTRACT - " + customerContract.CustomerContractID.ToString();
            }

            this.BuildViewBagCollection(customerContract);
            return View(customerContract);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOrEdit(CustomerContract customerContract)
        {            
            try
            {
                Session["Action"] = "";

                if (customerContract.ActiveDraftCustomerContractProductXRefStages.Count > 0
                    && customerContract.ActiveDraftCustomerContractProductXRefStages.First().StatusCodeID == ContractProductStatusKey.Pending_Upgrade)
                {
                    Session["Action"] = "Upgrade";
                }

                this.ExecutePreSaveConditions(customerContract, this._repositoty, customerContract.CustomerContractProductXRefStages.ToList());

                if (!this.IsModelValid)
                    return View(customerContract);

                if (customerContract.RecordVersion == 0)
                    this._repositoty.AddCustomerContract(customerContract);
                else
                {
                    this._repositoty.UpdateCustomerContract(customerContract);                    
                }
                    

                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus != ApplicationKeys.SaveUnsucessful)
                {
                    CustomerContract dbCustomerContract = this._repositoty.GetCustomerContractByID(customerContract.CustomerContractID);
                    return View("_ContractSummary", dbCustomerContract);                    
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(customerContract);
        }
     
        public ActionResult ContractBillingSummary(long customerContractID)
        {
            CustomerContract customerContract = this._repositoty.GetCustomerContractByID(customerContractID);
            CustomerContractProductXRefStage stage = customerContract.ActiveDraftCustomerContractProductXRefStages.First();
            ViewBag.Product = stage.Product.Description;
            ViewBag.Instances = stage.Instances;

            // Generate Billing Summary Report
            BillingSearchCriteriaModel searchCriteria = new BillingSearchCriteriaModel();
            searchCriteria.CustomerContractID = customerContractID;
            searchCriteria.BillingTypeCodeID = CustomerBillingTypeCodeKey.ContractSummary;
            searchCriteria.ContractTermTypeCodeID = customerContract.ContractTermTypeCodeID;
            searchCriteria.BeginDate = stage.BeginDate.Value;

            BaseBllingStrategy billingStrategy = BillingStrategyFactory.Instance.GetBillingStrategy(searchCriteria);
            Entity.BillingModels.CustomerBilling billing = billingStrategy.LoadContractBillingDetails();

            // Display Contract Summary View
            return View("Contract/_ContractBillingSummary", billing);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContractSummary(CustomerContract customerContract)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this._repositoty.ConfirmContract(customerContract);
                    int saveStatus = this._repositoty.SaveChanges();

                    if (saveStatus != ApplicationKeys.SaveUnsucessful)
                        return View("_ContractSuccess");

                }
                catch (Exception ex)
                {
                    ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
                }
            }

            this.BuildViewBagCollection(customerContract);
            this.BindValidationErrors(customerContract);
            return View("_ContractSummary", customerContract);
        }

        public ActionResult LoadCustomerContractProductXRef(long pProductID, long pCustomerID)
        {
            try
            {
               
                CustomerContractProductXRefStage contractProductXRefStage = new CustomerContractProductXRefStage();
                contractProductXRefStage.ProductID = pProductID;
                this._repositoty.LoadContractPricingDetails(contractProductXRefStage, pCustomerID);                
                return PartialView("_ProductPricingList", contractProductXRefStage.ProductPricings);

            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return PartialView("_ProductPricingList", null);
        }

        #region Search Contract
        
        public ActionResult SearchContract(long? pCustomerID)
        {
            ContractSearchCriteriaModel searchCriteria = null;
            searchCriteria = new ContractSearchCriteriaModel();
            ViewBag.ActionType = string.Empty;
            if (pCustomerID.HasValue)
            {
                searchCriteria.CustomerID = pCustomerID.Value;
            }

            return View("Search/_SearchContract", searchCriteria);
        }

        public ActionResult SearchContractByActionType(string pActionType)
        {
            ContractSearchCriteriaModel searchCriteria = null;
            searchCriteria = new ContractSearchCriteriaModel();

            searchCriteria.ActionType = pActionType;
            if (TempData.Keys.Contains("ActionType"))
                TempData["ActionType"] = searchCriteria.ActionType;
            else
                TempData.Add("ActionType",  searchCriteria.ActionType);
            return View("Search/_SearchContract", searchCriteria);
        }

        public ActionResult GetDefaultSearchResults(ContractSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.ContractIsModalDialog;
            
            AjaxGrid<CustomerContract> grid = GetDefaultAjaxGrid<CustomerContract>();
            
            object actionType = null;
            TempData.TryGetValue("ActionType", out actionType);

            if (actionType != null)
                searchCriteria.ActionType = actionType.ToString();

            ViewBag.ActionType = searchCriteria.ActionType;

            //AjaxGrid<CustomerContract> grid = GetAjaxGridByList<CustomerContract>(this._repositoty.GetCustomerContractsBySearchCriteria(searchCriteria));

            return View(GetViewName(searchCriteria.ActionType), grid);
        }

        public JsonResult GetSearchResults(ContractSearchCriteriaModel searchCriteria)
        {
            ViewBag.IsModalDialog = searchCriteria.ContractIsModalDialog;
            ViewBag.ActionType = searchCriteria.ActionType;
            
            AjaxGrid<CustomerContract> grid = GetAjaxGridByList<CustomerContract>(this._repositoty.GetCustomerContractsBySearchCriteria(searchCriteria));            
            return Json(new { Html = grid.ToJson(GetViewName(searchCriteria.ActionType), this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSearchResultsPaged(ContractSearchCriteriaModel searchCriteria, int? page)
        {
            ViewBag.IsModalDialog = searchCriteria.ContractIsModalDialog;
            ViewBag.ActionType = searchCriteria.ActionType;

            AjaxGrid<CustomerContract> grid = GetAjaxGridByList<CustomerContract>(this._repositoty.GetCustomerContractsBySearchCriteria(searchCriteria), page.HasValue ? page.Value : 1, 5);
            return Json(new { Html = grid.ToJson(GetViewName(searchCriteria.ActionType), this), grid.HasItems }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        
        [HttpPost]
        public ActionResult ConfirmContractStatusChange(long pCustomerContractID)
        {
            try
            {
                string updatedStatus = this._repositoty.ConfirmContractStatusChange(pCustomerContractID);
                return Content(updatedStatus);
            }
            catch(Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return  Content("Errored");
        }

        [HttpPost]
        public ActionResult CancelContractStatusChange(long pCustomerContractID)
        {
            try
            {
                string updatedStatus = this._repositoty.CancelContractStatusChange(pCustomerContractID);
                return Content(updatedStatus);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return Content("Errored");
        }

        public ActionResult ContractStatusUpdate(long pCustomerContractID, string pActionType)
        {
            CustomerContract contract = this._repositoty.GetCustomerContractByID(pCustomerContractID);
            
            if (pActionType == "Disconnect")
                contract.StatusCodeID = ContractProductStatusKey.Pending_Disconnect;
            else if (pActionType == "Suspend")
                contract.StatusCodeID = ContractProductStatusKey.Pending_Suspend;
            else if (pActionType == "Reconnect")
                contract.StatusCodeID = ContractProductStatusKey.Pending_Activation;
            else if (pActionType == "Terminate")
            {
                contract.StatusCodeID = ContractProductStatusKey.Pending_Terminate;

                ContractSearchCriteriaModel searchCriteria = new ContractSearchCriteriaModel();
                searchCriteria.CustomerID = contract.CustomerID;
                List<CustomerContract> contracts = this._repositoty.GetCustomerContractsBySearchCriteria(searchCriteria);

                // If there is atleast one contract for this customer which is NOT terminated
                if (!contracts.Exists(c => c.CustomerContractID != pCustomerContractID
                    && (!c.StatusCodeID.HasValue
                    || c.StatusCodeID.Value != ContractProductStatusKey.Terminate)))
                {
                    // Check if Customer has Child customers tagged, DO NOT allow to terminate this contract
                    contract.ActiveChildCustomers = this._repositoty.GetActiveContractChildCustomersByID(contract.CustomerID);
                    if (contract.ActiveChildCustomers.Count > 0)
                        contract.CanWrite = false;
                }                
            }                

            ViewBag.ActionType = pActionType;
            this.BuildViewBagCollection(contract);
            return View(contract);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContractStatusUpdate(CustomerContract customerContract)
        {
            try
            {
                customerContract.ActionType = CustomerContractActionType.StatusChange;
                this.ExecutePreSaveConditions<CustomerContractProductXRefStage>(customerContract, this._repositoty, null);

                string actionType = string.Empty;

                if (customerContract.StatusCodeID == ContractProductStatusKey.Pending_Disconnect)
                    actionType = "Disconnect";
                else if (customerContract.StatusCodeID == ContractProductStatusKey.Pending_Suspend)
                    actionType = "Suspend";                
                else if(customerContract.StatusCodeID == ContractProductStatusKey.Pending_Activation)
                    actionType = "Reconnect";              
                else
                    actionType = "Terminate";

                ViewBag.ActionType = actionType;

                if (!this.IsModelValid)
                    return View(customerContract);

                this._repositoty.InitiateContractStatusChange(customerContract);
                int saveStatus = this._repositoty.SaveChanges();

                if (saveStatus != ApplicationKeys.SaveUnsucessful)
                {
                    return View("_ContractSuccess", customerContract);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }     
               
            this.BuildViewBagCollection(customerContract);
            return View(customerContract);
        }

        public ActionResult UpgradeContract(long pCustomerContractID)
        {
            CustomerContract dbCustomerContract = this._repositoty.GetCustomerContractByID(pCustomerContractID);

            Session["Action"] = "Upgrade";
            Session["ContractTerm"] = dbCustomerContract.ContractTermTypeCodeID;

            CustomerContract upgradeContract = this._repositoty.CreateCustomerContract(dbCustomerContract.CustomerID);
            upgradeContract.ContractID = dbCustomerContract.ContractID;

            CustomerContractProductXRefStage stage = this._repositoty.CreateCustomerContractProductXRefStage();
            stage.StatusCodeID = ContractProductStatusKey.Pending_Upgrade;
            upgradeContract.CustomerContractProductXRefStages.Add(stage);
            
            this.BuildViewBagCollection(upgradeContract);
            return View("CreateOrEdit", upgradeContract);
        }


        public ActionResult Details(long pContractID)
        {
            CustomerContract contract = this._repositoty.GetCustomerContractByID(pContractID);
            return View(contract);
        }

        public ActionResult CustomerContracts(long pCustomerID)
        {
            ContractSearchCriteriaModel searchCriteria = new ContractSearchCriteriaModel();
            searchCriteria.CustomerID = pCustomerID;
            List<CustomerContract> contracts = this._repositoty.GetCustomerContractsBySearchCriteria(searchCriteria);
            return View("Contract/_CustomerContractsDisplay",contracts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._repositoty.Dispose();
            }
            base.Dispose(disposing);
        }

        private string GetViewName(string actionType)
        {
            string viewName = "Search/_ContractSearchResults";
            if (actionType == "StatusConfirm")
            {
                viewName = "_ContractListForProvisioning";
            }
            else if (actionType == "Upgrade")
            {
                viewName = "_ContractListForUpgrade";
            }
            else if (!string.IsNullOrEmpty(actionType))
            {
                viewName = "_ContractListForStatusChange";
            }
            return viewName;
        }      
    }
}
