﻿using Entity.AdminModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.AdminRepo;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COBRA.MVC
{
    public class AdminController : AdminControllerBase
    {
        public AdminController()
        {
            _uow = new AdminUnitOfWork();
            this._taxRepositoty = new TaxRepository(_uow);
            this._codeListrepositoty = new CodeListDetailRepository(_uow);
        }

        public ActionResult Tax(int? pTaxID)
        {
            try
            {
                Tax tax = null;
                
                if (pTaxID.HasValue
                    && pTaxID.Value > 0)
                {
                    tax = this._taxRepositoty.GetTaxByID(pTaxID.Value); 
                }
                else
                {
                    tax = this._taxRepositoty.CreateTax();
                }

                this.BuildViewBagCollection(tax);
                return View(tax);
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Tax(Tax tax)
        {
            try
            {                
                this.ExecutePreSaveConditions<Tax>(tax, this._taxRepositoty, null);

                if (!this.IsModelValid)
                    return View(tax);

                if (tax.RecordVersion == 0) // New Tax
                {
                    this._taxRepositoty.AddTax(tax);
                }
                else
                {
                    this._taxRepositoty.UpdateTax(tax);
                }

                int status = this._taxRepositoty.SaveChanges();
                if (status == ApplicationKeys.SaveUnsucessful)
                {
                    // Save should not unsucessful as the validations are done prior to invoking SaveChanges method 
                    this.BindValidationErrors(tax);
                    return View(tax);
                }
                else
                {
                    return View("_TaxSucess", tax);
                }            
            }
            catch (Exception ex)
            {
                ExceptionHandlingManager.Instance.HandleException(ex, ExceptionPolicyName.Controller);
            }

            return View(tax);
        }

        public ActionResult SearchTax()
        {
            return View(this._taxRepositoty.GetTaxes());
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._taxRepositoty.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}