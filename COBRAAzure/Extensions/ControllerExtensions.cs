﻿using Entity;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Entity.Model;
using Entity.ModelHelpers;

namespace COBRA.MVC
{
    public static class ControllerExtensions
    {
        //public static void BindValidationErrors(this Controller controller, AbstractEntity entity, string childObjPrefix = "")
        //{
        //    entity.ValidationErrors.ForEach(validation =>
        //    {
        //        controller.ModelState.AddModelError(childObjPrefix + validation.Key, validation.Message);
        //    });                                      
        //}

        public static void HandleConcurrencyErrors(this Controller controller, AbstractEntity entity, DbUpdateConcurrencyException exception)
        {
            exception.Entries.ToList()
                        .ForEach(entityEntry =>
                        {
                            DbPropertyValues databaseValues = entityEntry.GetDatabaseValues();
                            DbPropertyValues currentValues = entityEntry.OriginalValues;                                                      

                            databaseValues.PropertyNames.ToList().ForEach(property =>
                            {                                
                                if (databaseValues.GetValue<object>(property).ToString() != currentValues.GetValue<object>(property).ToString())
                                {
                                    entity.ValidationErrors.Add(new ValidationResult("Original value : " + databaseValues.GetValue<object>(property).ToString(), entityEntry.Entity, property, "ConcurrencyValidation", null));
                                }
                            });
                        });
        }        
    }
}