﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.WebPages;

namespace COBRA.MVC
{
    public static class HtmlHelperExtensions
    {
        #region Custom Controls methods
        public static MvcHtmlString CustomDropDownFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, TModel model, SelectList selectList, int index = -1)
        {
            Func<TModel, TProperty> compliedExpression = expression.Compile();
            string dropdownValue = (model == null) ? string.Empty : compliedExpression(model).ToString();

            string propertyName = ExpressionHelpers.Instance.GetPropertyName(expression);
            string modelName = ExpressionHelpers.Instance.GetModelName(expression);

            string propertyID = propertyName;
                        
            if (index >= 0)
            {
                propertyID = modelName + "s[" + index + "]." + propertyID;
            }
            

            return htmlHelper.MapSelectedValue(dropdownValue,htmlHelper.DropDownList(propertyID, selectList, htmlHelper.GetHTMLAttributes(expression, index)));
        }

        public static MvcHtmlString CustomDropDownFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, TModel model, int index = -1) where TModel : AbstractEntity
        {
            Func<TModel, TProperty> compliedExpression = expression.Compile();
            string dropdownValue = (model == null) ? string.Empty : compliedExpression(model).ToString();

            string propertyName = ExpressionHelpers.Instance.GetPropertyName(expression);
            string modelName = ExpressionHelpers.Instance.GetModelName(expression);

            string propertyID = propertyName;

            if (index >= 0)
            {
                propertyID = modelName + "s[" + index + "]." + propertyID;
            }

            SelectList dropDownSelectList = (SelectList)htmlHelper.ViewBag.EmptyList;

           
            SelectListPropertyInfo selectListPropertyInfo = GenericHelpers.Instance.GetSelectListPropertyInfo(model, propertyName);            
            Check.IsEqual(selectListPropertyInfo == null, "This property : " + propertyName + " is not tagged as SelectList property");

            string viewBagname = GenericHelpers.Instance.GetSelectListNameByProperty(modelName, propertyName, selectListPropertyInfo.DependencyCodeID >= 0 ? selectListPropertyInfo.DependencyCodeID.ToString() : "");
            if (htmlHelper.ViewData.ContainsKey(viewBagname))
                dropDownSelectList = (SelectList)htmlHelper.ViewData[viewBagname];

            var currentModel = ((WebViewPage)WebPageContext.Current.Page).Model;
            bool isReadOnly = IsReadOnlyProperty((AbstractEntity)currentModel, propertyName);
            
            if (isReadOnly)
            {
                IDictionary<string, object> htmlAttributes = new Dictionary<string, object>();
                IDictionary<string, object> currentHtmlAttribues = htmlHelper.GetHTMLAttributes(expression, index);
                if (currentHtmlAttribues != null)
                {                    
                    currentHtmlAttribues.ToList().ForEach(keyValuePair =>
                    {
                        htmlAttributes.Add(keyValuePair.Key, keyValuePair.Value);
                    });                    
                }

                htmlAttributes.Add("disabled", "disabled");
                return htmlHelper.MapSelectedValue(dropdownValue, htmlHelper.DropDownList(propertyID, dropDownSelectList, htmlAttributes));   
            }
            
            return htmlHelper.MapSelectedValue(dropdownValue, htmlHelper.DropDownList(propertyID, dropDownSelectList, htmlHelper.GetHTMLAttributes(expression, index)));
        }

        public static MvcHtmlString CustomEditorFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, bool isReadOnly, int index = -1) where TModel : AbstractEntity
        {
            string propertyName = ExpressionHelpers.Instance.GetPropertyName(expression);
            string modelName = ExpressionHelpers.Instance.GetModelName(expression);
            
            string propertyID = propertyName;
                        
            if (index >= 0)
            {
                propertyID = modelName + "s[" + index + "]." + propertyID;
            }

            if (!isReadOnly)
            {
                var currentModel = ((WebViewPage)WebPageContext.Current.Page).Model;
                isReadOnly = IsReadOnlyProperty((AbstractEntity)currentModel, propertyName);
            }
            
            if (isReadOnly)
            {
                return htmlHelper.TextBoxFor(expression, null, new { @readonly = "readonly" });
            }
            else
            {
                IDictionary<string, object> attributes = htmlHelper.GetHTMLAttributes(expression, index);
                MvcHtmlString htmlString = (attributes == null ? htmlHelper.EditorFor(expression, null, propertyID) : htmlHelper.EditorFor(expression, null, propertyID, attributes));                
                return htmlString;
            }                        
        }

        //public static MvcHtmlString CustomTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, TModel model, int index = -1)
        //{
        //    string propertyName = ExpressionHelpers.Instance.GetPropertyName(expression);
        //    string modelName = ExpressionHelpers.Instance.GetModelName(expression);

        //    string propertyID = propertyName;

        //    if (index >= 0)
        //    {
        //        propertyID = modelName + "s[" + index + "]." + propertyID;
        //    }

        //    IDictionary<string, object> attributes = htmlHelper.GetHTMLAttributes(expression, index);
        //    Func<TModel, TProperty> compliedExpression = expression.Compile();
        //    var textBoxValue = compliedExpression(model);
        //    MvcHtmlString htmlString = htmlHelper.TextBox(propertyID, textBoxValue, attributes);
        //    return htmlString;
        //}

        public static MvcHtmlString CustomHiddenFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, TModel model, int index = -1)
        {
            string propertyName = ExpressionHelpers.Instance.GetPropertyName(expression);
            string modelName = ExpressionHelpers.Instance.GetModelName(expression);
            string propertyID = propertyName;
            if (index >= 0)
            {
                propertyID = modelName + "s[" + index + "]." + propertyID;
            }

            Func<TModel, TProperty> compliedExpression = expression.Compile();
            if(model == null)
                return htmlHelper.Hidden(propertyID);

            var hiddenValue = compliedExpression(model);
            return htmlHelper.Hidden(propertyID, hiddenValue);
        }

        public static MvcHtmlString CustomValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, int index)
        {
            string propertyName = ExpressionHelpers.Instance.GetPropertyName(expression);
            string modelName = ExpressionHelpers.Instance.GetModelName(expression);

            string propertyID = propertyName;

            if (index >= 0)
            {
                propertyID = modelName + "s[" + index + "]." + propertyID;
            }

            return htmlHelper.ValidationMessage(propertyID);
        }

        public static MvcHtmlString GenerateHiddenControlsForReadOnly<TModel>(this HtmlHelper<TModel> htmlHelper, TModel model, int index = -1) where TModel : AbstractEntity
        {
            if (model == null)
                return MvcHtmlString.Empty;

            MvcHtmlString htmlString = MvcHtmlString.Create(string.Empty);
                        
            string propertyName = string.Empty;
            string modelName = typeof(TModel).Name;
            string propertyID = string.Empty; 
           Type modelType = typeof(TModel);

          List<MvcHtmlString> items = new List<MvcHtmlString>() ;
            model.ReadOnlyProperties.ForEach(readOnlyProperty =>
                {
                    if (index >= 0)
                    {
                        propertyID = modelName + "s[" + index + "]." + readOnlyProperty;
                    }
                    else
                    {
                        propertyID = readOnlyProperty;
                    }

                    var hiddenValue = modelType.GetProperty(readOnlyProperty).GetValue(model);
                    items.Add(htmlHelper.Hidden(propertyID, hiddenValue));
                });

            return Concat(items);
        }

        public static string GetModelPrefix<TModel>(this HtmlHelper html, TModel model, int index)
        {
            return ExpressionHelpers.Instance.GetModelPrefix(model, index);          
        }

        public static string ClientIDFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, int index)
        {
            string clientID = string.Empty;
            MemberExpression memberExpr = expression.Body as MemberExpression ?? ((UnaryExpression)expression.Body).Operand as MemberExpression;
            clientID = ExpressionHelpers.Instance.GetModelName(expression) + "s_" + index + "__" + ExpressionHelpers.Instance.GetPropertyName(expression);
            return clientID;
        }

        private static MvcHtmlString MapSelectedValue(this HtmlHelper html, string selectedValue, MvcHtmlString mvcHTMLString)
        {
            string currentHTMLString = mvcHTMLString.ToHtmlString();

            if (!string.IsNullOrEmpty(selectedValue))
            {
                currentHTMLString = currentHTMLString.Replace("<option value=\"" + selectedValue + "\">", "<option value=\"" + selectedValue + "\" selected>");
            }

            return MvcHtmlString.Create(currentHTMLString);
        }

        private static MvcHtmlString Concat(List<MvcHtmlString> items) 
        { 
            var sb = new StringBuilder(); 
            foreach (var item in items.Where(i => i != null)) 
                sb.Append(item.ToHtmlString());

            return MvcHtmlString.Create(sb.ToString()); 
        }

        private static bool IsReadOnlyProperty(AbstractEntity model, string propertyName)
        {
            string readOnlyPropertyName = string.Empty;

            if (model == null)
                return false;
           
            readOnlyPropertyName = model.ReadOnlyProperties.FirstOrDefault(property => property == propertyName);

            if (!string.IsNullOrEmpty(readOnlyPropertyName))
                return true;

            return false;
        }
        #endregion

        #region HTML Attributes methods
        public static void AddCustomHTMLAttributes<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string attributeKey, object attributeValue, int index = - 1)
        {
            html.AddDefualtHTMLAttributes(expression);
            
            string propertyMetaDataKey = ExpressionHelpers.Instance.GetModelName(expression) + "_" + ExpressionHelpers.Instance.GetPropertyName(expression) + "_MetaData";
            string customPropertyMetaDataKey = "Custom_" + propertyMetaDataKey;

            if(index >= 0)
                customPropertyMetaDataKey = customPropertyMetaDataKey + "_" + index;

            IDictionary<string, object> customAttributes = AttributeHelpers.Instance.GetAttributes(customPropertyMetaDataKey);
            if (customAttributes == null)
            {
                customAttributes = new Dictionary<string, object>();
                AttributeHelpers.Instance.AddAttributes(customPropertyMetaDataKey, customAttributes);
            }

            if (!customAttributes.ContainsKey(attributeKey))
            {
                customAttributes.Add(attributeKey, attributeValue);
            }

            AttributeHelpers.Instance.MergeAttributes(customPropertyMetaDataKey, propertyMetaDataKey);
        }

        public static void AddDefualtHTMLAttributes<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            string propertyName = ExpressionHelpers.Instance.GetPropertyName(expression);
            string propertyMetaDataKey = ExpressionHelpers.Instance.GetModelName(expression) + "_" + propertyName + "_MetaData";
            IDictionary<string, object> defualtAttributes = AttributeHelpers.Instance.GetAttributes(propertyMetaDataKey);
            if (defualtAttributes == null)
            {
                // Get Default Attributes of a property
                ViewDataDictionary<TModel> viewData = html.ViewData;
                var metaData = ModelMetadata.FromStringExpression(propertyName, viewData);
                defualtAttributes = html.GetUnobtrusiveValidationAttributes(propertyName, metaData);
                if (defualtAttributes.Count > 0)
                    AttributeHelpers.Instance.AddAttributes(propertyMetaDataKey, defualtAttributes);
            }
        }

        public static IDictionary<string, object> GetHTMLAttributes<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, int index = -1)
        {
            string propertyMetaDataKey = "Custom_" + ExpressionHelpers.Instance.GetModelName(expression) + "_" + ExpressionHelpers.Instance.GetPropertyName(expression) + "_MetaData";
            
            if(index >= 0)
                propertyMetaDataKey = propertyMetaDataKey + "_" + index;

            IDictionary<string, object> attributeCol = AttributeHelpers.Instance.GetAttributes(propertyMetaDataKey);

            if (attributeCol == null
                && index >= 0)
            {
                // Get Defualt attributes if there are no custom attributes
                propertyMetaDataKey = ExpressionHelpers.Instance.GetModelName(expression) + "_" + ExpressionHelpers.Instance.GetPropertyName(expression) + "_MetaData";
                attributeCol = AttributeHelpers.Instance.GetAttributes(propertyMetaDataKey);
            }

            return attributeCol;
        }

        public static void RemoveCustomHTMLAttributes<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string attributeKey, int index = - 1)
        {            
            string propertyMetaDataKey = ExpressionHelpers.Instance.GetModelName(expression) + "_" + ExpressionHelpers.Instance.GetPropertyName(expression) + "_MetaData";
            string customPropertyMetaDataKey = "Custom_" + propertyMetaDataKey;

            if (index >= 0)
                customPropertyMetaDataKey = customPropertyMetaDataKey + "_" + index;

            AttributeHelpers.Instance.RemoveAttributes(customPropertyMetaDataKey, attributeKey);
        }
        #endregion
    }
}