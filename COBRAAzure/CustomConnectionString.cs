﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace COBRA.MVC.Models
{
    public static class CustomConnectionString
    {
        public static string GetConnectionString()
        {
            //string providerName = "System.Data.SqlClient";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["COBRADbContext"].ConnectionString;

            //SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            //builder.ConnectionString = connectionString;

            var sqlConnFact = new SqlConnectionFactory();
            var sqlConn = sqlConnFact.CreateConnection(connectionString);
            return sqlConn.ConnectionString;
        }
    }
}