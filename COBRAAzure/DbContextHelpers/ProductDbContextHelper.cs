﻿using Entity.UnitOfWork;
using Entity.UnitOfWork.ProductRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COBRA.MVC
{
    public class ProductDbContextHelper : IDisposable
    {
        protected IProductRepository _productRepositoty = null;
        protected IServiceGroupRepository _serviceGroupRepositoty = null;
        protected IServiceRepository _serviceRepositoty = null;    
        protected IUnitOfWork _uow = null;

        public ProductDbContextHelper()
        {
            _uow = new ProductUnitOfWork();            
        }

        public IProductRepository ProductRepositoryInstance
        {
            get
            {
                if (this._productRepositoty == null)
                    this._productRepositoty = new ProductRepository(_uow);

                return this._productRepositoty;
            }
        }

        public IServiceGroupRepository ServiceGroupRepositoryInstance
        {
            get
            {
                if (this._serviceGroupRepositoty == null)
                    this._serviceGroupRepositoty = new ServiceGroupRepository(_uow);

                return this._serviceGroupRepositoty;
            }
        }

        public IServiceRepository ServiceRepositoryInstance
        {
            get
            {
                if (this._serviceRepositoty == null)
                    this._serviceRepositoty = new ServiceRepository(_uow);

                return this._serviceRepositoty;
            }
        }

        public void Dispose()
        {
            if (this._productRepositoty != null)
                this._productRepositoty.Dispose();
            if (this._serviceGroupRepositoty != null)
                this._serviceGroupRepositoty.Dispose();
            if (this._serviceRepositoty != null)
                this._serviceRepositoty.Dispose();
        }
    }
}