﻿using System.Web;
using System.Web.Optimization;

namespace COBRAAzure
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery.ui").Include(
                        "~/Scripts/ui/jquery.ui.core.js"
                        , "~/Scripts/ui/jquery.ui.widget.js"
                        , "~/Scripts/ui/jquery.ui.mouse.js"
                        , "~/Scripts/ui/jquery.ui.button.js"
                        , "~/Scripts/ui/jquery.ui.draggable.js"
                        , "~/Scripts/ui/jquery.ui.position.js"
                        , "~/Scripts/ui/jquery.ui.resizable.js"
                        , "~/Scripts/ui/jquery.ui.dialog.js"
                        , "~/Scripts/ui/jquery.ui.effect.js"
                        , "~/Scripts/OpenDialog.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jquery.GridMVC").Include(
                        "~/Scripts/app/api.js"
                        , "~/Scriptsapp/ui.js"
                        , "~/Scripts/gridmvc.min.js"
                        , "~/Scripts/gridmvc.js"
                        ,  "~/Scripts/URI.js"
                        , "~/Scripts/gridmvc-ext.js"
                        , "~/Scripts/ui/jquery.ui.resizable.js"
                        , "~/Scripts/ui/jquery.ui.dialog.js"
                        , "~/Scripts/ui/jquery.ui.effect.js"
                        , "~/Scripts/OpenDialog.js"
                         ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

           
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/ladda-bootstrap/spin.js",
                       "~/Scripts/ladda-bootstrap/ladda.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css"
                      , "~/Content/COBRASite.css"
                      , "~/Content/COBRATable.css"
                      , "~/Content/COBRAMenu.css"
                      , "~/Scripts/themes/base/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/Content/GridMVCcss").Include(
                      "~/Content/Gridmvc.css",
                      "~/Content/ladda-bootstrap/ladda-themeless.min.css"));
        }
    }
}
