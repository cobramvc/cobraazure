﻿using COBRA.MVC;
using System.Web;
using System.Web.Mvc;

namespace COBRAAzure
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomUIExceptionFilter());
            filters.Add(new AuthenticationFilterAttribute());
        }
    }
}
