﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.AdminModels
{
    public static class AdminConnectionString
    {
        public static string GetConnectionString()
        {
            string providerName = "System.Data.SqlClient";
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["COBRADbContext"].ConnectionString;

            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder();

            //Set the provider name.
            entityBuilder.Provider = providerName;

            // Set the provider-specific connection string.
            entityBuilder.ProviderConnectionString = connectionString;

            // Set the Metadata location.
            entityBuilder.Metadata = @"res://*/AdminModels.csdl|
                            res://*/AdminModels.ssdl|
                            res://*/AdminModels.msl";

            return entityBuilder.ToString();
        }
    }
}
