﻿using Entity.ModelHelpers;
using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.AdminModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(TaxrMetaData))]
    public partial class Tax : BaseEntity<Tax>
    {
        #region Custom Validations

        [SelfValidation]
        internal void ValidateEfectiveDate(DbContext context)
        {
            if (this.DbObjectBeforeChanged != null
                && this.DbObjectBeforeChanged.EffectiveDate >= this.EffectiveDate)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date should be greater than " + this.DbObjectBeforeChanged.EffectiveDate.ToShortDateString(), this, "EffectiveDate", "ValidateEfectiveDate", null));
            }
        }

        #endregion
    }

    public class TaxrMetaData
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [SelectList(CodeListKey.TaxTypeCodeList)]
        public int TypeCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        [SelectList(CodeListKey.StateList)]
        public Nullable<int> StateCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        [System.ComponentModel.DataAnnotations.Range(0, 100.0,ErrorMessage = "Out of range")]
        public decimal Tax1 { get; set; }

        [System.ComponentModel.DataAnnotations.Range(0, 100.0, ErrorMessage = "Out of range")]
        public Nullable<decimal> TaxOnTax { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "This field is required")]
        public System.DateTime EffectiveDate { get; set; }
    }
}
