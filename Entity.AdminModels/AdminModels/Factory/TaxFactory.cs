﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.AdminModels
{
    public  class TaxFactory : EntityFactory<TableCurrentKey>
    {
        private static TaxFactory instance = new TaxFactory();

        public static TaxFactory Instance
        {
            get { return instance; }
        }

        public Tax CreateTax(DbContext dbContext)
        {
            Tax newTax = new Tax();
            newTax.TaxID = Convert.ToInt32(base.GetPrimaryKey("Tax", dbContext).CurrentKey);
            newTax.RecordStatus = true;
            newTax.RecordVersion = 0;

            return newTax;
        }

    }
}
