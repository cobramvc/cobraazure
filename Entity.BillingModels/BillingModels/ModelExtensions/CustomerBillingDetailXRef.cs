﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.BillingModels
{
    public partial class CustomerBillingDetailXRef : BaseEntity<CustomerBillingDetailXRef>
    {
        [CustomProperty]
        public int Instances
        {
            get
            {
                if (this.CustomerContractProductXRef != null)
                    return this.CustomerContractProductXRef.Instances.Value;

                if (this.CustomerContractProductXRefStage != null)
                    return this.CustomerContractProductXRefStage.Instances.Value;

                return 0;
            }
        }

        [CustomProperty]
        public decimal TotalSubscriptionCharges
        {
            get { return this.SubscriptionCharges * this.Instances; }
        }

        [CustomProperty]
        public decimal TotalChargesWithOutTax
        {
            get { return this.TotalSubscriptionCharges + this.OneTimeCharges; }
        }

        [CustomProperty]
        public decimal TaxCharges
        {
            get { return (this.Tax != null) ? Math.Round((this.TotalChargesWithOutTax * this.Tax.Tax1) / 100, 2) : 0; }
        }

        [CustomProperty]
        public decimal TaxOnTaxCharges
        {
            get { return (this.Tax != null && this.Tax.TaxOnTax.HasValue) ? Math.Round((this.TaxCharges * this.Tax.TaxOnTax.Value) / 100, 2) : 0; }
        }

        [CustomProperty]
        public decimal TotalCharges
        {
            get { return this.TotalChargesWithOutTax + this.TaxCharges + this.TaxOnTaxCharges; }
        }
    }
}
