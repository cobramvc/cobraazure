﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.BillingModels
{
    public partial class CustomerBilling : BaseEntity<CustomerBilling>
    {
        [CustomProperty]
        public decimal TotalChargesWithOutDiscount
        {
            get
            {
                return this.CustomerBillingDetailXRefs.ToList().Sum(d => d.TotalCharges);
            }
        }

        [CustomProperty]
        public decimal TotalDiscount
        {
            get
            {
                decimal totalCharges = this.TotalChargesWithOutDiscount;
                return this.CustomerBillingDiscountXRefs.ToList().Sum(d => d.GetDiscountValue(totalCharges));
            }
        }

        [CustomProperty]
        public decimal TotalCharges
        {
            get
            {
                return this.TotalChargesWithOutDiscount - this.TotalDiscount;
            }
        }

        [CustomProperty]
        public string Product
        {
            get
            {
                return this.CustomerContract != null && this.CustomerContract.Product != null ? this.CustomerContract.Product.Name : string.Empty;
            }
        }
    }

}
