﻿using Entity.ModelHelpers;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.BillingModels
{
    public partial class CustomerBillingDiscountXRef : BaseEntity<CustomerBillingDiscountXRef>
    {
        public decimal GetDiscountValue(decimal totalCharges)
        {
            if (this.DiscountDetailXRef != null
                    && this.DiscountDetailXRef.Discount1 != null)
            {
                if (this.DiscountDetailXRef.Discount1.TypeCodeID == DiscountTypeCodeKey.Flat)
                {
                    return this.DiscountDetailXRef.Discount;
                }
                else
                {
                    return Math.Round((totalCharges * this.DiscountDetailXRef.Discount) / 100, 2);
                }
            }

            return 0;
        }

        public string DiscountDescription
        {
            get
            {
                if (this.DiscountDetailXRef != null
                  && this.DiscountDetailXRef.Discount1 != null)
                {
                    return this.DiscountDetailXRef.Discount1.Description;                }

                return string.Empty;
            }          
        }
    }
}
