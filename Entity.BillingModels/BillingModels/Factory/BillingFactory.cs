﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.BillingModels
{
    public class BillingFactory : EntityFactory<TableCurrentKey>
    {
        private static BillingFactory instance = new BillingFactory();

        public static BillingFactory Instance
        {
            get { return instance; }
        }

        public CustomerBilling CreateCustomerBilling(DbContext dbContext)
        {
            CustomerBilling newCustomerBilling = new CustomerBilling();
            newCustomerBilling.CustomerBillingID = base.GetPrimaryKey("CustomerBilling", dbContext).CurrentKey;
            newCustomerBilling.RecordStatus = true;
            newCustomerBilling.RecordVersion = 0;

            return newCustomerBilling;
        }

        public CustomerBillingDetailXRef CreateCustomerBillingDetailXRef(DbContext dbContext)
        {
            CustomerBillingDetailXRef newCustomerBillingDetailXRef = new CustomerBillingDetailXRef();
            newCustomerBillingDetailXRef.CustomerBillingDetailXRefID = base.GetPrimaryKey("CustomerBillingDetailXRef", dbContext).CurrentKey;
            newCustomerBillingDetailXRef.RecordStatus = true;
            newCustomerBillingDetailXRef.RecordVersion = 0;

            return newCustomerBillingDetailXRef;
        }

        public CustomerBillingDiscountXRef CreateCustomerBillingDiscountXRef(DbContext dbContext)
        {
            CustomerBillingDiscountXRef newCustomerBillingDiscountXRef = new CustomerBillingDiscountXRef();
            newCustomerBillingDiscountXRef.CustomerBillingDiscountXRefID = base.GetPrimaryKey("CustomerBillingDiscountXRef", dbContext).CurrentKey;
            newCustomerBillingDiscountXRef.RecordStatus = true;
            newCustomerBillingDiscountXRef.RecordVersion = 0;

            return newCustomerBillingDiscountXRef;
        }
    }
}
