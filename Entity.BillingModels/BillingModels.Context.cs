﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entity.BillingModels
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class BillingDbContext : DbContext
    {
        public BillingDbContext()
            : base(BillingConnectionString.GetConnectionString())
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CodeListDetail> CodeListDetails { get; set; }
        public virtual DbSet<CustomerBilling> CustomerBillings { get; set; }
        public virtual DbSet<CustomerBillingDetailXRef> CustomerBillingDetailXRefs { get; set; }
        public virtual DbSet<CustomerContract> CustomerContracts { get; set; }
        public virtual DbSet<CustomerContractProductXRef> CustomerContractProductXRefs { get; set; }
        public virtual DbSet<Tax> Taxes { get; set; }
        public virtual DbSet<TableCurrentKey> TableCurrentKeys { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServiceArea> ServiceAreas { get; set; }
        public virtual DbSet<CustomerContractProductXRefStage> CustomerContractProductXRefStages { get; set; }
        public virtual DbSet<CustomerBillingDiscountXRef> CustomerBillingDiscountXRefs { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<DiscountDetailXRef> DiscountDetailXRefs { get; set; }
        public virtual DbSet<Product> Products { get; set; }
    
        public virtual int GetBillingDetailsByContractID(Nullable<long> customerContractID, Nullable<System.DateTime> beginDate, Nullable<System.DateTime> endDate, Nullable<int> contractBillingTypeCodeID)
        {
            var customerContractIDParameter = customerContractID.HasValue ?
                new ObjectParameter("CustomerContractID", customerContractID) :
                new ObjectParameter("CustomerContractID", typeof(long));
    
            var beginDateParameter = beginDate.HasValue ?
                new ObjectParameter("BeginDate", beginDate) :
                new ObjectParameter("BeginDate", typeof(System.DateTime));
    
            var endDateParameter = endDate.HasValue ?
                new ObjectParameter("EndDate", endDate) :
                new ObjectParameter("EndDate", typeof(System.DateTime));
    
            var contractBillingTypeCodeIDParameter = contractBillingTypeCodeID.HasValue ?
                new ObjectParameter("ContractBillingTypeCodeID", contractBillingTypeCodeID) :
                new ObjectParameter("ContractBillingTypeCodeID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetBillingDetailsByContractID", customerContractIDParameter, beginDateParameter, endDateParameter, contractBillingTypeCodeIDParameter);
        }
    
        public virtual ObjectResult<GetDiscountsByContract_Result> GetDiscountsByContract(Nullable<long> customerContractID, Nullable<System.DateTime> asOfDate)
        {
            var customerContractIDParameter = customerContractID.HasValue ?
                new ObjectParameter("CustomerContractID", customerContractID) :
                new ObjectParameter("CustomerContractID", typeof(long));
    
            var asOfDateParameter = asOfDate.HasValue ?
                new ObjectParameter("AsOfDate", asOfDate) :
                new ObjectParameter("AsOfDate", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetDiscountsByContract_Result>("GetDiscountsByContract", customerContractIDParameter, asOfDateParameter);
        }
    }
}
