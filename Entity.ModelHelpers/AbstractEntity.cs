﻿using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelHelpers
{
    /// <summary>
    /// This abstract class is to define extended methods or members of Entity class
    /// </summary>
    [Serializable]
    public abstract class AbstractEntity
    {
        [CustomProperty]
        public bool IsDirty { get; set; }

        [CustomProperty]
        public EntityStateEnum ChangeStatus { get; set; }

        [CustomProperty]
        public bool CanWrite { get; set; }

        [CustomProperty]
        public bool CanDelete { get; set; }
        
        private List<ValidationResult> validationErrors = new List<ValidationResult>();
        private List<string> readOnlyProperties = new List<string>();

        public virtual void UpdateAuditFields<T>(DbPropertyValues originalValues, T dbContext) where T : DbContext
        {
            int recordversion = 1;
            
            if (originalValues != null)
            {
                recordversion = originalValues.GetValue<int>("RecordVersion") + 1;
            }
            
            //Get UserID from UserContext
            Type contextType = dbContext.GetType();
            PropertyInfo userProperty = contextType.GetProperty("User");
            object objUser = userProperty.GetValue(dbContext);
            long userID = (objUser == null) ? 1 : ((User)objUser).UserID;

            this.GetType().GetProperties().ToList()
                .ForEach(property =>
                {
                    if (property.Name.Contains("RecordVersion"))
                    {
                        property.SetValue(this, recordversion);
                    }

                    if (property.Name.Contains("UpdateBy"))
                    {
                        property.SetValue(this, userID);
                    }

                    if (property.Name.Contains("UpdateDate"))
                    {
                        property.SetValue(this, DateTime.Now);
                    }

                    if (recordversion == 1
                        && property.Name.Contains("CreateDate"))
                    {
                        property.SetValue(this, DateTime.Now);
                    }

                    if (recordversion == 1
                        && property.Name.Contains("CreateBy"))
                    {
                        property.SetValue(this, userID);
                    }
                });
        }

        public List<ValidationResult> ValidationErrors
        {
            get { return validationErrors; }
        }

        public List<string> ReadOnlyProperties
        {
            get { return readOnlyProperties; }
        }
    }
}
