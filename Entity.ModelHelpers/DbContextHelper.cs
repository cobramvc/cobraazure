﻿using Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelHelpers
{
    public class DbContextHelper
    {

        private static DbContextHelper helper = new DbContextHelper();

        private DbContextHelper()
        {
        }

        public static DbContextHelper Instance
        {
            get
            {
                return helper;
            }
        }


        public bool Validate(DbContext dbContext)
        {
            ObjectContext context = ((IObjectContextAdapter)dbContext).ObjectContext;
            bool IsValidationSucessful = true;

            if (context != null)
            {
                foreach (ObjectStateEntry entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified))
                {
                    DbEntityEntry dbEntityEntry = dbContext.Entry(entry.Entity);

                    if (!(dbEntityEntry.Entity is AbstractEntity))
                    {
                        if (dbEntityEntry.State != EntityState.Unchanged)
                        {
                            dbEntityEntry.State = EntityState.Detached;
                        }

                        continue;
                    }

                    AbstractEntity absEntity = (AbstractEntity)dbEntityEntry.Entity;

                    Validator dataAnnotationsValidator = ValidationFactory.CreateValidator(entry.Entity.GetType(), ValidationSpecificationSource.DataAnnotations);
                    absEntity.ValidationErrors.AddRange(dataAnnotationsValidator.Validate(entry.Entity));

                    Validator attributeValidator = ValidationFactory.CreateValidator(entry.Entity.GetType(), ValidationSpecificationSource.Attributes);
                    absEntity.ValidationErrors.AddRange(attributeValidator.Validate(entry.Entity));

                    // Note : Custom Validations with SelfValidation attribute decorated would not invoked using Validator, 
                    // as we decorating with MetaDataType attribute to Entity class. So, helper method is invoked to identify
                    // custom validation methods(decorated with SelfValidation attribute) at runtime and invoke at runtime
                    ValidationHelper.Instance.ExecuteSelfValidations(entry.Entity, dbContext);

                    // Log the validation messages to the configured Log Source
                    absEntity.ValidationErrors.ForEach(result =>
                    {
                        IsValidationSucessful = false;
                        LoggingManager.Instance.LogValidationErrors(result.Message, result.Target.GetType().ToString(), result.Tag);
                    });
                }
            }

            return IsValidationSucessful;
        }

        public bool ValidateEnity<T>(DbContext dbContext, T entity) where T : AbstractEntity
        {
            ObjectContext context = ((IObjectContextAdapter)dbContext).ObjectContext;
            bool IsValidationSucessful = true;

            if (context != null)
            {
                AbstractEntity absEntity = (AbstractEntity)entity;

                Validator dataAnnotationsValidator = ValidationFactory.CreateValidator(entity.GetType(), ValidationSpecificationSource.DataAnnotations);
                absEntity.ValidationErrors.AddRange(dataAnnotationsValidator.Validate(entity));

                Validator attributeValidator = ValidationFactory.CreateValidator(entity.GetType(), ValidationSpecificationSource.Attributes);
                absEntity.ValidationErrors.AddRange(attributeValidator.Validate(entity));

                // Note : Custom Validations with SelfValidation attribute decorated would not invoked using Validator, 
                // as we decorating with MetaDataType attribute to Entity class. So, helper method is invoked to identify
                // custom validation methods(decorated with SelfValidation attribute) at runtime and invoke at runtime
                ValidationHelper.Instance.ExecuteSelfValidations(entity, dbContext);

                // Log the validation messages to the configured Log Source
                absEntity.ValidationErrors.ForEach(result =>
                {
                    IsValidationSucessful = false;
                    LoggingManager.Instance.LogValidationErrors(result.Message, result.Target.GetType().ToString(), result.Tag);
                });              
            }

            return IsValidationSucessful;
        }

        public void UpdateAuditFields<T>(T dbContext) where T : DbContext
        {
            ObjectContext context = ((IObjectContextAdapter)dbContext).ObjectContext;
            
            if (context != null)
            {
                foreach (ObjectStateEntry entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified))
                {
                    DbEntityEntry dbEntityEntry = dbContext.Entry(entry.Entity);

                    AbstractEntity absEntity = (AbstractEntity)dbEntityEntry.Entity;

                    if (dbEntityEntry.State == EntityState.Added)
                    {
                        List<EdmProperty> keys = entry.EntitySet.ElementType.KeyProperties.ToList();
                        // Need to implement assigning PK logic
                        absEntity.UpdateAuditFields(null, dbContext);
                    }
                    else
                    {
                        absEntity.UpdateAuditFields(dbEntityEntry.OriginalValues, dbContext);
                    }
                }
            }

        }

        public bool UpdateProperties<T>(T targetObj, T sourceObj) where T : class
        {
            string[] domainFields = { "CreateBy", "CreateDate", "UpdateBy", "UpdateDate", "RecordStatus", "RecordVersion" };
            Type objType = typeof(T);
            PropertyInfo[] properties = objType.GetProperties();
            bool isDirty = false;
            
            List<PropertyInfo> sourceProperties = objType.GetProperties().ToList();
            //PropertyInfo targetDbObjectBeforeChangedProperty = sourceProperties.FirstOrDefault(prop => prop.Name == "DbObjectBeforeChanged");
            //PropertyInfo sourceDbObjectAfterChangedProperty = sourceProperties.FirstOrDefault(prop => prop.Name == "DbObjectAfterChanged");

            //if (targetDbObjectBeforeChangedProperty != null)
            //    targetDbObjectBeforeChangedProperty.SetValue(targetObj, GetSerializedObject<T>(targetObj));

            foreach (PropertyInfo property in properties)
            {
                if (!property.PropertyType.IsValueType
                    && property.PropertyType.FullName != "System.String")
                    continue;

                if (domainFields.Contains(property.Name))
                    continue;

                if (!property.CanWrite)
                    continue;

                if (property.GetCustomAttributes<CustomPropertyAttribute>().Count() > 0)
                    continue;

                object initialValue = property.GetValue(targetObj);
                object UpdateValue = property.GetValue(sourceObj);

                if (UpdateValue != null
                    && !UpdateValue.Equals(initialValue))
                {
                    property.SetValue(targetObj, UpdateValue);
                    isDirty = true;
                }
                else if (UpdateValue == null
                    && initialValue != null)
                {
                    property.SetValue(targetObj, UpdateValue);
                    isDirty = true;
                }
            }

            return isDirty;
        }


        public bool UpdateProperties<T>(T targetObj, T sourceObj, string[] excludePropertyList) where T : class
        {
            string[] domainFields = { "CreateBy", "CreateDate", "UpdateBy", "UpdateDate", "RecordStatus", "RecordVersion" };
            List<string> domainFieldList = domainFields.ToList();
            
            foreach (string s in excludePropertyList)
            {
                domainFieldList.Add(s);
            }

            Type objType = typeof(T);
            PropertyInfo[] properties = objType.GetProperties();
            bool isDirty = false;


            List<PropertyInfo> sourceProperties = objType.GetProperties().ToList();
            //PropertyInfo targetDbObjectBeforeChangedProperty = sourceProperties.FirstOrDefault(prop => prop.Name == "DbObjectBeforeChanged");

            //if (targetDbObjectBeforeChangedProperty != null)
            //    targetDbObjectBeforeChangedProperty.SetValue(targetObj, GetSerializedObject<T>(targetObj));

            foreach (PropertyInfo property in properties)
            {
                if (!property.PropertyType.IsValueType
                    && property.PropertyType.FullName != "System.String")
                    continue;

                if (!property.CanWrite)
                    continue;

                if (property.GetCustomAttributes<CustomPropertyAttribute>().Count() > 0)
                    continue;

                if (domainFieldList.Contains(property.Name))
                    continue;

                object initialValue = property.GetValue(targetObj);
                object UpdateValue = property.GetValue(sourceObj);

                if (UpdateValue != null
                    && !UpdateValue.Equals(initialValue))
                {
                    property.SetValue(targetObj, UpdateValue);
                    isDirty = true;
                }
                else if (UpdateValue == null
                    && initialValue != null)
                {
                    property.SetValue(targetObj, UpdateValue);
                    isDirty = true;
                }
            }

            return isDirty;
        }

        public bool CopyProperties<T, U>(T targetObj, U sourceObj, string[] excludePropertyList) where T : class where U : class
        {
            string[] domainFields = { "CreateBy", "CreateDate", "UpdateBy", "UpdateDate", "RecordStatus", "RecordVersion" };
            List<string> domainFieldList = domainFields.ToList();

            foreach (string s in excludePropertyList)
            {
                domainFieldList.Add(s);
            }

            Type targetObjType = typeof(T);
            PropertyInfo[] properties = targetObjType.GetProperties();

            Type sourceObjType = typeof(U);
            List<PropertyInfo> sourceProperties = sourceObjType.GetProperties().ToList();

            bool isDirty = false;
            
            //PropertyInfo targetDbObjectBeforeChangedProperty = properties.FirstOrDefault(prop => prop.Name == "DbObjectBeforeChanged");

            //if (targetDbObjectBeforeChangedProperty != null)
            //    targetDbObjectBeforeChangedProperty.SetValue(targetObj, GetSerializedObject<T>(targetObj));

            foreach (PropertyInfo property in properties)
            {
                if (!property.PropertyType.IsValueType
                    && property.PropertyType.FullName != "System.String")
                    continue;

                if (domainFieldList.Contains(property.Name))
                    continue;

                if (property.GetCustomAttributes<CustomPropertyAttribute>().Count() > 0)
                    continue;

                PropertyInfo sourceProperty = sourceProperties.Where(p => p.Name == property.Name).FirstOrDefault();
                if (sourceProperty == null)
                    continue;

                object initialValue = property.GetValue(targetObj);
                object UpdateValue = sourceProperty.GetValue(sourceObj);

                if (UpdateValue != null
                    && !UpdateValue.Equals(initialValue))
                {
                    property.SetValue(targetObj, UpdateValue);
                    isDirty = true;
                }
            }

            return isDirty;
        }

        public bool IsAnyPropertyDirty<T, U>(T targetObj, U sourceObj, string[] excludePropertyList)
            where T : class
            where U : class
        {
            string[] domainFields = { "CreateBy", "CreateDate", "UpdateBy", "UpdateDate", "RecordStatus", "RecordVersion" };
            List<string> domainFieldList = domainFields.ToList();

            foreach (string s in excludePropertyList)
            {
                domainFieldList.Add(s);
            }

            Type targetObjType = typeof(T);
            PropertyInfo[] properties = targetObjType.GetProperties();

            Type sourceObjType = typeof(U);
            List<PropertyInfo> sourceProperties = sourceObjType.GetProperties().ToList();         

            foreach (PropertyInfo property in properties)
            {
                if (!property.PropertyType.IsValueType
                    && property.PropertyType.FullName != "System.String")
                    continue;

                if (domainFieldList.Contains(property.Name))
                    continue;

                if (property.GetCustomAttributes<CustomPropertyAttribute>().Count() > 0)
                    continue;

                PropertyInfo sourceProperty = sourceProperties.Where(p => p.Name == property.Name).FirstOrDefault();
                if (sourceProperty == null)
                    continue;

                object initialValue = property.GetValue(targetObj);
                object UpdateValue = sourceProperty.GetValue(sourceObj);

                if (UpdateValue != null
                    && !UpdateValue.Equals(initialValue))
                {                    
                    return true;
                }
            }

            return false; ;
        }

        public void UpdateChangeStatusForEntityCollection<T>(List<T> sourceCol, List<T> dbCol, string[] keysToCompare, DbContext dbContext, bool onlyActive = true) where T : class
        {            
            Type objType = typeof(T);
            List<PropertyInfo> sourceProperties = objType.GetProperties().ToList();
            PropertyInfo changeStatusProperty = sourceProperties.FirstOrDefault(prop => prop.Name == "ChangeStatus");
            PropertyInfo isDirtyProperty = sourceProperties.FirstOrDefault(prop => prop.Name == "IsDirty");
            PropertyInfo recordStatusProperty = sourceProperties.FirstOrDefault(prop => prop.Name == "RecordStatus");            
            
            DbSet<T> entityDbSet = dbContext.Set<T>();
           
            if (onlyActive)
            {
                sourceCol = sourceCol.Where(source => (bool)recordStatusProperty.GetValue(source)).ToList();
                dbCol = dbCol.Where(dbObj => (bool)recordStatusProperty.GetValue(dbObj)).ToList();
            }

            //Identify the list of new objects
            sourceCol.ForEach(source =>
                {
                    List<T> tempdbCol = new List<T>();
                    int propertyIndex = 0;
                    foreach (string key in keysToCompare)
                    {
                        PropertyInfo keyProperty = sourceProperties.FirstOrDefault(prop => prop.Name == key);                        

                        if (propertyIndex == 0)
                            tempdbCol = dbCol.Where(dbObj => keyProperty.GetValue(dbObj).ToString() == keyProperty.GetValue(source).ToString()).ToList();
                        else
                            tempdbCol = tempdbCol.Where(dbObj => keyProperty.GetValue(dbObj).ToString() == keyProperty.GetValue(source).ToString()).ToList();

                        propertyIndex++;
                    }

                    //if no record found in target collection based on Keys, it indicates the source record is New
                    if (tempdbCol.Count == 0)
                    {                        
                        changeStatusProperty.SetValue(source, EntityStateEnum.New);
                        entityDbSet.Add(source);
                    }
                    else
                    {
                        changeStatusProperty.SetValue(source, EntityStateEnum.Update);
                        tempdbCol.ForEach(dbObj =>
                            {
                                changeStatusProperty.SetValue(dbObj, EntityStateEnum.Update);                                
                                bool isDirty = DbContextHelper.Instance.UpdateProperties(dbObj, source);
                                if (isDirty)
                                {
                                    isDirtyProperty.SetValue(dbObj, true);
                                    dbContext.Entry(dbObj).State = EntityState.Modified;
                                }                                
                            });
                    }
                });
                        
            //identify the list of deleted objects
            dbCol.ForEach(dbObj =>
                {
                    List<T> tempSourceCol = new List<T>();
                    int propertyIndex = 0;
                    foreach (string key in keysToCompare)
                    {
                        PropertyInfo keyProperty = sourceProperties.FirstOrDefault(prop => prop.Name == key);

                        if (propertyIndex == 0)
                            tempSourceCol = sourceCol.Where(source => keyProperty.GetValue(source).ToString() == keyProperty.GetValue(dbObj).ToString()).ToList();
                        else
                            tempSourceCol = tempSourceCol.Where(source => keyProperty.GetValue(source).ToString() == keyProperty.GetValue(dbObj).ToString()).ToList();

                        propertyIndex++;
                    }

                    //if no record found in target collection based on Keys, it indicates the source record is New
                    if (tempSourceCol.Count == 0)
                    {
                        //newCollection.Add(source);
                        changeStatusProperty.SetValue(dbObj, EntityStateEnum.Delete);
                        isDirtyProperty.SetValue(dbObj, true);
                        recordStatusProperty.SetValue(dbObj, false);
                        dbContext.Entry(dbObj).State = EntityState.Modified;
                    }
                });
        }

        //private T GetSerializedObject<T>(T obj)
        //{
        //    MemoryStream writer = new MemoryStream();            
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    formatter.Serialize(writer, obj);
        //    writer.Position = 0;
        //    return (T)formatter.Deserialize(writer);
        //}
    }
}
