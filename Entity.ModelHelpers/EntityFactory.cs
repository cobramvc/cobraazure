﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelHelpers
{
    public class EntityFactory<T> where T : class
    {
        public T GetPrimaryKey(string tableName, DbContext dbContext)
        {
            T currentPrimaryKey = dbContext.Database.SqlQuery<T>("GetPrimaryKey @TableName", new SqlParameter("@TableName", tableName)).FirstOrDefault();

            if (currentPrimaryKey == null)
            {
                throw new Exception("No entry found in TableCurrentKeys with TableName " + tableName);
            }

            return currentPrimaryKey;

        }


    }
}
