﻿using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelHelpers
{
    internal class ValidationHelper
    {
        private static ValidationHelper validateEntity = new ValidationHelper();

        private ValidationHelper()
        {
        }

        public static ValidationHelper Instance
        {
            get { return validateEntity; }
        }
       
        public void ExecuteSelfValidations(object target, DbContext context)
        {
            Type entityType = target.GetType();

            if (!(target is AbstractEntity))
            {
                return;
            }

            /* Identifying the child objects of Entity and validating those child objects is not 
             * required at this point, as this method is being invoked in SaveChanges overriden method for all 
             * Entity objects that are Added or Modified
             
            DbEntityEntry entityEntry = context.Entry(target);
            foreach (PropertyInfo property in target.GetType().GetProperties())
            {
                if(property.PropertyType.Namespace == target.GetType().Namespace
                    || (property.PropertyType.IsGenericType
                    && !property.PropertyType.IsValueType))
                {
                    object propertyValue = property.GetValue(target);
                    //DbPropertyEntry propertyEntry = entityEntry.Property(property.Name);
                    if (propertyValue == null)
                    {
                        continue;
                    }

                    if (property.PropertyType.IsGenericType
                    && !property.PropertyType.IsValueType)
                    {
                        //Type t = typeof(OrderDetail);
                        //HashSet<OrderDetail> list = ((HashSet<OrderDetail>)propertyValue); 
                        DbCollectionEntry colEntry = entityEntry.Collection(property.Name);
                        IEnumerable col = (IEnumerable)colEntry.CurrentValue;
                       // this.ExecuteSelfValidations(col.GetEnumerator().Current, context);
                        col.GetEnumerator().Reset();
                        IEnumerator records = col.GetEnumerator();

                        while (records.MoveNext())
                        {
                            this.ExecuteSelfValidations(records.Current, context);
                        }
                    }
                    else
                    {
                        DbMemberEntry complexEntry = entityEntry.Member(property.Name);
                        if (complexEntry is DbReferenceEntry)
                        {
                            continue;
                        }

                        this.ExecuteSelfValidations(propertyValue, context);
                    }
                }                                          
            }
            */

            List<MethodInfo> methods = entityType.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic).ToList();
            methods.ForEach(method =>
            {
                SelfValidationAttribute attributeType = method.GetCustomAttribute<SelfValidationAttribute>();

                if (attributeType != null)
                {
                    method.Invoke(target, new object[] { context });
                }
            });
        }
    }
}
