﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelHelpers
{
    [Serializable]
    public class BaseEntity<T> : AbstractEntity where T : class
    {
        [CustomProperty]
        public T DbObjectBeforeChanged { get; set; }        
    }
}
