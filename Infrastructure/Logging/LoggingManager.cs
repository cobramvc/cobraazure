﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class LoggingManager : EventSource
    {
        private static LoggingManager manager = new LoggingManager();

        public class Keywords
        {
            public const EventKeywords Entity = (EventKeywords)1;
            public const EventKeywords Model = (EventKeywords)2;
            public const EventKeywords View = (EventKeywords)4;
            public const EventKeywords Controller = (EventKeywords)8;
            public const EventKeywords General = (EventKeywords)16;
        }

        public class Tasks
        {
            public const EventTask ValidateEntity = (EventTask)1;
        }

        public static LoggingManager Instance
        {
            get { return manager; }
        }

        [Event(1, Level = EventLevel.Informational, Opcode = EventOpcode.Info)]
        public void EntityLog(string message)
        {
            if (this.IsEnabled())
            {
                WriteEvent(1, message);
            }
        }

        [Event(2, Keywords = Keywords.Model, Level = EventLevel.Warning, Task = Tasks.ValidateEntity)]
        public void LogValidationErrors(string errorMessage, string entityType, string methodName)
        {
            if (this.IsEnabled(EventLevel.Warning, Keywords.Model))
            {
                string validationError = "Entity Type : " + entityType + " Method :" + methodName + "Validation Message: " + errorMessage;
                WriteEvent(2, validationError);
            }
        }

        [Event(3, Level = EventLevel.Error)]
        public void LogExceptionBeforeHandling(Exception exception)
        {
            if (this.IsEnabled(EventLevel.Error, EventKeywords.None))
            {
                WriteEvent(3, "Before Handling - Message : " + exception.Message + " Stacktrace - " + exception.StackTrace );
            }
        }

        [Event(4, Level = EventLevel.Error)]
        public void LogExceptionAfterHandling(Exception exception)
        {
            if (this.IsEnabled(EventLevel.Error, EventKeywords.None))
            {
                WriteEvent(4, "After Handling - - Message : " + exception.Message + " Stacktrace - " + exception.StackTrace);
            }
        }
    }
}
