﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public enum BusinessRulesActionTypeEnum
    {
        //This action type is to be specified when the business rules are to be executed on either Object 
        // created through Factory class OR object materialized through Entity Framework
        ObjectMaterialize,

        //This action type is to be specified when the business rules are to be executed on existing Object 
        // to set the custom properties
        ObjectUpdate,

        //This action type is to be specified when the business rules are to be executed when the
        // Object created through MVC Model binding (in which case reference objects do not exist)
        HttpPost,

        // All OR None
        None
    }
}
