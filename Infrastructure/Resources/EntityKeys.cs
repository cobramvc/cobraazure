﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Resources
{
    //public class CustomerContractCartKey
    //{
    //    public const int DisconnectedContract = 1;
    //    public const int ActiveContract = 2;
    //}

    public class CustomerAddressTypeKey
    {
        public const int Home = 6;
        public const int Billing = 7;
    }

    public class ContractTermTypeKey
    {
        public const int Monthly = 34;
        public const int Quarterly = 35;
        public const int Yearly = 36;
    }

    public class ContractProductStatusKey
    {
        public const int Pending_Activation = 47;
        public const int Active = 48;
        public const int Suspend = 49;
        public const int Disconnect = 50;
        public const int Terminate = 51;
        public const int Cancelled = 52;
        public const int Pending_Suspend = 70;
        public const int Pending_Disconnect = 71;
        public const int Pending_Terminate = 72;
        //public const int Pending_Reconnection = 73;
        public const int Pending_Upgrade = 73;
    }

    public class ProductStatusKey
    {
        public const int Active = 28;
        public const int Cancel = 29;
    }

    public class BillingToParentKey
    {
        public const int Yes = 45;
        public const int No = 46;
    }

    public class CustomerBillingTypeCodeKey
    {
        public const int ContractSummary = 62;
        public const int Periodic = 63;
    }

    public class CustomerBillingDetailXRefTypeCodeKey
    {
        public const int Current = 55;
        public const int Adjustment = 56;
    }

    public class DiscountTypeCodeKey
    {
        public const int Flat = 57;
        public const int Percentage = 58;
    }

    public class ContractStatusChangeRequestStatusCodeKey
    {
        public const int Initiated = 74;
        public const int Confirmed = 75;
        public const int Cancelled = 76;
        public const int Failed = 77;
    }
}
