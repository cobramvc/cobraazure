﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class CodeListKey
    {
        public const string CustomerCategoryList = "CustomerCategoryList";
        public const string BillingOptionsList = "BillingOptionsList";
        public const string AddressTypeList = "AddressTypeList";
        public const string AttributeCategoryList = "AttributeCategoryList";
        public const string AttributeTypeList = "AttributeTypeList";
        public const string StateList = "StateList";
        public const string CityList = "CityList";
        public const string RegionList = "RegionList";
        public const string ServiceTypeList = "ServiceTypeList";
        public const string ProductStatusCodeList = "ProductStatusCodeList";
        public const string ProductTypeCodeList = "ProductTypeCodeList";
        public const string ProductContractTypeCodeList = "ProductContractTypeCodeList";
        public const string ContractTermTypeCodeList = "ContractTermTypeCodeList";
        public const string ContractTypeCodeList = "ContractTypeCodeList";
        public const string ContractProductStatusCodeList = "ContractProductStatusCodeList";
        public const string BillingToParentCodeList = "BillingToParentCodeList";
        public const string TaxTypeCodeList = "TaxTypeCodeList";
        public const string DiscountTypeCodeList = "DiscountTypeCodeList";
        public const string DiscountCategoryTypeCodeList = "DiscountCategoryTypeCodeList";
        public const string ContractStatusReasonList = "ContractStatusReasonList";
    }    
}
