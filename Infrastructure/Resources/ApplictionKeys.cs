﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public static class ApplicationKeys
    {
        public const int SaveUnsucessful = -1;
        public const int PageSize = 5;
    }

    public enum CustomerContractActionType
    {
        InstanceChange,
        StatusChange,
        ProductChange,
        InstanceDownGrade,
        NoChange
    }
}
