﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class BusinessRulesPropertyAttribute : Attribute
    {
        public BusinessRulesPropertyAttribute(string propertyName)
        {
            this.PropertyName = propertyName;
        }

        public string PropertyName
        {
            get;
            set;
        }
    }
}
