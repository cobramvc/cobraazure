﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class SelectListAttribute : Attribute
    {
        private string m_codeListKey = string.Empty;
        public SelectListAttribute(string codeListKey)
        {
            this.m_codeListKey = codeListKey;
        }

        public string CodeListKey
        {
            get
            {
                return this.m_codeListKey;
            }
        }
    }
}
