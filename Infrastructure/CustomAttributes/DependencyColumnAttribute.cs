﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class DependencyColumnAttribute : Attribute
    {
        private string m_columnName;
        public DependencyColumnAttribute(string columnName)
        {
            this.m_columnName = columnName;
        }

        public string DependencyColumn
        {
            get
            {
                return this.m_columnName;
            }
        }
    }
}
