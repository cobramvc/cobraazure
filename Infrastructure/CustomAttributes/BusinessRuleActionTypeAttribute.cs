﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class BusinessRuleActionTypeAttribute : Attribute
    {
        public BusinessRuleActionTypeAttribute(BusinessRulesActionTypeEnum actionType)
        {
            this.ActionType = actionType;
        }

        public BusinessRulesActionTypeEnum ActionType
        {
            get;
            set;
        }
    }
}
