﻿using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class ExceptionHandlingManager
    {
        private static ExceptionHandlingManager exManager = new ExceptionHandlingManager();
        private ExceptionHandlingManager()
        {
            BuildExceptionPolicy();
        }

        public static ExceptionHandlingManager Instance
        {
            get { return exManager; }
        }

        private void BuildExceptionPolicy()
        {

            List<ExceptionPolicyDefinition> policies = new List<ExceptionPolicyDefinition>();

            List<ExceptionPolicyEntry> entityPolicyEntries = new List<ExceptionPolicyEntry>();
            ExceptionPolicyEntry entityEntry = new ExceptionPolicyEntry(
                typeof(Exception),
                PostHandlingAction.ThrowNewException,
                new IExceptionHandler[] { new ReplaceHandler("An error has occured. Please contact your adminstrator", typeof(Exception)) }
                );

            entityPolicyEntries.Add(entityEntry);
            ExceptionPolicyDefinition policyException = new ExceptionPolicyDefinition(ExceptionPolicyName.EntityOrModel.ToString(), entityPolicyEntries);
            policies.Add(policyException);

            List<ExceptionPolicyEntry> controllerPolicyEntries = new List<ExceptionPolicyEntry>();
            ExceptionPolicyEntry controllerEntry = new ExceptionPolicyEntry(
                typeof(Exception),
                PostHandlingAction.ThrowNewException,
                new IExceptionHandler[] { new ReplaceHandler("An error has occured. Please contact your adminstrator", typeof(Exception)) }
                );

            controllerPolicyEntries.Add(controllerEntry);
            ExceptionPolicyDefinition controllerPolicyException = new ExceptionPolicyDefinition(ExceptionPolicyName.Controller.ToString(), controllerPolicyEntries);
            policies.Add(controllerPolicyException);

            List<ExceptionPolicyEntry> uiCustomFilterPolicyEntries = new List<ExceptionPolicyEntry>();
            ExceptionPolicyEntry uiCustomFilterEntry = new ExceptionPolicyEntry(
                typeof(Exception),
                PostHandlingAction.None,
                new IExceptionHandler[] { }
                );

            uiCustomFilterPolicyEntries.Add(uiCustomFilterEntry);
            ExceptionPolicyDefinition uiCustomFilterPolicyException = new ExceptionPolicyDefinition(ExceptionPolicyName.UICustomFilter.ToString(), uiCustomFilterPolicyEntries);
            policies.Add(uiCustomFilterPolicyException);


            ExceptionManager manager = new ExceptionManager(policies);
            ExceptionPolicy.SetExceptionManager(manager);
        }

        public void HandleException(Exception exception, ExceptionPolicyName policyName)
        {
            Exception exceptionToReThrow;

            LoggingManager.Instance.LogExceptionBeforeHandling(exception);
            bool notifyReThrow = ExceptionPolicy.HandleException(exception, policyName.ToString(), out exceptionToReThrow);

            if (exceptionToReThrow != null)
            {
                LoggingManager.Instance.LogExceptionAfterHandling(exceptionToReThrow);
            }

            if (notifyReThrow)
            {
                if (exceptionToReThrow != null)
                    throw exceptionToReThrow;
                else
                    throw exception;
            }
        }
    }

    public enum ExceptionPolicyName
    {
        Controller,
        EntityOrModel,
        UICustomFilter

    }

}
