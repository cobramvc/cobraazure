﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public static class Check
    {
        public static void IsEqual(bool isEqual, string message)
        {
            if (isEqual)
            {
                throw new Exception(message);
            }
        }
    }
}
