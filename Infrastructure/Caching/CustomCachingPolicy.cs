﻿using EFCache;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Caching
{
    public class CustomCachingPolicy : CachingPolicy
    {
        public CustomCachingPolicy()
        {
            this.MinCacheableRows = 0;
            this.MaxCacheableRows = 1000;
            this.CacheableTables = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this.NonCacheableTables = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        }

        /// <summary> 
        /// Gets or sets the minimal number of cacheable rows. 
        /// </summary> 
        /// <value>Minimal number of cacheable rows.</value> 
        public int MinCacheableRows { get; set; }

        /// <summary> 
        /// Gets or sets the maximum number of cacheable rows. 
        /// </summary> 
        /// <value>Maximum number of cacheable rows.</value> 
        public int MaxCacheableRows { get; set; }

        /// <summary> 
        /// Gets the collection of cacheable tables. 
        /// </summary> 
        /// <value>The cacheable tables.</value> 
        public ICollection<string> CacheableTables { get; private set; }

        /// <summary> 
        /// Gets the collection of non cacheable tables. 
        /// </summary> 
        /// <value>The non cacheable tables.</value> 
        public ICollection<string> NonCacheableTables { get; private set; }

        
        protected override bool CanBeCached(ReadOnlyCollection<System.Data.Entity.Core.Metadata.Edm.EntitySetBase> affectedEntitySets, string sql, IEnumerable<KeyValuePair<string, object>> parameters)
        {            
            // check if we have any non-cacheable tables in the query 
            if (this.NonCacheableTables.Count > 0)
            {
                foreach (EntitySetBase esb in affectedEntitySets)
                {
                    if (this.NonCacheableTables.Contains(esb.Name))
                    {
                        return false;
                    }
                }
            }

            // by default everything is cacheable 
            if (this.CacheableTables.Count == 0)
            {
                return true;
            }
            else
            {
                // unless the user has explicitly specified which tables to cache 
                foreach (EntitySetBase esb in affectedEntitySets)
                {
                    if (!this.CacheableTables.Contains(esb.Name))
                    {
                        return false;
                    }
                }

                return true;
            }
        }


    }


}
