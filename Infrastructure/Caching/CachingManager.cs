﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Caching
{
    public class CachingManager
    {
        private static CachingManager instance = new CachingManager();
        private CachingDbConfiguration customDbConfiguration = null;
        private static readonly object m_readonly = new object();

        private CachingManager()
        {
        }

        public static CachingManager Instance
        {
            get { return instance; }
        }

        public void RegisterCachingConfiguration()
        {
            if (customDbConfiguration == null)
            {
                lock (m_readonly)
                {
                    if (customDbConfiguration == null)
                    {
                        customDbConfiguration = new CachingDbConfiguration();
                        System.Data.Entity.DbConfiguration.SetConfiguration(customDbConfiguration);
                    }                        
                }
            }
        }
    }
}
