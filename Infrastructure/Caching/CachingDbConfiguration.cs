﻿using EFCache;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Caching
{    
    public class CachingDbConfiguration : DbConfiguration
    {
        public CachingDbConfiguration()
        {
            //this.SetDefaultConnectionFactory(new CustomDbConnectionFactory());
            var transactionHandler = new CacheTransactionHandler(new InMemoryCache());
            AddInterceptor(transactionHandler);
            Loaded += (sender, args) =>
                            args.ReplaceService<DbProviderServices>((s, _) => new CachingProviderServices(s, transactionHandler, new CustomCachingPolicy()));
        }
    }
}
