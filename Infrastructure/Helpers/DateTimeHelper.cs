﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class DateTimeHelper
    {
        private static DateTimeHelper helper = new DateTimeHelper();

        private DateTimeHelper()
        {
        }

        public static DateTimeHelper Instance
        {
            get
            {
                return helper;
            }
        }

        public DateTime GetNextMonthLastDay()
        {
            DateTime afterTwoMonths = DateTime.Now.Date.AddMonths(2);
            return afterTwoMonths.AddDays(-DateTime.Now.Day - 1);            
        }

        public int GetNumberOfDaysLeftInThisMonth(DateTime currentDate)
        {
            DateTime nextMonthDate = currentDate.Date.AddMonths(1);
            DateTime lastDayDate = nextMonthDate.AddDays(-currentDate.Day - 1);
            return (lastDayDate - currentDate).Days;
        }
    }
}
