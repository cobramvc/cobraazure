﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class DiscountSearchCriteriaModel
    {
        public string DiscountName { get; set; }
        public bool IsModalDialog { get; set; }
    }
}
