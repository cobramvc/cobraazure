﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class CustomerSearchCriteriaModel
    {
        public string SFirstName { get; set; }
        public string SLastName { get; set; }
        public string SMobile { get; set; }
        public string SEmail { get; set; }
        public string SAddress { get; set; }
        public string SCustomerNo { get; set; }
        public bool IsModalDialog { get; set; }
    }
}
