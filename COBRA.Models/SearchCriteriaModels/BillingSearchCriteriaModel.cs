﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class BillingSearchCriteriaModel
    {
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public long CustomerContractID{ get; set; }
        public int BillingTypeCodeID { get; set; }
        public int ContractTermTypeCodeID { get; set; }
        public long CustomerID { get; set; }
    }
}
