﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class ServiceGroupSearchCriteriaModel
    {
        public string ServiceGroupName { get; set; }
        public bool IsModalDialog { get; set; }
    }
}
