﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class ContractSearchCriteriaModel
    {
        public string ContractID { get; set; }
        public long CustomerID { get; set; }        
        public bool ContractIsModalDialog { get; set; }
        public string ActionType { get; set; }
    }
}
