﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class ProductSearchCriteriaModel
    {
        public string ProductDescription { get; set; }
        public bool IsModalDialog { get; set; }

        public long productCustomerID { get; set; }
        public int productContractTermTypeCodeID { get; set; }
        public bool IsContractProductSearch { get; set; }
    }
}
