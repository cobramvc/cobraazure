﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class ServiceSearchCriteriaModel
    {
        public string ServiceName { get; set; }
        public bool IsModalDialog { get; set; }

        [SelectList(CodeListKey.ServiceTypeList)]
        public int TypeCodeID { get; set; }
    }
}
