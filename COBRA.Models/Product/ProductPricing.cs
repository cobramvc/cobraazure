﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class ProductPricing
    {
        public long ProductID{ get; set; }
        public string ProductDescription { get; set; }
        public long ServiceID { get; set; }
        public string ServiceDescription { get; set; }
        public int CategoryCodeID { get; set; }
        public long ServiceAreaID { get; set; }
        public decimal SubscriptionCharges { get; set; }
        public decimal OneTimeCharges { get; set; }
        public bool IsServiceGroup { get; set; }
        public long ServicePricingID { get; set; }
        public long ServiceGroupPricingID { get; set; }
        public DateTime EffectiveDate { get; set; }
        public decimal Installation { get; set; }
    }
}
