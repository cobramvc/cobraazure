﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
   public  class ReportItem<T, U>
    {
        public T XValue { get; set; }
        public U YValue { get; set; }
    }
}
