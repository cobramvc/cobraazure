﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COBRA.Models
{
    public class ContractBillingDetailXRef
    {
        public int ServiceContractRank { get; set; }
        public long CustomerContractProductXRefID { get; set; }        
        public long PricingID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal SubscriptionCharges { get; set; }
        public decimal OneTimeCharges { get; set; }
        public long ServiceAreaID { get; set; }
        public long ServiceID { get; set; }
        public int? TaxID { get; set; }
        public decimal? Tax { get; set; }
        public decimal? TaxOnTax { get; set; }
        public bool IsPricingChanged { get; set; }
        public bool IsTaxChanged { get; set; }
        public decimal? Discount { get; set; }
        public int Instances { get; set; }
    }
}
