﻿using Billing;
using Billing.BillingStrategy;
using COBRA.Models;
using Entity.UnitOfWork;
using Entity.UnitOfWork.ContractRepo;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillingService
{
    class Program
    {
        private static CustomerContractRepository _repository = null;
        static void Main(string[] args)
        {
            _repository = new CustomerContractRepository(new CustomerContractUnitOfWork());
            BillingSearchCriteriaModel searchCriteria = new BillingSearchCriteriaModel();
            searchCriteria.CustomerContractID = 11;
            searchCriteria.BillingTypeCodeID = CustomerBillingTypeCodeKey.Periodic;
            searchCriteria.BeginDate = DateTime.Today.AddDays(1).AddMonths(1);
            searchCriteria.ContractTermTypeCodeID = ContractTermTypeKey.Monthly;

            BaseBllingStrategy billingStrategy = BillingStrategyFactory.Instance.GetBillingStrategy(searchCriteria);
            billingStrategy.LoadContractBillingDetails();            
        }

        
        private static void InitializeContractBilling(ContractBilling contractBilling)
        {            
            contractBilling.BeginDate = DateTime.Today.AddDays(1).AddMonths(1);            
        }
    }
}
