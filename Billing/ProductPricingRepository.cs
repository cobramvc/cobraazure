﻿using Entity.ProductModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.ProductRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
    public class ProductPricingRepository
    {
        //private long _productID;
        //private IServiceRepository _serviceRepository = null;
        //private IServiceGroupRepository _serviceGroupRepository = null;
        //private IProductRepository _productRepository = null;
        //private ProductUnitOfWork unitOfWork = null;
        //private readonly object _readOnly = new object();

        //private List<ServicePricing> _servicePricings = new List<ServicePricing>();
        //private List<ServiceGroupPricing> _serviceGroupPricings = new List<ServiceGroupPricing>();
        //private Product _product = null;

        //public ProductPricingRepository(long productID)
        //{
        //    this._productID = productID;
        //    unitOfWork = new ProductUnitOfWork();
        //    this._productRepository = new ProductRepository(unitOfWork);
        //    this._product = this._productRepository.GetProductByID(this._productID);
        //    LoadServicePricings();
        //}

        //private IServiceRepository ServiceRepository
        //{
        //    get
        //    {
        //        lock(_readOnly)
        //        {
        //            if(this._serviceRepository == null)
        //                this._serviceRepository = new ServiceRepository(unitOfWork);
        //            return this._serviceRepository;
        //        }                
        //    }
        //}

        //private IServiceGroupRepository ServiceGroupRepository
        //{
        //    get
        //    {
        //        lock (_readOnly)
        //        {
        //            if (this._serviceGroupRepository == null)
        //                this._serviceGroupRepository = new ServiceGroupRepository(unitOfWork);
        //            return this._serviceGroupRepository;
        //        }
        //    }
        //}

        //private void LoadServicePricings()
        //{
        //    if (this._product == null)
        //        return;

        //    if (this._product.ServiceID.HasValue)
        //    {
        //        //this._servicePricings = ;
        //        return;
        //    }

        //    if (this._product.ServiceGroupID.HasValue)
        //    {
        //        //this._serviceGroupPricings = ;
        //        return;
        //    }
        //}

        //public List<ContractBillingDetailXRef> GetAdditionalBillingDetails(ContractBillingDetailXRef contractBillingDetailXRef)
        //{
        //    List<ContractBillingDetailXRef> newContractBillingDetailXRefList = new List<ContractBillingDetailXRef>();

        //    if (this._product == null)
        //        return newContractBillingDetailXRefList;


        //    if (this._product.ServiceID.HasValue)
        //    {
        //        return GetAdditionalBillingDetailsByService(contractBillingDetailXRef);
        //    }
        //    else if (this._product.ServiceGroupID.HasValue)
        //    {
        //        return GetAdditionalBillingDetailsByServiceGroup(contractBillingDetailXRef);
        //    }

        //    return newContractBillingDetailXRefList;
        //}

        //private List<ContractBillingDetailXRef> GetAdditionalBillingDetailsByService(ContractBillingDetailXRef contractBillingDetailXRef)
        //{
        //    List<ContractBillingDetailXRef> newContractBillingDetailXRefList = new List<ContractBillingDetailXRef>();
        //    DateTime endDate = contractBillingDetailXRef.EndDate.Value;

        //    ServicePricing previousServicePricing = this._servicePricings
        //       .Where(pricing => pricing.RecordStatus
        //         && pricing.EffectiveDate.HasValue
        //         && pricing.EffectiveDate.Value <= contractBillingDetailXRef.BeginDate)
        //         .OrderByDescending(pricing => pricing.Version)
        //         .FirstOrDefault();

        //    int version = (previousServicePricing == null) ? 1 : previousServicePricing.Version;

        //    // Get the list of Service Pricings that are effective with in the date range
        //    List<ServicePricing> servicePricings = this._servicePricings
        //            .Where(pricing => pricing.RecordStatus
        //              && pricing.EffectiveDate.HasValue
        //              && pricing.EffectiveDate.Value <= contractBillingDetailXRef.EndDate.Value
        //              //&& pricing.EffectiveDate.Value > contractBillingDetailXRef.BeginDate                      
        //              && pricing.Version >= version)
        //              .OrderBy(pricing => pricing.Version)
        //              .ToList();

           
        //    // Add the old Pricing to the new Pricing collection
        //    //if(previousServicePricing != null)
        //    //    servicePricings.Add(previousServicePricing);

        //    // Sort the list based on Version ascending
        //    //servicePricings = servicePricings.OrderBy(pricing => pricing.Version).ToList();

        //    // If there are no new and previous Pricing 
        //    // this can never happen if we have pricing defined
        //    if (servicePricings.Count == 0)
        //        return newContractBillingDetailXRefList;

        //    ContractBillingDetailXRef newcontractBillingDetailXRef = null;
        //    ContractBillingDetailXRef prevContractBillingDetailXRef = null;  
        //    int pricingIndex = 0;

        //    // Split-up Billing Details based on Pricing changes
            
        //    servicePricings.ForEach(pricing =>
        //        {
        //            if (pricingIndex == 0)
        //            {  
        //                // This is unusual scenario where in we have no Pricing Information on Billing Detail Begin Date
        //                // which I dont think is a possible scenario
        //                if(pricing.EffectiveDate.Value >= contractBillingDetailXRef.BeginDate)
        //                    contractBillingDetailXRef.BeginDate = pricing.EffectiveDate.Value;
                        
        //                contractBillingDetailXRef.ProductPricing.PricingID = pricing.ServicePricingID;
        //                LoadServicePricingDetailsByIDServiceAreaID(contractBillingDetailXRef);
        //                prevContractBillingDetailXRef = contractBillingDetailXRef;
        //            }
        //            else
        //            {
        //                newcontractBillingDetailXRef = new ContractBillingDetailXRef();
        //                newcontractBillingDetailXRef.BeginDate = pricing.EffectiveDate.Value;
        //                newcontractBillingDetailXRef.ProductPricing = new PricingInfo();
        //                newcontractBillingDetailXRef.ProductPricing.PricingID = pricing.ServicePricingID;

        //                // Copy the properties that are same as parent record
        //                newcontractBillingDetailXRef.ProductPricing.ServiceAreaID = contractBillingDetailXRef.ProductPricing.ServiceAreaID;
        //                newcontractBillingDetailXRef.Instances = contractBillingDetailXRef.Instances;
        //                newcontractBillingDetailXRef.ProductID = contractBillingDetailXRef.ProductID;

        //                LoadServicePricingDetailsByIDServiceAreaID(newcontractBillingDetailXRef);

        //                newContractBillingDetailXRefList.Add(newcontractBillingDetailXRef);

        //                prevContractBillingDetailXRef.EndDate = pricing.EffectiveDate.Value.AddDays(-1);
        //                prevContractBillingDetailXRef = newcontractBillingDetailXRef;
        //            }

        //            pricingIndex++;
        //        });

        //    prevContractBillingDetailXRef.EndDate = endDate;

        //    //newContractBillingDetailXRefList = newContractBillingDetailXRefList.OrderBy(billingDetail => billingDetail.BeginDate).ToList();

        //    // Set End Dates of the Billing Details that were spilit-up
                  
        //    //newContractBillingDetailXRefList.ForEach(billingDetail =>
        //    //    {
        //    //        if (prevContractBillingDetailXRef == null)
        //    //            contractBillingDetailXRef.EndDate = billingDetail.BeginDate.AddDays(-1);
        //    //        else
        //    //            prevContractBillingDetailXRef.EndDate = billingDetail.BeginDate.AddDays(-1);

        //    //        prevContractBillingDetailXRef = billingDetail;
        //    //    });

        //    //if (newContractBillingDetailXRefList.Count > 0)
        //    //    newContractBillingDetailXRefList.Last().EndDate = endDate;

        //    return newContractBillingDetailXRefList;
        //}

        //private List<ContractBillingDetailXRef> GetAdditionalBillingDetailsByServiceGroup(ContractBillingDetailXRef contractBillingDetailXRef)
        //{
        //    List<ContractBillingDetailXRef> newContractBillingDetailXRefList = new List<ContractBillingDetailXRef>();
        //    DateTime endDate = contractBillingDetailXRef.EndDate.Value;


        //    ServiceGroupPricing previousServiceGroupPricing = this._serviceGroupPricings
        //        .Where(pricing => pricing.RecordStatus
        //          && pricing.EffectiveDate.HasValue
        //          && pricing.EffectiveDate.Value <= contractBillingDetailXRef.BeginDate)
        //          .OrderByDescending(pricing => pricing.Version)
        //          .FirstOrDefault();

        //    int version = (previousServiceGroupPricing == null) ? 1 : previousServiceGroupPricing.Version;

        //    List<ServiceGroupPricing> serviceGroupPricings = this._serviceGroupPricings
        //            .Where(pricing => pricing.RecordStatus
        //              && pricing.EffectiveDate.HasValue                      
        //              && pricing.EffectiveDate.Value <= contractBillingDetailXRef.EndDate.Value
        //              //&& pricing.EffectiveDate.Value > contractBillingDetailXRef.BeginDate
        //               && pricing.Version >= version)
        //              .OrderBy(pricing => pricing.Version)
        //              .ToList();



        //    //// Add the old Pricing to the new Pricing collection
        //    //if (previousServiceGroupPricing != null)
        //    //    serviceGroupPricings.Add(previousServiceGroupPricing);

        //    //// Sort the list based on Version ascending
        //    //serviceGroupPricings = serviceGroupPricings.OrderBy(pricing => pricing.Version).ToList();

        //    // If there are no new and previous Pricing 
        //    // this can never happen if we have pricing defined
        //    if (serviceGroupPricings.Count == 0)
        //        return newContractBillingDetailXRefList;

        //    ContractBillingDetailXRef newcontractBillingDetailXRef = null;
        //    ContractBillingDetailXRef prevContractBillingDetailXRef = null;
        //    int pricingIndex = 0;


        //    // Split-up Billing Details based on Pricing changes
        //    serviceGroupPricings.ForEach(pricing =>
        //    {
        //        if (pricingIndex == 0)
        //        {
        //            // This is unusual scenario where in we have no Pricing Information on Billing Detail Begin Date
        //            // which I dont think is a possible scenario
        //            if (pricing.EffectiveDate.Value >= contractBillingDetailXRef.BeginDate)
        //                contractBillingDetailXRef.BeginDate = pricing.EffectiveDate.Value;
                    
        //            contractBillingDetailXRef.ProductPricing.PricingID = pricing.ServiceGroupPricingID;
        //            LoadServiceGroupPricingDetailsByIDServiceAreaID(contractBillingDetailXRef);
        //            prevContractBillingDetailXRef = contractBillingDetailXRef;
        //        }
        //        else
        //        {
        //            newcontractBillingDetailXRef = new ContractBillingDetailXRef();
        //            newcontractBillingDetailXRef.BeginDate = pricing.EffectiveDate.Value;
        //            newcontractBillingDetailXRef.ProductPricing = new PricingInfo();
        //            newcontractBillingDetailXRef.ProductPricing.PricingID = pricing.ServiceGroupPricingID;

        //            newcontractBillingDetailXRef.ProductPricing.ServiceAreaID = contractBillingDetailXRef.ProductPricing.ServiceAreaID;
        //            newcontractBillingDetailXRef.Instances = contractBillingDetailXRef.Instances;
        //            newcontractBillingDetailXRef.ProductID = contractBillingDetailXRef.ProductID;

        //            LoadServiceGroupPricingDetailsByIDServiceAreaID(newcontractBillingDetailXRef);
        //            newContractBillingDetailXRefList.Add(newcontractBillingDetailXRef);

        //            prevContractBillingDetailXRef.EndDate = pricing.EffectiveDate.Value.AddDays(-1);
        //            prevContractBillingDetailXRef = newcontractBillingDetailXRef;
        //        }

        //        pricingIndex++;
        //    });

        //    prevContractBillingDetailXRef.EndDate = endDate;

        //    //newContractBillingDetailXRefList = newContractBillingDetailXRefList.OrderBy(billingDetail => billingDetail.BeginDate).ToList();

        //    //// Set End Dates of the Billing Details that were spilit-up
        //    //ContractBillingDetailXRef prevContractBillingDetailXRef = null;
        //    //newContractBillingDetailXRefList.ForEach(billingDetail =>
        //    //{
        //    //    if (prevContractBillingDetailXRef == null)
        //    //        contractBillingDetailXRef.EndDate = billingDetail.BeginDate.AddDays(-1);
        //    //    else
        //    //        prevContractBillingDetailXRef.EndDate = billingDetail.BeginDate.AddDays(-1);

        //    //    prevContractBillingDetailXRef = billingDetail;
        //    //});

        //    //if (newContractBillingDetailXRefList.Count > 0)
        //    //    newContractBillingDetailXRefList.Last().EndDate = endDate;

        //    return newContractBillingDetailXRefList;
        //}

        //private void LoadServicePricingDetailsByIDServiceAreaID(ContractBillingDetailXRef contractBillingDetailXRef)
        //{
        //    contractBillingDetailXRef.ProductPricing.OneTimeCharges = 0;
        //    contractBillingDetailXRef.ProductPricing.SubscriptionCharges = 0;

        //    if (contractBillingDetailXRef.ProductPricing == null)
        //        return;

        //    if (contractBillingDetailXRef.ProductPricing.ServiceAreaID == 0)
        //        return;

        //    ServicePricing servicePricing = this._servicePricings.FirstOrDefault(pricing => pricing.ServicePricingID == contractBillingDetailXRef.ProductPricing.PricingID);

        //    if (servicePricing == null)
        //        return;

        //    ServicePricingDetailXRef pricingDetailXRef = servicePricing.ServicePricingDetailXRefs.FirstOrDefault(pricingDetail =>
        //        pricingDetail.ServiceAreaID.HasValue
        //        && pricingDetail.ServiceAreaID.Value == contractBillingDetailXRef.ProductPricing.ServiceAreaID);

        //    if (pricingDetailXRef == null)
        //        return;

        //    contractBillingDetailXRef.ProductPricing.OneTimeCharges = pricingDetailXRef.OneTimeCharges.HasValue ? pricingDetailXRef.OneTimeCharges.Value : 0;
        //    contractBillingDetailXRef.ProductPricing.SubscriptionCharges = pricingDetailXRef.SubscriptionCharges.HasValue ? pricingDetailXRef.SubscriptionCharges.Value : 0;
        //}

        //private void LoadServiceGroupPricingDetailsByIDServiceAreaID(ContractBillingDetailXRef contractBillingDetailXRef)
        //{
        //    contractBillingDetailXRef.ProductPricing.OneTimeCharges = 0;
        //    contractBillingDetailXRef.ProductPricing.SubscriptionCharges = 0;

        //    if (contractBillingDetailXRef.ProductPricing == null)
        //        return;

        //    if (contractBillingDetailXRef.ProductPricing.ServiceAreaID == 0)
        //        return;

        //    ServiceGroupPricing serviceGroupPricing = this._serviceGroupPricings.FirstOrDefault(pricing => pricing.ServiceGroupPricingID == contractBillingDetailXRef.ProductPricing.PricingID);

        //    if (serviceGroupPricing == null)
        //        return;

        //    ServiceGroupPricingDetailXRef pricingDetailXRef = serviceGroupPricing.ServiceGroupPricingDetailXRefs.FirstOrDefault(pricingDetail =>
        //        pricingDetail.ServiceAreaID.HasValue
        //        && pricingDetail.ServiceAreaID.Value == contractBillingDetailXRef.ProductPricing.ServiceAreaID);

        //    if (pricingDetailXRef == null)
        //        return;

        //    contractBillingDetailXRef.ProductPricing.OneTimeCharges = pricingDetailXRef.OneTimeCharges.HasValue ? pricingDetailXRef.OneTimeCharges.Value : 0;
        //    contractBillingDetailXRef.ProductPricing.SubscriptionCharges = pricingDetailXRef.SubscriptionCharges.HasValue ? pricingDetailXRef.SubscriptionCharges.Value : 0;
        //}
    }
}
