﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
    public class ContractBillingDetailXRef
    {
        public long ContractBillingDetailXRefID { get; set; }
        //public long BillingID { get; set; }
        public long ProductID { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public PricingInfo ProductPricing { get; set; }
        public PricingInfo ContractPricing { get; set; }        
        public decimal Discount { get; set; }
        public int Instances { get; set; }

        // Additional Properties to track Change Status Info
        public bool HasInstanceChanged { get; set; }
    }
}
