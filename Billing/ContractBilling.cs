﻿using COBRA.Models;
using Entity.BillingModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
    public class ContractBilling
    {
        public ContractBilling()
        {           
        }

        public long BillingID { get; set; }
        public long CustomerContractID { get;  set; }
        public int BillingTypeCodeID { get; set; }
        public CustomerContract CustomerContract { get;   set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<ContractBillingDetailXRef> ContractBillingDetailXRefs { get; set; }
    }
}
