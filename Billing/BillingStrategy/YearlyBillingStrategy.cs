﻿using COBRA.Models;
using Entity.ContractModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing.BillingStrategy
{
    public class YearlyBillingStrategy : BaseBllingStrategy
    {
        public YearlyBillingStrategy(BillingSearchCriteriaModel billingSearchCriteria)
            :base(billingSearchCriteria)
        {
            
        }

        protected override void SetEndDate()
        {
            if (this._contractBilling.EndDate.HasValue)
                return;

            DateTime periodEndDate = this._contractBilling.BeginDate;          

            periodEndDate = periodEndDate.AddMonths(13 - periodEndDate.Month);

            periodEndDate = periodEndDate.AddDays(-periodEndDate.Day);

            this._contractBilling.EndDate = periodEndDate;
        }
    }
}
