﻿using Billing.BillingStrategy;
using COBRA.Models;
using Entity.BillingModels;
using Infrastructure.Resources;

namespace Billing
{
    public class BillingStrategyFactory
    {
        private static BillingStrategyFactory _instance = new BillingStrategyFactory();
        
        private BillingStrategyFactory()
        {
        }

        public static BillingStrategyFactory Instance
        {
            get { return _instance; }
        }

        public BaseBllingStrategy GetBillingStrategy(BillingSearchCriteriaModel billingSearchCriteria)
        {
            BaseBllingStrategy strategy = null;

            if(billingSearchCriteria.ContractTermTypeCodeID == ContractTermTypeKey.Monthly)
                strategy = new MonthlyBillingStrategy(billingSearchCriteria);
            else if (billingSearchCriteria.ContractTermTypeCodeID == ContractTermTypeKey.Quarterly)
                strategy = new QuarterlyBillingStrategy(billingSearchCriteria);
            else if (billingSearchCriteria.ContractTermTypeCodeID == ContractTermTypeKey.Yearly)
                strategy = new YearlyBillingStrategy(billingSearchCriteria);

            return strategy;
        } 
    }
}
