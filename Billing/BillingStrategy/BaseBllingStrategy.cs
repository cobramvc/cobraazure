﻿using COBRA.Models;
using Entity.BillingModels;
using Entity.UnitOfWork;
using Entity.UnitOfWork.BillingRepo;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing.BillingStrategy
{
    public abstract class BaseBllingStrategy
    {
        protected ContractBilling _contractBilling = new ContractBilling();
        private ProductPricingRepository pricingRepository = null;
        private IBillingRepository _repository = null;

        protected abstract void SetEndDate();

        public BaseBllingStrategy(BillingSearchCriteriaModel billingSearchCriteria)
        {
            this._repository = new BillingRepository(new BillingUnitOfWork());
            this._contractBilling.CustomerContractID = billingSearchCriteria.CustomerContractID;
            this._contractBilling.BillingTypeCodeID = billingSearchCriteria.BillingTypeCodeID;
            this._contractBilling.BeginDate = billingSearchCriteria.BeginDate;

            if(billingSearchCriteria.EndDate != DateTime.MinValue)
                this._contractBilling.EndDate = billingSearchCriteria.EndDate;
            this._contractBilling.CustomerContract = this._repository.GetCustomerContractByID(this._contractBilling.CustomerContractID);
        }
    
        public CustomerBilling LoadContractBillingDetails()
        {
            if (this._contractBilling.CustomerContract == null)
                return null;// TODO : It should not be the case

            // Set End date based on ContractTerm (if not already)
            this.SetEndDate();
            
            // Generate Billing Report for the specified Contract#, Billing period
            // Create anew CustomerBilling instance and add generated illing report details
            this._contractBilling.ContractBillingDetailXRefs = this._repository.GetBillingDetailsByContract(this._contractBilling.CustomerContractID, this._contractBilling.BeginDate, this._contractBilling.EndDate.Value, this._contractBilling.BillingTypeCodeID);
            CustomerBilling customerBilling = this.CreateCustomerBilling();
            this.AddCustomerBillingDetailXRefs(customerBilling, this._contractBilling.ContractBillingDetailXRefs);

            // Get most recent Periodic Billing Report generated for the specified Contract #
            // Validate if there are any adjustments or arrears to be added to the current Billing report
            CustomerBilling existingCustomerBilling = this._repository.GetMostRecentCustomerBilling(this._contractBilling.CustomerContractID, CustomerBillingTypeCodeKey.Periodic);          
            if (existingCustomerBilling != null)
            {
                this.AddAdjustmentCustomerBillingDetailXRefs(existingCustomerBilling, customerBilling);                
            }
            else
            {
                //Add any discounts applicable to current Billing Report
                // only if the contract is new contract to the customer
                if (this._contractBilling.CustomerContract.CustomerContractProductXRefs.Count == 0)
                {
                    List<DiscountDetailXRef> discountDetails = this._repository.GetDiscountsByContractID(this._contractBilling.CustomerContract.CustomerContractID, this._contractBilling.BeginDate);
                    this.AddCustomerDiscountDetailXRefs(customerBilling, discountDetails);
                }                
            }

            //Save Current Billing Report
            this._repository.AddCustomerBilling(customerBilling);
            this._repository.SaveChanges();

            return this.GetRefreshedCustomerBilling(customerBilling.CustomerBillingID);
        }      

        protected CustomerBilling CreateCustomerBilling()
        {
            CustomerBilling newCustomerBilling = this._repository.CreateCustomerBilling();
            newCustomerBilling.BeginDate = this._contractBilling.BeginDate;
            newCustomerBilling.EndDate = this._contractBilling.EndDate.Value;
            newCustomerBilling.CustomerContractID = this._contractBilling.CustomerContractID;
            newCustomerBilling.TypeCodeID = this._contractBilling.BillingTypeCodeID;
            newCustomerBilling.StatusCodeID = 1; //to be modified
            newCustomerBilling.CustomerID = this._contractBilling.CustomerContract.CustomerID; //to be modified

            return newCustomerBilling;
        }

        protected void AddCustomerBillingDetailXRefs(CustomerBilling customerBilling, List<ContractBillingDetailXRef> contractBillingDetailXRefs)
        {

            contractBillingDetailXRefs.ToList()
                .ForEach(contractBillingDetailXRef =>
                {
                    CustomerBillingDetailXRef newCustomerBillingDetailXRef = this._repository.CreateCustomerBillingDetailXRef();
                    newCustomerBillingDetailXRef.CustomerBillingID = customerBilling.CustomerBillingID;

                    if(this._contractBilling.BillingTypeCodeID == CustomerBillingTypeCodeKey.ContractSummary)
                    {
                        newCustomerBillingDetailXRef.CustomerContractProductXRefStageID = contractBillingDetailXRef.CustomerContractProductXRefID;
                    }
                    else
                    {
                        newCustomerBillingDetailXRef.CustomerContractProductXRefID = contractBillingDetailXRef.CustomerContractProductXRefID;
                    }
                    
                    newCustomerBillingDetailXRef.ServiceID = contractBillingDetailXRef.ServiceID;
                    newCustomerBillingDetailXRef.ServiceAreaID = contractBillingDetailXRef.ServiceAreaID;
                    newCustomerBillingDetailXRef.BeginDate = contractBillingDetailXRef.BeginDate;
                    newCustomerBillingDetailXRef.EndDate = contractBillingDetailXRef.EndDate.Value;
                    newCustomerBillingDetailXRef.PricingID = contractBillingDetailXRef.PricingID;
                    newCustomerBillingDetailXRef.TaxID = contractBillingDetailXRef.TaxID.HasValue ? contractBillingDetailXRef.TaxID.Value : 0;                    
                    newCustomerBillingDetailXRef.OneTimeCharges = (contractBillingDetailXRef.BeginDate.Date == this._contractBilling.BeginDate.Date) ? contractBillingDetailXRef.OneTimeCharges : 0;
                    newCustomerBillingDetailXRef.SubscriptionCharges = BillingHelper.Instance.CalculateCharges(contractBillingDetailXRef.SubscriptionCharges, contractBillingDetailXRef.BeginDate, contractBillingDetailXRef.EndDate.Value);
                    newCustomerBillingDetailXRef.TypeCodeID = CustomerBillingDetailXRefTypeCodeKey.Current;
                    
                    customerBilling.CustomerBillingDetailXRefs.Add(newCustomerBillingDetailXRef);
                });          
        }

        protected void AddCustomerDiscountDetailXRefs(CustomerBilling customerBilling, List<DiscountDetailXRef> discountDetailXRefList)
        {

            discountDetailXRefList.ToList()
                .ForEach(discountDetailXRef =>
                {
                    CustomerBillingDiscountXRef newCustomerDiscountDetailXRef = this._repository.CreateCustomerBillingDiscountXRef();
                    newCustomerDiscountDetailXRef.CustomerBillingID = customerBilling.CustomerBillingID;
                    newCustomerDiscountDetailXRef.DiscountDetailXRefID = discountDetailXRef.DiscountDetailXRefID;           
                    customerBilling.CustomerBillingDiscountXRefs.Add(newCustomerDiscountDetailXRef);
                });
        }

        protected void AddAdjustmentCustomerBillingDetailXRefs(CustomerBilling existingCustomerBilling, CustomerBilling customerBilling)
        {
            if (existingCustomerBilling == null)
                return;

            if (this._contractBilling.BillingTypeCodeID == CustomerBillingTypeCodeKey.ContractSummary)
            {
                this.AddContractSummaryAdjustmentCustomerBillingDetailXRefs(existingCustomerBilling, customerBilling);
                return;
            }

            if (existingCustomerBilling.EndDate >= customerBilling.BeginDate)
                return;

            //Recaclculate billing for the previous period to check for any adjustments to be made
            List<ContractBillingDetailXRef> reCalculatedContractBillingDetailXRefs = this._repository.GetBillingDetailsByContract(this._contractBilling.CustomerContractID, existingCustomerBilling.BeginDate, existingCustomerBilling.EndDate, this._contractBilling.BillingTypeCodeID);

            List<CustomerBillingDetailXRef> existingCustomerBillingDetailXRefs = existingCustomerBilling.CustomerBillingDetailXRefs.Where(detail => detail.TypeCodeID == CustomerBillingDetailXRefTypeCodeKey.Current).ToList();

            reCalculatedContractBillingDetailXRefs.ForEach(contractBillingDetailXRef =>
                {
                    CustomerBillingDetailXRef existingMatchingCustomerBillingDetailXRef = existingCustomerBillingDetailXRefs.FirstOrDefault(existingDetail =>
                        existingDetail.CustomerContractProductXRefID == contractBillingDetailXRef.CustomerContractProductXRefID
                        && existingDetail.ServiceID == contractBillingDetailXRef.ServiceID
                       && existingDetail.BeginDate == contractBillingDetailXRef.BeginDate);

                    if (existingMatchingCustomerBillingDetailXRef == null
                        || existingMatchingCustomerBillingDetailXRef.EndDate != contractBillingDetailXRef.EndDate.Value)
                    {
                        CustomerBillingDetailXRef newCustomerBillingDetailXRef = this._repository.CreateCustomerBillingDetailXRef();
                        newCustomerBillingDetailXRef.CustomerBillingID = customerBilling.CustomerBillingID;
                        newCustomerBillingDetailXRef.CustomerContractProductXRefID = contractBillingDetailXRef.CustomerContractProductXRefID;
                        newCustomerBillingDetailXRef.ServiceID = contractBillingDetailXRef.ServiceID;
                        newCustomerBillingDetailXRef.ServiceAreaID = contractBillingDetailXRef.ServiceAreaID;
                        newCustomerBillingDetailXRef.BeginDate = contractBillingDetailXRef.BeginDate;
                        newCustomerBillingDetailXRef.EndDate = contractBillingDetailXRef.EndDate.Value;
                        newCustomerBillingDetailXRef.PricingID = contractBillingDetailXRef.PricingID;
                        newCustomerBillingDetailXRef.TaxID = contractBillingDetailXRef.TaxID.HasValue ? contractBillingDetailXRef.TaxID.Value : 0;                                           
                        newCustomerBillingDetailXRef.TypeCodeID = CustomerBillingDetailXRefTypeCodeKey.Adjustment;

                        newCustomerBillingDetailXRef.OneTimeCharges = contractBillingDetailXRef.OneTimeCharges;     // what about this?

                        if (existingMatchingCustomerBillingDetailXRef == null)
                            newCustomerBillingDetailXRef.SubscriptionCharges = BillingHelper.Instance.CalculateCharges(contractBillingDetailXRef.SubscriptionCharges, contractBillingDetailXRef.BeginDate, contractBillingDetailXRef.EndDate.Value);
                        else
                            newCustomerBillingDetailXRef.SubscriptionCharges = BillingHelper.Instance.CalculateCharges(contractBillingDetailXRef.SubscriptionCharges, contractBillingDetailXRef.BeginDate, contractBillingDetailXRef.EndDate.Value) - BillingHelper.Instance.CalculateCharges(existingMatchingCustomerBillingDetailXRef.SubscriptionCharges, existingMatchingCustomerBillingDetailXRef.BeginDate, existingMatchingCustomerBillingDetailXRef.EndDate);


                        customerBilling.CustomerBillingDetailXRefs.Add(newCustomerBillingDetailXRef);
                    }                  
                });           
        }

        protected void AddContractSummaryAdjustmentCustomerBillingDetailXRefs(CustomerBilling existingCustomerBilling, CustomerBilling customerBilling)
        {

           // If there is no overlapping timelnes, do not proceed as there would not be any adjustments to be done
            if (existingCustomerBilling.EndDate < customerBilling.BeginDate)
                return;
            
            List<CustomerBillingDetailXRef> existingCustomerBillingDetailXRefs = existingCustomerBilling.CustomerBillingDetailXRefs.Where(detail => detail.TypeCodeID == CustomerBillingDetailXRefTypeCodeKey.Current).ToList();

            existingCustomerBillingDetailXRefs.ForEach(contractBillingDetailXRef =>
            {
                //If the Billing record End date greater than Contract Sumary Begin Date, this has to be considered for adjustments
                // as we have already charged to the customer(which indicates by billing record) and this has to be deducted 
                if (contractBillingDetailXRef.EndDate >= this._contractBilling.BeginDate
                     && contractBillingDetailXRef.BeginDate < this._contractBilling.EndDate)
                {
                    CustomerBillingDetailXRef newCustomerBillingDetailXRef = this._repository.CreateCustomerBillingDetailXRef();
                    newCustomerBillingDetailXRef.CustomerBillingID = customerBilling.CustomerBillingID;
                    newCustomerBillingDetailXRef.CustomerContractProductXRefID = contractBillingDetailXRef.CustomerContractProductXRefID;
                    newCustomerBillingDetailXRef.ServiceID = contractBillingDetailXRef.ServiceID;
                    newCustomerBillingDetailXRef.ServiceAreaID = contractBillingDetailXRef.ServiceAreaID;
                    newCustomerBillingDetailXRef.BeginDate = (contractBillingDetailXRef.BeginDate < this._contractBilling.BeginDate) ? this._contractBilling.BeginDate : contractBillingDetailXRef.BeginDate;
                    newCustomerBillingDetailXRef.EndDate = (contractBillingDetailXRef.EndDate > this._contractBilling.EndDate.Value) ? this._contractBilling.EndDate.Value : contractBillingDetailXRef.EndDate;
                    newCustomerBillingDetailXRef.PricingID = contractBillingDetailXRef.PricingID;
                    newCustomerBillingDetailXRef.TaxID = contractBillingDetailXRef.TaxID;
                    newCustomerBillingDetailXRef.TypeCodeID = CustomerBillingDetailXRefTypeCodeKey.Adjustment;

                    newCustomerBillingDetailXRef.OneTimeCharges = 0;     // what about this?

                    decimal perDaySubscriptionCharges = BillingHelper.Instance.CalculatePerDayCharges(contractBillingDetailXRef.SubscriptionCharges, contractBillingDetailXRef.BeginDate, contractBillingDetailXRef.EndDate);
                    newCustomerBillingDetailXRef.SubscriptionCharges = 0 - BillingHelper.Instance.CalculateTotalChargesBasedOnPerDay(perDaySubscriptionCharges, newCustomerBillingDetailXRef.BeginDate, newCustomerBillingDetailXRef.EndDate);                 
                    customerBilling.CustomerBillingDetailXRefs.Add(newCustomerBillingDetailXRef);
                }             
            });
        }

        protected CustomerBilling GetRefreshedCustomerBilling(long customerBillingID)
        {
            // Get the stored current Billing Report along with foreign key references
            CustomerBilling cb = this._repository.GetCustomerBillingByID(customerBillingID);            
            cb.CustomerBillingDetailXRefs.ToList().ForEach(cd =>
            {
                cd.Service = this._repository.GetServiceByID(cd.ServiceID);
                cd.Tax = this._repository.GetTaxByID(cd.TaxID);
                cd.TypeCode = this._repository.GetCodeByID(cd.TypeCodeID);

                if (cd.CustomerContractProductXRefID.HasValue)
                {
                    cd.CustomerContractProductXRef = this._repository.GetCustomerContractProductXRefByID(cd.CustomerContractProductXRefID.Value);                    
                }                    
                else
                {
                    cd.CustomerContractProductXRefStage = this._repository.GeCustomerContractProductXRefStageByID(cd.CustomerContractProductXRefStageID.Value);
                }                    
            });

            cb.CustomerBillingDiscountXRefs.ToList().ForEach(discountXRef =>
            {
                discountXRef.DiscountDetailXRef = this._repository.GetDiscountDetailXRefByID(discountXRef.DiscountDetailXRefID);
            });

            return cb;
        }
        
    }
}
