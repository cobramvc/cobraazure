﻿using COBRA.Models;
using Entity.ContractModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing.BillingStrategy
{
    public class QuarterlyBillingStrategy : BaseBllingStrategy
    {
        public QuarterlyBillingStrategy(BillingSearchCriteriaModel billingSearchCriteria)
            :base(billingSearchCriteria)
        {
            
        }

        protected override void SetEndDate()
        {
            if (this._contractBilling.EndDate.HasValue)
                return;

            DateTime periodEndDate = this._contractBilling.BeginDate;
            int currentMonthInQuarter = 0;     
            Math.DivRem(periodEndDate.Month, 3, out currentMonthInQuarter);

            periodEndDate = periodEndDate.AddMonths(4 - currentMonthInQuarter);
            
            periodEndDate = periodEndDate.AddDays(-periodEndDate.Day);

            this._contractBilling.EndDate = periodEndDate;
        }
    }
}
