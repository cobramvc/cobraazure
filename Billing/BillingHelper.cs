﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billing
{
    public class BillingHelper
    {
        private static BillingHelper _helper = new BillingHelper();
        private BillingHelper()
        {
        }

        public static BillingHelper Instance
        {
            get { return _helper; }
        }

        public decimal CalculateCharges(decimal originalCharges, DateTime beginDate, DateTime endDate)
        {
            int numberOfDays = 0;
            int monthDays = 0;
            decimal finalCharges = 0;

            // if beginDate and EndDate in same month
            if (beginDate.Month == endDate.Month
                && beginDate.Year == endDate.Year)
            {
                numberOfDays = (endDate - beginDate).Days + 1;
                monthDays = beginDate.AddMonths(1).AddDays(-beginDate.Day).Day;
                finalCharges = (originalCharges * numberOfDays) / monthDays;
            }
            else
            {
                // Get No.of days in the first month
                monthDays = beginDate.AddMonths(1).AddDays(-beginDate.Day).Day;
                numberOfDays = monthDays - beginDate.Day;
                finalCharges = (originalCharges * numberOfDays) / monthDays;

                for (int iMonth = 1; iMonth <= 12; iMonth++)
                {
                    if (beginDate.AddMonths(iMonth).Month == endDate.Month
                        && beginDate.AddMonths(iMonth).Year == endDate.Year)
                    {
                        // for the last month
                        monthDays = endDate.AddMonths(1).AddDays(-endDate.Day).Day;
                        numberOfDays = endDate.Day;
                        finalCharges += (originalCharges * numberOfDays) / monthDays;
                        break;
                    }
                    else
                    {
                        finalCharges += originalCharges;
                    }
                }
            }

            return Math.Round(finalCharges, 4, MidpointRounding.ToEven);
        }

        public decimal CalculatePerDayCharges(decimal originalCharges, DateTime beginDate, DateTime endDate)
        {
            int numberOfDays = Convert.ToInt32((endDate - beginDate).TotalDays);
            return Math.Round(originalCharges / numberOfDays, 4, MidpointRounding.ToEven);
        }

        public decimal CalculateTotalChargesBasedOnPerDay(decimal perDayCharges, DateTime beginDate, DateTime endDate)
        {
            int numberOfDays = Convert.ToInt32((endDate - beginDate).TotalDays);
            return Math.Round(perDayCharges * numberOfDays, 4, MidpointRounding.ToEven);
        }
    }
}
