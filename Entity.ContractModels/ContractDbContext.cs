﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    public partial class ContractDbContext
    {        
        public ContractDbContext(string connectionString)
            : base(connectionString)
        {
               
        }

        #region Public Properties
        public ObjectContext DbObjectContext
        {
            get
            {
                return ((IObjectContextAdapter)this).ObjectContext;
            }
        }

        // To get User Information of current DbContext and this has to be
        // referred in all DbContext types 
        public User User
        {
            get; set;
        }

        #endregion

        #region Caching Extensions

        //private EFCachingConnection CachingConnection
        //{
        //    get { return (EFCachingConnection)this.Database.Connection; }
        //}

        //public ICache Cache
        //{
        //    get { return CachingConnection.Cache; }
        //    set { CachingConnection.Cache = value; }
        //}

        //public CachingPolicy CachingPolicy
        //{
        //    get { return CachingConnection.CachingPolicy; }
        //    set { CachingConnection.CachingPolicy = value; }
        //}

        #endregion

        public override int SaveChanges()
        {
            // Validate Custom Validatins defined at Entity object level           

            if (!DbContextHelper.Instance.Validate(this))
            {
                return ApplicationKeys.SaveUnsucessful;
            }

            DbContextHelper.Instance.UpdateAuditFields(this);
            return base.SaveChanges();
        }
    }

    //public class ExtendedContractDbContext : ContractDbContext
    //{


    //    //#region Caching Extensions

    //    //private EFCachingConnection CachingConnection
    //    //{
    //    //    get { return this.UnwrapConnection<EFCachingConnection>(); }
    //    //}

    //    //public ICache Cache
    //    //{
    //    //    get { return CachingConnection.Cache; }
    //    //    set { CachingConnection.Cache = value; }
    //    //}

    //    //public CachingPolicy CachingPolicy
    //    //{
    //    //    get { return CachingConnection.CachingPolicy; }
    //    //    set { CachingConnection.CachingPolicy = value; }
    //    //}

    //    //#endregion

    //}
}
