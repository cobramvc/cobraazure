﻿using Entity.ModelHelpers;
using Infrastructure.Resources;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    public class Contractfactory : EntityFactory<TableCurrentKey>
    {
        private static Contractfactory instance = new Contractfactory();

        public static Contractfactory Instance
        {
            get { return instance; }
        }

        public CustomerContract CreateCustomerContract(DbContext dbContext)
        {
            CustomerContract newCustomerContract = new CustomerContract();
            newCustomerContract.CustomerContractID = base.GetPrimaryKey("CustomerContract", dbContext).CurrentKey;
            newCustomerContract.RecordStatus = true;
            newCustomerContract.RecordVersion = 0;

            return newCustomerContract;
        }

        public CustomerContractProductXRef CreateCustomerContractProductXRef(DbContext dbContext)
        {
            CustomerContractProductXRef newCustomerContractProductXRef = new CustomerContractProductXRef();
            newCustomerContractProductXRef.CustomerContractProductXRefID = base.GetPrimaryKey("CustomerContractProductXRef", dbContext).CurrentKey;
            newCustomerContractProductXRef.RecordStatus = true;
            newCustomerContractProductXRef.RecordVersion = 0;            

            return newCustomerContractProductXRef;
        }

        public CustomerContractProductXRefStage CreateCustomerContractProductXRefStage(DbContext dbContext)
        {
            CustomerContractProductXRefStage newCustomerContractProductXRefStage = new CustomerContractProductXRefStage();
            newCustomerContractProductXRefStage.CustomerContractProductXRefStageID = base.GetPrimaryKey("CustomerContractProductXRefStage", dbContext).CurrentKey;
            newCustomerContractProductXRefStage.RecordStatus = true;
            newCustomerContractProductXRefStage.IsDraft = true;
            newCustomerContractProductXRefStage.RecordVersion = 0;
            newCustomerContractProductXRefStage.StatusCodeID = ContractProductStatusKey.Pending_Activation;

            return newCustomerContractProductXRefStage;
        }

        public CustomerContractAdjustmentXRef CreateCustomerContractAdjustmentXRef(DbContext dbContext)
        {
            CustomerContractAdjustmentXRef newCustomerContractAdjustmentXRef = new CustomerContractAdjustmentXRef();
            newCustomerContractAdjustmentXRef.AdjustmentsXRefID = base.GetPrimaryKey("CustomerContractAdjustmentXRef", dbContext).CurrentKey;
            newCustomerContractAdjustmentXRef.RecordStatus = true;            
            newCustomerContractAdjustmentXRef.RecordVersion = 0;
            newCustomerContractAdjustmentXRef.StatusCodeID = ContractProductStatusKey.Pending_Activation;

            return newCustomerContractAdjustmentXRef;
        }

        public CustomerContractAdhocDiscountXRef CreateCustomerContractAdhocDiscountXRef(DbContext dbContext)
        {
            CustomerContractAdhocDiscountXRef newCustomerContractAdhocDiscountXRef = new CustomerContractAdhocDiscountXRef();
            newCustomerContractAdhocDiscountXRef.AdhocDiscountXRefID = base.GetPrimaryKey("CustomerContractAdhocDiscountXRef", dbContext).CurrentKey;
            newCustomerContractAdhocDiscountXRef.RecordStatus = true;
            newCustomerContractAdhocDiscountXRef.RecordVersion = 0;
            newCustomerContractAdhocDiscountXRef.StatusCodeID = ContractProductStatusKey.Pending_Activation;

            return newCustomerContractAdhocDiscountXRef;
        }

        public CustomerContractStatusChangeRequestXRef CreateCustomerContractStatusChangeRequestXRef(DbContext dbContext)
        {
            CustomerContractStatusChangeRequestXRef newCustomerContractStatusChangeRequestXRef = new CustomerContractStatusChangeRequestXRef();
            newCustomerContractStatusChangeRequestXRef.StatusChangeRequestXRefID = base.GetPrimaryKey("CustomerContractStatusChangeRequestXRef", dbContext).CurrentKey;
            newCustomerContractStatusChangeRequestXRef.RecordStatus = true;
            newCustomerContractStatusChangeRequestXRef.RecordVersion = 0;
            newCustomerContractStatusChangeRequestXRef.RequestStatusCodeID = ContractStatusChangeRequestStatusCodeKey.Initiated;

            return newCustomerContractStatusChangeRequestXRef;
        }
    }
}
