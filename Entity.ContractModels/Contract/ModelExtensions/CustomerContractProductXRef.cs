﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractProductXRefMetaData))]
    //[Serializable]
    public partial class CustomerContractProductXRef : BaseEntity<CustomerContractProductXRef>
    {

        //public void SetEndDate()
        //{
        //    this.EndDate = (DateTime.Now.Date.AddDays(-1) < this.BeginDate) ? this.BeginDate : DateTime.Now.Date.AddDays(-1);
        //    if (this.EndDate.Value.Date == this.BeginDate.Value.Date)
        //        this.RecordStatus = false;
        //}

        public void SetEndDate(DateTime effectiveDate)
        {
            this.EndDate = (effectiveDate.Date.AddDays(-1) < this.BeginDate) ? this.BeginDate : effectiveDate.Date.AddDays(-1);
            if (this.EndDate.Value.Date == this.BeginDate.Value.Date)
                this.RecordStatus = false;
        }

        public void Delete()
        {
            this.EndDate = this.BeginDate;
            this.RecordStatus = false;                          
        }
    }

    public class CustomerContractProductXRefMetaData
    {
    }
}
