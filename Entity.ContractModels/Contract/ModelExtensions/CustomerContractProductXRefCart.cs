﻿using COBRA.Models;
using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractProductXRefCartMetaData))]
    //[Serializable]
    public partial class CustomerContractProductXRefCart : BaseEntity<CustomerContractProductXRefCart>
    {        
        public string ProductDescription { get; set; }        
        public string ServiceNames { get; set; }        
        public decimal SubscriptionCharges { get; set; }        
        public decimal OneTimeCharges { get; set; }        
        public decimal Installation { get; set; }        
        public decimal Tax { get; set; }
        public decimal EffectiveTotalCharges { get; set; }

        public bool HasInstanceChanged { get; set; }
        public bool HasStatusChanged { get; set; }
        // Contract latest Effective Date
        public DateTime? ContractEffectiveDate { get; set; }

        [CustomProperty]
        public decimal Total
        {
            get
            {
                decimal total = this.SubscriptionCharges + this.OneTimeCharges + this.Installation + this.Tax - (this.Discount.HasValue ? this.Discount.Value : 0);
                return this.Instances.HasValue ? total * this.Instances.Value : total; 
            }
        }
        [CustomProperty]
        public decimal TotalWithOutDiscount
        {
            get
            {
                return this.SubscriptionCharges + this.OneTimeCharges + this.Installation + this.Tax;
            }            
        }

        public List<ProductPricing> ProductPricings
        {
            get;
            set;
        }
    }

    public class CustomerContractProductXRefCartMetaData
    {
        [SelectList(CodeListKey.ContractProductStatusCodeList)]
        public int StatusCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [System.ComponentModel.DataAnnotations.Range(0, Int32.MaxValue, ErrorMessage = "Invalid Discount")]  
        public Nullable<decimal> Discount { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [System.ComponentModel.DataAnnotations.Range(0, Int32.MaxValue, ErrorMessage = "Invalid No.of Instances")]  
        public Nullable<int> Instances { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        public Nullable<System.DateTime> BeginDate { get; set; }        
    }
}
