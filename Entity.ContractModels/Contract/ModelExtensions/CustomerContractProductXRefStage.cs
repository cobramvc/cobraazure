﻿using COBRA.Models;
using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractProductXRefStageMetaData))]
    public partial class CustomerContractProductXRefStage : BaseEntity<CustomerContractProductXRefStage>
    {
        public List<ProductPricing> ProductPricings { get; set; }             
        public string ProductDescription { get; set; }
        public string ServiceNames { get; set; }
    }

    public class CustomerContractProductXRefStageMetaData
    {
        [SelectList(CodeListKey.ContractProductStatusCodeList)]
        public int StatusCodeID { get; set; }
        
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [System.ComponentModel.DataAnnotations.Range(1, Int32.MaxValue, ErrorMessage = "Invalid No.of Instances")]
        public Nullable<int> Instances { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        public Nullable<System.DateTime> BeginDate { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [System.ComponentModel.DataAnnotations.Range(1, long.MaxValue, ErrorMessage = "The field is required")]
        public long ProductID { get; set; }

    }
}
