﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractAdjustmentXRefMetaData))]
    public partial class CustomerContractAdjustmentXRef : BaseEntity<CustomerContractAdjustmentXRef>
    {
    }

    public class CustomerContractAdjustmentXRefMetaData
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [SelectList(CodeListKey.ContractTypeCodeList)]
        public int CategoryCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [SelectList(CodeListKey.ContractTermTypeCodeList)]
        public int TypeCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        public decimal Amount { get; set; }

    }

}
