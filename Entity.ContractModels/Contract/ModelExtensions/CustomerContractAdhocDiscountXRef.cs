﻿using Entity.ModelHelpers;
using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractAdhocDiscountXRefMetaData))]
    public partial class CustomerContractAdhocDiscountXRef : BaseEntity<CustomerContractAdhocDiscountXRef>
    {
        public long CustomerID { get; set; }
    }

    public class CustomerContractAdhocDiscountXRefMetaData
    {
        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [SelectList(CodeListKey.ContractTypeCodeList)]
        public int DiscountTypeCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        [SelectList(CodeListKey.ContractTermTypeCodeList)]
        public int StatusCodeID { get; set; }

        [System.ComponentModel.DataAnnotations.Required(ErrorMessage = "The field is required")]
        public decimal Discount { get; set; }

    }
}
