﻿using COBRA.Models;
using Entity.ModelHelpers;
using Infrastructure;
using Infrastructure.Resources;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractCartMetaData))]
    //[Serializable]
    public partial class CustomerContractCart : BaseEntity<CustomerContractCart>
    {
        public List<Product> Products
        {
            get;
            set;
        }

        public int StatusCodeID
        {
            get;
            set;
        }
        

        public List<CustomerContractProductXRefCart> ActiveCustomerContractProductXRefCarts
        {
            get { return this.CustomerContractProductXRefCarts.Where(p => p.RecordStatus).ToList(); }
        }        
        
        #region Custom Validations

        [SelfValidation]
        internal void AtleastOneActiveProductEnrolled(DbContext context)
        {
            if (this.CustomerContractProductXRefCarts.Count(p => p.RecordStatus) == 0)               
            {
                this.ValidationErrors.Add(new ValidationResult("Atleast one product to be active for a Contract", this, "CustomerContractCartID", "AtleastOneActiveProductEnrolled", null));
            }
        }

        [SelfValidation]
        internal void IsEffectiveDateValid(DbContext context)
        {
            CustomerContractProductXRefCart activeProductXRef = this.ActiveCustomerContractProductXRefCarts.FirstOrDefault();

            if (activeProductXRef == null)
                return;

            if (!activeProductXRef.HasInstanceChanged
                && !activeProductXRef.HasStatusChanged)
                return;

            if (!activeProductXRef.ContractEffectiveDate.HasValue)
                return;

           
            if (!activeProductXRef.BeginDate.HasValue)
                return;

            if (activeProductXRef.BeginDate.Value < activeProductXRef.ContractEffectiveDate.Value)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date cannot be less than " + activeProductXRef.ContractEffectiveDate.Value.ToShortDateString(), this, "CustomerContractCartID", "IsEffectiveDateValid", null));
            }
        }
       
        [SelfValidation]
        internal void IsProductActiveForThisContract(DbContext context)
        {
            if (this.ActiveCustomerContractProductXRefCarts.Count == 0)
                return;

            CustomerContractProductXRefCart activeProductXRef = this.ActiveCustomerContractProductXRefCarts.First();

            if (activeProductXRef.StatusCodeID != ContractProductStatusKey.Active)
                return;

            if (activeProductXRef.Product == null)
                return;

            if (activeProductXRef.Product.BeginDate > activeProductXRef.BeginDate)
            {
                this.ValidationErrors.Add(new ValidationResult("The product is not active on Effective Date", this, "CustomerContractCartID", "IsProductActiveForThisContract", null));
            }
        }

        #endregion
    }

    public class CustomerContractCartMetaData
    {
        [SelectList(CodeListKey.ContractTypeCodeList)]
        public int ContractTypeCodeID { get; set; }

        [SelectList(CodeListKey.ContractTermTypeCodeList)]
        public int ContractTermTypeCodeID { get; set; }
    }
}
