﻿using Entity.ModelHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractStatusChangeRequestXRefMetaData))]
    public partial class CustomerContractStatusChangeRequestXRef : BaseEntity<CustomerContractStatusChangeRequestXRef>
    {
    }

    public class CustomerContractStatusChangeRequestXRefMetaData
    {
    }
}
