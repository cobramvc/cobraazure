﻿using Entity.ModelHelpers;
using Infrastructure;
using Infrastructure.Resources;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ContractModels
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(CustomerContractMetaData))]
    //[Serializable]
    public partial class CustomerContract : BaseEntity<CustomerContract>
    {
        
        [CustomProperty]
        public bool IsNewContract
        {
            get { return this.ActiveCustomerContractProductXRef == null && !this.StatusCodeID.HasValue; }
        }

        [CustomProperty]
        public List<CustomerContractProductXRefStage> DraftCustomerContractProductXRefStages
        {
            get { return this.CustomerContractProductXRefStages.Where(stage => stage.IsDraft).ToList();  }
        }

        [CustomProperty]
        public List<CustomerContractProductXRefStage> ActiveDraftCustomerContractProductXRefStages
        {
            get { return this.CustomerContractProductXRefStages.Where(stage => stage.IsDraft && stage.RecordStatus).ToList(); }
        }

        [CustomProperty]
        public CustomerContractProductXRef  ActiveCustomerContractProductXRef
        {
            get { return this.CustomerContractProductXRefs
                    .FirstOrDefault(productXRef => productXRef.RecordStatus
                && productXRef.BeginDate.Value.Date <= DateTime.Now.Date
                && (!productXRef.EndDate.HasValue || productXRef.EndDate.Value.Date >= DateTime.Now.Date));
            }
        }

        [CustomProperty]
        public List<CustomerContractProductXRef> FutureActiveCustomerContractProductXRefs
        {
            get
            {
                return this.CustomerContractProductXRefs
                  .Where(productXRef => productXRef.RecordStatus
              && productXRef.BeginDate.Value.Date > DateTime.Now.Date).ToList();
            }
        }

        [CustomProperty]
        public CustomerContractProductXRef CurrentActiveCustomerContractProductXRef
        {
            get { return this.CustomerContractProductXRefs.Where(product => !product.EndDate.HasValue && product.RecordStatus).FirstOrDefault(); }
        }

        [CustomProperty]
        public CustomerContractStatusChangeRequestXRef InitiatedStatusChangeRequest
        {
            get {
                return this.CustomerContractStatusChangeRequestXRefs.FirstOrDefault(status =>
                                          status.RecordStatus
                                          && status.RequestStatusCodeID == ContractStatusChangeRequestStatusCodeKey.Initiated); }
        }

        [CustomProperty]
        public CustomerContractActionType ActionType { get; set; }

        [CustomProperty]
        public List<Customer> ActiveChildCustomers
        {
            get; set;
        }


        #region ViewModel properties

        [CustomProperty]
        public int? viewStatusReasonCodeID { get; set; }

        [CustomProperty]
        public string viewStatusReason{ get; set; }

        [CustomProperty]
        public DateTime? viewEffectiveDate{ get; set; }


        #endregion
        #region Custom Validations

        [SelfValidation]
        internal void IsContractDirty(DbContext context)
        {            
            if (this.ActionType != CustomerContractActionType.NoChange)
                return;

            this.ValidationErrors.Add(new ValidationResult("No changes done to save Contract", this, "CustomerContractID", "AtleastOneActiveProductEnrolled", null));
        }

        [SelfValidation]
        internal void AtleastOneActiveProductEnrolled(DbContext context)
        {
            // Do not validate if the request is to change Status
            if (this.ActionType == CustomerContractActionType.StatusChange)
                return;

            if(this.StatusCodeID == ContractProductStatusKey.Cancelled)
                return;

            if (this.ActiveDraftCustomerContractProductXRefStages.Count == 0
                && this.ActiveCustomerContractProductXRef == null
                && this.FutureActiveCustomerContractProductXRefs.Count == 0)
            {
                this.ValidationErrors.Add(new ValidationResult("Atleast one product to be active for a Contract", this, "CustomerContractID", "AtleastOneActiveProductEnrolled", null));
            }
        }

        [SelfValidation]
        internal void IsEffectiveDateValid(DbContext context)
        {
            // Do not validate if the request is to change Status
            if (this.ActionType == CustomerContractActionType.StatusChange)
                return;

            //If this is Upgrade request, validate with Effective Date with original Contract
            if (this.StatusCodeID == ContractProductStatusKey.Pending_Upgrade)
            {
                this.IsEffectiveDateForUpgrade(context);
                return;
            }                 

            CustomerContractProductXRefStage activeProductXRef = this.ActiveDraftCustomerContractProductXRefStages.FirstOrDefault();

            if (activeProductXRef == null)
                return;
           
            if (activeProductXRef.DbObjectBeforeChanged == null)
                return;

            if (!activeProductXRef.DbObjectBeforeChanged.BeginDate.HasValue)
                return;


            if (!activeProductXRef.BeginDate.HasValue)
                return;

            if (activeProductXRef.BeginDate.Value < activeProductXRef.DbObjectBeforeChanged.BeginDate.Value)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date cannot be less than " + activeProductXRef.DbObjectBeforeChanged.BeginDate.Value.ToShortDateString(), this, "CustomerContractID", "IsEffectiveDateValid", null));
            }
        }

        [SelfValidation]
        internal void IsProductActiveForThisContract(DbContext context)
        {
            // Do not validate if the request is to change Status
            if (this.ActionType == CustomerContractActionType.StatusChange)
                return;

            if (this.ActiveDraftCustomerContractProductXRefStages.Count == 0)
                return;

            CustomerContractProductXRefStage activeProductXRef = this.ActiveDraftCustomerContractProductXRefStages.First();

            if (activeProductXRef.StatusCodeID != ContractProductStatusKey.Active)
                return;

            if (activeProductXRef.Product == null)
                return;

            if (activeProductXRef.Product.BeginDate > activeProductXRef.BeginDate)
            {
                this.ValidationErrors.Add(new ValidationResult("The product is not active on Effective Date", this, "CustomerContractID", "IsProductActiveForThisContract", null));
            }
        }

        [SelfValidation]
        internal void IsValidStatusChange(DbContext context)
        {
            // Do not validate if the request is NOT to change Status
            if (this.ActionType != CustomerContractActionType.StatusChange)
                return;

            if (this.StatusCodeID == ContractProductStatusKey.Pending_Activation
                && string.IsNullOrEmpty(this.viewStatusReason))             
            {
                this.ValidationErrors.Add(new ValidationResult("Status Reason is required", this, "viewStatusReason", "IsValidStatusChange", null));
            }

            if (this.StatusCodeID != ContractProductStatusKey.Pending_Activation
                && (!this.viewStatusReasonCodeID.HasValue || this.viewStatusReasonCodeID.Value <= 0))
            {
                this.ValidationErrors.Add(new ValidationResult("Status Reason is required", this, "viewStatusReasonCodeID", "IsValidStatusChange", null));
            }

            if (!this.viewEffectiveDate.HasValue
             || this.viewEffectiveDate.Value < DateTime.Now.Date)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date should be greater than or equal to today", this, "viewEffectiveDate", "IsValidStatusChange", null));
                return;
            }

            if (this.DbObjectBeforeChanged == null)            
                return;
            

            if (this.DbObjectBeforeChanged.ActiveCustomerContractProductXRef == null)
                return;

            DateTime beginDate = this.DbObjectBeforeChanged.ActiveCustomerContractProductXRef.BeginDate.Value;
            if (beginDate > this.viewEffectiveDate.Value)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date should be greater than or equal to" + beginDate.ToShortDateString(), this, "viewEffectiveDate", "IsValidStatusChange", null));
                return;
            }

        }

        private void IsEffectiveDateForUpgrade(DbContext dbContext)
        {
            CustomerContractProductXRefStage activeProductXRef = this.ActiveDraftCustomerContractProductXRefStages.FirstOrDefault();
            DateTime effectiveDate = DateTime.Today;

            if (activeProductXRef == null)
                return;

            if (activeProductXRef.DbObjectBeforeChanged != null
                && activeProductXRef.DbObjectBeforeChanged.BeginDate.HasValue)
            {
                effectiveDate = activeProductXRef.DbObjectBeforeChanged.BeginDate.Value;
            }
            else if (this.viewEffectiveDate.HasValue)
            {
                effectiveDate = this.viewEffectiveDate.Value;
            }
            
            if (activeProductXRef.BeginDate.Value < effectiveDate)
            {
                this.ValidationErrors.Add(new ValidationResult("Effective Date cannot be less than " + effectiveDate.ToShortDateString(), this, "CustomerContractID", "IsEffectiveDateValid", null));
            }
        }
        #endregion
    }

    public class CustomerContractMetaData
    {
        [SelectList(CodeListKey.ContractTypeCodeList)]
        public int ContractTypeCodeID { get; set; }

        [SelectList(CodeListKey.ContractTermTypeCodeList)]
        public int ContractTermTypeCodeID { get; set; }
        
        [SelectList(CodeListKey.ContractStatusReasonList)]        
        [DependencyColumn("StatusCodeID")]
        public int? viewStatusReasonCodeID { get; set; }

    }


}
